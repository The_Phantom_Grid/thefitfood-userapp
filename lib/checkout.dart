import 'dart:math';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:userapp/Getters/GetPromoCode.dart';
import 'package:userapp/Screens/OrderPlacedSuccessfully.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:userapp/Screens/PaymentOption.dart';
import 'Components/CommonUtility.dart';
import 'Components/LoaderComponents.dart';
import 'Getters/GetAddress.dart';
import 'Getters/GetCartProducts.dart';
import 'Network/httpRequests.dart';
import 'Screens/SearchLocationMap.dart';
import 'constants.dart';

_checkoutState checkOutState;

class checkout extends StatefulWidget {
  @override
  _checkoutState createState() {
    checkOutState = _checkoutState();
    return checkOutState;
  }
}

class _checkoutState extends State<checkout> {
  bool loading = true,
      emptyCart = false,
      loadAddress = true,
      deliveryAvailable = false,
      loadPromo = true,
      homeDeliveryAvail = false,
      placingOrder = false;
  TextEditingController orderInstruction = TextEditingController();
  TextEditingController promocodeController = TextEditingController();
  HttpRequests requests = HttpRequests();
  CartProduct cartProduct;
  String cartTotal, deliveryAddress, CART_ID, SHOPPING_LIST_ID, ORDER_ID;
  String deliveryStartTime, deliveryCloseTime, outOfRadius, deliveryType;
  int deliveryTime;
  String _selectedTime = "Tap to select",
      deliveryDay = "Today",
      deliveryTitle = "Home delivery available from";
  GetMyAddress addressList;
  int deliveryTypeOption = -1, promocodeIndex = -1;
  PromoCode promoCode;
  double cashbackAmount = 0, grandTotal, deliveryFees, tax, discount;

  getCartProducts() async {
    var response = await requests.getCart();

    cartProduct = CartProduct.fromJson(response);
    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      if (cartProduct.product.isNotEmpty) {
        SHOPPING_LIST_ID = cartProduct.product[0].shoppingListId.toString();
        promoApplied = false;
        cashbackAmount = 0;
        cartTotal =
            double.tryParse(cartProduct.total.cartSubtotal).toStringAsFixed(2);
        discount = double.tryParse(cartProduct.total.overallDiscount);
        tax = double.tryParse(cartProduct.total.totaltax);
        deliveryFees = double.tryParse(
            STORE_DETAILS.data.shopDetails.deliveryCharge.toString());
        grandTotal = double.tryParse(cartProduct.total.grandtotal);
        getShoppingCartId();
        print("TOTAL $cartTotal");
        setState(() {
          loading = false;
          emptyCart = false;
        });
      } else {
        setState(() {
          loading = false;
          emptyCart = true;
          // Get.back();
        });
      }
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  getDeliveryAddress() async {
    initValues();
    var response = await requests.getDeliveryAddress();

    addressList = GetMyAddress.fromJson(response);
    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      if (addressList.details.isNotEmpty) {
        setState(() {
          deliveryAvailable = false;
          outOfRadius = "Please select delivery address";
          loadAddress = false;
        });
        // checkDistance(0);
      } else {
        setState(() {
          deliveryAvailable = false;
          outOfRadius = "Please add a new delivery address";
          loadAddress = false;
        });
      }
    } else {
      setState(() {
        loadAddress = false;
      });
    }
  }

  getShoppingCartId() async {
    var response = await requests.getShoppingCartId(SHOPPING_LIST_ID);

    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      CART_ID = response['cart_id']['cart_id'].toString();
      print("CART ID?>>>> $CART_ID");
      // // placeOrder();
    } else {
      setState(() {
        // placingOrder = false;
        showToast("Something went wrong", Colors.black);
      });
    }
  }

  bool noPromoAvail = false;

  getPromoCodes() async {
    var response = await requests.getPromoCodes();
    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      setState(() {
        promoCode = PromoCode.fromJson(response);
        loadPromo = false;
        if (promoCode.info.isEmpty) {
          noPromoAvail = true;
        } else {
          noPromoAvail = false;
        }
      });
      // // placeOrder();
    } else {
      setState(() {
        loadPromo = false;
        noPromoAvail = true;
        // placingOrder = false;
        showToast("Something went wrong", Colors.black);
      });
    }
  }

  String PROMOCODE;
  bool promoApplied = false, applyingPromo = false;

  validatePromoCode() async {
    var response = await requests.validatePromoCode(cartTotal, PROMOCODE);
    // Get.back();
    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      setState(() {
        // applyingPromo = false;
        if (response['info']['status'].toString().toLowerCase() == "success") {
          print("PROMOCODE APPLIED");
          promoApplied = true;
          PROMOCODE = response['info']['promocode'].toString().toUpperCase();
          cashbackAmount =
              double.tryParse(response['info']['cashback_amt'].toString());
          grandTotal = grandTotal - cashbackAmount;
          discount = discount + cashbackAmount;
        } else {
          promoApplied = false;
          print(response['info']['message']);
          showToast(response['info']['message'].toString(), Colors.redAccent);
        }

      });
      // // placeOrder();
    } else {
      setState(() {
        noPromoAvail = true;
        // applyingPromo = false;
        // placingOrder = false;
        showToast("Something went wrong", Colors.black);
      });
    }
  }

  // placeOrder() async {
  //   setState(() {
  //     placingOrder = true;
  //   });
  //   // paymentmode = "paytm";
  //
  //
  //   var response = await requests.placeCustomerOrder(
  //       CART_ID,
  //       deliveryAddress,
  //       dDay.toString(),
  //       deliveryType,
  //       "",
  //       orderInstruction.text.toString(),
  //       "CASH",
  //       "",
  //       "1",
  //       SHOPPING_LIST_ID,
  //       tSlot.toString(),
  //       "");
  //
  //   if (response != null &&
  //       response['status'].toString().toLowerCase() == "success") {
  //     setState(() {
  //       placingOrder = false;
  //     });
  //
  //     ORDER_ID = response['orderid'].toString();
  //     Get.offAll(OrderPlacedSuccessfully(
  //         ORDER_ID, deliveryAddress, deliveryTypeOption, tSlot.toString()));
  //   } else {
  //     setState(() {
  //       placingOrder = false;
  //     });
  //     showToast("Something went wrong. Please try again", Colors.black);
  //   }
  // }

  showAddressList() {
    Get.bottomSheet(
      Container(
        color: Colors.white,
        height: MediaQuery.of(context).size.height * .2,
        child: Column(
          children: <Widget>[
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 4.0, vertical: 2),
                    child: GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Icon(Icons.close)),
                  ),
                  Divider()
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(4),
              color: Colors.white,
              height: 100,
              width: double.infinity,
              child: ListView.separated(
                  physics: AlwaysScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return index == addressList.details.length ?
                    GestureDetector(
                      onTap: () async {
                        Get.back();
                        await Get.to(SearchLocationMap(null, null, false));
                        getDeliveryAddress();
                      },
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 22.0),
                          child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Icon(Icons.add),
                                Text(
                                  "Add",
                                  style: smallTextStyleBlack,
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ) :
                    Container(
                      width: 200,
                      height: 100,
                      child: Card(
                        child: ListTile(
                          onTap: () {
                            checkDistance(index);
                            // setState(() {
                            //   deliveryAddress = addressList
                            //       .details[index].address
                            //       .toString();
                            // });
                            Get.back();
                          },
                          title: Text(
                              "${addressList.details[index].fullName}",
                              style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis),
                          subtitle: Text(
                            "${addressList.details[index].address}",
                            style: smallTextStyleBlack,
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return SizedBox(
                      width: 8,
                    );
                  },
                  itemCount: addressList.details.length + 1),
            ),
          ],
        ),
      ),
      isDismissible: true,
      isScrollControlled: true,
      enableDrag: true,
    );
  }

  selectDay(MapEntry<int, DateTime> e) {
    print("KEY:: ${e.key}");
    if (!offDays.contains("${e.key}"))
      setState(() {
        selectedDay = e.key;
        _selectedTime = "Tap to select";
      });
    else {
      showToast("Store is closed on ${DateFormat('EEE').format(e.value)}",
          Colors.black);
    }

    print(selectedDay);
  }

  Widget buildTimeSlot() {
    return GestureDetector(
      child: Text(
        "$_selectedTime",
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      onTap: () {
        DatePicker.showTime12hPicker(context, onConfirm: (time) {
          print("${DateFormat('yyyyMMdd').format(DateTime.now())} date now");
          if (deliveryDay.contains("Tomorrow")) {
            if (time.isBefore(DateTime.parse(
                "${DateFormat('yyyy-MM-dd').format(
                    DateTime.now())} $deliveryStartTime"))) {
              Flushbar(
                message: "Please select time between business hours",
                icon: Icon(Icons.info_outline, color: mColor),
                duration: Duration(seconds: 2),
              )
                ..show(context);
            } else if (time.isAfter(DateTime.parse(
                "${DateFormat('yyyy-MM-dd').format(
                    DateTime.now())} $deliveryCloseTime"))) {
              Flushbar(
                message: "Please select time between business hours",
                icon: Icon(Icons.info_outline, color: mColor),
                duration: Duration(seconds: 2),
              )
                ..show(context);
            } else {
              setState(() {
                _selectedTime =
                "${DateFormat('hh:mma').format(
                    DateTime.fromMillisecondsSinceEpoch(
                        time.millisecondsSinceEpoch))}"
                    " to ${DateFormat('hh:mma').format(
                    DateTime.fromMillisecondsSinceEpoch(time
                        .add(Duration(minutes: deliveryTime))
                        .millisecondsSinceEpoch))}";
//                        ${DateFormat('hh:mma').format(
//                        DateTime.fromMillisecondsSinceEpoch(DateTime
//                            .parse("${DateFormat(
//                            'yyyy-MM-dd').format(
//                            DateTime
//                                .now())} $deliveryCloseTime")
//                            .millisecondsSinceEpoch))}";
              });
            }
          } else {
            if (time.isBefore(DateTime.parse(
                "${DateFormat('yyyy-MM-dd').format(
                    DateTime.now())} $deliveryStartTime"))) {
              Flushbar(
                message: "Please select time between business hours",
                icon: Icon(Icons.info_outline, color: mColor),
                duration: Duration(seconds: 2),
              )
                ..show(context);
            } else if (time.isAtSameMomentAs(DateTime.now()) ||
                time.isBefore(DateTime.fromMillisecondsSinceEpoch(
                    DateTime
                        .now()
                        .millisecondsSinceEpoch - 60000)) &&
                    selectedDay == time.weekday) {
              Flushbar(
                message: "Delivery time cannot be set",
                icon: Icon(Icons.info_outline, color: mColor),
                duration: Duration(seconds: 2),
              )
                ..show(context);
            } else if (time.isAfter(DateTime.parse(
                "${DateFormat('yyyy-MM-dd').format(
                    DateTime.now())} $deliveryCloseTime"))) {
              Flushbar(
                message: "Please select time between business hours",
                icon: Icon(Icons.info_outline, color: mColor),
                duration: Duration(seconds: 2),
              )
                ..show(context);
            } else {
              setState(() {
                print("DELIVERY TIME: $deliveryTime");
                _selectedTime =
                "${DateFormat('hh:mma').format(
                    DateTime.fromMillisecondsSinceEpoch(
                        time.millisecondsSinceEpoch))}"
                    " to ${DateFormat('hh:mma').format(
                    DateTime.fromMillisecondsSinceEpoch(time
                        .add(Duration(minutes: deliveryTime))
                        .millisecondsSinceEpoch))}";
//                        ${DateFormat('hh:mma').format(
//                        DateTime.fromMillisecondsSinceEpoch(DateTime
//                            .parse("${DateFormat(
//                            'yyyy-MM-dd').format(
//                            DateTime
//                                .now())} $deliveryCloseTime")
//                            .millisecondsSinceEpoch))}";
              });
            }
          }
        });
      },
    );
  }

  List<String> dList = [];
  Map<int, DateTime> wDays = {};
  String offDays = "";
  int selectedDay = -1;

  getWD() {
    wDays.clear();
    dList.clear();
    offDays =
    STORE_DETAILS.data.shopDetails.storeCloseDays.contains("null")
        ? ""
        : STORE_DETAILS.data.shopDetails.storeCloseDays
        .replaceAll(',', "");
    var weekday = DateTime
        .now()
        .weekday - 1;
    var now = DateTime.now();

    for (int i = 1; i < 7; i++) {
      wDays[now.weekday] = now;
      dList.add(DateFormat('d EEE').format(now));
      now = now.add(Duration(days: 1));
    }

    wDays[weekday] = now;
    dList.add(DateFormat('d EEE').format(now));
    wDays.forEach((key, value) {
      print("$key |||| ${DateFormat('EEE').format(value)}");
    });
    wDays.entries.map((e) {
      if (!offDays.contains(e.key.toString())) {
        selectedDay = e.key;
      }
    });
  }

  Widget weekDays() {
    getWD();
    return Container(
//      margin: EdgeInsets.only(top: 10),
        height: 50,
        child: ListView(
          itemExtent: 55,
          scrollDirection: Axis.horizontal,
          children: wDays.entries.map((e) {
            return GestureDetector(
              onTap: () {
                selectDay(e);
              },
              child: Container(
                decoration: selectedDay != e.key
                    ? BoxDecoration(
                  color: offDays.contains(e.key.toString())
                      ? Colors.red
                      : Colors.green,
                )
                    : BoxDecoration(
                  border: Border.all(color: Colors.green),
                  color: Colors.white,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      DateFormat('EEE').format(e.value),
                      style: TextStyle(
                          color: selectedDay != e.key
                              ? Colors.white
                              : Colors.green,
                          fontSize: 13,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Text(
                      DateFormat('d').format(e.value),
                      style: TextStyle(
                          color: selectedDay != e.key
                              ? Colors.white
                              : Colors.green),
                    ),
                  ],
                ),
              ),
            );
          }).toList(),
        ));
  }

  Widget loadingCircle() {
    return SizedBox(
        height: 25,
        width: 25,
        child: SpinKitCircle(
          color: Colors.white,
          size: 25,
        ));
  }

  onChangeDeliveryType(int option) {
    setState(() {
      if (option == 0) {
        if (homeDeliveryAvail) {
          deliveryTypeOption = option;
          deliveryAvailable = false;
          deliveryType = "homedelivery";
          deliveryAddress = null;
          outOfRadius = "Please select delivery address";
          deliveryTitle = "Home delivery is available from ";
          deliveryStartTime = STORE_DETAILS.data.shopDetails.deliveryStartTime;
          deliveryCloseTime = STORE_DETAILS.data.shopDetails.deliveryCloseTime;
          _selectedTime = "Tap to select";
          selectedDay = -1;
        } else {
          showToast("Home delivery not available at this moment", Colors.black);
        }
      } else if (option == 1) {
        deliveryTypeOption = option;
        deliveryAvailable = true;
        deliveryAddress = STORE_DETAILS.data.shopDetails.location.toString();
        deliveryType = "pickup";
        deliveryTitle = "Store pickup is available from ";
        deliveryStartTime = STORE_DETAILS.data.shopDetails.storeOpenTime;
        deliveryCloseTime = STORE_DETAILS.data.shopDetails.storeCloseTime;
        _selectedTime = "Tap to select";
        selectedDay = -1;
      }
    });
  }

  List<Details> addressInRadius = [];
  List<String> keys = [];
  double deliveryRadius;

  checkDistance(int index) async {
    double lat1 = STORE_DETAILS.data.shopDetails.lat;
    double lon1 = STORE_DETAILS.data.shopDetails.lng;
    var p = 0.017453292519943295;
    var c = cos;
    var a;
    double lat2, lon2;
    double distanceInKm;

    lat2 = double.tryParse(addressList.details[index].lat.toString());
    lon2 = double.tryParse(addressList.details[index].lng.toString());

    a = 0.5 -
        c((lat2 - lat1) * p) / 2 +
        c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    distanceInKm = 12742 * asin(sqrt(a));

    print("DISTANCE>> $distanceInKm");
    // Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    // var di = await geolocator.distanceBetween(lat1, lon1, lat2, lon2);
    // print("DIIIIISSTANCE>>>> ${di/1000}");

    setState(() {
      if (deliveryRadius > distanceInKm) {
        deliveryAddress = addressList.details[index].address.toString();
        deliveryAvailable = true;
      } else {
        deliveryAddress = null;
        deliveryAvailable = false;
        outOfRadius =
        "The address is out of delivery area for this restaurant. Please try with a different address.";
      }
    });
  }

  validateDetails() {
    if (double.tryParse(cartTotal) < STORE_DETAILS.data.shopDetails.minOrder) {
      showAlert();
    } else if (deliveryType == null && deliveryTypeOption == -1) {
      showToast("Please select delivery type.", Colors.black);
    } else if (deliveryAddress == null || deliveryAddress == "") {
      showToast("Please select delivery address.", Colors.black);
    } else if (selectedDay == -1 || _selectedTime == "Tap to select") {
      showToast("Please select delivery day & time.", Colors.black);
    } else
      proceedToPayment();
  }

  proceedToPayment() async {
    var f = DateFormat('dd MMM yyy');
    var dDay = "${DateFormat('dd-MM-yyy').format(wDays[selectedDay])}";
    var tSlot = "${f.format(wDays[selectedDay])}, $_selectedTime";

    print("DDAY: $dDay || TSLOT: $tSlot");
    List<String> orderInfo = [];
    orderInfo.add(CART_ID);
    orderInfo.add(deliveryAddress);
    orderInfo.add(dDay.toString());
    orderInfo.add(deliveryType);
    orderInfo.add("");
    orderInfo.add(orderInstruction.text.toString());
    orderInfo.add("CASH");
    orderInfo.add(PROMOCODE);
    orderInfo.add(cashbackAmount.toStringAsFixed(2));
    orderInfo.add("1");
    orderInfo.add(SHOPPING_LIST_ID);
    orderInfo.add(tSlot.toString());
    orderInfo.add("");

    await Get.to(PaymentOption(grandTotal.toString(), orderInfo));
    getCartProducts();
  }

  initValues() {
    print("STORE DETAIL: $STORE_DETAILS");
    setState(() {
      deliveryStartTime = STORE_DETAILS.data.shopDetails.deliveryStartTime;
      deliveryCloseTime = STORE_DETAILS.data.shopDetails.deliveryCloseTime;
      deliveryTime = STORE_DETAILS.data.shopDetails.deliveryTime;
      deliveryRadius =
          double.tryParse(STORE_DETAILS.data.shopDetails.deliveryRadius);
      if (deliveryRadius == 0)
        homeDeliveryAvail = false;
      else
        homeDeliveryAvail = true;
    });
  }

  showAlert() {
    print("ALERT");
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (context) {
          return AlertDialog(
            title: Text(
              "Alert",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
            ),
            content: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Your cart value is less than minimum order amount. To proceed please add more product to your cart.",
                    style: TextStyle(fontSize: 12),
                  ),
                  Text(
                    "Store's minimum order value is ${STORE_DETAILS.data.shopDetails.minOrder}",
                    style: TextStyle(fontSize: 12),
                  )
                ],
              ),
            ),
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            actions: <Widget>[
              ButtonTheme(
                height: 40,
                minWidth: 100,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                child: RaisedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    "Cancel",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ],
          );
        });
  }

  // showPromoSheet() {
  //   return showBarModalBottomSheet(
  //       expand: true,
  //       isDismissible: true,
  //       enableDrag: true,
  //       backgroundColor: Colors.transparent,
  //       context: context,
  //       builder: (context) {
  //         return StatefulBuilder(
  //           builder: (context, setState) {
  //             return Container(
  //               child: Column(
  //                 children: <Widget>[
  //                   Container(
  //                     color: Colors.white,
  //                     padding: EdgeInsets.symmetric(
  //                         horizontal: 8, vertical: 16),
  //                     child: Row(
  //                       children: <Widget>[
  //                         Expanded(
  //                           child: Container(
  //                             height: 40,
  //                             child: TextField(
  //                               controller: promocodeController,
  //                               autofocus: false,
  //                               onChanged: (text) {
  //                                 setState(() {
  //                                   PROMOCODE =
  //                                       promocodeController.text.toUpperCase();
  //                                 });
  //                               },
  //                               decoration: InputDecoration(
  //                                 filled: true,
  //                                 fillColor: Colors.grey.withOpacity(.2),
  //                                 enabledBorder: OutlineInputBorder(
  //                                     borderSide: BorderSide(
  //                                         color: Colors.white,
  //                                         width: 0),
  //                                     borderRadius:
  //                                     BorderRadius.circular(6)),
  //                                 focusedBorder: OutlineInputBorder(
  //                                     borderSide: BorderSide(
  //                                         color: Colors.white,
  //                                         width: 0),
  //                                     borderRadius:
  //                                     BorderRadius.circular(6)),
  //                                 contentPadding:
  //                                 EdgeInsets.symmetric(
  //                                     horizontal: 8, vertical: 4),
  //                                 hintStyle: TextStyle(fontSize: 12),
  //                                 labelStyle: TextStyle(fontSize: 11),
  //                                 enabled: true,
  //                                 hintText: "Enter Coupon Code",
  //                               ),
  //                               cursorColor: Colors.black,
  //                               showCursor: true,
  //                             ),
  //                           ),
  //                         ),
  //                         Container(
  //                           padding: EdgeInsets.symmetric(
  //                               horizontal: 16, vertical: 8),
  //                           child: GestureDetector(
  //                               onTap: () async {
  //                                 setState(() {
  //                                   applyingPromo = true;
  //                                 });
  //                                 await validatePromoCode();
  //                                 setState(() {
  //                                   applyingPromo = false;
  //                                   promocodeController.text = "";
  //                                 });
  //                                 Get.back();
  //                               },
  //                               child: Text("Apply", style: TextStyle(
  //                                   color: Colors.blue),)),
  //                         )
  //                       ],
  //                     ),
  //                   ),
  //                   Divider(height: 0, color: Colors.black,),
  //                   applyingPromo ? Center(child: Padding(
  //                     padding: const EdgeInsets.all(60.0),
  //                     child: Text(
  //                       "Applying promocode...", style: largeTextStyleBlack,),
  //                   ),) :
  //                   noPromoAvail ?
  //                   Container(
  //                     padding: EdgeInsets.all(60),
  //                     child: Center(child: Text(
  //                       "No Promocode Available",
  //                       style: largeTextStyleBlack,),),
  //                   ) :
  //                   Expanded(
  //                     child: ListView.separated(
  //                         padding: EdgeInsets.only(top: 20),
  //                         shrinkWrap: true,
  //                         itemCount: promoCode.info.length,
  //                         separatorBuilder: (context, index) {
  //                           return SizedBox(height: 8,);
  //                         },
  //                         itemBuilder: (context, index) {
  //                           return Container(
  //                             padding: EdgeInsets.all(4),
  //                             color: Colors.white,
  //                             child: ListTile(
  //                               onTap: () {
  //                                 setState(() {
  //                                   promocodeController.text =
  //                                       promoCode.info[index].code
  //                                           .toUpperCase();
  //                                   PROMOCODE =
  //                                       promoCode.info[index].code
  //                                           .toUpperCase();
  //                                 });
  //                               },
  //                               title: Text(
  //                                 promoCode.info[index].code.toUpperCase(),
  //                                 style: largeTextStyleBlack,),
  //                               subtitle: Text(
  //                                 promoCode.info[index].promocodeDesc,
  //                                 style: smallTextStyle,),
  //                             ),
  //                           );
  //                         }
  //                     ),
  //                   )
  //                 ],
  //               ),
  //             );
  //           },
  //         );
  //       }
  //   );
  // }

  @override
  void initState() {
    // TODO: implement initState
    getCartProducts();
    getDeliveryAddress();
    getPromoCodes();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.only(bottom: 60),
        child: Container(
          child: Column(
            children: <Widget>[
              Stack(children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 80 + MediaQuery
                      .of(context)
                      .padding
                      .top,
                  color: Colors.grey,
                  child: Row(
                      children: [
                        Expanded(
                            flex: 1,
                            child: GestureDetector(
                                onTap: () {
                                  Get.back();
                                },
                                child: Icon(Icons.arrow_back))),
                        Expanded(
                            flex: 4,
                            child: Text(
                              "Cart",
                            )),
                        emptyCart ? SizedBox() : loading
                            ? SizedBox()
                            : Expanded(
                            flex: 2,
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                              child: AutoSizeText(
                                "₹ ${cartTotal}",
                                minFontSize: 9,
                                maxFontSize: 16,
                                textAlign: TextAlign.end,
                              ),
                            ))
                      ]),
                ),
                emptyCart ? Center(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 160),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset(
                          'drawables/start_shopping.png',
                          height: 150,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text("Your Cart looks empty."),
                        SizedBox(
                          height: 10,
                        ),
                        ButtonTheme(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          height: 40,
                          minWidth: 200,
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text("Start Shopping"),
                            color: mColor,
                            textColor: Colors.white,
                          ),
                        )
                      ],
                    ),
                  ),
                ) : SizedBox(),
                emptyCart ? SizedBox() : Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: Colors.grey, width: .5),
                        borderRadius: BorderRadius.all(Radius.circular(12))),
                    margin: EdgeInsets.only(
                        top: 40 + MediaQuery
                            .of(context)
                            .padding
                            .top),
                    child: Column(children: [
                      Padding(
                        padding: EdgeInsets.all(12),
                        child: loading ? multipleListTileLoader() : Column(
                          children: <Widget>[
                            Container(
                              child: ListView.separated(
                                separatorBuilder: (context, index) {
                                  return SizedBox(
                                    height: 20,
                                  );
                                },
                                padding: EdgeInsets.only(top: 0),
                                shrinkWrap: true,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: cartProduct.product.length,
                                itemBuilder: (context, index) {
                                  return ProductCard(
                                      UniqueKey(), cartProduct.product[index]);
                                },
                              ),
                            ),
                            SizedBox(
                              height: 16,
                            ),
                            TextField(
                              autofocus: false,
                              controller: orderInstruction,
                              onChanged: (text) {},
                              onSubmitted: (text) {},
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black12, width: 0),
                                    borderRadius:
                                    BorderRadius.circular(6)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.black12,
                                        width: 0),
                                    borderRadius:
                                    BorderRadius.circular(6)),
                                contentPadding:
                                EdgeInsets.symmetric(
                                    horizontal: 4, vertical: 4),
                                hintStyle: TextStyle(fontSize: 12),
                                labelStyle: TextStyle(fontSize: 11),
                                enabled: true,
                                hintText:
                                "Any instructions?",
                              ),
                              cursorColor: Colors.black,
                              showCursor: true,
                            ),
                            // Padding(
                            //   padding: EdgeInsets.symmetric(
                            //       vertical: 8.0),
                            //   child: Row(
                            //     children: <Widget>[
                            //       Padding(
                            //         padding:
                            //         EdgeInsets.only(right: 8),
                            //         child: Image.asset(
                            //           'assets/meal-prep-time.png',
                            //           height: 20,
                            //           color: Colors.blue[300],
                            //         ),
                            //       ),
                            //       Expanded(
                            //           child: Text(
                            //             "Meal preparation time ${STORE_DETAILS.data.shopDetails.deliveryTime} minutes",
                            //             style: smallTextStyle,
                            //           ))
                            //     ],
                            //   ),
                            // )
                          ],
                        ),
                      ),
                    ]),
                  ),
                ),
              ]),
              emptyCart ? SizedBox() : loading
                  ?
              Column(
                children: <Widget>[listTileLoader(), multipleListTileLoader()],)
                  :
              loadAddress ? SizedBox() : Column(
                children: <Widget>[
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              onChangeDeliveryType(0);
                            },
                            child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 8),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 4),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(15)),
                                    border: Border.all(
                                        color: deliveryTypeOption == 0
                                            ? mColor
                                            : Colors.black)
                                ),
                                child: Center(child: Text("Home Delivery",
                                  style: deliveryTypeOption == 0
                                      ? textStyleLargeColor
                                      : largeTextStyleBlack,))),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: GestureDetector(
                            onTap: () {
                              onChangeDeliveryType(1);
                            },
                            child: Container(
                                margin: EdgeInsets.symmetric(horizontal: 8),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 8, vertical: 4),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(15)),
                                    border: Border.all(
                                        color: deliveryTypeOption == 1
                                            ? mColor
                                            : Colors.black)
                                ),
                                child: Center(child: Text("Store Pickup",
                                  style: deliveryTypeOption == 1
                                      ? textStyleLargeColor
                                      : largeTextStyleBlack,))),
                          ),
                        ),
                      ],
                    ),
                  ),
                  deliveryTypeOption == -1 ? SizedBox() : Column(
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Container(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: ListTile(
                            subtitle: addressList.details.isEmpty ?
                            Text("Please add delivery address.",
                              style: smallTextStyleBlack,) :
                            deliveryAvailable ?
                            Text(
                              "$deliveryAddress", style: smallTextStyleBlack,) :
                            Text("$outOfRadius", style: TextStyle(
                                fontSize: 11, color: Colors.redAccent),),
                            contentPadding: EdgeInsets.all(0),
                            trailing: deliveryTypeOption == 1
                                ? null
                                : GestureDetector(
                                onTap: () async {
                                  if (addressList.details.isNotEmpty)
                                    showAddressList();
                                  else {
                                    await Get.to(
                                        SearchLocationMap(null, null, false));
                                    getDeliveryAddress();
                                  }
                                },
                                child: Text(addressList.details.isEmpty
                                    ? "add"
                                    : "change", style: TextStyle(
                                    fontSize: 11, color: Colors.blue),)),
                            title: Text(
                              "Deliver To", style: textStyleLargeColor,),
                          )
                      ),
                    ],
                  ),
                  SizedBox(height: 16,),
                  Container(
                    width: double.infinity,
                    alignment: Alignment.center,
                    color: Colors.teal,
                    child: Text(
                      "$deliveryTitle ${DateFormat('hh:mma').format(
                          DateTime.fromMillisecondsSinceEpoch(DateTime
                              .parse("${DateFormat('yyyy-MM-dd').format(
                              DateTime.now())} $deliveryStartTime")
                              .millisecondsSinceEpoch))} to ${DateFormat(
                          'hh:mma').format(
                          DateTime.fromMillisecondsSinceEpoch(DateTime
                              .parse("${DateFormat('yyyy-MM-dd').format(
                              DateTime.now())} $deliveryCloseTime")
                              .millisecondsSinceEpoch))}",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  weekDays(),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(flex: 1, child: Text(
                          "Delivery Time:", style: textStyleLargeColor,)),
                        Expanded(
                            flex: 1,
                            child: buildTimeSlot()
                        )
                      ],
                    ),
                  ),
                  // SizedBox(height: 16,),
                  // Container(
                  //     height: 80,
                  //     padding: EdgeInsets.symmetric(horizontal: 16),
                  //     child: Row(
                  //       children: [
                  //         Expanded(
                  //             flex: 2,
                  //             child: Container(
                  //                 padding: EdgeInsets.symmetric(
                  //                     horizontal: 16, vertical: 8),
                  //                 decoration: BoxDecoration(
                  //                   color: mColor.withOpacity(.2),
                  //                   borderRadius:
                  //                   BorderRadius.all(Radius.circular(12)),
                  //                 ),
                  //                 child: Column(
                  //                   crossAxisAlignment:
                  //                   CrossAxisAlignment.start,
                  //                   children: [
                  //                     Text(
                  //                       "Fit Points",
                  //                       style: TextStyle(
                  //                           fontSize: 16,
                  //                           color: mColor,
                  //                           fontWeight: FontWeight.bold),
                  //                     ),
                  //                     Text(
                  //                       "Use this points for partial or Full payment of your Order",
                  //                       style: TextStyle(
                  //                           fontSize: 12,
                  //                           fontWeight: FontWeight.w100),
                  //                     )
                  //                   ],
                  //                 ))),
                  //         Expanded(
                  //             flex: 1,
                  //             child: Container(
                  //                 padding: EdgeInsets.symmetric(
                  //                     horizontal: 16, vertical: 8),
                  //                 decoration: BoxDecoration(
                  //                   color: mColor.withOpacity(.2),
                  //                   borderRadius:
                  //                   BorderRadius.all(Radius.circular(12)),
                  //                 ),
                  //                 child: Column(
                  //                   children: <Widget>[
                  //                     Text(
                  //                       "125",
                  //                       style: TextStyle(
                  //                           fontSize: 16,
                  //                           color: mColor,
                  //                           fontWeight: FontWeight.bold),
                  //                     ),
                  //                     SizedBox(
                  //                       height: 8,
                  //                     ),
                  //                     Container(
                  //                       height: 22,
                  //                       width: double.infinity,
                  //                       decoration: BoxDecoration(
                  //                           color: mColor,
                  //                           borderRadius: BorderRadius.all(
                  //                               Radius.circular(12))),
                  //                       child: Center(
                  //                           child: Text(
                  //                             "Use Points",
                  //                             style: TextStyle(
                  //                                 fontSize: 11,
                  //                                 color: Colors.white),
                  //                           )),
                  //                     ),
                  //                   ],
                  //                 ))),
                  //       ],
                  //     )),
                  // SizedBox(
                  //   height: 16,
                  // ),
                  // loadPromo ? SizedBox() : Padding(
                  //   padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  //   child: promoApplied ?
                  //   Container(
                  //     child: ListTile(
                  //       contentPadding: EdgeInsets.all(0),
                  //       trailing: GestureDetector(
                  //           onTap: () {
                  //             setState(() {
                  //               grandTotal = double.tryParse(
                  //                   cartProduct.total.grandtotal);
                  //               discount = double.tryParse(
                  //                   cartProduct.total.overallDiscount);
                  //               promoApplied = false;
                  //             });
                  //           },
                  //           child: Icon(
                  //             Icons.remove_circle, color: Colors.redAccent,
                  //             size: 18,)),
                  //       title: Text(PROMOCODE, style: textStyleLargeColor,),
                  //       subtitle: Text("Promocode  applied successfully!",
                  //         style: TextStyle(color: Colors.green, fontSize: 12),),
                  //     ),
                  //   ) :
                  //   GestureDetector(
                  //     onTap: () {
                  //       showPromoSheet();
                  //     },
                  //     child: Container(
                  //       child: Column(
                  //         crossAxisAlignment: CrossAxisAlignment.start,
                  //         children: <Widget>[
                  //           Text(
                  //             "Apply Coupon Code",
                  //             style: textStyleLargeColor,
                  //           ),
                  //           SizedBox(
                  //             height: 8,
                  //           ),
                  //           Container(
                  //               height: 50,
                  //               decoration: BoxDecoration(
                  //                   borderRadius:
                  //                   BorderRadius.all(Radius.circular(10)),
                  //                   border: Border.all(
                  //                       color: Colors.grey, width: .5)),
                  //               child: Row(
                  //                 children: [
                  //                   Expanded(
                  //                       flex: 2,
                  //                       child: Padding(
                  //                         padding: const EdgeInsets.all(4.0),
                  //                         child: Container(
                  //                           child: Text("Coupon Code",
                  //                             style: TextStyle(
                  //                                 color: Colors.grey),),
                  //                         ),
                  //                       )),
                  //                   Expanded(
                  //                     flex: 1,
                  //                     child: Container(
                  //                       decoration: BoxDecoration(
                  //                         color: mColor,
                  //                         borderRadius: BorderRadius.all(
                  //                             Radius.circular(8)),
                  //                       ),
                  //                       child: Center(
                  //                           child: Text(
                  //                             "APPLY",
                  //                             style: TextStyle(
                  //                                 color: Colors.white),
                  //                           )),
                  //                     ),
                  //                   )
                  //                 ],
                  //               )),
                  //         ],
                  //       ),
                  //     ),
                  //   ),
                  // ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Bill Details",
                          style: textStyleLargeColor,
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Item total",
                                    style: mediumTextStyleBlack,
                                  ),
                                  Text(
                                    "₹ $cartTotal",
                                    style: mediumTextStyleBlack,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Discounts",
                                    style: mediumTextStyleBlack,
                                  ),
                                  Text(
                                    "₹ ${discount.toStringAsFixed(2)}",
                                    style: mediumTextStyleBlack,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Delivery fees",
                                    style: mediumTextStyleBlack,
                                  ),
                                  Text(
                                    "₹ ${deliveryTypeOption == 0
                                        ? deliveryFees.toStringAsFixed(2)
                                        : deliveryTypeOption == 1
                                        ? "0.00"
                                        : deliveryFees.toStringAsFixed(2)}",
                                    style: mediumTextStyleBlack,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Taxes & Charges",
                                    style: mediumTextStyleBlack,
                                  ),
                                  Text(
                                    "₹ ${tax.toStringAsFixed(2)}",
                                    style: mediumTextStyleBlack,
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Divider(
                                height: 1,
                                color: Colors.black,
                              ),
                              SizedBox(
                                height: 4,
                              ),
                              Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "Grand Total",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  deliveryTypeOption == 0 ?
                                  Text("₹ ${(grandTotal + deliveryFees)
                                      .toStringAsFixed(2)}",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold)) :
                                  deliveryTypeOption == 1 ?
                                  Text("₹ ${(grandTotal)
                                      .toStringAsFixed(2)}",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold)) :
                                  Text("₹ ${(grandTotal + deliveryFees)
                                      .toStringAsFixed(2)}",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold))
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 22,
                        ),
                        GestureDetector(
                          onTap: () {
                            print(deliveryAddress);
                            if (!placingOrder) {
                              // getShoppingCartId();
                              // Get.to(PaymentOption(grandTotal));
                              validateDetails();
                            }
                          },
                          child: Container(
                            //width: 100.0,
                            height: 60.0,
                            decoration: BoxDecoration(
                              color: mColor,
                              borderRadius: BorderRadius.circular(6.0),
                            ),
                            child: Center(
                              child: placingOrder ?
                              SizedBox(
                                  height: 25,
                                  width: 25,

                                  child: SpinKitCircle(
                                    color: Colors.white,
                                    size: 25,
                                  )
                              ) :
                              Text(
                                'Checkout For Payment',
                                style: TextStyle(
                                    fontSize: 17.0, color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProductCard extends StatefulWidget {
  Product product;

  ProductCard(Key key, this.product) : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard>
    with AutomaticKeepAliveClientMixin {
  int itemCount = 0,
      totalLeft = 0;
  HttpRequests requests = HttpRequests();

  increment() {
    if (itemCount <= totalLeft) {
      itemCount++;
      addItemToCart(itemCount);
    }
    else{
            Flushbar(
        icon: Icon(
          Icons.cancel,
          color: mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color: mColor),
        ),
      )..show(context);
    }
  }

  decrement() {
    if (itemCount <= 1) {
      setState(() {
        itemCount--;
      });

      print("$itemCount");
      addItemToCart(itemCount);
    } else {
      itemCount--;
      addItemToCart(itemCount);
    }
  }

  addItemToCart(int qty) async {
    print("F $qty");
    var response = await requests.addToCart(
      widget.product.masterproductid,
      widget.product.merchantlistid,
      "",
      "",
      qty,);

    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      setState(() {
        print("CART UPDATED!");
        checkOutState.getCartProducts();
        getShoppingListCount();
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    itemCount = widget.product.quantity;
    totalLeft = widget.product.totalLeft;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                LimitedBox(
                  maxHeight: 55,
                  maxWidth: 80,
                  child: Container(
                    height: 55,
                    width: 80,
                    decoration: BoxDecoration(
                        color: mColor.withOpacity(.4),
                        borderRadius: BorderRadius.all(Radius.circular(18))),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(18)),
                      child: CachedNetworkImage(
                        placeholder: (context, _) =>
                            Image.asset('assets/default-image.jpg'),
                        imageUrl: widget.product.icon,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(4),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "${widget.product.productname}",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: mediumTextStyleBlack,
                        ),
                        Image.asset('assets/food-category.png', height: 12,
                          color: widget.product.type == "veg"
                              ? Colors.green
                              : Colors.red[700],),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Container(
                width: 80,
                height: 30,
                decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 1,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                    borderRadius: BorderRadius.all(Radius.circular(3))),
                child: Container(
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            decrement();
                          },
                          child: Container(
                            child: Icon(
                              Icons.remove,
                              size: 16,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        child: Center(child: Text("$itemCount")),
                      ),
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            increment();
                          },
                          child: Container(
                            child: Icon(
                              Icons.add,
                              size: 16,
                              color: Colors.black,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Text(
                "₹ ${widget.product.sellingPrice}",
                style: largeTextStyleBlack,
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}