import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:userapp/Getters/GetDeals.dart';

import 'Getters/GetNearbyStores.dart';
import 'package:flutter/material.dart';

import 'Getters/GetUser.dart';
import 'Network/httpRequests.dart';

NearbyStores nearbyStores;

class StreamVars extends GetxController {
  RxBool addingItem = false.obs;
  RxBool noInternet = false.obs;
}

User userDetails;
var auth = new HttpRequests();

Future<int> getCartCount() async {
  print(userDetails.data.loginDetails.authKey);
  var r =
  await auth.getShoppingListCount(userDetails.data.loginDetails.authKey);
  return r['data'];
}

showToast(String msg, Color color) {
  Fluttertoast.showToast(
    msg: msg,
    timeInSecForIosWeb: 2,
    toastLength: Toast.LENGTH_LONG,
    backgroundColor: color,
    textColor: Colors.white,
    fontSize: 13,
  );
}

StreamVars streamVars = Get.put(StreamVars());

Widget neoLoaderStack() {
  return Obx(() => Visibility(
    visible: streamVars.addingItem.value,
    child: Center(
        child: Image.asset(
          'drawables/neomart_loader.gif',
          height: 80,
        )),
  ));
}

Widget neoLoader() {
  return Center(
      child: Image.asset(
        'drawables/neomart_loader.gif',
        height: 80,));
}
