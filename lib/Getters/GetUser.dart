class User {
  Meta meta;
  Data data;

  User({this.meta, this.data});

  User.fromJson(Map<String, dynamic> json) {
    meta = json['meta'] != null ? new Meta.fromJson(json['meta']) : null;
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.meta != null) {
      data['meta'] = this.meta.toJson();
    }
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Meta {
  String status;
  String message;

  Meta({this.status, this.message});

  Meta.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class Data {
  LoginDetails loginDetails;

//  List<Null> validOffer;

  Data({this.loginDetails});

  Data.fromJson(Map<String, dynamic> json) {
    loginDetails = json['login_details'] != null
        ? new LoginDetails.fromJson(json['login_details'])
        : null;
//    if (json['valid_offer'] != null) {
//      validOffer = new List<Null>();
//      json['valid_offer'].forEach((v) {
//        validOffer.add(new Null.fromJson(v));
//      });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.loginDetails != null) {
      data['login_details'] = this.loginDetails.toJson();
    }
//    if (this.validOffer != null) {
//      data['valid_offer'] = this.validOffer.map((v) => v.toJson()).toList();
//    }
    return data;
  }
}

class LoginDetails {
  String authKey;
  String isNewuser;
  int userId;
  Null facebookId;
  String isRegistered;
  String profileSetup;
  String storeSetup;
  String address;
  String lat;
  String lng;

  LoginDetails(
      {this.authKey,
      this.isNewuser,
      this.userId,
      this.facebookId,
      this.isRegistered,
      this.profileSetup,
      this.storeSetup,
      this.address,
      this.lat,
      this.lng});

  LoginDetails.fromJson(Map<String, dynamic> json) {
    authKey = json['auth_key'];
    isNewuser = json['is_newuser'];
    userId = json['user_id'];
    facebookId = json['facebook_id'];
    isRegistered = json['is_registered'];
    profileSetup = json['profile_setup'];
    storeSetup = json['store_setup'];
    address = json['address'];
    lat = json['lat'].toString();
    lng = json['lng'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['auth_key'] = this.authKey;
    data['is_newuser'] = this.isNewuser;
    data['user_id'] = this.userId;
    data['facebook_id'] = this.facebookId;
    data['is_registered'] = this.isRegistered;
    data['profile_setup'] = this.profileSetup;
    data['store_setup'] = this.storeSetup;
    data['address'] = this.address;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    return data;
  }
}
