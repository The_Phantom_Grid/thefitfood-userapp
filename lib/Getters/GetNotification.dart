import 'package:flutter/foundation.dart';

class UserNotification {
  List<NotificationData> data;
  String message;
  String status;

  UserNotification({this.data, this.message, this.status});

  UserNotification.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<NotificationData>();
      json['data'].forEach((v) {
        data.add(new NotificationData.fromJson(v));
      });
    }
    message = json['message'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['status'] = this.status;
    return data;
  }
}

class NotificationData {
  int id;
  String sender;
  String senderId;
  String orderId;
  int templateId;
  int channelId;
  String eventId;
  String subject;
  String messageContent;
  int receiverId;
  String currentStatus;
  String sentAt;
  String deliveredAt;
  String readAt;
  String resendAt;
  String username;
  String email;
  String mobile;
  String address;
  String city;
  String lat;
  String lng;
  String userId;
  String firstname;
  String middlename;
  String lastname;
  String avatarBaseUrl;
  String avatarPath;
  String gender;
  String notificationId;

  NotificationData(
      {this.id,
      this.sender,
      this.senderId,
      this.orderId,
      this.templateId,
      this.channelId,
      this.eventId,
      this.subject,
      this.messageContent,
      this.receiverId,
      this.currentStatus,
      this.sentAt,
      this.deliveredAt,
      this.readAt,
      this.resendAt,
      this.username,
      this.email,
      this.mobile,
      this.address,
      this.city,
      this.lat,
      this.lng,
      this.userId,
      this.firstname,
      this.middlename,
      this.lastname,
      this.avatarBaseUrl,
      this.avatarPath,
      this.gender,
      this.notificationId});

  NotificationData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    sender = json['sender'].toString();
    senderId = json['sender_id'].toString();
    orderId = json['order_id'].toString();
    templateId = json['template_id'];
    channelId = json['channel_id'];
    eventId = json['event_id'];
    subject = json['subject'];
    messageContent = json['message_content'];
    receiverId = json['receiver_id'];
    currentStatus = json['current_status'];
    sentAt = json['sent_at'];
    deliveredAt = json['delivered_at'].toString();
    readAt = json['read_at'].toString();
    resendAt = json['resend_at'].toString();
    username = json['username'].toString();
    email = json['email'];
    mobile = json['mobile'].toString();
    address = json['address'];
    city = json['city'];
    lat = json['lat'].toString();
    lng = json['lng'].toString();
    userId = json['user_id'].toString();
    firstname = json['firstname'];
    middlename = json['middlename'].toString();
    lastname = json['lastname'].toString();
    avatarBaseUrl = json['avatar_base_url'];
    avatarPath = json['avatar_path'];
    gender = json['gender'].toString();
    notificationId = json['notification_id'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sender'] = this.sender;
    data['sender_id'] = this.senderId;
    data['order_id'] = this.orderId;
    data['template_id'] = this.templateId;
    data['channel_id'] = this.channelId;
    data['event_id'] = this.eventId;
    data['subject'] = this.subject;
    data['message_content'] = this.messageContent;
    data['receiver_id'] = this.receiverId;
    data['current_status'] = this.currentStatus;
    data['sent_at'] = this.sentAt;
    data['delivered_at'] = this.deliveredAt;
    data['read_at'] = this.readAt;
    data['resend_at'] = this.resendAt;
    data['username'] = this.username;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['address'] = this.address;
    data['city'] = this.city;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['user_id'] = this.userId;
    data['firstname'] = this.firstname;
    data['middlename'] = this.middlename;
    data['lastname'] = this.lastname;
    data['avatar_base_url'] = this.avatarBaseUrl;
    data['avatar_path'] = this.avatarPath;
    data['gender'] = this.gender;
    data['notification_id'] = this.notificationId;
    return data;
  }
}

class TotalNotification {
  List<Data> data;
  String message;
  String status;
  String version;
  String iosVersion;

  TotalNotification(
      {this.data, this.message, this.status, this.version, this.iosVersion});

  TotalNotification.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
    message = json['message'].toString();
    status = json['status'].toString();
    version = json['version'].toString();
    iosVersion = json['ios_version'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['status'] = this.status;
    data['version'] = this.version;
    data['ios_version'] = this.iosVersion;
    return data;
  }
}

class Data {
  int totalMessages;

  Data({this.totalMessages});

  Data.fromJson(Map<String, dynamic> json) {
    totalMessages = json['total_messages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total_messages'] = this.totalMessages;
    return data;
  }
}

//class TotalNotification {
//  List<TotalUnreadNotification> data;
//  String message;
//  String status;
//  String version;
//  String iosVersion;
//
//  TotalNotification(
//      {this.data, this.message, this.status, this.version, this.iosVersion});
//
//  TotalNotification.fromJson(Map<String, dynamic> json) {
//    if (json['data'] != null) {
//      data = new List<TotalUnreadNotification>();
//      json['data'].forEach((v) {
//        data.add(new TotalUnreadNotification.fromJson(v));
//      });
//    }
//    message = json['message'].toString();
//    status = json['status'].toString();
//    version = json['version'].toString();
//    iosVersion = json['ios_version'].toString();
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.data != null) {
//      data['data'] = this.data.map((v) => v.toJson()).toList();
//    }
//    data['message'] = this.message;
//    data['status'] = this.status;
//    data['version'] = this.version;
//    data['ios_version'] = this.iosVersion;
//    return data;
//  }
//}
//
//class TotalUnreadNotification {
//  int totalMessages;
//
//  TotalUnreadNotification({this.totalMessages});
//
//  TotalUnreadNotification.fromJson(Map<String, dynamic> json) {
//    totalMessages = json['total_messages'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['total_messages'] = this.totalMessages;
//    return data;
//  }
//}
