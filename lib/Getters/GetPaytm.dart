class PaytmWallet {
  int orderid;
  PaytmInfo paytm;
  String status;

  PaytmWallet({this.orderid, this.paytm, this.status});

  PaytmWallet.fromJson(Map<String, dynamic> json) {
    orderid = json['orderid'];
    paytm =
        json['paytm'] != null ? new PaytmInfo.fromJson(json['paytm']) : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderid'] = this.orderid;
    if (this.paytm != null) {
      data['paytm'] = this.paytm.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class PaytmInfo {
  String mID;
  String oRDERID;
  String cUSTID;
  String mOBILENO;
  String eMAIL;
  String cHANNELID;
  String tXNAMOUNT;
  String wEBSITE;
  String iNDUSTRYTYPEID;
  String checksum;
  String cALLBACKURL;
  String MERCHANT_KEY;
  String RAZORPAY_KEY;

  PaytmInfo(
      {this.mID,
      this.oRDERID,
      this.cUSTID,
      this.mOBILENO,
      this.eMAIL,
      this.cHANNELID,
      this.tXNAMOUNT,
      this.wEBSITE,
      this.iNDUSTRYTYPEID,
      this.checksum,
      this.cALLBACKURL,
      this.MERCHANT_KEY,
      this.RAZORPAY_KEY});

  PaytmInfo.fromJson(Map<String, dynamic> json) {
    mID = json['MID'].toString();
    oRDERID = json['ORDER_ID'].toString();
    cUSTID = json['CUST_ID'].toString();
    mOBILENO = json['MOBILE_NO'].toString();
    eMAIL = json['EMAIL'];
    cHANNELID = json['CHANNEL_ID'].toString();
    tXNAMOUNT = json['TXN_AMOUNT'].toString();
    wEBSITE = json['WEBSITE'];
    iNDUSTRYTYPEID = json['INDUSTRY_TYPE_ID'].toString();
    checksum = json['Checksum'].toString();
    cALLBACKURL = json['CALLBACK_URL'];
    MERCHANT_KEY = json['MERCHANT_KEY'];
    RAZORPAY_KEY = json['RAZORPAY_KEY'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['MID'] = this.mID;
    data['ORDER_ID'] = this.oRDERID;
    data['CUST_ID'] = this.cUSTID;
    data['MOBILE_NO'] = this.mOBILENO;
    data['EMAIL'] = this.eMAIL;
    data['CHANNEL_ID'] = this.cHANNELID;
    data['TXN_AMOUNT'] = this.tXNAMOUNT;
    data['WEBSITE'] = this.wEBSITE;
    data['INDUSTRY_TYPE_ID'] = this.iNDUSTRYTYPEID;
    data['Checksum'] = this.checksum;
    data['CALLBACK_URL'] = this.cALLBACKURL;
    data['MERCHANT_KEY'] = this.MERCHANT_KEY;
    data['RAZORPAY_KEY'] = this.RAZORPAY_KEY;
    return data;
  }
}

/////////////Paytm Response//////////

class PaytmResponse {
  String bANKTXNID;
  String sTATUS;
  String mID;
  String cURRENCY;
  String rESPCODE;
  String rESPMSG;
  String oRDERID;
  String cHECKSUMHASH;
  String tXNAMOUNT;

  PaytmResponse(
      {this.bANKTXNID,
      this.sTATUS,
      this.mID,
      this.cURRENCY,
      this.rESPCODE,
      this.rESPMSG,
      this.oRDERID,
      this.cHECKSUMHASH,
      this.tXNAMOUNT});

  PaytmResponse.fromJson(Map<String, dynamic> json) {
    bANKTXNID = json['BANKTXNID'];
    sTATUS = json['STATUS'];
    mID = json['MID'];
    cURRENCY = json['CURRENCY'];
    rESPCODE = json['RESPCODE'];
    rESPMSG = json['RESPMSG'];
    oRDERID = json['ORDERID'];
    cHECKSUMHASH = json['CHECKSUMHASH'];
    tXNAMOUNT = json['TXNAMOUNT'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['BANKTXNID'] = this.bANKTXNID;
    data['STATUS'] = this.sTATUS;
    data['MID'] = this.mID;
    data['CURRENCY'] = this.cURRENCY;
    data['RESPCODE'] = this.rESPCODE;
    data['RESPMSG'] = this.rESPMSG;
    data['ORDERID'] = this.oRDERID;
    data['CHECKSUMHASH'] = this.cHECKSUMHASH;
    data['TXNAMOUNT'] = this.tXNAMOUNT;
    return data;
  }
}
