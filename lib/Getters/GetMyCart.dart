class MyCartItems {
  List<StoreItemProduct> product;

//  Total total;
//  String status;

  MyCartItems({this.product});

  MyCartItems.fromJson(Map<String, dynamic> json) {
    if (json['product'] != null) {
      product = new List<StoreItemProduct>();
      json['product'].forEach((v) {
        product.add(new StoreItemProduct.fromJson(v));
      });
    }
//    total = json['total'] != null ? new Total.fromJson(json['total']) : null;
//    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.product != null) {
      data['product'] = this.product.map((v) => v.toJson()).toList();
    }
//    if (this.total != null) {
//      data['total'] = this.total.toJson();
//    }
//    data['status'] = this.status;
    return data;
  }
}

class StoreItemProduct {
  String shoppingListId;
  String merchantid;
  int userId;
  int totalItems;
  double price;
  String storeid;
  String systemCategory;
  String storeName;
  String storeAddress;
  String storeIcon;
  String avgTimeToDeliver;
  String shipping;
  String deliveryRadius;
  int minOrder;
  String storeOpenTime;
  String storeCloseTime;
  String pincode;
  double lat;
  double lng;

  StoreItemProduct(
      {this.shoppingListId,
      this.merchantid,
      this.userId,
      this.totalItems,
      this.price,
      this.storeid,
      this.systemCategory,
      this.storeName,
      this.storeAddress,
      this.storeIcon,
      this.avgTimeToDeliver,
      this.shipping,
      this.deliveryRadius,
      this.minOrder,
      this.storeOpenTime,
      this.storeCloseTime,
      this.pincode,
      this.lat,
      this.lng});

  StoreItemProduct.fromJson(Map<String, dynamic> json) {
    shoppingListId = json['shopping_list_id'].toString();
    merchantid = json['merchantid'].toString();
    userId = json['userId'];
    totalItems = json['total_items'];
    price = json['price'].toDouble();
    storeid = json['storeid'].toString();
    systemCategory = json['system_category'].toString();
    storeName = json['store_name'].toString();
    storeAddress = json['store_address'].toString();
    storeIcon = json['store_icon'].toString();
    avgTimeToDeliver = json['avg_time_to_deliver'].toString();
    shipping = json['shipping'].toString();
    deliveryRadius = json['delivery_radius'].toString();
    minOrder = json['min_order'] == "" ? 0 : json['min_order'];
    storeOpenTime = json['store_open_time'].toString();
    storeCloseTime = json['store_close_time'].toString();
    pincode = json['pincode'].toString();
    lat = double.parse(json['lat'].toString());
    lng = double.parse(json['lng'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['shopping_list_id'] = this.shoppingListId;
    data['merchantid'] = this.merchantid;
    data['userId'] = this.userId;
    data['total_items'] = this.totalItems;
    data['price'] = this.price;
    data['storeid'] = this.storeid;
    data['system_category'] = this.systemCategory;
    data['store_name'] = this.storeName;
    data['store_address'] = this.storeAddress;
    data['store_icon'] = this.storeIcon;
    data['avg_time_to_deliver'] = this.avgTimeToDeliver;
    data['shipping'] = this.shipping;
    data['delivery_radius'] = this.deliveryRadius;
    data['min_order'] = this.minOrder;
    data['store_open_time'] = this.storeOpenTime;
    data['store_close_time'] = this.storeCloseTime;
    data['pincode'] = this.pincode;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    return data;
  }
}

class Total {
  int totaltax;

  Total({this.totaltax});

  Total.fromJson(Map<String, dynamic> json) {
    totaltax = json['totaltax'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totaltax'] = this.totaltax;
    return data;
  }
}
