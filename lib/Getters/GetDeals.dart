class Deals {
  Info info;
  String status;
  int comingSoon;

  Deals({this.info, this.status, this.comingSoon});

  Deals.fromJson(Map<String, dynamic> json) {
    info = json['info'] != null ? new Info.fromJson(json['info']) : null;
    status = json['status'];
    comingSoon = json['coming_soon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.info != null) {
      data['info'] = this.info.toJson();
    }
    data['status'] = this.status;
    data['coming_soon'] = this.comingSoon;
    return data;
  }
}

class Info {
  List<TopOfferBanner> topOfferBanner;
  List<ProductBanner> productBanner;
  List<TopSaversToday> topSaversToday;
  List<ServiceBanner> serviceBanner;

  Info(
      {this.topOfferBanner,
      this.productBanner,
      this.topSaversToday,
      this.serviceBanner});

  Info.fromJson(Map<String, dynamic> json) {
    if (json['top_offer_banner'] != null) {
      topOfferBanner = new List<TopOfferBanner>();
      json['top_offer_banner'].forEach((v) {
        topOfferBanner.add(new TopOfferBanner.fromJson(v));
      });
    }
    if (json['product_banner'] != null) {
      productBanner = new List<ProductBanner>();
      json['product_banner'].forEach((v) {
        productBanner.add(new ProductBanner.fromJson(v));
      });
    }
    if (json['top_savers_today'] != null) {
      topSaversToday = new List<TopSaversToday>();
      json['top_savers_today'].forEach((v) {
        topSaversToday.add(new TopSaversToday.fromJson(v));
      });
    }
    if (json['service_banner'] != null) {
      serviceBanner = new List<ServiceBanner>();
      json['service_banner'].forEach((v) {
        serviceBanner.add(new ServiceBanner.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.topOfferBanner != null) {
      data['top_offer_banner'] =
          this.topOfferBanner.map((v) => v.toJson()).toList();
    }
    if (this.productBanner != null) {
      data['product_banner'] =
          this.productBanner.map((v) => v.toJson()).toList();
    }
    if (this.topSaversToday != null) {
      data['top_savers_today'] =
          this.topSaversToday.map((v) => v.toJson()).toList();
    }
    if (this.serviceBanner != null) {
      data['service_banner'] =
          this.serviceBanner.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TopOfferBanner {
  int id;
  String bannerUrl;
  double lat;
  double lng;
  String createdAt;
  String updatedAt;
  int isactive;
  String type;
  String displayConfig;
  Null ownerId;

  TopOfferBanner(
      {this.id,
      this.bannerUrl,
      this.lat,
      this.lng,
      this.createdAt,
      this.updatedAt,
      this.isactive,
      this.type,
      this.displayConfig,
      this.ownerId});

  TopOfferBanner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bannerUrl = json['banner_url'];
    lat = json['lat'];
    lng = json['lng'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isactive = json['isactive'];
    type = json['type'];
    displayConfig = json['display_config'];
    ownerId = json['owner_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['banner_url'] = this.bannerUrl;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    data['type'] = this.type;
    data['display_config'] = this.displayConfig;
    data['owner_id'] = this.ownerId;
    return data;
  }
}

class TopSaversToday {
  int id;
  int merchantId;
  int storeId;
  int categoryId;
  int subcatId;
  int merchantProductId;
  int shoppingListQty;
  String catName;
  String subcatName;
  int masterProductId;
  String productname;
  String manufacturer;
  String measure;
  String sku;
  String productdescription;
  double productMrp;
  double sellingPrice;
  String productImage;
  int totalSold;
  int totalLeft;
  String storeName;
  String address;
  String location;
  String state;
  String city;
  int pincode;
  double lat;
  double lng;
  String locality;
  String country;
  String storeManager;
  String storeDescr;
  int systemCategoryId;
  int rating;
  int phoneNo;
  int storeTypeId;
  int deliveryTime;
  String logo;

  TopSaversToday(
      {this.id,
      this.merchantId,
      this.storeId,
      this.categoryId,
      this.subcatId,
      this.merchantProductId,
      this.shoppingListQty,
      this.catName,
      this.subcatName,
      this.masterProductId,
      this.productname,
      this.manufacturer,
      this.measure,
      this.sku,
      this.productdescription,
      this.productMrp,
      this.sellingPrice,
      this.productImage,
      this.totalSold,
      this.totalLeft,
      this.storeName,
      this.address,
      this.location,
      this.state,
      this.city,
      this.pincode,
      this.lat,
      this.lng,
      this.locality,
      this.country,
      this.storeManager,
      this.storeDescr,
      this.systemCategoryId,
      this.rating,
      this.phoneNo,
      this.storeTypeId,
      this.deliveryTime,
      this.logo});

  TopSaversToday.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    merchantId = json['merchant_id'];
    storeId = json['store_id'];
    categoryId = json['category_id'];
    subcatId = json['subcat_id'];
    merchantProductId = json['merchant_product_id'];
    shoppingListQty = json['shopping_list_qty'];
    catName = json['cat_name'];
    subcatName = json['subcat_name'];
    masterProductId = json['master_product_id'];
    productname = json['productname'];
    manufacturer = json['manufacturer'];
    measure = json['measure'];
    sku = json['sku'];
    productdescription = json['productdescription'];
    productMrp = json['product_mrp'].toDouble();
    sellingPrice = json['selling_price'].toDouble();
    productImage = json['product_image'];
    totalSold = json['total_sold'];
    totalLeft = json['total_left'];
    storeName = json['store_name'].toString();
    address = json['address'].toString();
    location = json['location'].toString();
    state = json['state'].toString();
    city = json['city'].toString();
    pincode = json['pincode'];
    lat = json['lat'];
    lng = json['lng'];
    locality = json['locality'].toString();
    country = json['country'].toString();
    storeManager = json['store_manager'].toString();
    storeDescr = json['store_descr'].toString();
    systemCategoryId = json['system_category_id'];
    rating = json['rating'];
    phoneNo = json['phone_no'];
    storeTypeId = json['store_type_id'];
    deliveryTime = json['delivery_time'];
    logo = json['logo'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['merchant_id'] = this.merchantId;
    data['store_id'] = this.storeId;
    data['category_id'] = this.categoryId;
    data['subcat_id'] = this.subcatId;
    data['merchant_product_id'] = this.merchantProductId;
    data['shopping_list_qty'] = this.shoppingListQty;
    data['cat_name'] = this.catName;
    data['subcat_name'] = this.subcatName;
    data['master_product_id'] = this.masterProductId;
    data['productname'] = this.productname;
    data['manufacturer'] = this.manufacturer;
    data['measure'] = this.measure;
    data['sku'] = this.sku;
    data['productdescription'] = this.productdescription;
    data['product_mrp'] = this.productMrp;
    data['selling_price'] = this.sellingPrice;
    data['product_image'] = this.productImage;
    data['total_sold'] = this.totalSold;
    data['total_left'] = this.totalLeft;
    data['store_name'] = this.storeName;
    data['address'] = this.address;
    data['location'] = this.location;
    data['state'] = this.state;
    data['city'] = this.city;
    data['pincode'] = this.pincode;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['locality'] = this.locality;
    data['country'] = this.country;
    data['store_manager'] = this.storeManager;
    data['store_descr'] = this.storeDescr;
    data['system_category_id'] = this.systemCategoryId;
    data['rating'] = this.rating;
    data['phone_no'] = this.phoneNo;
    data['store_type_id'] = this.storeTypeId;
    data['delivery_time'] = this.deliveryTime;
    data['logo'] = this.logo;
    return data;
  }
}

class ProductBanner {
  int id;
  String bannerUrl;
  double lat;
  double lng;
  String createdAt;
  String updatedAt;
  int isactive;
  String type;
  String displayConfig;
  Null ownerId;

  ProductBanner(
      {this.id,
      this.bannerUrl,
      this.lat,
      this.lng,
      this.createdAt,
      this.updatedAt,
      this.isactive,
      this.type,
      this.displayConfig,
      this.ownerId});

  ProductBanner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bannerUrl = json['banner_url'];
    lat = json['lat'];
    lng = json['lng'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isactive = json['isactive'];
    type = json['type'];
    displayConfig = json['display_config'];
    ownerId = json['owner_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['banner_url'] = this.bannerUrl;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    data['type'] = this.type;
    data['display_config'] = this.displayConfig;
    data['owner_id'] = this.ownerId;
    return data;
  }
}

class ServiceBanner {
  int id;
  String bannerUrl;
  double lat;
  double lng;
  String createdAt;
  String updatedAt;
  int isactive;
  String type;
  String displayConfig;
  Null ownerId;

  ServiceBanner(
      {this.id,
      this.bannerUrl,
      this.lat,
      this.lng,
      this.createdAt,
      this.updatedAt,
      this.isactive,
      this.type,
      this.displayConfig,
      this.ownerId});

  ServiceBanner.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    bannerUrl = json['banner_url'];
    lat = json['lat'];
    lng = json['lng'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isactive = json['isactive'];
    type = json['type'];
    displayConfig = json['display_config'];
    ownerId = json['owner_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['banner_url'] = this.bannerUrl;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    data['type'] = this.type;
    data['display_config'] = this.displayConfig;
    data['owner_id'] = this.ownerId;
    return data;
  }
}
