class ViewMyCart {
  List<ViewProduct> product;
  ViewcartTotal total;
  String status;

  ViewMyCart({this.product, this.total, this.status});

  ViewMyCart.fromJson(Map<String, dynamic> json) {
    if (json['product'] != null) {
      product = new List<ViewProduct>();
      json['product'].forEach((v) {
        product.add(new ViewProduct.fromJson(v));
      });
    }
//    total = json['total'] != null ? new ViewcartTotal.fromJson(json['total']) : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.product != null) {
      data['product'] = this.product.map((v) => v.toJson()).toList();
    }
    if (this.total != null) {
      data['total'] = this.total.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class ViewProduct {
  String productname;
  int quantity;
  String taxStatus;
  int masterproductid;
  int merchantlistid;
  int merchantid;
  String storeName;
  String storeIcon;
  String measure;
  String icon;
  int pricewithtax;
  int pricewithouttax;
  int sellingPrice;
  int productMrp;
  int tax;
  int avgTimeToDeliver;
  int totalLeft;
  int shipping;
  int minOrder;
  int offerId;
  String offerName;
  String offerDescr;
  String comboPrice;
  String offerImage;
  String offerMrp;

  ViewProduct(
      {this.productname,
      this.quantity,
      this.taxStatus,
      this.masterproductid,
      this.merchantlistid,
      this.merchantid,
      this.storeName,
      this.storeIcon,
      this.measure,
      this.icon,
      this.pricewithtax,
      this.pricewithouttax,
      this.sellingPrice,
      this.productMrp,
      this.tax,
      this.avgTimeToDeliver,
      this.totalLeft,
      this.shipping,
      this.minOrder,
      this.offerId,
      this.offerName,
      this.offerDescr,
      this.comboPrice,
      this.offerImage,
      this.offerMrp});

  ViewProduct.fromJson(Map<String, dynamic> json) {
    productname = json['productname'].toString();
    quantity = json['quantity'];
    taxStatus = json['tax_status'].toString();
    masterproductid = json['masterproductid'];
    merchantlistid = json['merchantlistid'];
    merchantid = json['merchantid'];
    storeName = json['store_name'];
    storeIcon = json['store_icon'];
    measure = json['measure'];
    icon = json['icon'];
    pricewithtax = json['pricewithtax'];
    pricewithouttax = json['pricewithouttax'];
    sellingPrice = json['selling_price'];
    productMrp = json['product_mrp'];
    tax = json['tax'];
    avgTimeToDeliver = json['avg_time_to_deliver'];
    totalLeft = json['total_left'];
    shipping = json['shipping'];
    minOrder = json['min_order'];
    offerId = json['offer_id'];
    offerName = json['offer_name'];
    offerDescr = json['offer_descr'];
    comboPrice = json['combo_price'].toString();
    offerImage = json['offer_image'];
    offerMrp = json['offer_mrp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['productname'] = this.productname;
    data['quantity'] = this.quantity;
    data['tax_status'] = this.taxStatus;
    data['masterproductid'] = this.masterproductid;
    data['merchantlistid'] = this.merchantlistid;
    data['merchantid'] = this.merchantid;
    data['store_name'] = this.storeName;
    data['store_icon'] = this.storeIcon;
    data['measure'] = this.measure;
    data['icon'] = this.icon;
    data['pricewithtax'] = this.pricewithtax;
    data['pricewithouttax'] = this.pricewithouttax;
    data['selling_price'] = this.sellingPrice;
    data['product_mrp'] = this.productMrp;
    data['tax'] = this.tax;
    data['avg_time_to_deliver'] = this.avgTimeToDeliver;
    data['total_left'] = this.totalLeft;
    data['shipping'] = this.shipping;
    data['min_order'] = this.minOrder;
    data['offer_id'] = this.offerId;
    data['offer_name'] = this.offerName;
    data['offer_descr'] = this.offerDescr;
    data['combo_price'] = this.comboPrice;
    data['offer_image'] = this.offerImage;
    data['offer_mrp'] = this.offerMrp;
    return data;
  }
}

class ViewcartTotal {
  int paytmAvail;
  int subtotal;
  int grandtotal;
  int totalTax;
  int shipping;
  int totaldue;
  int totalItems;

  ViewcartTotal(
      {this.paytmAvail,
      this.subtotal,
      this.grandtotal,
      this.totalTax,
      this.shipping,
      this.totaldue,
      this.totalItems});

  ViewcartTotal.fromJson(Map<String, int> json) {
    paytmAvail = json['paytm_avail'];
    subtotal = int.parse(json['subtotal'].toString());
    grandtotal = int.parse(json['grandtotal'].toString());
    totalTax = json['total_tax'];
    shipping = int.parse(json['shipping'].toString());
    totaldue = json['totaldue'];
    totalItems = int.parse(json['total_items'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['paytm_avail'] = this.paytmAvail;
    data['subtotal'] = this.subtotal;
    data['grandtotal'] = this.grandtotal;
    data['total_tax'] = this.totalTax;
    data['shipping'] = this.shipping;
    data['totaldue'] = this.totaldue;
    data['total_items'] = this.totalItems;
    return data;
  }
}
