class SearchProduct {
  List<SearchProductData> data;
  String message;

  SearchProduct({this.data, this.message});

  SearchProduct.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<SearchProductData>();
      json['data'].forEach((v) {
        data.add(new SearchProductData.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class SearchProductData {
  int id;
  String image;
  String storename;
  int storeId;
  int shoppingListQty;
  String address;
  String city;
  String productName;
  String measure;
  String productDescr;
  int masterProductId;
  int merchantproductList;
  int merchantid;
  int catId;
  int subcatId;
  String catName;
  String manufacturer;
  String subcatName;
  String productImage;
  double latitude;
  double longitude;
  double distance;
  String productUnit;
  String mrp;
  double discount;
  double sellingPrice;
  int totalLeft;
  String type;
  String calories;

  SearchProductData({
    this.id,
    this.image,
    this.storename,
    this.storeId,
    this.shoppingListQty,
    this.address,
    this.city,
    this.productName,
    this.measure,
      this.productDescr,
      this.masterProductId,
      this.merchantproductList,
      this.merchantid,
      this.catId,
      this.subcatId,
      this.catName,
    this.manufacturer,
    this.subcatName,
    this.productImage,
    this.latitude,
    this.longitude,
    this.distance,
    this.productUnit,
    this.mrp,
    this.discount,
    this.sellingPrice,
    this.totalLeft,
    this.type,
    this.calories,
  });

  SearchProductData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'].toString();
    storename = json['storename'].toString();
    storeId = json['store_id'];
    shoppingListQty = json['shopping_list_qty'];
    address = json['address'].toString();
    city = json['city'].toString();
    productName = json['product_name'].toString();
    measure = json['measure'].toString();
    productDescr = json['product_descr'].toString();
    masterProductId = json['master_product_id'];
    merchantproductList = json['merchantproduct_list'];
    merchantid = json['merchantid'];
    catId = json['cat_id'];
    subcatId = json['subcat_id'];
    catName = json['cat_name'].toString();
    manufacturer = json['manufacture'].toString();
    subcatName = json['subcat_name'].toString();
    productImage = json['product_image'].toString();
    latitude = json['latitude'];
    longitude = json['longitude'];
    distance = json['distance'];
    productUnit = json['product_unit'].toString();
    mrp = json['mrp'] == null ? "0" : json['mrp'].toString();
    discount = double.tryParse(json['discount'].toString());
    sellingPrice = double.tryParse(json['selling_price'].toString());
    totalLeft = json['total_left'];
    type = json['type'].toString().toLowerCase();
    calories = json['calories'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['storename'] = this.storename;
    data['store_id'] = this.storeId;
    data['shopping_list_qty'] = this.shoppingListQty;
    data['address'] = this.address;
    data['city'] = this.city;
    data['product_name'] = this.productName;
    data['measure'] = this.measure;
    data['product_descr'] = this.productDescr;
    data['master_product_id'] = this.masterProductId;
    data['merchantproduct_list'] = this.merchantproductList;
    data['merchantid'] = this.merchantid;
    data['cat_id'] = this.catId;
    data['subcat_id'] = this.subcatId;
    data['cat_name'] = this.catName;
    data['manufacture'] = this.manufacturer;
    data['subcat_name'] = this.subcatName;
    data['product_image'] = this.productImage;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['distance'] = this.distance;
    data['product_unit'] = this.productUnit;
    data['mrp'] = this.mrp;
    data['discount'] = this.discount;
    data['selling_price'] = this.sellingPrice;
    data['total_left'] = this.totalLeft;
    data['type'] = this.type;
    data['calories'] = this.calories;
    return data;
  }
}
