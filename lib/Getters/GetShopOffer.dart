class OfferByShop {
  List<ShopOfferInfo> info;
  String status;

  OfferByShop({this.info, this.status});

  OfferByShop.fromJson(Map<String, dynamic> json) {
    if (json['info'] != null) {
      info = new List<ShopOfferInfo>();
      json['info'].forEach((v) {
        info.add(new ShopOfferInfo.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.info != null) {
      data['info'] = this.info.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class ShopOfferInfo {
  String id;
  String merchantId;
  String storeName;
  String address;
  String location;
  String state;
  double lat;
  double lng;
  String locality;
  String logo;
  String storeId;
  int countProducts;
  int percentDiscount;
  int rating;
  String description;

  ShopOfferInfo(
      {this.id,
      this.merchantId,
      this.storeName,
      this.address,
      this.location,
      this.state,
      this.lat,
      this.lng,
      this.locality,
      this.logo,
      this.storeId,
      this.countProducts,
      this.percentDiscount,
      this.rating,
      this.description});

  ShopOfferInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    merchantId = json['merchant_id'].toString();
    storeName = json['store_name'].toString();
    address = json['address'].toString();
    location = json['location'].toString();
    state = json['state'].toString();
    lat = json['lat'];
    lng = json['lng'];
    locality = json['locality'].toString();
    logo = json['logo'].toString();
    storeId = json['store_id'].toString();
    countProducts = json['count_products'];
    percentDiscount = json['percent_discount'];
    rating = json['rating'];
    description = json['description'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['merchant_id'] = this.merchantId;
    data['store_name'] = this.storeName;
    data['address'] = this.address;
    data['location'] = this.location;
    data['state'] = this.state;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['locality'] = this.locality;
    data['logo'] = this.logo;
    data['store_id'] = this.storeId;
    data['count_products'] = this.countProducts;
    data['percent_discount'] = this.percentDiscount;
    data['rating'] = this.rating;
    data['description'] = this.description;
    return data;
  }
}
