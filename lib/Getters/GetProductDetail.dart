// To parse this JSON data, do
//
//     final productDetail = productDetailFromJson(jsonString);

import 'dart:convert';

ProductDetail productDetailFromJson(String str) =>
    ProductDetail.fromJson(json.decode(str));

String productDetailToJson(ProductDetail data) => json.encode(data.toJson());

class ProductDetail {
  ProductDetail({
    this.data,
    this.message,
  });

  List<ProductInfo> data;
  String message;

  factory ProductDetail.fromJson(Map<String, dynamic> json) => ProductDetail(
        data: List<ProductInfo>.from(
            json["data"].map((x) => ProductInfo.fromJson(x))),
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "message": message,
      };
}

class ProductInfo {
  ProductInfo({
    this.id,
    this.productName,
    this.skuNumber,
    this.manufacture,
    this.productDescr,
    this.masterProductId,
    this.merchantproductList,
    this.shoppingListQty,
    this.catId,
    this.subcatId,
    this.catName,
    this.subcatName,
    this.productImage,
    this.productUnit,
    this.keyIngredients,
    this.calories,
    this.carbs,
    this.protiens,
    this.fats,
    this.mrp,
    this.sellingPrice,
    this.totalLeft,
    this.totalCount,
    this.type,
  });

  int id;
  String productName;
  String skuNumber;
  String manufacture;
  String productDescr;
  int masterProductId;
  int merchantproductList;
  int shoppingListQty;
  int catId;
  int subcatId;
  String catName;
  String subcatName;
  List<ProductImage> productImage;
  String productUnit;
  String keyIngredients;
  String calories;
  String carbs;
  String protiens;
  String fats;
  int mrp;
  int sellingPrice;
  int totalLeft;
  int totalCount;
  String type;

  factory ProductInfo.fromJson(Map<String, dynamic> json) => ProductInfo(
        id: json["id"],
        productName: json["product_name"].toString(),
        skuNumber: json["sku_number"].toString(),
        manufacture: json["manufacture"].toString(),
        productDescr: json["product_descr"].toString(),
        masterProductId: json["master_product_id"],
        merchantproductList: json["merchantproduct_list"],
        shoppingListQty: json["shopping_list_qty"],
        catId: json["cat_id"],
        subcatId: json["subcat_id"],
        catName: json["cat_name"].toString(),
        subcatName: json["subcat_name"].toString(),
        productImage: List<ProductImage>.from(
            json["product_image"].map((x) => ProductImage.fromJson(x))),
        productUnit: json["product_unit"].toString(),
        keyIngredients: json["key_ingredients"].toString(),
        calories: json["calories"].toString(),
        carbs: json["carbs"].toString(),
        protiens: json["protiens"].toString(),
        fats: json["fats"].toString(),
        mrp: json["mrp"],
        sellingPrice: json["selling_price"],
        totalLeft: json["total_left"],
        totalCount: json["total_count"],
        type: json["type"].toString().toLowerCase(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "product_name": productName,
        "sku_number": skuNumber,
        "manufacture": manufacture,
        "product_descr": productDescr,
        "master_product_id": masterProductId,
        "merchantproduct_list": merchantproductList,
        "shopping_list_qty": shoppingListQty,
        "cat_id": catId,
        "subcat_id": subcatId,
        "cat_name": catName,
        "subcat_name": subcatName,
        "product_image":
            List<dynamic>.from(productImage.map((x) => x.toJson())),
    "product_unit": productUnit,
    "key_ingredients": keyIngredients,
    "calories": calories,
    "carbs": carbs,
    "protiens": protiens,
    "fats": fats,
    "mrp": mrp,
    "selling_price": sellingPrice,
    "total_left": totalLeft,
    "total_count": totalCount,
    "type": type,
  };
}

class ProductImage {
  ProductImage({
    this.imagePath,
  });

  String imagePath;

  factory ProductImage.fromJson(Map<String, dynamic> json) => ProductImage(
        imagePath: json["image_path"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "image_path": imagePath,
      };
}
