class Order {
  Myorder myorder;
  String status;

  Order({this.myorder, this.status});

  Order.fromJson(Map<String, dynamic> json) {
    myorder = json['myorder'] != "No order Placed"
        ? new Myorder.fromJson(json['myorder'])
        : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.myorder != null) {
      data['myorder'] = this.myorder.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class Myorder {
  List<Recent> recent;

//  List<Past> past;

  Myorder({this.recent});

  Myorder.fromJson(Map<String, dynamic> json) {
    if (json['recent'] != null) {
      recent = new List<Recent>();
      json['recent'].forEach((v) {
        recent.add(new Recent.fromJson(v));
      });
    }
//    if (json['past'] != null) {
//      past = new List<Past>();
//      json['past'].forEach((v) {
//        past.add(new Past.fromJson(v));
//      });
//    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.recent != null) {
      data['recent'] = this.recent.map((v) => v.toJson()).toList();
    }
//    if (this.past != null) {
//      data['past'] = this.past.map((v) => v.toJson()).toList();
//    }
    return data;
  }
}

class Recent {
  String storeName;
  String avgTimeToDeliver;
  String id;
  String invoiceNumber;
  int merchantId;
  String paymentStatus;
  int storeId;
  String invoiceDate;
  int shipping;
  double subtotal;
  double grandtotal;
  String orderStatus;
  int phoneno;
  String merchantname;
  String storeLogo;
  String currentStatus;
  int totalItems;
  String orderDate;
  String completedDate;
  String paymentMode;
  int rating;

  Recent(
      {this.storeName,
      this.avgTimeToDeliver,
      this.id,
      this.invoiceNumber,
      this.merchantId,
      this.paymentStatus,
      this.storeId,
      this.invoiceDate,
      this.shipping,
      this.subtotal,
      this.grandtotal,
      this.orderStatus,
      this.phoneno,
      this.merchantname,
      this.storeLogo,
      this.currentStatus,
      this.totalItems,
      this.orderDate,
      this.completedDate,
      this.paymentMode,
      this.rating});

  Recent.fromJson(Map<String, dynamic> json) {
    storeName = json['store_name'];
    avgTimeToDeliver = json['avg_time_to_deliver'].toString();
    id = json['id'].toString();
    invoiceNumber = json['invoiceNumber'];
    merchantId = json['merchant_id'];
    paymentStatus = json['paymentStatus'];
    storeId = json['store_id'];
    invoiceDate = json['invoiceDate'];
    shipping = json['shipping'];
    subtotal = json['subtotal'].toDouble();
    grandtotal = json['grandtotal'].toDouble();
    orderStatus = json['order_status'];
    phoneno = json['phoneno'];
    merchantname = json['merchantname'];
    storeLogo = json['store_logo'];
    currentStatus = json['current_status'];
    totalItems = json['total_items'];
    orderDate = json['order_date'];
    completedDate = json['completed_date'];
    paymentMode = json['payment_mode'];
    rating = json['rating'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['store_name'] = this.storeName;
    data['avg_time_to_deliver'] = this.avgTimeToDeliver;
    data['id'] = this.id;
    data['invoiceNumber'] = this.invoiceNumber;
    data['merchant_id'] = this.merchantId;
    data['paymentStatus'] = this.paymentStatus;
    data['store_id'] = this.storeId;
    data['invoiceDate'] = this.invoiceDate;
    data['shipping'] = this.shipping;
    data['subtotal'] = this.subtotal;
    data['grandtotal'] = this.grandtotal;
    data['order_status'] = this.orderStatus;
    data['phoneno'] = this.phoneno;
    data['merchantname'] = this.merchantname;
    data['store_logo'] = this.storeLogo;
    data['current_status'] = this.currentStatus;
    data['total_items'] = this.totalItems;
    data['order_date'] = this.orderDate;
    data['completed_date'] = this.completedDate;
    data['payment_mode'] = this.paymentMode;
    data['rating'] = this.rating;
    return data;
  }
}

//class Past {
//  String storeName;
//  String avgTimeToDeliver;
//  String id;
//  String invoiceNumber;
//  int merchantId;
//  String paymentStatus;
//  String invoiceDate;
//  int shipping;
//  int subtotal;
//  double grandtotal;
//  String orderStatus;
//  int phoneno;
//  String merchantname;
//  int rating;
//
//  Past(
//      {this.storeName,
//        this.avgTimeToDeliver,
//        this.id,
//        this.invoiceNumber,
//        this.merchantId,
//        this.paymentStatus,
//        this.invoiceDate,
//        this.shipping,
//        this.subtotal,
//        this.grandtotal,
//        this.orderStatus,
//        this.phoneno,
//        this.merchantname,
//        this.rating});
//
//  Past.fromJson(Map<String, dynamic> json) {
//    storeName = json['store_name'];
//    avgTimeToDeliver = json['avg_time_to_deliver'].toString();
//    id = json['id'].toString();
//    invoiceNumber = json['invoiceNumber'];
//    merchantId = json['merchant_id'];
//    paymentStatus = json['paymentStatus'];
//    invoiceDate = json['invoiceDate'];
//    shipping = json['shipping'];
//    subtotal = json['subtotal'].toInt();
//    grandtotal = json['grandtotal'].toDouble();
//    orderStatus = json['order_status'];
//    phoneno = json['phoneno'];
//    merchantname = json['merchantname'];
//    rating = json['rating'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['store_name'] = this.storeName;
//    data['avg_time_to_deliver'] = this.avgTimeToDeliver;
//    data['id'] = this.id;
//    data['invoiceNumber'] = this.invoiceNumber;
//    data['merchant_id'] = this.merchantId;
//    data['paymentStatus'] = this.paymentStatus;
//    data['invoiceDate'] = this.invoiceDate;
//    data['shipping'] = this.shipping;
//    data['subtotal'] = this.subtotal;
//    data['grandtotal'] = this.grandtotal;
//    data['order_status'] = this.orderStatus;
//    data['phoneno'] = this.phoneno;
//    data['merchantname'] = this.merchantname;
//    data['rating'] = this.rating;
//    return data;
//  }
//}
