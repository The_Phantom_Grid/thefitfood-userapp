class MyOrderDetails {
  List<Orderdetails> orderdetails;
  String status;

  MyOrderDetails({this.orderdetails, this.status});

  MyOrderDetails.fromJson(Map<String, dynamic> json) {
    if (json['orderdetails'] != null) {
      orderdetails = new List<Orderdetails>();
      json['orderdetails'].forEach((v) {
        orderdetails.add(new Orderdetails.fromJson(v));
      });
    }
    status = json['status'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderdetails != null) {
      data['orderdetails'] = this.orderdetails.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class Orderdetails {
  String deliveryAddress;
  double totalDue;
  String deliveryType;
  String ratings;
  String paymentStatus;
  String comments;
  int customerRatings;
  String customerRatingComments;
  String description;
  String orderStatus;
  String totalTimeToDeliver;
  double prevBalance;
  String expectedDeliveryDate;
  String paymentMode;
  String orderDate;
  String orderInstructions;
  int orderId;
  String promocodeApplied;
  int promocodeAmount;
  int customerId;
  int merchantId;
  int storeId;
  String storeImage;
  String assignedDeliveryBoyId;
  String assignedDeliveryBoyName;
  String assignedDeliveryBoyNumber;
  String totalTimeToDeliverInMinutes;
  String orderMadeThrough;
  String invoiceNumber;
  double subtotal;
  double shipping;
  double grandtotal;
  double totalpaid;
  String totalrefund;
  String discountCode;
  double discount;
  double discountPer;
  String discountFlag;
  double discountAmount;
  String totalTax;
  List<OrderedItems> orderedItems;

  Orderdetails(
      {this.deliveryAddress,
      this.totalDue,
      this.deliveryType,
      this.ratings,
      this.paymentStatus,
      this.comments,
      this.customerRatings,
      this.customerRatingComments,
      this.description,
      this.orderStatus,
      this.totalTimeToDeliver,
      this.prevBalance,
      this.expectedDeliveryDate,
      this.paymentMode,
      this.orderDate,
      this.orderInstructions,
      this.orderId,
      this.promocodeApplied,
      this.promocodeAmount,
      this.customerId,
      this.merchantId,
      this.storeId,
      this.storeImage,
      this.assignedDeliveryBoyId,
      this.assignedDeliveryBoyName,
      this.assignedDeliveryBoyNumber,
      this.totalTimeToDeliverInMinutes,
      this.orderMadeThrough,
      this.invoiceNumber,
      this.subtotal,
      this.shipping,
      this.grandtotal,
      this.totalpaid,
      this.totalrefund,
      this.discountCode,
      this.discount,
      this.discountPer,
      this.discountFlag,
      this.discountAmount,
      this.totalTax,
      this.orderedItems});

  Orderdetails.fromJson(Map<String, dynamic> json) {
    deliveryAddress = json['delivery_address'];
    totalDue = double.tryParse(json['totalDue'].toString());
    deliveryType = json['delivery_type'];
    ratings = json['ratings'].toString();
    paymentStatus = json['payment_status'];
    comments = json['comments'];
    customerRatings = json['customer_ratings'];
    customerRatingComments = json['customer_rating_comments'].toString();
    description = json['description'].toString();
    orderStatus = json['order_status'];
    totalTimeToDeliver = json['total_time_to_deliver'];
    prevBalance = json['prev_balance'].toDouble();
    expectedDeliveryDate = json['expected_delivery_date'];
    paymentMode = json['payment_mode'];
    orderDate = json['order_date'];
    orderInstructions = json['order_instructions'].toString();
    orderId = json['order_id'];
    promocodeApplied = json['promocode_applied'].toString();
    promocodeAmount = json['promocode_amount'];
    customerId = json['customer_id'];
    merchantId = json['merchant_id'];
    storeId = json['store_id'];
    storeImage = json['store_image'];
    assignedDeliveryBoyId = json['assigned_delivery_boy_id'].toString();
    assignedDeliveryBoyName = json['assigned_delivery_boy_name'].toString();
    assignedDeliveryBoyNumber = json['assigned_delivery_boy_number'].toString();
    totalTimeToDeliverInMinutes = json['total_time_to_deliver_in_minutes'];
    orderMadeThrough = json['order_made_through'];
    invoiceNumber = json['invoiceNumber'];
    subtotal = json['subtotal'].toDouble();
    shipping = double.tryParse(json['shipping'].toString());
    grandtotal = double.tryParse(json['grandtotal'].toString());
    totalpaid = double.tryParse(json['totalpaid'].toString());
    totalrefund = json['totalrefund'].toString();
    discountCode = json['discount_code'].toString();
    discount = double.tryParse(json['discount'].toString());
    discountPer = double.tryParse(json['discount_per'].toString());
    discountFlag = json['discount_flag'].toString();
    discountAmount = double.tryParse(json['discount_amount'].toString());
    totalTax = json['total_tax'].toString();
    if (json['ordered_items'] != null) {
      orderedItems = new List<OrderedItems>();
      json['ordered_items'].forEach((v) {
        orderedItems.add(new OrderedItems.fromJson(v).toString().isEmpty
            ? null
            : new OrderedItems.fromJson(v));
      });
//      orderedItems = json['ordered_items'].map<OrderedItems>((j) => OrderedItems.fromJson(j).toList());
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['delivery_address'] = this.deliveryAddress;
    data['total_due'] = this.totalDue;
    data['delivery_type'] = this.deliveryType;
    data['ratings'] = this.ratings;
    data['payment_status'] = this.paymentStatus;
    data['comments'] = this.comments;
    data['customer_ratings'] = this.customerRatings;
    data['customer_rating_comments'] = this.customerRatingComments;
    data['description'] = this.description;
    data['order_status'] = this.orderStatus;
    data['total_time_to_deliver'] = this.totalTimeToDeliver;
    data['prev_balance'] = this.prevBalance;
    data['expected_delivery_date'] = this.expectedDeliveryDate;
    data['payment_mode'] = this.paymentMode;
    data['order_date'] = this.orderDate;
    data['order_instructions'] = this.orderInstructions;
    data['order_id'] = this.orderId;
    data['promocode_applied'] = this.promocodeApplied;
    data['promocode_amount'] = this.promocodeAmount;
    data['customer_id'] = this.customerId;
    data['merchant_id'] = this.merchantId;
    data['store_id'] = this.storeId;
    data['store_image'] = this.storeImage;
    data['assigned_delivery_boy_id'] = this.assignedDeliveryBoyId;
    data['assigned_delivery_boy_name'] = this.assignedDeliveryBoyName;
    data['assigned_delivery_boy_number'] = this.assignedDeliveryBoyNumber;
    data['total_time_to_deliver_in_minutes'] = this.totalTimeToDeliverInMinutes;
    data['order_made_through'] = this.orderMadeThrough;
    data['invoiceNumber'] = this.invoiceNumber;
    data['subtotal'] = this.subtotal;
    data['shipping'] = this.shipping;
    data['grandtotal'] = this.grandtotal;
    data['totalpaid'] = this.totalpaid;
    data['totalrefund'] = this.totalrefund;
    data['discount_code'] = this.discountCode;
    data['discount'] = this.discount;
    data['discount_per'] = this.discountPer;
    data['discount_flag'] = this.discountFlag;
    data['discount_amount'] = this.discountAmount;
    data['total_tax'] = this.totalTax;
    if (this.orderedItems != null) {
      data['ordered_items'] = this.orderedItems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderedItems {
  String productname;
  int quantity;
  String taxStatus;
  String measure;
  double pricewithtax;
  int pricewithouttax;
  double sellingPrice;
  double productMrp;
  String tax;
  String avgTimeToDeliver;
  int shipping;
  String productImage;
  int offerId;
  String offerName;
  String offerDescr;
  String offerPrice;
  String offerImage;

  OrderedItems(
      {this.productname,
      this.quantity,
      this.taxStatus,
      this.measure,
      this.pricewithtax,
      this.pricewithouttax,
      this.sellingPrice,
      this.productMrp,
      this.tax,
      this.avgTimeToDeliver,
      this.shipping,
      this.productImage,
      this.offerId,
      this.offerName,
      this.offerDescr,
      this.offerPrice,
      this.offerImage});

  OrderedItems.fromJson(Map<String, dynamic> json) {
    productname = json['productname'];
    quantity = json['quantity'];
    taxStatus = json['tax_status'];
    measure = json['measure'];
    pricewithtax = json['pricewithtax'].toDouble();
    pricewithouttax = json['pricewithouttax'].toInt();
    sellingPrice = json['selling_price'].toDouble();
    productMrp = json['product_mrp'].toDouble();
    tax = json['tax'].toString();
    avgTimeToDeliver = json['avg_time_to_deliver'].toString();
    shipping = json['shipping'];
    productImage = json['product_image'];
    offerId = json['offer_id'];
    offerName = json['offer_name'];
    offerDescr = json['offer_descr'];
    offerPrice = json['offer_price'].toString();
    offerImage = json['offer_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['productname'] = this.productname;
    data['quantity'] = this.quantity;
    data['tax_status'] = this.taxStatus;
    data['measure'] = this.measure;
    data['pricewithtax'] = this.pricewithtax;
    data['pricewithouttax'] = this.pricewithouttax;
    data['selling_price'] = this.sellingPrice;
    data['product_mrp'] = this.productMrp;
    data['tax'] = this.tax;
    data['avg_time_to_deliver'] = this.avgTimeToDeliver;
    data['shipping'] = this.shipping;
    data['product_image'] = this.productImage;
    data['offer_id'] = this.offerId;
    data['offer_name'] = this.offerName;
    data['offer_descr'] = this.offerDescr;
    data['offer_price'] = this.offerPrice;
    data['offer_image'] = this.offerImage;
    return data;
  }
}
