class ComboOffer {
  List<ComboInfo> info;
  String status;

  ComboOffer({this.info, this.status});

  ComboOffer.fromJson(Map<String, dynamic> json) {
    if (json['info'] != null) {
      info = new List<ComboInfo>();
      json['info'].forEach((v) {
        info.add(new ComboInfo.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.info != null) {
      data['info'] = this.info.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class ComboInfo {
  int id;
  int shoppingListQty;
  String collectionName;
  String descr;
  int merchantId;
  int storeId;
  String validFrom;
  String validTo;
  int comboPrice;
  int inventory;
  String storeName;
  String address;
  String location;
  String state;
  String city;
  String pincode;
  double lat;
  double lng;
  String locality;
  String country;
  String logo;
  List<CollectionProducts> collectionProducts;
  List<CollectionImages> collectionImages;

  ComboInfo(
      {this.id,
      this.shoppingListQty,
      this.collectionName,
      this.descr,
      this.merchantId,
      this.storeId,
      this.validFrom,
      this.validTo,
      this.comboPrice,
      this.inventory,
      this.storeName,
      this.address,
      this.location,
      this.state,
      this.city,
      this.pincode,
      this.lat,
      this.lng,
      this.locality,
      this.country,
      this.logo,
      this.collectionProducts,
      this.collectionImages});

  ComboInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    shoppingListQty = json['shopping_list_qty'];
    collectionName = json['collection_name'].toString();
    descr = json['descr'].toString();
    merchantId = json['merchant_id'];
    storeId = json['store_id'];
    validFrom = json['valid_from'];
    validTo = json['valid_to'];
    comboPrice = json['combo_price'];
    inventory = json['inventory'];
    storeName = json['store_name'].toString();
    address = json['address'].toString();
    location = json['location'].toString();
    state = json['state'].toString();
    city = json['city'].toString();
    pincode = json['pincode'].toString();
    lat = json['lat'];
    lng = json['lng'];
    locality = json['locality'].toString();
    country = json['country'].toString();
    logo = json['logo'].toString();
    if (json['collection_products'] != null) {
      collectionProducts = new List<CollectionProducts>();
      json['collection_products'].forEach((v) {
        collectionProducts.add(new CollectionProducts.fromJson(v));
      });
    }
    if (json['collection_images'] != null) {
      collectionImages = new List<CollectionImages>();
      json['collection_images'].forEach((v) {
        collectionImages.add(new CollectionImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['shopping_list_qty'] = this.shoppingListQty;
    data['collection_name'] = this.collectionName;
    data['descr'] = this.descr;
    data['merchant_id'] = this.merchantId;
    data['store_id'] = this.storeId;
    data['valid_from'] = this.validFrom;
    data['valid_to'] = this.validTo;
    data['combo_price'] = this.comboPrice;
    data['inventory'] = this.inventory;
    data['store_name'] = this.storeName;
    data['address'] = this.address;
    data['location'] = this.location;
    data['state'] = this.state;
    data['city'] = this.city;
    data['pincode'] = this.pincode;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['locality'] = this.locality;
    data['country'] = this.country;
    data['logo'] = this.logo;
    if (this.collectionProducts != null) {
      data['collection_products'] =
          this.collectionProducts.map((v) => v.toJson()).toList();
    }
    if (this.collectionImages != null) {
      data['collection_images'] =
          this.collectionImages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CollectionProducts {
  int id;
  int collectionId;
  int masterProductId;
  String productName;
  String productDescr;
  String productImage;
  int merchantProductId;
  int merchantId;
  int storeId;
  int catId;
  int subcatId;
  int productPrice;
  String createdId;
  String updatedAt;
  int isactive;
  String actualProductPrice;

  CollectionProducts(
      {this.id,
      this.collectionId,
      this.masterProductId,
      this.productName,
      this.productDescr,
      this.productImage,
      this.merchantProductId,
      this.merchantId,
      this.storeId,
      this.catId,
      this.subcatId,
      this.productPrice,
      this.createdId,
      this.updatedAt,
      this.isactive,
      this.actualProductPrice});

  CollectionProducts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    collectionId = json['collection_id'];
    masterProductId = json['master_product_id'];
    productName = json['product_name'];
    productDescr = json['product_descr'];
    productImage = json['product_image'];
    merchantProductId = json['merchant_product_id'];
    merchantId = json['merchant_id'];
    storeId = json['store_id'];
    catId = json['cat_id'];
    subcatId = json['subcat_id'];
    productPrice = json['product_price'];
    createdId = json['created_id'].toString();
    updatedAt = json['updated_at'].toString();
    isactive = json['isactive'];
    actualProductPrice = json['actual_product_price'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['collection_id'] = this.collectionId;
    data['master_product_id'] = this.masterProductId;
    data['product_name'] = this.productName;
    data['product_descr'] = this.productDescr;
    data['product_image'] = this.productImage;
    data['merchant_product_id'] = this.merchantProductId;
    data['merchant_id'] = this.merchantId;
    data['store_id'] = this.storeId;
    data['cat_id'] = this.catId;
    data['subcat_id'] = this.subcatId;
    data['product_price'] = this.productPrice;
    data['created_id'] = this.createdId;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    data['actual_product_price'] = this.actualProductPrice;
    return data;
  }
}

class CollectionImages {
  int id;
  int collectionId;
  String imageUrl;
  String createdAt;
  String updatedAt;
  int isactive;

  CollectionImages(
      {this.id,
      this.collectionId,
      this.imageUrl,
      this.createdAt,
      this.updatedAt,
      this.isactive});

  CollectionImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    collectionId = json['collection_id'];
    imageUrl = json['image_url'];
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    isactive = json['isactive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['collection_id'] = this.collectionId;
    data['image_url'] = this.imageUrl;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    return data;
  }
}
