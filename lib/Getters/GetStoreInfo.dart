// To parse this JSON data, do
//
//     final storeInfo = storeInfoFromJson(jsonString);

import 'dart:convert';

StoreInfo storeInfoFromJson(String str) => StoreInfo.fromJson(json.decode(str));

String storeInfoToJson(StoreInfo data) => json.encode(data.toJson());

class StoreInfo {
  StoreInfo({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory StoreInfo.fromJson(Map<String, dynamic> json) => StoreInfo(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.storeId,
    this.merchantId,
    this.storeName,
    this.mobile,
    this.address,
    this.storeDescription,
    this.location,
    this.imageUrl,
    this.storeTypeIcon,
    this.storeType,
    this.logo,
    this.lat,
    this.lng,
    this.bestSeller,
    this.promoCode,
    this.productCategories,
  });

  int storeId;
  int merchantId;
  String storeName;
  String mobile;
  String address;
  String storeDescription;
  String location;
  String imageUrl;
  String storeTypeIcon;
  String storeType;
  String logo;
  String lat;
  String lng;
  BestSeller bestSeller;
  PromoCode promoCode;
  List<ProductCategory> productCategories;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        storeId: json["store_id"],
        merchantId: json["merchant_id"],
        storeName: json["store_name"].toString(),
        mobile: json["mobile"].toString(),
        address: json["address"].toString(),
        storeDescription: json["store_description"].toString(),
        location: json["location"].toString(),
        imageUrl: json["image_url"].toString(),
        storeTypeIcon: json["store_type_icon"].toString(),
        storeType: json["store_type"].toString(),
        logo: json["logo"].toString(),
        lat: json["lat"].toString(),
        lng: json["lng"].toString(),
        bestSeller: BestSeller.fromJson(json["best_seller"]),
        promoCode: PromoCode.fromJson(json["promo_code"]),
        productCategories: List<ProductCategory>.from(
            json["product_categories"].map((x) => ProductCategory.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "store_id": storeId,
        "merchant_id": merchantId,
        "store_name": storeName,
        "mobile": mobile,
        "address": address,
        "store_description": storeDescription,
        "location": location,
        "image_url": imageUrl,
        "store_type_icon": storeTypeIcon,
        "store_type": storeType,
        "logo": logo,
        "lat": lat,
        "lng": lng,
        "best_seller": bestSeller.toJson(),
        "promo_code": promoCode.toJson(),
        "product_categories":
            List<dynamic>.from(productCategories.map((x) => x.toJson())),
      };
}

class BestSeller {
  BestSeller({
    this.id,
    this.name,
    this.image,
    this.price,
    this.calories,
    this.category,
  });

  int id;
  String name;
  String image;
  String price;
  String calories;
  String category;

  factory BestSeller.fromJson(Map<String, dynamic> json) => BestSeller(
        id: json["id"],
        name: json["name"].toString(),
        image: json["image"].toString(),
        price: json["price"].toString(),
        calories: json["calories"].toString(),
        category: json["category"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "image": image,
        "price": price,
        "calories": calories,
        "category": category,
      };
}

class PromoCode {
  PromoCode({
    this.id,
    this.name,
    this.description,
    this.discount,
  });

  int id;
  String name;
  String description;
  String discount;

  factory PromoCode.fromJson(Map<String, dynamic> json) => PromoCode(
        id: json["id"],
        name: json["name"].toString(),
        description: json["description"].toString(),
        discount: json["discount"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "description": description,
        "discount": discount,
      };
}

class ProductCategory {
  ProductCategory({
    this.catId,
    this.catName,
    this.catImage,
  });

  int catId;
  String catName;
  String catImage;

  factory ProductCategory.fromJson(Map<String, dynamic> json) =>
      ProductCategory(
        catId: json["category_id"],
        catName: json["category_name"].toString(),
        catImage: json["icons"].toString(),
      );

  Map<String, dynamic> toJson() =>
      {
        "category_id": catId,
        "category_name": catName,
        "icons": catImage,
      };
}
