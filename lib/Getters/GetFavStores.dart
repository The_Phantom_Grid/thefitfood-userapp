class FavStores {
  List<FavInfo> info;
  String status;
  String message;

  FavStores({this.info, this.status, this.message});

  FavStores.fromJson(Map<String, dynamic> json) {
    if (json['info'] != null) {
      info = new List<FavInfo>();
      json['info'].forEach((v) {
        info.add(new FavInfo.fromJson(v));
      });
    }
    status = json['status'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.info != null) {
      data['info'] = this.info.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    return data;
  }
}

class FavInfo {
  String storeId;
  String storeOpenTime;
  String storeCloseTime;
  String merchantId;
  String storeLogo;
  String imageUrl;
  String storeName;
  String storeType;
  String mobile;
  String storeTypeIcon;
  String address;
  String location;
  int order_enabled;
  String storeDescr;
  String minOrder;
  double lat;
  double lng;
  int rating;
  double distance;
  int favouriteShop;
  List<CategoryidArrayFav> categoryidArray;

  FavInfo(
      {this.storeId,
      this.storeOpenTime,
      this.storeCloseTime,
      this.merchantId,
      this.storeLogo,
      this.imageUrl,
      this.storeName,
      this.storeType,
      this.mobile,
      this.storeTypeIcon,
      this.address,
      this.location,
      this.order_enabled,
      this.storeDescr,
      this.minOrder,
      this.lat,
      this.lng,
      this.rating,
      this.distance,
      this.favouriteShop,
      this.categoryidArray});

  FavInfo.fromJson(Map<String, dynamic> json) {
    storeId = json['store_id'].toString();
    storeOpenTime = json['store_open_time'].toString();
    storeCloseTime = json['store_close_time'].toString();
    merchantId = json['merchant_id'].toString();
    storeLogo = json['store_logo'].toString();
    imageUrl = json['image_url'].toString();
    storeName = json['store_name'].toString();
    storeType = json['store_type'].toString();
    mobile = json['mobile'].toString();
    storeTypeIcon = json['store_type_icon'].toString();
    address = json['address'].toString();
    location = json['location'].toString();
    order_enabled = json['order_enabled'];
    storeDescr = json['store_descr'].toString();
    minOrder = json['min_order'].toString();
    lat = json['lat'];
    lng = json['lng'];
    rating = json['rating'];
    distance = json['distance'];
    favouriteShop = json['favourite_shop'];
    if (json['categoryid_array'] != null) {
      categoryidArray = new List<CategoryidArrayFav>();
      json['categoryid_array'].forEach((v) {
        categoryidArray.add(new CategoryidArrayFav.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['store_id'] = this.storeId;
    data['store_open_time'] = this.storeOpenTime;
    data['store_close_time'] = this.storeCloseTime;
    data['merchant_id'] = this.merchantId;
    data['store_logo'] = this.storeLogo;
    data['image_url'] = this.imageUrl;
    data['store_name'] = this.storeName;
    data['store_type'] = this.storeType;
    data['mobile'] = this.mobile;
    data['store_type_icon'] = this.storeTypeIcon;
    data['address'] = this.address;
    data['location'] = this.location;
    data['order_enabled'] = this.order_enabled;
    data['store_descr'] = this.storeDescr;
    data['min_order'] = this.minOrder;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['rating'] = this.rating;
    data['distance'] = this.distance;
    data['favourite_shop'] = this.favouriteShop;
    if (this.categoryidArray != null) {
      data['categoryid_array'] =
          this.categoryidArray.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CategoryidArrayFav {
  int categoryId;
  String categoryName;

  CategoryidArrayFav({this.categoryId, this.categoryName});

  CategoryidArrayFav.fromJson(Map<String, dynamic> json) {
    categoryId = json['category_id'];
    categoryName = json['category_name'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_id'] = this.categoryId;
    data['category_name'] = this.categoryName;
    return data;
  }
}
