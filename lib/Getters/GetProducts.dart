class ProductList {
  List<DataP> data;
  String message;

  ProductList({this.data, this.message});

  ProductList.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<DataP>();
      json['data'].forEach((v) {
        data.add(new DataP.fromJson(v));
      });
    }
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    return data;
  }
}

class DataP {
  int id;
  String image;
  String storename;
  String address;
  String city;
  String productName;
  String skuNumber;
  String manufacture;
  String productDescr;
  int masterProductId;
  int merchantproductList;
  int shoppingListQty;
  int catId;
  int subcatId;
  String catName;
  String subcatName;
  String productImage;
  double latitude;
  double longitude;
  String productUnit;
  double mrp;
  double sellingPrice;
  int totalLeft;
  int totalCount;
  String type;
  String calories;

  DataP({
    this.id,
    this.image,
    this.storename,
    this.address,
    this.city,
    this.productName,
    this.skuNumber,
    this.manufacture,
    this.productDescr,
      this.masterProductId,
      this.merchantproductList,
      this.shoppingListQty,
      this.catId,
    this.subcatId,
    this.catName,
    this.subcatName,
    this.productImage,
    this.latitude,
    this.longitude,
    this.productUnit,
    this.mrp,
    this.sellingPrice,
    this.totalLeft,
    this.totalCount,
    this.type,
    this.calories,
  });

  DataP.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'].toString();
    storename = json['storename'].toString();
    address = json['address'].toString();
    city = json['city'].toString();
    productName = json['product_name'].toString();
    skuNumber = json['sku_number'].toString();
    manufacture = json['manufacture'].toString();
    productDescr = json['product_descr'].toString();
    masterProductId = json['master_product_id'];
    merchantproductList = json['merchantproduct_list'];
    shoppingListQty = json['shopping_list_qty'];
    catId = json['cat_id'];
    subcatId = json['subcat_id'];
    catName = json['cat_name'].toString();
    subcatName = json['subcat_name'].toString();
    productImage = json['product_image'].toString();
    latitude = json['latitude'];
    longitude = json['longitude'];
    productUnit = json['product_unit'].toString();
    mrp = json['mrp'].toDouble();
    sellingPrice = json['selling_price'].toDouble();
    totalLeft = json['total_left'];
    totalCount = json['total_count'];
    type = json['type'].toString().toLowerCase();
    calories = json['calories'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['storename'] = this.storename;
    data['address'] = this.address;
    data['city'] = this.city;
    data['product_name'] = this.productName;
    data['sku_number'] = this.skuNumber;
    data['manufacture'] = this.manufacture;
    data['product_descr'] = this.productDescr;
    data['master_product_id'] = this.masterProductId;
    data['merchantproduct_list'] = this.merchantproductList;
    data['shopping_list_qty'] = this.shoppingListQty;
    data['cat_id'] = this.catId;
    data['subcat_id'] = this.subcatId;
    data['cat_name'] = this.catName;
    data['subcat_name'] = this.subcatName;
    data['product_image'] = this.productImage;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['product_unit'] = this.productUnit;
    data['mrp'] = this.mrp;
    data['selling_price'] = this.sellingPrice;
    data['total_left'] = this.totalLeft;
    data['total_count'] = this.totalCount;
    data['type'] = this.type;
    data['calories'] = this.calories;
    return data;
  }
}
