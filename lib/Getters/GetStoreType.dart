class StoreType {
  List<StoreTypeData> data;
  String status;

  StoreType({this.data, this.status});

  StoreType.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<StoreTypeData>();
      json['data'].forEach((v) {
        data.add(new StoreTypeData.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class StoreTypeData {
  String id;
  String catName;
  String icons;

  StoreTypeData({this.id, this.catName, this.icons});

  StoreTypeData.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    catName = json['cat_name'].toString();
    icons = json['icons'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['cat_name'] = this.catName;
    data['icons'] = this.icons;
    return data;
  }
}
