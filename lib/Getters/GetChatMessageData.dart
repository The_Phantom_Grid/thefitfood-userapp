//class MessageData {
//  MID mID;
//
//  MessageData({this.mID});
//
//  MessageData.fromJson(Map<String, String> json) {
//    mID = json['MID'] != null ? new MID.fromJson(json['MID']) : null;
//  }
//
//  Map<String, String> toJson() {
//    final Map<String, String> data = new Map<String, String>();
//    if (this.mID != null) {
//      data['MID'] = this.mID.toJson();
//    }
//    return data;
//  }
//}
//
//class MID {
//  MsgData messageData;
//
//  MID({this.messageData});
//
//  MID.fromJson(Map<String, dynamic> json) {
//    messageData = json['messageData'] != null
//        ? new MsgData.fromJson(json['messageData'])
//        : null;
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.messageData != null) {
//      data['messageData'] = this.messageData.toJson();
//    }
//    return data;
//  }
//}
//
//class MsgData {
//  String address;
//  String chatDate;
//  String storeName;
//  String mobile;
//  String orderId;
//  String subTotal;
//  String grandTotalAmount;
//  String imageUrl;
//  int storeId;
//  String user;
//  String msgCounter;
//  String chatTime;
//  String deliveryCharges;
//  String userUid;
//  String merchantId;
//  String totalItem;
//  String storeLogo;
//  String customerOrderId;
//  String message;
//
//  MsgData(
//      {this.address,
//        this.chatDate,
//        this.storeName,
//        this.mobile,
//        this.orderId,
//        this.subTotal,
//        this.grandTotalAmount,
//        this.imageUrl,
//        this.storeId,
//        this.user,
//        this.msgCounter,
//        this.chatTime,
//        this.deliveryCharges,
//        this.userUid,
//        this.merchantId,
//        this.totalItem,
//        this.storeLogo,
//        this.customerOrderId,
//        this.message});
//
//  MsgData.fromJson(Map<String, dynamic> json) {
//    address = json['address'];
//    chatDate = json['chatDate'];
//    storeName = json['storeName'];
//    mobile = json['mobile'];
//    orderId = json['orderId'];
//    subTotal = json['subTotal'];
//    grandTotalAmount = json['grandTotalAmount'];
//    imageUrl = json['imageUrl'];
//    storeId = json['storeId'];
//    user = json['user'];
//    msgCounter = json['msgCounter'];
//    chatTime = json['chatTime'];
//    deliveryCharges = json['deliveryCharges'];
//    userUid = json['userUid'];
//    merchantId = json['merchantId'];
//    totalItem = json['totalItem'];
//    storeLogo = json['storeLogo'];
//    customerOrderId = json['customerOrderId'];
//    message = json['message'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['address'] = this.address;
//    data['chatDate'] = this.chatDate;
//    data['storeName'] = this.storeName;
//    data['mobile'] = this.mobile;
//    data['orderId'] = this.orderId;
//    data['subTotal'] = this.subTotal;
//    data['grandTotalAmount'] = this.grandTotalAmount;
//    data['imageUrl'] = this.imageUrl;
//    data['storeId'] = this.storeId;
//    data['user'] = this.user;
//    data['msgCounter'] = this.msgCounter;
//    data['chatTime'] = this.chatTime;
//    data['deliveryCharges'] = this.deliveryCharges;
//    data['userUid'] = this.userUid;
//    data['merchantId'] = this.merchantId;
//    data['totalItem'] = this.totalItem;
//    data['storeLogo'] = this.storeLogo;
//    data['customerOrderId'] = this.customerOrderId;
//    data['message'] = this.message;
//    return data;
//  }
//}
