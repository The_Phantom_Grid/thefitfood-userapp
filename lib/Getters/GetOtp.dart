class OtpSend {
  String message;
  String type;
  String userid;

  OtpSend({this.message, this.type, this.userid});

  OtpSend.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    type = json['type'];
    userid = json['userid'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['type'] = this.type;
    data['userid'] = this.userid;
    return data;
  }
}
