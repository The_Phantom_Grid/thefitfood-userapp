class Message {
  MessageData messageData;

  Message({this.messageData});

  Message.fromJson(Map<String, dynamic> json) {
    messageData = json['messageData'] != null
        ? new MessageData.fromJson(json['messageData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.messageData != null) {
      data['messageData'] = this.messageData.toJson();
    }
    return data;
  }
}

class MessageData {
  String userUid;
  String grandTotalAmount;
  String chatTime;
  String message;
  int storeId;
  String merchantId;
  String mobile;
  String totalItem;
  String imageUrl;
  String deliveryCharges;
  String msgCounter;
  String address;
  String chatDate;
  String orderId;
  String subTotal;
  String storeLogo;
  String storeName;
  String customerOrderId;
  String user;

  MessageData(
      {this.userUid,
      this.grandTotalAmount,
      this.chatTime,
      this.message,
      this.storeId,
      this.merchantId,
      this.mobile,
      this.totalItem,
      this.imageUrl,
      this.deliveryCharges,
      this.msgCounter,
      this.address,
      this.chatDate,
      this.orderId,
      this.subTotal,
      this.storeLogo,
      this.storeName,
      this.customerOrderId,
      this.user});

  MessageData.fromJson(Map<String, dynamic> json) {
    userUid = json['userUid'];
    grandTotalAmount = json['grandTotalAmount'];
    chatTime = json['chatTime'];
    message = json['message'];
    storeId = json['storeId'];
    merchantId = json['merchantId'];
    mobile = json['mobile'];
    totalItem = json['totalItem'];
    imageUrl = json['imageUrl'];
    deliveryCharges = json['deliveryCharges'];
    msgCounter = json['msgCounter'];
    address = json['address'];
    chatDate = json['chatDate'];
    orderId = json['orderId'];
    subTotal = json['subTotal'];
    storeLogo = json['storeLogo'];
    storeName = json['storeName'];
    customerOrderId = json['customerOrderId'];
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userUid'] = this.userUid;
    data['grandTotalAmount'] = this.grandTotalAmount;
    data['chatTime'] = this.chatTime;
    data['message'] = this.message;
    data['storeId'] = this.storeId;
    data['merchantId'] = this.merchantId;
    data['mobile'] = this.mobile;
    data['totalItem'] = this.totalItem;
    data['imageUrl'] = this.imageUrl;
    data['deliveryCharges'] = this.deliveryCharges;
    data['msgCounter'] = this.msgCounter;
    data['address'] = this.address;
    data['chatDate'] = this.chatDate;
    data['orderId'] = this.orderId;
    data['subTotal'] = this.subTotal;
    data['storeLogo'] = this.storeLogo;
    data['storeName'] = this.storeName;
    data['customerOrderId'] = this.customerOrderId;
    data['user'] = this.user;
    return data;
  }
}
