class StoreDetails {
  Data data;
  String message;

  StoreDetails({this.data, this.message});

  StoreDetails.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Data {
  ShopDetails shopDetails;
  String storeTypeName;
  StoreOwners storeOwners;
  List<StoreImages> storeImages;

  Data(
      {this.shopDetails,
      this.storeTypeName,
      this.storeOwners,
      this.storeImages});

  Data.fromJson(Map<String, dynamic> json) {
    shopDetails = json['shop_details'] != null
        ? new ShopDetails.fromJson(json['shop_details'])
        : null;
    storeTypeName = json['store_type_name'];
    storeOwners = json['store_owners'] != null
        ? new StoreOwners.fromJson(json['store_owners'])
        : null;
    if (json['store_images'] != null) {
      storeImages = new List<StoreImages>();
      json['store_images'].forEach((v) {
        storeImages.add(new StoreImages.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.shopDetails != null) {
      data['shop_details'] = this.shopDetails.toJson();
    }
    data['store_type_name'] = this.storeTypeName;
    if (this.storeOwners != null) {
      data['store_owners'] = this.storeOwners.toJson();
    }
    if (this.storeImages != null) {
      data['store_images'] = this.storeImages.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ShopDetails {
  int id;
  int merchantId;
  String storeName;
  String address;
  String location;
  String state;
  String city;
  String pincode;
  double lat;
  double lng;
  String locality;
  String country;
  String storeManager;
  String storeDescr;
  String mCategoryIDs;
  int systemCategoryId;
  String createdAt;
  String updatedAt;
  int isactive;
  String panCardImg;
  String aadharCardImg;
  double gst;
  String status;
  String aadharCard;
  String panCard;
  int rating;
  String phoneNo;
  int storeTypeId;
  int deliveryTime;
  String deliveryRadius;
  double deliveryCharge;
  int minOrder;
  String storeOpenTime;
  String storeCloseTime;
  String deliveryStartTime;
  String deliveryCloseTime;
  String storeCloseDays;
  String logo;
  String gstImage;
  int storeImageAvail;
  int documentsAvail;
  String fssai;
  int netOrders;
  int netCustomers;
  String csrComments;
  int categoryFlag;

  ShopDetails(
      {this.id,
      this.merchantId,
      this.storeName,
      this.address,
      this.location,
      this.state,
      this.city,
      this.pincode,
      this.lat,
      this.lng,
      this.locality,
      this.country,
      this.storeManager,
      this.storeDescr,
      this.mCategoryIDs,
      this.systemCategoryId,
      this.createdAt,
      this.updatedAt,
      this.isactive,
      this.panCardImg,
      this.aadharCardImg,
      this.gst,
      this.status,
      this.aadharCard,
      this.panCard,
      this.rating,
      this.phoneNo,
      this.storeTypeId,
      this.deliveryTime,
      this.deliveryRadius,
      this.deliveryCharge,
      this.minOrder,
      this.storeOpenTime,
      this.storeCloseTime,
      this.deliveryStartTime,
      this.deliveryCloseTime,
      this.storeCloseDays,
      this.logo,
      this.gstImage,
      this.storeImageAvail,
      this.documentsAvail,
      this.fssai,
      this.netOrders,
      this.netCustomers,
      this.csrComments,
      this.categoryFlag});

  ShopDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    merchantId = json['merchant_id'];
    storeName = json['store_name'];
    address = json['address'].toString();
    location = json['location'];
    state = json['state'];
    city = json['city'];
    pincode = json['pincode'].toString();
    lat = json['lat'];
    lng = json['lng'];
    locality = json['locality'];
    country = json['country'];
    storeManager = json['store_manager'];
    storeDescr = json['store_descr'];
    mCategoryIDs = json['m_category_IDs'].toString();
    systemCategoryId = json['system_category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isactive = json['isactive'];
    panCardImg = json['pan_card_img'];
    aadharCardImg = json['aadhar_card_img'];
    gst = double.tryParse(json['gst'].toString());
    status = json['status'];
    aadharCard = json['aadhar_card'].toString();
    panCard = json['pan_card'].toString();
    rating = json['rating'];
    phoneNo = json['phone_no'].toString();
    storeTypeId = json['store_type_id'];
    deliveryTime = json['delivery_time'] == "" ? 0 : json['delivery_time'];
    deliveryRadius = json['delivery_radius'].toString();
    deliveryCharge = json['delivery_charge'] == ""
        ? 0
        : double.tryParse(json['delivery_charge'].toString());
    minOrder = json['min_order'] == "" ? 0 : json['min_order'];
    storeOpenTime = json['store_open_time'];
    storeCloseTime = json['store_close_time'];
    deliveryStartTime = json['delivery_start_time'];
    deliveryCloseTime = json['delivery_close_time'];
    storeCloseDays = json['store_close_days'].toString();
    logo = json['logo'];
    gstImage = json['gst_image'];
    storeImageAvail = json['store_image_avail'];
    documentsAvail = json['documents_avail'];
    fssai = json['fssai'].toString();
    netOrders = json['net_orders'];
    netCustomers = json['net_customers'];
    csrComments = json['csr_comments'];
    categoryFlag = json['category_flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['merchant_id'] = this.merchantId;
    data['store_name'] = this.storeName;
    data['address'] = this.address;
    data['location'] = this.location;
    data['state'] = this.state;
    data['city'] = this.city;
    data['pincode'] = this.pincode;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['locality'] = this.locality;
    data['country'] = this.country;
    data['store_manager'] = this.storeManager;
    data['store_descr'] = this.storeDescr;
    data['m_category_IDs'] = this.mCategoryIDs;
    data['system_category_id'] = this.systemCategoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    data['pan_card_img'] = this.panCardImg;
    data['aadhar_card_img'] = this.aadharCardImg;
    data['gst'] = this.gst;
    data['status'] = this.status;
    data['aadhar_card'] = this.aadharCard;
    data['pan_card'] = this.panCard;
    data['rating'] = this.rating;
    data['phone_no'] = this.phoneNo;
    data['store_type_id'] = this.storeTypeId;
    data['delivery_time'] = this.deliveryTime;
    data['delivery_radius'] = this.deliveryRadius;
    data['delivery_charge'] = this.deliveryCharge;
    data['min_order'] = this.minOrder;
    data['store_open_time'] = this.storeOpenTime;
    data['store_close_time'] = this.storeCloseTime;
    data['delivery_start_time'] = this.deliveryStartTime;
    data['delivery_close_time'] = this.deliveryCloseTime;
    data['store_close_days'] = this.storeCloseDays;
    data['logo'] = this.logo;
    data['gst_image'] = this.gstImage;
    data['store_image_avail'] = this.storeImageAvail;
    data['documents_avail'] = this.documentsAvail;
    data['fssai'] = this.fssai;
    data['net_orders'] = this.netOrders;
    data['net_customers'] = this.netCustomers;
    data['csr_comments'] = this.csrComments;
    data['category_flag'] = this.categoryFlag;
    return data;
  }
}

class StoreOwners {
  String merchantOwner;
  String storeManager;

  StoreOwners({this.merchantOwner, this.storeManager});

  StoreOwners.fromJson(Map<String, dynamic> json) {
    merchantOwner = json['merchant_owner'];
    storeManager = json['store_manager'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['merchant_owner'] = this.merchantOwner;
    data['store_manager'] = this.storeManager;
    return data;
  }
}

class StoreImages {
  int id;
  int merchantstoreId;
  int merchantId;
  String path;
  bool status;
  String imageFor;
  String imageType;
  String createdAt;
  String updatedAt;
  int isactive;

  StoreImages(
      {this.id,
      this.merchantstoreId,
      this.merchantId,
      this.path,
      this.status,
      this.imageFor,
      this.imageType,
      this.createdAt,
      this.updatedAt,
      this.isactive});

  StoreImages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    merchantstoreId = json['merchantstore_id'];
    merchantId = json['merchant_id'];
    path = json['path'].toString();
    status = json['status'];
    imageFor = json['image_for'].toString();
    imageType = json['image_type'].toString();
    createdAt = json['created_at'].toString();
    updatedAt = json['updated_at'].toString();
    isactive = json['isactive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['merchantstore_id'] = this.merchantstoreId;
    data['merchant_id'] = this.merchantId;
    data['path'] = this.path;
    data['status'] = this.status;
    data['image_for'] = this.imageFor;
    data['image_type'] = this.imageType;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    return data;
  }
}
