class GetMyAddress {
  List<Details> details;
  String status;

  GetMyAddress({this.details, this.status});

  GetMyAddress.fromJson(Map<String, dynamic> json) {
    if (json['details'] != null) {
      details = new List<Details>();
      json['details'].forEach((v) {
        details.add(new Details.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.details != null) {
      data['details'] = this.details.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class Details {
  int id;
  int userId;
  String address;
  int pincode;
  String city;
  String state;
  String lat;
  String lng;
  int isDefault;
  int isActive;
  String createdAt;
  String updatedAt;
  String fullName;
  String flatHouseno;
  String colonyStreet;
  String landmark;

  Details(
      {this.id,
      this.userId,
      this.address,
      this.pincode,
      this.city,
      this.state,
      this.lat,
      this.lng,
      this.isDefault,
      this.isActive,
      this.createdAt,
      this.updatedAt,
      this.fullName,
      this.flatHouseno,
      this.colonyStreet,
      this.landmark});

  Details.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    address = json['address'];
    pincode = json['pincode'];
    city = json['city'].toString();
    state = json['state'].toString();
    lat = json['lat'].toString();
    lng = json['lng'].toString();
    isDefault = json['is_default'];
    isActive = json['is_active'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'].toString();
    fullName = json['full_name'].toString();
    flatHouseno = json['flat_houseno'].toString();
    colonyStreet = json['colony_street'].toString();
    landmark = json['landmark'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['address'] = this.address;
    data['pincode'] = this.pincode;
    data['city'] = this.city;
    data['state'] = this.state;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['is_default'] = this.isDefault;
    data['is_active'] = this.isActive;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['full_name'] = this.fullName;
    data['flat_houseno'] = this.flatHouseno;
    data['colony_street'] = this.colonyStreet;
    data['landmark'] = this.landmark;
    return data;
  }
}
