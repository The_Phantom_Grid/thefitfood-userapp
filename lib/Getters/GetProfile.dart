class Profile {
  List<ProfileData> prifiledata;
  String status;

  Profile({this.prifiledata, this.status});

  Profile.fromJson(Map<String, dynamic> json) {
    if (json['prifiledata'] != null) {
      prifiledata = new List<ProfileData>();
      json['prifiledata'].forEach((v) {
        prifiledata.add(new ProfileData.fromJson(v));
      });
    }
    status = json['status'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.prifiledata != null) {
      data['prifiledata'] = this.prifiledata.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class ProfileData {
  int id;
  String username;
  String email;
  String lat;
  String lng;
  String mobile;
  String address;
  String city;
  String landmark;
  int pincode;
  String firstname;
  String phone;
  String avatarBaseUrl;
  String avatarPath;

  ProfileData(
      {this.id,
      this.username,
      this.email,
      this.lat,
      this.lng,
      this.mobile,
      this.address,
      this.city,
      this.landmark,
      this.pincode,
      this.firstname,
      this.phone,
      this.avatarBaseUrl,
      this.avatarPath});

  ProfileData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'].toString();
    email = json['email'];
    lat = json['lat'].toString();
    lng = json['lng'].toString();
    mobile = json['mobile'].toString();
    address = json['address'];
    city = json['city'];
    landmark = json['landmark'];
    pincode = json['pincode'];
    firstname = json['firstname'];
    phone = json['phone'].toString();
    avatarBaseUrl = json['avatar_base_url'];
    avatarPath = json['avatar_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['email'] = this.email;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['mobile'] = this.mobile;
    data['address'] = this.address;
    data['city'] = this.city;
    data['landmark'] = this.landmark;
    data['pincode'] = this.pincode;
    data['firstname'] = this.firstname;
    data['phone'] = this.phone;
    data['avatar_base_url'] = this.avatarBaseUrl;
    data['avatar_path'] = this.avatarPath;
    return data;
  }
}
