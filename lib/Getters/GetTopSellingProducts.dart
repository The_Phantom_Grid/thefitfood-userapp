class TopSellingProduct {
  Data data;
  String message;
  int comingSoon;

  TopSellingProduct({this.data, this.message, this.comingSoon});

  TopSellingProduct.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    comingSoon = json['coming_soon'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    data['coming_soon'] = this.comingSoon;
    return data;
  }
}

class Data {
  List<TopSellingProducts> topSellingProducts;
  List<ProductHistory> productHistory;

  Data({this.topSellingProducts, this.productHistory});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['top_selling_products'] != null) {
      topSellingProducts = new List<TopSellingProducts>();
      json['top_selling_products'].forEach((v) {
        topSellingProducts.add(new TopSellingProducts.fromJson(v));
      });
    }
    if (json['product_history'] != null) {
      productHistory = new List<ProductHistory>();
      json['product_history'].forEach((v) {
        productHistory.add(new ProductHistory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.topSellingProducts != null) {
      data['top_selling_products'] =
          this.topSellingProducts.map((v) => v.toJson()).toList();
    }
    if (this.productHistory != null) {
      data['product_history'] =
          this.productHistory.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TopSellingProducts {
  String productImage;
  String merchantid;
  String storeId;
  String masterproductid;
  String storeName;
  String merchantProductListId;
  int shoppingListQty;
  int itemLeft;
  String storename;
  String address;
  String city;
  String mrp;
  double sellingPrice;
  double discount;
  String catId;
  String subcatId;
  double latitude;
  double longitude;
  double distance;
  String productName;
  String productUnit;
  String skuNumber;
  String manufacture;
  String productDescr;
  String catName;
  String subcatName;

  TopSellingProducts(
      {this.productImage,
      this.merchantid,
      this.storeId,
      this.masterproductid,
      this.storeName,
      this.merchantProductListId,
      this.shoppingListQty,
      this.itemLeft,
      this.storename,
      this.address,
      this.city,
      this.mrp,
      this.sellingPrice,
      this.discount,
      this.catId,
      this.subcatId,
      this.latitude,
      this.longitude,
      this.distance,
      this.productName,
      this.productUnit,
      this.skuNumber,
      this.manufacture,
      this.productDescr,
      this.catName,
      this.subcatName});

  TopSellingProducts.fromJson(Map<String, dynamic> json) {
    productImage = json['product_image'].toString();
    merchantid = json['merchantid'].toString();
    storeId = json['storeId'].toString();
    masterproductid = json['masterproductid'].toString();
    storeName = json['store_name'].toString();
    merchantProductListId = json['merchant_product_list_id'].toString();
    shoppingListQty = json['shopping_list_qty'];
    itemLeft = json['item_left'];
    storename = json['storename'].toString();
    address = json['address'].toString();
    city = json['city'].toString();
    mrp = json['mrp'].toString();
    sellingPrice = double.tryParse(json['selling_price'].toString());
    discount = double.tryParse(json['discount'].toString());
    catId = json['cat_id'].toString();
    subcatId = json['subcat_id'].toString();
    latitude = json['latitude'];
    longitude = json['longitude'];
    distance = json['distance'];
    productName = json['product_name'].toString();
    productUnit = json['product_unit'].toString();
    skuNumber = json['sku_number'].toString();
    manufacture = json['manufacture'].toString();
    productDescr = json['product_descr'].toString();
    catName = json['cat_name'].toString();
    subcatName = json['subcat_name'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_image'] = this.productImage;
    data['merchantid'] = this.merchantid;
    data['storeId'] = this.storeId;
    data['masterproductid'] = this.masterproductid;
    data['store_name'] = this.storeName;
    data['merchant_product_list_id'] = this.merchantProductListId;
    data['shopping_list_qty'] = this.shoppingListQty;
    data['item_left'] = this.itemLeft;
    data['storename'] = this.storename;
    data['address'] = this.address;
    data['city'] = this.city;
    data['mrp'] = this.mrp;
    data['selling_price'] = this.sellingPrice;
    data['discount'] = this.discount;
    data['cat_id'] = this.catId;
    data['subcat_id'] = this.subcatId;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['distance'] = this.distance;
    data['product_name'] = this.productName;
    data['product_unit'] = this.productUnit;
    data['sku_number'] = this.skuNumber;
    data['manufacture'] = this.manufacture;
    data['product_descr'] = this.productDescr;
    data['cat_name'] = this.catName;
    data['subcat_name'] = this.subcatName;
    return data;
  }
}

class ProductHistory {
  String productImage;
  String productName;
  String productUnit;
  String mrp;

  ProductHistory(
      {this.productImage, this.productName, this.productUnit, this.mrp});

  ProductHistory.fromJson(Map<String, dynamic> json) {
    productImage = json['product_image'].toString();
    productName = json['product_name'].toString();
    productUnit = json['product_unit'].toString();
    mrp = json['mrp'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['product_image'] = this.productImage;
    data['product_name'] = this.productName;
    data['product_unit'] = this.productUnit;
    data['mrp'] = this.mrp;
    return data;
  }
}
