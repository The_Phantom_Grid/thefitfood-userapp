// To parse this JSON data, do
//
//     final promoCode = promoCodeFromJson(jsonString);

import 'dart:convert';

PromoCode promoCodeFromJson(String str) => PromoCode.fromJson(json.decode(str));

String promoCodeToJson(PromoCode data) => json.encode(data.toJson());

class PromoCode {
  PromoCode({
    this.info,
    this.status,
  });

  List<Info> info;
  String status;

  factory PromoCode.fromJson(Map<String, dynamic> json) => PromoCode(
        info: List<Info>.from(json["info"].map((x) => Info.fromJson(x))),
        status: json["status"],
      );

  Map<String, dynamic> toJson() => {
        "info": List<dynamic>.from(info.map((x) => x.toJson())),
        "status": status,
      };
}

class Info {
  Info({
    this.id,
    this.minAmount,
    this.code,
    this.promocodeDesc,
    this.appUser,
    this.appMerchant,
    this.web,
    this.orderSeq,
    this.amount,
    this.fromDate,
    this.toDate,
    this.createdAt,
    this.updatedAt,
    this.isactive,
  });

  int id;
  int minAmount;
  String code;
  String promocodeDesc;
  String appUser;
  String appMerchant;
  String web;
  int orderSeq;
  int amount;
  String fromDate;
  String toDate;
  String createdAt;
  String updatedAt;
  int isactive;

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        id: json["id"],
        minAmount: json["min_amount"],
        code: json["code"].toString(),
        promocodeDesc: json["promocode_desc"].toString(),
        appUser: json["app_user"].toString(),
        appMerchant: json["app_merchant"].toString(),
        web: json["web"].toString(),
        orderSeq: json["order_seq"],
        amount: json["amount"],
        fromDate: json["from_date"].toString(),
        toDate: json["to_date"].toString(),
        createdAt: json["created_at"].toString(),
        updatedAt: json["updated_at"].toString(),
        isactive: json["isactive"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "min_amount": minAmount,
        "code": code,
        "promocode_desc": promocodeDesc,
        "app_user": appUser,
        "app_merchant": appMerchant,
        "web": web,
        "order_seq": orderSeq,
        "amount": amount,
        "from_date": fromDate,
        "to_date": toDate,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "isactive": isactive,
      };
}
