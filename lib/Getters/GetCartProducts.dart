class CartProduct {
  List<Product> product;
  Total total;
  String status;

  CartProduct({this.product, this.total, this.status});

  CartProduct.fromJson(Map<String, dynamic> json) {
    if (json['product'] != null) {
      product = new List<Product>();
      json['product'].forEach((v) {
        product.add(new Product.fromJson(v));
      });
    }
    total = json['total'] != null ? new Total.fromJson(json['total']) : null;
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.product != null) {
      data['product'] = this.product.map((v) => v.toJson()).toList();
    }
    if (this.total != null) {
      data['total'] = this.total.toJson();
    }
    data['status'] = this.status;
    return data;
  }
}

class Product {
  int shoppingListItemId;
  int shoppingListId;
  int storeId;
  String productname;
  int quantity;
  String taxStatus;
  int categoryId;
  int subcatId;
  String subCatName;
  String prodDesc;
  String sku;
  String catName;
  String subcatName;
  int masterproductid;
  int merchantlistid;
  int merchantid;
  String storeName;
  int systemCategory;
  String storeIcon;
  String measure;
  String icon;
  double pricewithtax;
  double pricewithouttax;
  double sellingPrice;
  double productMrp;
  double tax;
  int avgTimeToDeliver;
  int totalLeft;
  int shipping;
  int offerId;
  int offerExpired;
  String offerName;
  String offerImage;
  String offerDescr;
  String inventory;
  double comboPrice;
  double offerMrp;
  String type;

  Product(
      {this.shoppingListItemId,
      this.shoppingListId,
      this.storeId,
      this.productname,
      this.quantity,
      this.taxStatus,
      this.categoryId,
      this.subcatId,
      this.subCatName,
      this.prodDesc,
      this.sku,
      this.catName,
      this.subcatName,
      this.masterproductid,
      this.merchantlistid,
      this.merchantid,
      this.storeName,
      this.systemCategory,
      this.storeIcon,
      this.measure,
      this.icon,
      this.pricewithtax,
      this.pricewithouttax,
      this.sellingPrice,
      this.productMrp,
      this.tax,
    this.avgTimeToDeliver,
    this.totalLeft,
    this.shipping,
    this.offerId,
    this.offerExpired,
    this.offerName,
    this.offerImage,
    this.offerDescr,
    this.inventory,
    this.comboPrice,
    this.offerMrp,
    this.type
  });

  Product.fromJson(Map<String, dynamic> json) {
    shoppingListItemId = json['shopping_list_item_id'];
    shoppingListId = json['shopping_list_id'];
    storeId = json['store_id'];
    productname = json['productname'].toString();
    quantity = json['quantity'];
    taxStatus = json['tax_status'].toString();
    categoryId = json['category_id'];
    subcatId = json['subcat_id'];
    subCatName = json['sub_cat_name'];
    prodDesc = json['prod_desc'];
    sku = json['sku'];
    catName = json['cat_name'];
    subcatName = json['subcat_name'];
    masterproductid = json['masterproductid'];
    merchantlistid = json['merchantlistid'];
    merchantid = json['merchantid'];
    storeName = json['store_name'];
    systemCategory = json['system_category'];
    storeIcon = json['store_icon'];
    measure = json['measure'];
    icon = json['icon'];
    pricewithtax = json['pricewithtax'].toDouble();
    pricewithouttax = json['pricewithouttax'].toDouble();
    sellingPrice = json['selling_price'].toDouble();
    productMrp = json['product_mrp'].toDouble();
    tax = json['tax'].toDouble();
    avgTimeToDeliver = json['avg_time_to_deliver'];
    totalLeft = json['total_left'];
    shipping = json['shipping'];
    offerId = json['offer_id'];
    offerExpired = json['offer_expired'];
    offerName = json['offer_name'];
    offerImage = json['offer_image'];
    offerDescr = json['offer_descr'];
    inventory = json['inventory'].toString();
    comboPrice = double.tryParse(json['combo_price'].toString());
    offerMrp = double.tryParse(json['offer_mrp'].toString());
    type = json['type'].toString().toLowerCase();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['shopping_list_item_id'] = this.shoppingListItemId;
    data['shopping_list_id'] = this.shoppingListId;
    data['store_id'] = this.storeId;
    data['productname'] = this.productname;
    data['quantity'] = this.quantity;
    data['tax_status'] = this.taxStatus;
    data['category_id'] = this.categoryId;
    data['subcat_id'] = this.subcatId;
    data['sub_cat_name'] = this.subCatName;
    data['prod_desc'] = this.prodDesc;
    data['sku'] = this.sku;
    data['cat_name'] = this.catName;
    data['subcat_name'] = this.subcatName;
    data['masterproductid'] = this.masterproductid;
    data['merchantlistid'] = this.merchantlistid;
    data['merchantid'] = this.merchantid;
    data['store_name'] = this.storeName;
    data['system_category'] = this.systemCategory;
    data['store_icon'] = this.storeIcon;
    data['measure'] = this.measure;
    data['icon'] = this.icon;
    data['pricewithtax'] = this.pricewithtax;
    data['pricewithouttax'] = this.pricewithouttax;
    data['selling_price'] = this.sellingPrice;
    data['product_mrp'] = this.productMrp;
    data['tax'] = this.tax;
    data['avg_time_to_deliver'] = this.avgTimeToDeliver;
    data['total_left'] = this.totalLeft;
    data['shipping'] = this.shipping;
    data['offer_id'] = this.offerId;
    data['offer_expired'] = this.offerExpired;
    data['offer_name'] = this.offerName;
    data['offer_image'] = this.offerImage;
    data['offer_descr'] = this.offerDescr;
    data['inventory'] = this.inventory;
    data['combo_price'] = this.comboPrice;
    data['offer_mrp'] = this.offerMrp;
    data['type'] = this.type;
    return data;
  }
}

class Total {
  String totaltax;
  String overallDiscount;
  int offerExpired;
  String cartSubtotal;
  String deliveryFees;
  String grandtotal;
  double bill_subtotal;

  Total(
      {this.totaltax,
      this.overallDiscount,
      this.offerExpired,
      this.cartSubtotal,
      this.deliveryFees,
      this.grandtotal,
      this.bill_subtotal});

  Total.fromJson(Map<String, dynamic> json) {
    totaltax = json['totaltax'].toString();
    overallDiscount = json['overall_discount'].toString();
    offerExpired = json['offer_expired'];
    cartSubtotal = json['cart_subtotal'].toString();
    deliveryFees = json['delivery_fees'].toString();
    grandtotal = json['grandtotal'].toString();
    bill_subtotal = double.tryParse(json['bill_subtotal'].toString());
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totaltax'] = this.totaltax;
    data['overall_discount'] = this.overallDiscount;
    data['offer_expired'] = this.offerExpired;
    data['cart_subtotal'] = this.cartSubtotal;
    data['delivery_fees'] = this.deliveryFees;
    data['grandtotal'] = this.grandtotal;
    data['bill_subtotal'] = this.bill_subtotal;
    return data;
  }
}
