class StoreSearchData {
  List<Info> info;
  String status;

  StoreSearchData({this.info, this.status});

  StoreSearchData.fromJson(Map<String, dynamic> json) {
    if (json['info'] != null) {
      info = new List<Info>();
      json['info'].forEach((v) {
        info.add(new Info.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.info != null) {
      data['info'] = this.info.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class Info {
  int id;
  double distance;
  int netCustomers;
  int netOrders;
  String name;
  String storeDescr;
  String address;
  String city;
  String opentime;
  String closetime;
  int storeTypeId;
  int mobile;
  String location;
  String locality;
  int orderEnabled;
  String storeType;
  String storeTypeIcon;
  String logo;
  String deliveryTime;
  int deliveryRadius;
  int pincode;
  double latitude;
  double longitude;
  int rating;
  int favrt;
  String storeName;
  int minorder;
  int runningOffers;
  int deliveryCharge;
  List<Storeimages> storeimages;
  List<Category> category;

  Info(
      {this.id,
      this.distance,
      this.netCustomers,
      this.netOrders,
      this.name,
      this.storeDescr,
      this.address,
      this.city,
      this.opentime,
      this.closetime,
      this.storeTypeId,
      this.mobile,
      this.location,
      this.locality,
      this.orderEnabled,
      this.storeType,
      this.storeTypeIcon,
      this.logo,
      this.deliveryTime,
      this.deliveryRadius,
      this.pincode,
      this.latitude,
      this.longitude,
      this.rating,
      this.favrt,
      this.storeName,
      this.minorder,
      this.runningOffers,
      this.deliveryCharge,
      this.storeimages,
      this.category});

  Info.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    distance = json['distance'];
    netCustomers = json['net_customers'];
    netOrders = json['net_orders'];
    name = json['name'].toString();
    storeDescr = json['store_descr'].toString();
    address = json['address'].toString();
    city = json['city'].toString();
    opentime = json['opentime'].toString();
    closetime = json['closetime'].toString();
    storeTypeId = json['store_type_id'];
    mobile = json['mobile'];
    location = json['location'].toString();
    locality = json['locality'].toString();
    orderEnabled = json['order_enabled'];
    storeType = json['store_type'].toString();
    storeTypeIcon = json['store_type_icon'].toString();
    logo = json['logo'].toString();
    deliveryTime = json['delivery_time'].toString();
    deliveryRadius = json['delivery_radius'];
    pincode = json['pincode'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    rating = json['rating'];
    favrt = json['favrt'];
    storeName = json['storeName'].toString();
    minorder = json['minorder'];
    runningOffers = json['running_offers'];
    deliveryCharge = json['delivery_charge'];
    if (json['storeimages'] != null) {
      storeimages = new List<Storeimages>();
      json['storeimages'].forEach((v) {
        storeimages.add(new Storeimages.fromJson(v));
      });
    }
    if (json['category'] != null) {
      category = new List<Category>();
      json['category'].forEach((v) {
        category.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['distance'] = this.distance;
    data['net_customers'] = this.netCustomers;
    data['net_orders'] = this.netOrders;
    data['name'] = this.name;
    data['store_descr'] = this.storeDescr;
    data['address'] = this.address;
    data['city'] = this.city;
    data['opentime'] = this.opentime;
    data['closetime'] = this.closetime;
    data['store_type_id'] = this.storeTypeId;
    data['mobile'] = this.mobile;
    data['location'] = this.location;
    data['locality'] = this.locality;
    data['order_enabled'] = this.orderEnabled;
    data['store_type'] = this.storeType;
    data['store_type_icon'] = this.storeTypeIcon;
    data['logo'] = this.logo;
    data['delivery_time'] = this.deliveryTime;
    data['delivery_radius'] = this.deliveryRadius;
    data['pincode'] = this.pincode;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['rating'] = this.rating;
    data['favrt'] = this.favrt;
    data['storeName'] = this.storeName;
    data['minorder'] = this.minorder;
    data['running_offers'] = this.runningOffers;
    data['delivery_charge'] = this.deliveryCharge;
    if (this.storeimages != null) {
      data['storeimages'] = this.storeimages.map((v) => v.toJson()).toList();
    }
    if (this.category != null) {
      data['category'] = this.category.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Storeimages {
  int id;
  int merchantstoreId;
  int merchantId;
  String path;
  int status;
  String imageFor;
  String imageType;
  String createdAt;
  Null updatedAt;
  int isactive;

  Storeimages(
      {this.id,
      this.merchantstoreId,
      this.merchantId,
      this.path,
      this.status,
      this.imageFor,
      this.imageType,
      this.createdAt,
      this.updatedAt,
      this.isactive});

  Storeimages.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    merchantstoreId = json['merchantstore_id'];
    merchantId = json['merchant_id'];
    path = json['path'];
    status = json['status'];
    imageFor = json['image_for'];
    imageType = json['image_type'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    isactive = json['isactive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['merchantstore_id'] = this.merchantstoreId;
    data['merchant_id'] = this.merchantId;
    data['path'] = this.path;
    data['status'] = this.status;
    data['image_for'] = this.imageFor;
    data['image_type'] = this.imageType;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['isactive'] = this.isactive;
    return data;
  }
}

class Category {
  int categoryId;
  String categoryName;
  String icons;

  Category({this.categoryId, this.categoryName, this.icons});

  Category.fromJson(Map<String, dynamic> json) {
    categoryId = json['category_id'];
    categoryName = json['category_name'];
    icons = json['icons'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['category_id'] = this.categoryId;
    data['category_name'] = this.categoryName;
    data['icons'] = this.icons;
    return data;
  }
}
