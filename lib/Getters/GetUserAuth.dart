class VerifyOtp {
  String id;
  String mobile;
  String authkey;
  String newUser;
  String status;

  VerifyOtp({this.id, this.mobile, this.authkey, this.newUser, this.status});

  VerifyOtp.fromJson(Map<String, dynamic> json) {
    id = json['id'].toString();
    mobile = json['mobile'].toString();
    authkey = json['authkey'].toString();
    newUser = json['new_user'].toString();
    status = json['status'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['mobile'] = this.mobile;
    data['authkey'] = this.authkey;
    data['new_user'] = this.newUser;
    data['status'] = this.status;
    return data;
  }
}
