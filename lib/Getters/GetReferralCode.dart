// To parse this JSON data, do
//
//     final referralCode = referralCodeFromJson(jsonString);

import 'dart:convert';

ReferralCode referralCodeFromJson(String str) =>
    ReferralCode.fromJson(json.decode(str));

String referralCodeToJson(ReferralCode data) => json.encode(data.toJson());

class ReferralCode {
  ReferralCode({
    this.message,
    this.status,
    this.title,
    this.content,
    this.data,
  });

  String message;
  String status;
  String title;
  List<String> content;
  List<ReferralData> data;

  factory ReferralCode.fromJson(Map<String, dynamic> json) => ReferralCode(
        message: json["message"],
        status: json["status"],
        title: json["title"].toString(),
        content: List<String>.from(json["content"].map((x) => x)),
        data: List<ReferralData>.from(
            json["data"].map((x) => ReferralData.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        "status": status,
        "title": title,
        "content": List<dynamic>.from(content.map((x) => x)),
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class ReferralData {
  ReferralData({
    this.referalCode,
    this.paytmNo,
  });

  String referalCode;
  String paytmNo;

  factory ReferralData.fromJson(Map<String, dynamic> json) => ReferralData(
        referalCode: json["referal_code"].toString(),
        paytmNo: json["paytm_no"].toString(),
      );

  Map<String, dynamic> toJson() => {
        "referal_code": referalCode,
        "paytm_no": paytmNo,
      };
}
