class RunningOffer {
  List<RunningOffersInfo> info;
  String status;

  RunningOffer({this.info, this.status});

  RunningOffer.fromJson(Map<String, dynamic> json) {
    if (json['info'] != null) {
      info = new List<RunningOffersInfo>();
      json['info'].forEach((v) {
        info.add(new RunningOffersInfo.fromJson(v));
      });
    }
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.info != null) {
      data['info'] = this.info.map((v) => v.toJson()).toList();
    }
    data['status'] = this.status;
    return data;
  }
}

class RunningOffersInfo {
  int id;
  int merchantId;
  int totalLeft;
  String storeName;
  String address;
  String location;
  String state;
  double lat;
  double lng;
  String locality;
  String logo;
  int storeId;
  int countProducts;
  int percentDiscount;
  int categoryId;
  int subcatId;
  String catName;
  String subcatName;
  int masterProductId;
  int merchantproductList;
  int shoppingListQty;
  double mrp;
  double sellingPrice;
  String productImage;
  String productName;
  String productDescr;
  String manufacturer;
  String measure;

  RunningOffersInfo(
      {this.id,
      this.merchantId,
      this.totalLeft,
      this.storeName,
      this.address,
      this.location,
      this.state,
      this.lat,
      this.lng,
      this.locality,
      this.logo,
      this.storeId,
      this.countProducts,
      this.percentDiscount,
      this.categoryId,
      this.subcatId,
      this.catName,
      this.subcatName,
      this.masterProductId,
      this.merchantproductList,
      this.shoppingListQty,
      this.mrp,
      this.sellingPrice,
      this.productImage,
      this.productName,
      this.productDescr,
      this.manufacturer,
      this.measure});

  RunningOffersInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    merchantId = json['merchant_id'];
    totalLeft = json['total_left'];
    storeName = json['store_name'].toString();
    address = json['address'].toString();
    location = json['location'].toString();
    state = json['state'];
    lat = json['lat'];
    lng = json['lng'];
    locality = json['locality'];
    logo = json['logo'].toString();
    storeId = json['store_id'];
    countProducts = json['count_products'];
    percentDiscount = json['percent_discount'];
    categoryId = json['category_id'];
    subcatId = json['subcat_id'];
    catName = json['cat_name'].toString();
    subcatName = json['subcat_name'].toString();
    masterProductId = json['master_product_id'];
    merchantproductList = json['merchantproduct_list'];
    shoppingListQty = json['shopping_list_qty'];
    mrp = json['mrp'].toDouble();
    sellingPrice = json['selling_price'].toDouble();
    productImage = json['product_image'];
    productName = json['product_name'];
    productDescr = json['product_descr'];
    manufacturer = json['manufacturer'];
    measure = json['measure'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['merchant_id'] = this.merchantId;
    data['total_left'] = this.totalLeft;
    data['store_name'] = this.storeName;
    data['address'] = this.address;
    data['location'] = this.location;
    data['state'] = this.state;
    data['lat'] = this.lat;
    data['lng'] = this.lng;
    data['locality'] = this.locality;
    data['logo'] = this.logo;
    data['store_id'] = this.storeId;
    data['count_products'] = this.countProducts;
    data['percent_discount'] = this.percentDiscount;
    data['category_id'] = this.categoryId;
    data['subcat_id'] = this.subcatId;
    data['cat_name'] = this.catName;
    data['subcat_name'] = this.subcatName;
    data['master_product_id'] = this.masterProductId;
    data['merchantproduct_list'] = this.merchantproductList;
    data['shopping_list_qty'] = this.shoppingListQty;
    data['mrp'] = this.mrp;
    data['selling_price'] = this.sellingPrice;
    data['product_image'] = this.productImage;
    data['product_name'] = this.productName;
    data['product_descr'] = this.productDescr;
    data['manufacturer'] = this.manufacturer;
    data['measure'] = this.measure;
    return data;
  }
}
