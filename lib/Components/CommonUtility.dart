import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:userapp/Components/PermissionStatusPage.dart';
import 'package:userapp/Components/VariableController.dart';
import 'package:userapp/Getters/GetProfile.dart';
import 'package:userapp/Getters/GetStoreDetails.dart';
import 'package:userapp/Getters/GetStoreInfo.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:userapp/constants.dart';

var mColor = Color(0xFF3dad00);
String LAT, LONG;
User USER_DETAILES;
StoreDetails STORE_DETAILS;
StoreInfo STORE_INFO;
String MERCHANT_ID, STORE_ID, USER_ID;
String TOKEN;
HttpRequests auth = HttpRequests();
List<Address> addresses;
DynamicVar v = Get.put(DynamicVar());
Position _currentPosition;
SharedPreferences preferences;
Address first;
ProfileData PROFILE_DATA;
var screenSize = Get.context.size;
TextStyle textStyleMediumColor = TextStyle(fontSize: 12, color: mColor);
TextStyle textStyleMediumHeadingColor =
TextStyle(fontSize: 12, color: mColor, fontWeight: FontWeight.bold);
TextStyle textStyleSmallColor = TextStyle(fontSize: 10, color: mColor);
TextStyle textStyleLargeColor = TextStyle(fontSize: 13, color: mColor);
TextStyle headingTextStyleColor =
TextStyle(fontSize: 13, color: mColor, fontWeight: FontWeight.bold);
TextStyle smallTextStyle = TextStyle(fontSize: 11, color: Colors.black45);
TextStyle smallTextStyleBlack = TextStyle(fontSize: 11, color: Colors.black);
TextStyle mediumTextStyleBlack = TextStyle(fontSize: 12, color: Colors.black);
TextStyle largeTextStyleBlack = TextStyle(fontSize: 13, color: Colors.black);

showUpdateLocation() {
  Get.bottomSheet(
      Container(
          color: Colors.white,
          padding: EdgeInsets.all(15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Change Location",
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
              SizedBox(
                height: 15,
              ),
              RichText(
                  text: TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "We have detected that you are in ",
                      style: TextStyle(color: Colors.black),
                    ),
                    TextSpan(
                      text: "${first.subLocality} - ${first.postalCode}.",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                    TextSpan(
                      text: " Do you wish to change your selected location?",
                      style: TextStyle(color: Colors.black),
                    ),
                  ])),
              SizedBox(
                height: 15,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: OutlineButton(
                      onPressed: () {
                        Get.back();
                        v.showUpdateLocation.value = false;
                      },
                      child: Text("Cancel"),
                    ),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    flex: 1,
                    child: FlatButton(
                      onPressed: () {
                        Get.back();
                        v.current_location.value = first.subLocality;
                        v.current_postalCode.value = first.postalCode;
                        v.showUpdateLocation.value = false;
                        setLocation(_currentPosition.latitude.toString(),
                            _currentPosition.longitude.toString());
                      },
                      child: Text("Update Location"),
                      textColor: Colors.white,
                      color: mColor,
                    ),
                  )
                ],
              )
            ],
          )),
      isDismissible: false,
      isScrollControlled: true,
      persistent: true,
      enableDrag: false);
}

getMyLocation() {
  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
  try {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      _currentPosition = position;
      getFullAddress();
    });
  } catch (e) {
    print("LOCATION EXCEPTION -> $e");
  }
}

getFullAddress() async {
  preferences = await SharedPreferences.getInstance();
  var coordinates =
  Coordinates(_currentPosition.latitude, _currentPosition.longitude);
  addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
  first = addresses.first;
  relocate();
}

relocate() async {
  var r = await auth.getProfile(USER_DETAILES.data.loginDetails.authKey);
  PROFILE_DATA = ProfileData.fromJson(r);
  var lat = double.tryParse(r['prifiledata'][0]['lat'].toString());
  var lng = double.tryParse(r['prifiledata'][0]['lng'].toString());
  print("LAT:::: $LAT || LONG::: $LONG");

  var c = Coordinates(lat, lng);

  addresses = await Geocoder.local.findAddressesFromCoordinates(c);
  Address current = addresses.first;
  v.current_location.value = current.subLocality;
  v.current_postalCode.value = current.postalCode;
  print(
      "CURRENT POSITION: ${v.current_location.value} | ${v.current_postalCode.value}");
  print(
      "CURRENT POSITION 2: ${_currentPosition.latitude} | ${_currentPosition.longitude}");
  setLocation(LAT.toString(), LONG.toString());
  if (first.subLocality != v.current_location.value) {
    if (v.showUpdateLocation.value) showUpdateLocation();
  }
}

setLocation(String lat, String lng) async {
  LAT = lat;
  LONG = lng;
  await auth.updateLocation(lat, lng, USER_DETAILES.data.loginDetails.authKey);
  showToast("Location Updated", Colors.blueGrey);
  preferences?.setString('lastLocation', v.current_location.value);
  preferences?.setString('postalCode', v.current_postalCode.value);
  preferences?.setString('lat', lat.toString());
  preferences?.setString('lng', lng.toString());
  // nearmeState.getData();
}

updateLocation(double lat, double lng) async {
  var r = await auth.updateLocation(
      lat, lng, USER_DETAILES.data.loginDetails.authKey);

  preferences?.setString('lastLocation', v.current_location.value);
  preferences?.setString('postalCode', v.current_postalCode.value);
  preferences?.setDouble('lat', lat);
  preferences?.setDouble('lng', lng);
  // nearmeState.getData();

  if (r['message'].toString().contains("Location updated successfully")) {
//      getNotifications();
    Fluttertoast.showToast(
        msg: "Location Updated", toastLength: Toast.LENGTH_LONG);
  } else {
    Fluttertoast.showToast(
        msg: "${r['message'].toString()}", toastLength: Toast.LENGTH_LONG);
  }
}

getShoppingListCount() async {
  var r = await auth.getShoppingListCount(
      USER_DETAILES.data.loginDetails.authKey);
  v.cartCount.value = r['data'] == null ? 0 : r['data'];
}

Future askPermissions(PermissionStatus permissionStatus, String permissionName,
    Permission permission) async {
  PermissionStatus status = permissionStatus;
  print("PERMISSION STATUS>>>: $status");

  if (status.isGranted)
    return true;
  else if (status.isDenied)
    await permission.request();
  else if (status.isRestricted)
    Get.to(PermissionStatusPage(permissionName));
  else if (status.isPermanentlyDenied)
    Get.to(PermissionStatusPage(permissionName));
  else if (status.isUndetermined) await permission.request();
}
