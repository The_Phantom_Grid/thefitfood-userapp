import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';

class DynamicVar extends GetxController {
  RxBool showUpdateLocation = true.obs;
  RxString current_location = "".obs;
  RxString current_postalCode = "".obs;
  RxInt cartCount = 0.obs;
}
