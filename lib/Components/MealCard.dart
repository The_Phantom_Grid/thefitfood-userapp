import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/ProductDetail.dart';
import 'package:userapp/Getters/GetProducts.dart';

import '../Network/httpRequests.dart';
import '../constants.dart';
import 'CommonUtility.dart';
import '../Screens/Home/Dashboard.dart';

class MealCard extends StatefulWidget {
  DataP data;

  MealCard(Key key, this.data) : super(key: key);

  @override
  _MealCardState createState() => _MealCardState();
}

class _MealCardState extends State<MealCard>
    with AutomaticKeepAliveClientMixin {
  bool showAddButton = true, adding = false;
  HttpRequests requests = HttpRequests();
  int itemCount = 0, totalLeft = 0;

  increment() {
    if (itemCount != totalLeft) {
      itemCount++;
      showAddButton = false;
      addItemToCart(itemCount);
    } else {
      showToast("Out of stock!", Colors.redAccent);
    }
  }

  decrement() {
    if (itemCount <= 1) {
      setState(() {
        itemCount = 0;
      });

      print("$itemCount");
      addItemToCart(itemCount);
    } else {
      itemCount--;
      addItemToCart(itemCount);
    }
  }

  addItemToCart(int qty) async {
    print("F $qty");
    setState(() {
      adding = true;
    });
    var response = await requests.addToCart(widget.data.masterProductId,
        widget.data.merchantproductList, "", "", qty);

    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      setState(() {
        print("CART UPDATED!");
        adding = false;
        dashboardState.getProducts();
        getShoppingListCount();
      });
    } else {
      setState(() {
        adding = false;
        dashboardState.getProducts();
        getShoppingListCount();
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    itemCount = widget.data.shoppingListQty;
    if (itemCount > 0) showAddButton = false;
    totalLeft = widget.data.totalLeft;
    // print("${widget.data.productName} | ${widget.data.sellingPrice} | ${widget.data.shoppingListQty}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: adding,
      child: GestureDetector(
        onTap: () async {
          await Get.to(
              ProductDetailPage(widget.data.merchantproductList.toString()));
          // dashboardState.getProducts();
        },
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.withOpacity(.4), width: .5),
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(18))),
          child: Column(
            children: <Widget>[
              Expanded(
                flex: 3,
                child: Stack(
                  fit: StackFit.expand,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(18.0),
                      child: Opacity(
                        opacity: adding ? .3 : 1,
                        child: CachedNetworkImage(
                          placeholder: (context, _) =>
                              Image.asset('assets/default-image.jpg'),
                          imageUrl: widget.data.productImage,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    !adding ? SizedBox() : Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(18.0),
                        border: Border.all(color: Colors.transparent, width: 0),
                        color: Colors.transparent,
                      ),
                      child: Center(
                        child: SizedBox(
                            height: 25,
                            child: SpinKitThreeBounce(
                              color: mColor,
                              size: 25,
                            )
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.all(4),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${widget.data.productName}",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 12),
                      ),
                      Row(
                        children: <Widget>[

                          SizedBox(width: 4,),
                          Text(
                            "₹ ${widget.data.sellingPrice}",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: textStyleLargeColor,
                          ),
                        ],
                      ),
                      totalLeft == 0 ?
                      Container(
                        child: Text("OUT OF STOCK", style: TextStyle(
                            fontSize: 12, color: Colors.redAccent),),
                      ) :
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: smallTextStyle,
                            ),
                          ),
                          Expanded(
                            child: GestureDetector(
                              onTap: () {
                                print("TOTAL LEFT>> ${totalLeft} ");
                                increment();
                              },
                              child: Container(
                                height: 22,
                                // width: double.infinity,
                                decoration: showAddButton || itemCount == 0
                                    ? BoxDecoration(
                                    color: mColor,
                                    borderRadius:
                                    BorderRadius.all(Radius.circular(12)))
                                    : null,
                                child: showAddButton || itemCount == 0
                                    ? Container(
                                  height: 22,
                                  width: double.infinity,
                                  decoration: BoxDecoration(
                                      color: mColor,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(12))),
                                  child: Center(
                                      child: Text(
                                        "ADD",
                                        style: TextStyle(
                                            fontSize: 11, color: Colors.white),
                                      )),
                                )
                                    : Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: GestureDetector(
                                        onTap: () {
                                          decrement();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: mColor,
                                          ),
                                          child: Center(
                                            child: Text("-",
                                                style: TextStyle(
                                                    color: Colors.white)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Center(child: Text("$itemCount")),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: GestureDetector(
                                        onTap: () {
                                          increment();
                                        },
                                        child: Container(
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: mColor,
                                          ),
                                          child: Center(
                                            child: Text(
                                              "+",
                                              style: TextStyle(
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // child: Container(
                            //   height: 22,
                            //   width: double.infinity,
                            //   decoration: BoxDecoration(
                            //       color: mColor,
                            //       borderRadius: BorderRadius.all(Radius.circular(12))
                            //   ),
                            //   child: Center(child: Text("ADD", style: TextStyle(fontSize: 11, color: Colors.white),)),
                            // ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  // TODO: implement wantKeepAlive
  bool get wantKeepAlive => true;
}
