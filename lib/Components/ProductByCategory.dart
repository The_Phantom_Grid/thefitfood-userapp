import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/MealCard.dart';
import 'package:userapp/Getters/GetStoreInfo.dart';

import '../Getters/GetProducts.dart';
import '../Getters/GetSearchProduct.dart';
import '../Network/httpRequests.dart';
import '../checkout.dart';
import '../constants.dart';
import 'CommonUtility.dart';
import 'LoaderComponents.dart';
import 'ProductDetail.dart';

_ProductByCategoryState productByCategoryState;

class ProductByCategory extends StatefulWidget {
  ProductCategory CATEGORY;

  ProductByCategory(this.CATEGORY);

  @override
  _ProductByCategoryState createState() {
    productByCategoryState = _ProductByCategoryState();
    return productByCategoryState;
  }
}

class _ProductByCategoryState extends State<ProductByCategory> {
  TextEditingController searchController = TextEditingController();
  HttpRequests requests = HttpRequests();

  // SearchProduct product;
  ProductList productList;
  bool loading = false,
      // loadingCart = true,
      notFound = false,
      showContainer = false;
  bool showCloseIcon = false, showSearchContainer = true;

  // String searchByValue = "Search for Products";
  // List<String> searchBy = [
  //   "Search for Products",
  //   "Search for Brands",
  // ];
  // int sortValue = 1;
  //
  // int itemCount = 0;

  getProduct() async {
    setState(() {
      loading = true;
    });
    var response = await requests.getProductsByCategory(
        widget.CATEGORY.catId.toString(), "100", "0");
    setState(() {
      productList = ProductList.fromJson(response);
    });
    if (productList.data.length == 0) {
      setState(() {
        loading = false;
        notFound = true;
        showContainer = false;
      });
    } else {
      setState(() {
        loading = false;
        notFound = false;
        showContainer = true;
      });
      // selectSortValue(sortValue);
    }
  }

  //
  // selectSortValue(int val) {
  //   setState(() {
  //     sortValue = val;
  //   });
  //   switch (sortValue) {
  //     case 1:
  //       high2low();
  //       break;
  //     case 2:
  //       low2high();
  //       break;
  //   }
  // }
  //
  // showSortFilter() {
  //   showModalBottomSheet(
  //       context: context,
  //       isDismissible: true,
  //       isScrollControlled: true,
  //       builder: (context) {
  //         return StatefulBuilder(
  //             builder: (BuildContext context, StateSetter setState) {
  //               return Container(
  //                 child: Column(
  //                   crossAxisAlignment: CrossAxisAlignment.start,
  //                   mainAxisSize: MainAxisSize.min,
  //                   children: <Widget>[
  //                     Padding(
  //                       padding: const EdgeInsets.all(8.0),
  //                       child: Text(
  //                         "SORT",
  //                         style: TextStyle(fontSize: 18),
  //                       ),
  //                     ),
  //                     Divider(
  //                       height: 0,
  //                     ),
  //                     RadioListTile(
  //                       onChanged: (val) {
  //                         selectSortValue(val);
  //                         Navigator.pop(context);
  //                       },
  //                       value: 1,
  //                       groupValue: sortValue,
  //                       title: Text("Price  -  High to Low"),
  //                     ),
  //                     RadioListTile(
  //                       onChanged: (val) {
  //                         selectSortValue(val);
  //                         Navigator.pop(context);
  //                       },
  //                       value: 2,
  //                       groupValue: sortValue,
  //                       title: Text("Price  -  Low to High"),
  //                     ),
  //                   ],
  //                 ),
  //               );
  //             });
  //       });
  // }
  //
  // low2high() {
  //   if (!notFound) {
  //     Comparator<SearchProductData> priceComparator = (a, b) =>
  //         double.tryParse(a.sellingPrice.toString())
  //             .compareTo(double.tryParse(b.sellingPrice.toString()));
  //     setState(() {
  //       product.data.sort(priceComparator);
  //     });
  //   }
  // }
  //
  // high2low() {
  //   if (!notFound) {
  //     Comparator<SearchProductData> priceComparator = (b, a) =>
  //         double.tryParse(a.sellingPrice.toString())
  //             .compareTo(double.tryParse(b.sellingPrice.toString()));
  //     setState(() {
  //       product.data.sort(priceComparator);
  //     });
  //   }
  // }

  @override
  void initState() {
    // TODO: implement initState
    getProduct();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final StreamVars streamVars = Get.put(StreamVars());
    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: mColor,
            elevation: 0.0,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            title: Text("${widget.CATEGORY.catName.toUpperCase()}")),
        body: Stack(
          children: <Widget>[
            loading
                ? productLoader()
                : !showSearchContainer
                    ? SizedBox()
                    : Padding(
                        padding: const EdgeInsets.symmetric(vertical: 4.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
//                   Container(
//                     padding: EdgeInsets.all(4),
//                     color: Colors.white,
//                     height: 40,
//                     child: Row(
//                       children: <Widget>[
//                         Expanded(
//                           child: TextField(
//                             controller: searchController,
//                             autofocus: false,
//                             onChanged: (text) {
//                               if (searchController.text.isNotEmpty) {
//                                 setState(() {
//                                   showCloseIcon = true;
//                                 });
//                               }
//                             },
//                             onSubmitted: (text) {
//                               if (searchController.text.isNotEmpty) {
//                                 searchController.text = text;
//                                 setState(() {
//                                   loading = true;
//                                   showSearchContainer = true;
//                                 });
//                                 searchProduct();
//                               }
//                             },
//                             decoration: InputDecoration(
//                               suffixIcon: showCloseIcon
//                                   ? GestureDetector(
//                                 onTap: () {
//                                   setState(() {
//                                     searchController.text = "";
//                                     showCloseIcon = false;
//                                     showSearchContainer = true;
//                                     notFound = false;
//                                   });
//                                 },
//                                 child: Icon(
//                                   Icons.close,
//                                   color: Colors.black,
//                                   size: 18,
//                                 ),
//                               )
//                                   : PopupMenuButton<String>(
//                                 itemBuilder: (context) {
//                                   return searchBy.map((str) {
//                                     return PopupMenuItem(
//                                       value: str,
//                                       child: Text(str),
//                                     );
//                                   }).toList();
//                                 },
//                                 child: Icon(
//                                     Icons.arrow_drop_down,
//                                     color: Colors.black),
//                                 onSelected: (v) {
//                                   setState(() {
//                                     searchByValue = v;
//                                     print(searchByValue);
//                                     searchController.text = "";
//                                   });
//                                 },
//                               ),
//                               filled: true,
//                               fillColor: Colors.black12,
//                               enabledBorder: OutlineInputBorder(
//                                   borderSide: BorderSide(
//                                       color: Colors.black12,
//                                       width: 0),
//                                   borderRadius:
//                                   BorderRadius.circular(12)),
//                               focusedBorder: OutlineInputBorder(
//                                   borderSide: BorderSide(
//                                       color: Colors.black12,
//                                       width: 0),
//                                   borderRadius:
//                                   BorderRadius.circular(12)),
//                               contentPadding: EdgeInsets.all(4),
//                               hintStyle: TextStyle(fontSize: 13),
//                               labelStyle: TextStyle(fontSize: 13),
//                               prefixIcon: Icon(Icons.search,
//                                   color: Colors.black),
// //                    suffixIcon: Text(resultCount),
//                               enabled: true,
//                               hintText: "$searchByValue...",
//                             ),
//                             cursorColor: Colors.black,
//                             showCursor: true,
//                           ),
//                         ),
//                         SizedBox(
//                           width: 6,
//                         ),
//                         GestureDetector(
//                           onTap: () {
//                             showSortFilter();
//                           },
//                           child: Padding(
//                             padding: const EdgeInsets.all(4.0),
//                             child: Row(
//                               children: <Widget>[
//                                 Text("Sort"),
//                                 SizedBox(
//                                   width: 5,
//                                 ),
//                                 Icon(
//                                   Icons.sort,
//                                   color: Colors.black,
//                                 )
//                               ],
//                             ),
//                           ),
//                         )
//                       ],
//                     ),
//                   )
                            notFound
                                ? SingleChildScrollView(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.min,
                                        children: <Widget>[
                                          Image.asset(
                                            'drawables/no_result_found.png',
                                            height: 206,
                                            width: 172,
                                          ),
                                          Text(
                                            "Oops!!!",
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          Text(
                                            "No result found.",
                                            style: TextStyle(fontSize: 20),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : showContainer
                                    ? Expanded(
                                        child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 8),
                                            child: GridView.builder(
                                                addAutomaticKeepAlives: true,
                                                itemCount:
                                                    productList.data.length,
                                                shrinkWrap: true,
                                                padding: EdgeInsets.only(
                                                    top: 8, bottom: 120),
                                                physics:
                                                    AlwaysScrollableScrollPhysics(),
                                                gridDelegate:
                                                    SliverGridDelegateWithFixedCrossAxisCount(
                                                        crossAxisCount: 2,
                                                        childAspectRatio:
                                                            MediaQuery.of(context)
                                                                        .size
                                                                        .width <
                                                                    375
                                                                ? 0.85
                                                                : 0.95,
                                                        crossAxisSpacing: 8,
                                                        mainAxisSpacing: 8),
                                                itemBuilder: (context, index) {
                                                  return MealCard(
                                                      UniqueKey(),
                                                      productList.data[index]);
                                                })),
                                        // child: ListView.builder(
                                        //     padding:
                                        //         EdgeInsets.only(bottom: 120),
                                        //     itemCount: product.data.length,
                                        //     itemBuilder: (context, index) {
                                        //       return ProductCard(product.data[index]);
                                        //     }),
                                      )
                                    : SizedBox()
                          ],
                        ),
                      ),
            neoLoaderStack()
          ],
        ),
        floatingActionButton: Stack(
          fit: StackFit.loose,
          children: <Widget>[
            FloatingActionButton(
              elevation: 8.0,
              heroTag: 'cart',
              tooltip: 'Cart',
              backgroundColor: mColor,
              onPressed: () async {
                await Get.to(checkout());
                productByCategoryState.getProduct();
                getShoppingListCount();
                // onBackPressed();
//                  offerState.getComboOffers();
              },
              child: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
            ),
            v.cartCount.value == null || v.cartCount.value < 1
                ? SizedBox()
                : Positioned(
                    right: 0,
                    top: 0,
                    child: AnimatedOpacity(
                      opacity: v.cartCount.value == 0 ? 0 : 1.0,
                      duration: Duration(milliseconds: 0),
                      child: Container(
                        padding: EdgeInsets.all(2),
                        decoration: new BoxDecoration(
                          color: Colors.white,
                          border: Border.all(color: mColor),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        constraints: BoxConstraints(
                          minWidth: 20,
                          minHeight: 20,
                        ),
                        child: v.cartCount.value == 0
                            ? SizedBox()
                            : Obx(() => Text(
                                  '${v.cartCount.value}',
                                  style: TextStyle(
                                    color: mColor,
                                    fontSize: 12,
                                  ),
                                  textAlign: TextAlign.center,
                                )),
                      ),
                    )),
          ],
        ),
      ),
    );
  }
}

class ProductCardByCat extends StatefulWidget {
  DataP data;

  ProductCardByCat(Key key, this.data) : super(key: key);

  @override
  _ProductCardByCatState createState() => _ProductCardByCatState();
}

class _ProductCardByCatState extends State<ProductCardByCat> {
  bool showAddButton = true;
  HttpRequests requests = HttpRequests();
  int itemCount = 0, totalLeft = 0;

  increment() {
    if (itemCount != totalLeft) {
      itemCount++;
      showAddButton = false;
      addItemToCart(itemCount);
    }
  }

  decrement() {
    if (itemCount <= 1) {
      setState(() {
        itemCount--;
      });

      print("$itemCount");
      addItemToCart(itemCount);
    } else {
      itemCount--;
      addItemToCart(itemCount);
    }
  }

  addItemToCart(int qty) async {
    print("F $qty");
    var response = await requests.addToCart(widget.data.masterProductId,
        widget.data.merchantproductList, "", "", qty);

    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      setState(() {
        print("CART UPDATED!");
        getShoppingListCount();
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    itemCount = widget.data.shoppingListQty;
    if (itemCount > 0) showAddButton = false;
    totalLeft = widget.data.totalLeft;
    // print("${widget.data.productName} | ${widget.data.sellingPrice} | ${widget.data.shoppingListQty}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () async {
        await Get.to(
            ProductDetailPage(widget.data.merchantproductList.toString()));

        productByCategoryState.getProduct();
        // dashboardState.getProducts();
      },
      child: Container(
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.withOpacity(.4), width: .5),
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(18))),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(18.0),
                child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.transparent, width: 0),
                    color: Colors.white,
                  ),
                  child: CachedNetworkImage(
                    placeholder: (context, _) =>
                        Image.asset('assets/default-image.jpg'),
                    imageUrl: widget.data.productImage,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.all(4),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "${widget.data.productName}",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "₹ ${widget.data.sellingPrice}",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: textStyleLargeColor,
                    ),
                    Row(
                      children: <Widget>[

                        Expanded(
                          child: GestureDetector(
                            onTap: () {
                              increment();
                            },
                            child: Container(
                              height: 22,
                              // width: double.infinity,
                              decoration: showAddButton || itemCount == 0
                                  ? BoxDecoration(
                                      color: mColor,
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(12)))
                                  : null,
                              child: showAddButton || itemCount == 0
                                  ? Container(
                                      height: 22,
                                      width: double.infinity,
                                      decoration: BoxDecoration(
                                          color: mColor,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(12))),
                                      child: Center(
                                          child: Text(
                                        "ADD",
                                        style: TextStyle(
                                            fontSize: 11, color: Colors.white),
                                      )),
                                    )
                                  : Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: GestureDetector(
                                            onTap: () {
                                              decrement();
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: mColor,
                                              ),
                                              child: Center(
                                                child: Text("-",
                                                    style: TextStyle(
                                                        color: Colors.white)),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          flex: 2,
                                          child:
                                              Center(child: Text("$itemCount")),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: GestureDetector(
                                            onTap: () {
                                              increment();
                                            },
                                            child: Container(
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: mColor,
                                              ),
                                              child: Center(
                                                child: Text(
                                                  "+",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                            ),
                          ),
                          // child: Container(
                          //   height: 22,
                          //   width: double.infinity,
                          //   decoration: BoxDecoration(
                          //       color: mColor,
                          //       borderRadius: BorderRadius.all(Radius.circular(12))
                          //   ),
                          //   child: Center(child: Text("ADD", style: TextStyle(fontSize: 11, color: Colors.white),)),
                          // ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
