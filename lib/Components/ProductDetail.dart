import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:userapp/Getters/GetProductDetail.dart';
import 'package:userapp/constants.dart';

import '../Network/httpRequests.dart';
import '../Network/httpRequests.dart';
import 'CommonUtility.dart';
import 'CommonUtility.dart';
import 'CommonUtility.dart';
import 'LoaderComponents.dart';
import 'LoaderComponents.dart';

class ProductDetailPage extends StatefulWidget {
  String merchantListId;

  ProductDetailPage(this.merchantListId);

  @override
  _ProductDetailPageState createState() => _ProductDetailPageState();
}

class _ProductDetailPageState extends State<ProductDetailPage> {
  bool showAddButton = true, loading = true, adding = false;
  HttpRequests requests = HttpRequests();
  ProductDetail productDetail;
  int itemCount = 0, totalLeft = 0;

  increment() {
    print("IN");
    if (itemCount != totalLeft) {
      itemCount++;
      showAddButton = false;
      addItemToCart(itemCount);
    } else {
      showToast("Out of stock!", mColor);
      print("ELSE");
    }
  }

  decrement() {
    if (itemCount <= 1) {
      setState(() {
        itemCount--;
      });
      showAddButton = true;
      print("$itemCount");
      addItemToCart(itemCount);
    } else {
      itemCount--;
      addItemToCart(itemCount);
    }
  }

  addItemToCart(int qty) async {
    print("F $qty");
    setState(() {
      adding = true;
    });
    var response = await requests.addToCart(
        productDetail.data[0].masterProductId,
        productDetail.data[0].merchantproductList,
        "",
        "",
        qty);

    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      setState(() {
        adding = false;
        print("CART UPDATED!");
        getShoppingListCount();
      });
    } else {
      setState(() {
        adding = false;
        getShoppingListCount();
      });
    }
  }

  // onTapAdd() {
  //   print("tap");
  //   setState(() {
  //     showAddButton = showAddButton ? false : true;
  //   });
  // }

  getProductDetail() async {
    var response = await requests.getProductDetail(widget.merchantListId);

    productDetail = ProductDetail.fromJson(response);
    if (response != null &&
        response['message'].toString().toLowerCase() == "success") {
      print("RESPONSE ->> $response");
      setState(() {
        itemCount = productDetail.data[0].shoppingListQty;
        if (itemCount > 0) showAddButton = false;
        totalLeft = productDetail.data[0].totalLeft;


        loading = false;
      });
    } else {
      setState(() {
        loading = false;
      });
    }
  }

  initValues() {}

  @override
  void initState() {
    // TODO: implement initState
    getProductDetail();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.only(bottom: 100),
        child: Stack(
          children: <Widget>[
            Container(
                height: 170 + MediaQuery
                    .of(context)
                    .padding
                    .top,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: mColor,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                child: loading ? SizedBox() : CarouselSlider(
                  height: 200,
                  initialPage: 0,
                  enlargeCenterPage: true,
                  pauseAutoPlayOnTouch: Duration(seconds: 5),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  autoPlay: productDetail.data[0].productImage.length == 1
                      ? false
                      : true,
                  autoPlayInterval: Duration(seconds: 3),
                  enableInfiniteScroll: true,
                  items: productDetail.data[0].productImage.map((e) =>
                      ClipRRect(
                        borderRadius: BorderRadius.circular(8),
                        child: CachedNetworkImage(
                          placeholder: (context, _) =>
                              Image.asset('assets/default-image.jpg'),
                          imageUrl: e.imagePath,
                          fit: BoxFit.contain,
                        ),
                      )).toList(),
                )),
            Positioned(
                top: 50,
                left: 20,
                child: GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Icon(
                      Icons.arrow_back,
                      color: Colors.white,
                    ))),
            Padding(
              padding: EdgeInsets.only(
                  top: 140 + MediaQuery.of(context).padding.top),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 22),
                        height: 160,
                        decoration: BoxDecoration(
                            border:
                            Border.all(color: Colors.grey.withOpacity(.4)),
                            color: Colors.white,
                            borderRadius:
                            BorderRadius.all(Radius.circular(25))),
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 8),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                flex: 3,
                                child: loading ? listTileLoader() : Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Text(
                                      "${productDetail.data[0].productName}",
                                      style: TextStyle(
                                          fontSize: 15, color: mColor),
                                    ),
                                    Row(
                                      children: <Widget>[

                                        Text(
                                          "₹ ${productDetail.data[0]
                                              .sellingPrice}",
                                          style: TextStyle(fontSize: 13),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                            child: Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              children: <Widget>[
                                                // Row(
                                                //   children: <Widget>[
                                                //     Image.asset(
                                                //       'assets/offer.png',
                                                //       height: 15,
                                                //     ),
                                                //     Text(
                                                //       " 50% off",
                                                //       style:
                                                //       smallTextStyleBlack,
                                                //     ),
                                                //     Text(
                                                //       " | Use code IAMFIT",
                                                //       style:
                                                //       smallTextStyleBlack,
                                                //     ),
                                                //   ],
                                                // ),
                                                // Text(
                                                //   "view details",
                                                //   style: TextStyle(
                                                //       fontSize: 11,
                                                //       color: Colors.blue),
                                                // )
                                              ],
                                            ),
                                          ),
                                        ),
                                        adding ? SizedBox(
                                            height: 25,
                                            child: SpinKitThreeBounce(
                                              color: mColor,
                                              size: 25,
                                            )
                                        ) : Expanded(
                                          flex: 1,
                                          child: totalLeft == 0 ?
                                          Container(
                                            child: Text("OUT OF STOCK",
                                              style: TextStyle(fontSize: 12,
                                                  color: Colors.redAccent,
                                                  fontWeight: FontWeight
                                                      .bold),),
                                          ) :
                                          GestureDetector(
                                            onTap: () {
                                              print("TAP");
                                              increment();
                                            },
                                            child: Container(
                                              height: 35,
                                              // width: double.infinity,
                                              decoration: showAddButton
                                                  ? BoxDecoration(
                                                  color: mColor,
                                                  borderRadius:
                                                  BorderRadius.all(
                                                      Radius.circular(
                                                          12)))
                                                  : null,
                                              child: showAddButton
                                                  ? Container(
                                                height: 35,
                                                width: double.infinity,
                                                decoration: BoxDecoration(
                                                    color: mColor,
                                                    borderRadius:
                                                    BorderRadius.all(
                                                        Radius
                                                            .circular(
                                                            12))),
                                                child: Center(
                                                    child: Text(
                                                      "ADD",
                                                      style: TextStyle(
                                                          fontSize: 11,
                                                          color:
                                                          Colors.white),
                                                    )),
                                              )
                                                  : Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 1,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        decrement();
                                                      },
                                                      child: Container(
                                                        decoration:
                                                        BoxDecoration(
                                                          shape:
                                                          BoxShape.circle,
                                                          color: mColor,
                                                        ),
                                                        child: Center(
                                                          child: Text("-",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white)),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 2,
                                                    child: Center(
                                                        child: Text(
                                                            "$itemCount")),
                                                  ),
                                                  Expanded(
                                                    flex: 1,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        increment();
                                                      },
                                                      child: Container(
                                                        decoration:
                                                        BoxDecoration(
                                                          shape:
                                                          BoxShape.circle,
                                                          color: mColor,
                                                        ),
                                                        child: Center(
                                                          child: Text(
                                                            "+",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          // child: Container(
                                          //   height: 22,
                                          //   width: double.infinity,
                                          //   decoration: BoxDecoration(
                                          //       color: mColor,
                                          //       borderRadius: BorderRadius.all(Radius.circular(12))
                                          //   ),
                                          //   child: Center(child: Text("ADD", style: TextStyle(fontSize: 11, color: Colors.white),)),
                                          // ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        top: -30,
                        left: 40,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(12.0),
                          child: Container(
                            height: 60,
                            width: 60,
                            decoration: BoxDecoration(
                              // border: Border.all(color: mColor),
                              color: Colors.white,
                              // borderRadius:
                              //     BorderRadius.all(Radius.circular(12))
                            ),
                            child: CachedNetworkImage(
                              placeholder: (context, _) =>
                                  Image.asset('assets/default-image.jpg'),
                              imageUrl: STORE_INFO.info[0].logo,
                              fit: BoxFit.cover,),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  loading ? Column(
                    children: <Widget>[
                      listTileLoader(),
                      multipleListTileLoader(),
                      multipleListTileLoader(),
                    ],
                  ) : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[

                  ListTile(
                  title: Text(
                    "Description:\n",
                    style: TextStyle(fontSize: 18),
                  ),
              subtitle: Text("${productDetail.data[0].productDescr}"),
            ),
      Divider(),
      Card(
        child: Column(
            children: <Widget>[
        Row(
        children: <Widget>[
        Expanded(
        child: ListTile(
        title: Text("Company Name"),
        subtitle:
        Text("${productDetail.data[0].manufacture}"),
      ),
    ),
    Expanded(
    child: ListTile(
    title: Text(
    "Sub Category",
    textAlign: TextAlign.end,
    ),
    subtitle: Text(
    "${productDetail.data[0].subcatName}",
    textAlign: TextAlign.right,
    ),
    ),
    ),
    ],
    ),
    Row(
    children: <Widget>[
    Expanded(
    child: ListTile(
    title: Text("Parent Category"),
    subtitle: Text("${productDetail.data[0].catName}"),
    ),
    ),
    Expanded(
    child: ListTile(
    title: Text(
    "MRP",
    textAlign: TextAlign.end,
    ),
    subtitle: Text(
    "₹ ${productDetail.data[0].mrp}",
    textAlign: TextAlign.right,
    ),
    ),
    ),
    ],
    ),
    Row(
    children: <Widget>[
    Expanded(
    child: ListTile(
    title: Text("Unit"),
    subtitle:
    Text("${productDetail.data[0].productUnit}"),
    ),
    ),
    Expanded(
    child: ListTile(
    title:
    Text("SRP", textAlign: TextAlign.end),
    subtitle: Text(
    "₹ ${productDetail.data[0].sellingPrice}",
    textAlign: TextAlign.right,
    ),
    ),
    )   ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ]),
    )]  )));
  }
}
