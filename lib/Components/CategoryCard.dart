import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/ProductByCategory.dart';
import 'package:userapp/Getters/GetStoreInfo.dart';

import 'CommonUtility.dart';
import 'CommonUtility.dart';

class CategoryCard extends StatefulWidget {
  List<ProductCategory> productCategories;

  CategoryCard(this.productCategories);

  @override
  _CategoryCardState createState() => _CategoryCardState();
}

class _CategoryCardState extends State<CategoryCard> {
  List<Color> _colors = [Colors.orangeAccent, Colors.lightBlueAccent, Colors.green,Colors.pinkAccent];

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(20))),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 0),
        child: Column(
          children: <Widget>[

            Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Categories",
                style:TextStyle(fontSize: 14, color: Colors.black,fontWeight: FontWeight.bold),
              ),
            ),

            SizedBox(
              height: 8,
            ),
            Expanded(
              child: Center(
                child: ListView.separated(
                    shrinkWrap: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        width: 0,
                      );
                    },
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.productCategories.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          print("category name"+widget.productCategories[index].catName);
                          Get.to(ProductByCategory(
                              widget.productCategories[index]));
                        },
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                          elevation: 5,
                          child:Container (
                              color: _colors[index % 4],
                      child:
                            Padding(
                              padding: EdgeInsets.all(10),
                            
                            child:Row(
                          children: <Widget>[
                            Text(
                              "${widget.productCategories[index].catName}",
                              style: TextStyle(
                                  fontSize: 11, color: Colors.black),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Container(
                              height: 40,
                              width: 40,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white
                                // border:
                                //     Border.all(color: Colors.grey.withOpacity(.4)),
                                // color: mColor.withOpacity(.4),
                              ),
                              child: CachedNetworkImage(
                                height: 30,
                                width: 30,
                                placeholder: (context, _) =>
                                    Image.asset('assets/default-image.jpg'),
                                imageUrl:
                                    widget.productCategories[index].catImage,
                                fit: BoxFit.contain,
                              ),
                            ),


                          ],
                        ),
                        ) ) ));
                    }),
              ),
            ),
          ],
         ),
      ),
    );
  }
}
