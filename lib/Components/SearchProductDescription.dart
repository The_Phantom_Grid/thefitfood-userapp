import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:userapp/Getters/GetNearbyStores.dart';
import 'package:userapp/Getters/GetProducts.dart';
import 'package:userapp/Getters/GetSearchProduct.dart';
import 'package:userapp/Getters/GetUser.dart';

import '../Getters/GetStoreDetails.dart';
import '../Screens/MyCart.dart';
import '../Screens/StoreDetail.dart';
import '../Network/httpRequests.dart';
import 'CommonUtility.dart';

class SearchProductDescription extends StatefulWidget {
  User user;
  SearchProductData productData;
  int _itemCount;
  String storeId;
  String merchantId;

  SearchProductDescription(this.user, this.productData, this._itemCount,
      this.storeId, this.merchantId);

  @override
  _SearchProductDescriptionState createState() =>
      _SearchProductDescriptionState();
}

class _SearchProductDescriptionState extends State<SearchProductDescription> {
  var mColor = Color(0xFF3dad00);

//  var mAccentColor = Color(0xFFf82d70);
  bool addButton = true, loading = true;
  int itemCount, cartItem;
  var offer_id = "", offer_price = "";
  HttpRequests auth = HttpRequests();
  StoreDetails storeDetails;

  addToCart(itemCount) async {
    await auth.addItemToCart(
        widget.productData.masterProductId,
        widget.productData.merchantproductList,
        offer_id,
        offer_price,
        itemCount,
        widget.merchantId,
        widget.storeId,
        widget.user.data.loginDetails.userId,
        widget.user.data.loginDetails.authKey);

    getShoppingListCount();
  }

  addItem() {
    if (itemCount == widget.productData.totalLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color: mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color: mColor),
        ),
      )..show(context);
    } else {
      setState(() {
        itemCount++;
      });
      addToCart(itemCount);
    }
  }

  getShoppingListCount() async {
    var r =
        await auth.getShoppingListCount(widget.user.data.loginDetails.authKey);
    setState(() {
      cartItem = r['data'];
      loading = false;
    });
  }

//  getStoreDetails() async {
//    var r = await auth.getStoreDetail(
//        widget.storeId, widget.user.data.loginDetails.authKey);
//    setState(() {
//      var jData = json.decode(r.body);
//      storeDetails = StoreDetails.fromJson(jData);
//      setState(() {
//        loading = false;
//      });
//    });
//  }

  @override
  void initState() {
    // TODO: implement initState
    itemCount = widget._itemCount;
    print("ITEMLEFT: ${widget.productData.totalLeft}");
    if (itemCount != 0) addButton = false;
    getShoppingListCount();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              color: Colors.black,
            ),
            centerTitle: true,
            backgroundColor: mColor,
            title: Text(
              "${widget.productData.storename}",
              style: TextStyle(color: Colors.white),
            ),
            actions: <Widget>[
              Center(
                  child: Padding(
                padding: EdgeInsets.only(right: 12.0),
                child: Container(
                  height: 40,
                  width: 40,
                  padding: EdgeInsets.all(1.0),
                  decoration: BoxDecoration(shape: BoxShape.circle),
                  child: GestureDetector(
                    onTap: () async {
                      await Navigator.push(context,
                          MaterialPageRoute(builder: (context) => MyCart()));
                      getShoppingListCount();
                    },
                    child: Stack(
                      children: <Widget>[
                        Container(
                            child: Icon(
                          Icons.shopping_cart,
                          color: Colors.white,
                          size: 35,
                        )),
                        Positioned(
                            top: 5,
                            right: 15,
                            child: cartItem == null
                                ? SizedBox()
                                : Text(
                                    "$cartItem",
                                    style: TextStyle(
                                        color: mColor,
                                        fontSize: 11,
                                        fontWeight: FontWeight.bold),
                                  ))
                      ],
                    ),
                  ),
                ),
              ))
            ]),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Card(
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.only(left: 2, right: 2),
                          child: CarouselSlider(
                            height: 200,
                            initialPage: 0,
                            enlargeCenterPage: true,
                            pauseAutoPlayOnTouch: Duration(seconds: 5),
                            autoPlayAnimationDuration:
                                Duration(milliseconds: 800),
                            autoPlayCurve: Curves.fastOutSlowIn,
                            autoPlay: true,
                            autoPlayInterval: Duration(seconds: 3),
                            enableInfiniteScroll: true,
                            items: <Widget>[
                              Image.network(widget.productData.productImage)
                            ],
                          ),
                        ),
                      ),
                      Divider(),
                      Card(
                          elevation: 0.0,
                          child: ListTile(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          StoreDetail(widget.storeId)));
                            },
                            title: Text("Seller:"),
                            subtitle: Text(
                              "${widget.productData.storename}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  color: Colors.blue,
                                  decoration: TextDecoration.underline),
                            ),
                          )),
                      Card(
                        elevation: 0.0,
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                "${widget.productData.productName}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Divider(),
                            ListTile(
                              title: Text(
                                "Description:\n",
                                style: TextStyle(fontSize: 18),
                              ),
                              subtitle:
                                  Text("${widget.productData.productDescr}"),
                            ),
                            Divider(),
                            Card(
                              elevation: 0.0,
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: ListTile(
                                          title: Text("Company Name"),
                                          subtitle: Text(
                                              "${widget.productData.manufacturer}"),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListTile(
                                          title: Text(
                                            "Sub Category",
                                            textAlign: TextAlign.end,
                                          ),
                                          subtitle: Text(
                                            "${widget.productData.subcatName}",
                                            textAlign: TextAlign.right,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: ListTile(
                                          title: Text("Parent Category"),
                                          subtitle: Text(
                                              "${widget.productData.catName}"),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListTile(
                                          title: Text(
                                            "MRP",
                                            textAlign: TextAlign.end,
                                          ),
                                          subtitle: Text(
                                            "₹ ${widget.productData.mrp}",
                                            textAlign: TextAlign.right,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: ListTile(
                                          title: Text("Unit"),
                                          subtitle: Text(
                                              "₹ ${widget.productData.productUnit}"),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
        bottomNavigationBar: SafeArea(
          child: Stack(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    color: mColor,
                    onPressed: () {
                      setState(() {
                        if (itemCount <= 1) {
                          addButton = true;
                          itemCount--;
                          addToCart(itemCount);
                        } else
                          itemCount--;
                        addToCart(itemCount);
                      });
                    },
                    child: Icon(
                      Icons.remove,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "${itemCount}",
                    style: TextStyle(color: Colors.black),
                  ),
                  FlatButton(
                    color: mColor,
                    onPressed: () {
                      addItem();
                    },
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
              Visibility(
                visible: addButton,
                child: ButtonTheme(
                  minWidth: double.infinity,
                  height: 50.0,
                  child: RaisedButton(
                    disabledColor: mColor,
                    disabledTextColor: Colors.white,
                    child: widget.productData.totalLeft == 0
                        ? Text("Out Of Stock")
                        : Text("Add to Cart"),
                    onPressed: widget.productData.totalLeft == 0
                        ? null
                        : () {
                            setState(() {
                              addButton = false;
                              addItem();
                            });
                          },
                    textColor: Colors.white,
                    color: mColor,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
