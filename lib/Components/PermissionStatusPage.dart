import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:userapp/Components/CommonUtility.dart';

class PermissionStatusPage extends StatefulWidget {
  String permissionName;

  PermissionStatusPage(this.permissionName);

  @override
  _PermissionStatusPageState createState() => _PermissionStatusPageState();
}

class _PermissionStatusPageState extends State<PermissionStatusPage> {
  Widget locationAccess() {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(32.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Can't access your ${widget.permissionName}",
                  style: TextStyle(fontSize: 13, fontWeight: FontWeight.bold)),
              SizedBox(
                height: 15,
              ),
              Text(
                "In order to access your ${widget.permissionName} you must provide ${widget.permissionName} permission to application and try again.",
                style: mediumTextStyleBlack,
              ),
              Text(
                "Goto Setting > NeoMart Seller > ${widget.permissionName}",
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 15,
              ),
              Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: FlatButton(
                      onPressed: () async {
                        openAppSettings();
                      },
                      child: Text("Open Settings"),
                      textColor: Colors.white,
                      color: mColor,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: locationAccess(),
    );
  }
}
