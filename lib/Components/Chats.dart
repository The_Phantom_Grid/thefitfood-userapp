import 'dart:convert';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:userapp/Getters/GetChatMessageData.dart';
import 'package:userapp/Getters/GetMessageData.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/ChatScreen.dart';

import '../constants.dart';

class Chats extends StatefulWidget {
  User user;

  Chats(this.user);

  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<Chats> {
  var mColor = Color(0xFF3dad00);
  TextEditingController chatController = TextEditingController();
  String userName, storeName, lastMessage;
  int msgCounter, time;
  List item = [], messageList = [];
  bool loading = true, chatAvail = true;

//  final dbInstance =  FirebaseDatabase.instance;
  DatabaseReference dbRef, userDbRef;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userName = widget.user.data.loginDetails.userId.toString();
    dbRef = FirebaseDatabase.instance
        .reference()
        .child("mesageUsers")
        .child("merchant");
//    userDbRef = FirebaseDatabase.instance.reference().child("mesageUsers").child("user").child(userName);
    initValues();
  }

  initValues() async {
    setState(() {
      loading = true;
      chatAvail = true;
      item.clear();
    });
    await dbRef.once().then((value) {
      Map mapOfmaps = value.value;
      mapOfmaps.forEach((key, value) {
        item.add(jsonEncode(value));
//        print(item);
      });
    });
    print(item);
    filterList();
  }

  filterList() {
    setState(() {
      messageList.clear();
    });
    for (int x = 0; x < item.length; x++) {
      if (json.decode(item[x].toString()).containsKey(userName)) {
        messageList.add(jsonDecode(item[x]));
      }
    }
    if (messageList.length == 0 && messageList.isEmpty) {
      setState(() {
        loading = false;
        chatAvail = true;
      });
    } else {
      setState(() {
        messageList.sort((x, y) => int.parse(
                x[userName]['messageData']['chatTime'].toString())
            .compareTo(
                int.parse(y[userName]['messageData']['chatTime'].toString())));
        messageList = messageList.reversed.toList();
        loading = false;
        chatAvail = false;
      });
    }
  }

  sendData(int index) async {
    String merchantId,
        storeId,
        gTotal,
        totalItems,
        imageUrl,
        logo,
        deliveryCharge,
        subTotal,
        phone,
        orderId,
        location,
        storeName,
        chatDate,
        chatTime,
        msg;
    merchantId =
        messageList[index][userName]['messageData']['merchantId'].toString();
    storeId = messageList[index][userName]['messageData']['storeId'].toString();
    gTotal = messageList[index][userName]['messageData']['grandTotalAmount']
        .toString();
    totalItems =
        messageList[index][userName]['messageData']['totalItem'].toString();
    imageUrl =
        messageList[index][userName]['messageData']['imageUrl'].toString();
    logo = messageList[index][userName]['messageData']['storeLogo'].toString();
    deliveryCharge = messageList[index][userName]['messageData']
            ['deliveryCharges']
        .toString();
    subTotal =
        messageList[index][userName]['messageData']['subTotal'].toString();
    phone = messageList[index][userName]['messageData']['mobile'].toString();
    location =
        messageList[index][userName]['messageData']['address'].toString();
    storeName =
        messageList[index][userName]['messageData']['storeName'].toString();
    orderId = messageList[index][userName]['messageData']['orderId'].toString();
    chatDate =
        messageList[index][userName]['messageData']['chatDate'].toString();
    chatTime =
        messageList[index][userName]['messageData']['chatTime'].toString();
    msg = messageList[index][userName]['messageData']['message'].toString();

    print("message $msg");

    Map<String, dynamic> data = {
      'user': widget.user,
      'merchantId': merchantId,
      'storeId': storeId,
      'gTotal': gTotal,
      'totalItems': totalItems,
      'imageUrl': imageUrl,
      'logo': logo,
      'deliveryCharge': deliveryCharge,
      'subTotal': subTotal,
      'phone': phone,
      'location': location,
      'storeName': storeName,
      'orderId': orderId,
      'chatDate': chatDate,
      'chatTime': chatTime,
      'msg': msg
    };

    await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChatScreen(data, "Chats", widget.user)));
    initValues();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: mColor,
          leading: IconButton(
            onPressed: () {
//            initValues();
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          ),
          title: Text(
            'Chats',
            style: TextStyle(color: Colors.white),
          ),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                initValues();
              },
              icon: Icon(Icons.refresh, color: Colors.white),
            )
          ],
        ),
        body: loading
            ? neoLoader()
            : chatAvail
                ? Center(
                    child: Text(
                      "No Chats Available",
                      style: TextStyle(fontSize: 17),
                    ),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(12),
                        child: Text(
                          "Chats (${messageList.length})",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 17),
                        ),
                      ),
                      Divider(
                        height: 0,
                      ),
                      Expanded(
                        child: ListView.separated(
                          separatorBuilder: (context, index) {
                            return Divider(
                              height: 0,
                              indent: 80,
                              endIndent: 10,
                            );
                          },
//              padding: EdgeInsets.symmetric(vertical: 12),
                          itemCount: messageList.length,
                          itemBuilder: (context, index) {
                            return InkWell(
                              onTap: () {
                                print(
                                    "MSGCOUNT USER>>> ${messageList[index][userName]['messageData']['msgCounter']}");
                                sendData(index);
                              },
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    height: 100,
                                    child: ListTile(
                                      leading: ClipOval(
                                        child: Image.network(
                                          messageList[index][userName]
                                              ['messageData']['storeLogo'],
                                          height: 55,
                                          width: 55,
                                        ),
                                      ),
                                      title: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(
                                            messageList[index][userName]
                                                ['messageData']['storeName'],
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(
                                            "Order Id: ${messageList[index][userName]['messageData']['orderId']}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13),
                                          ),
                                        ],
                                      ),
                                      subtitle: Text(
                                        messageList[index][userName]
                                            ['messageData']['message'],
                                        maxLines: 1,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      trailing: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: <Widget>[
                                          Text(
                                            "Total Items: ${messageList[index][userName]['messageData']['totalItem']}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 13),
                                          ),
                                          messageList[index][userName]
                                                              ['messageData']
                                                          ['msgCounter'] ==
                                                      "0" ||
                                                  messageList[index][userName]
                                                              ['messageData']
                                                          ['userUid']
                                                      .toString()
                                                      .contains("$userName")
                                              ? SizedBox()
                                              : ClipOval(
                                                  child: Container(
                                                      alignment:
                                                          Alignment.center,
                                                      height: 20,
                                                      width: 20,
                                                      color: mColor,
                                                      child: Text(
                                                        messageList[index]
                                                                    [userName]
                                                                ['messageData']
                                                            ['msgCounter'],
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 12),
                                                      ))),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                      bottom: 16,
                                      right: 16,
                                      child: Text(
                                        DateFormat('hh:mm a').format(
                                            DateTime.fromMillisecondsSinceEpoch(
                                                int.parse(messageList[index]
                                                            [userName]
                                                        ['messageData']
                                                    ['chatTime']))),
                                        style: TextStyle(color: mColor),
                                      ))
                                ],
                              ),
                            );
                          },
                        ),
                      ),
//        StreamBuilder<DataSnapshot>(
//          stream: dbRef.once().asStream(),
//          builder: (context, snapshot){
//            if(snapshot.hasData && !snapshot.hasError && snapshot.data.value != null) {
//              Map mapOfmaps = snapshot.data.value;
//              mapOfmaps.forEach((key, value) {
//                item.add(jsonEncode(value));
//              });
////              filterList();
//              messageList.clear();
//              for(int x = 0; x < item.length; x++){
//                if(json.decode(item[x].toString()).containsKey(userName)) {
//                  print("YESSSS");
//                  messageList.add(jsonDecode(item[x]));
//                }
//              }
//              print(messageList.length);
//              return Expanded(
//                child: ListView.separated(
//                  separatorBuilder: (context, index) {
//                    return Divider(
//                      height: 0,
//                      indent: 80,
//                      endIndent: 10,
//                    );
//                  },
////              padding: EdgeInsets.symmetric(vertical: 12),
//                  itemCount: messageList.length,
//                  itemBuilder: (context, index){
//                    return InkWell(
//                      onTap: (){
//                        sendData(index);
//                      },
//                      child: Stack(
//                        children: <Widget>[
//                          Container(
//                            height: 100,
//                            child: ListTile(
//                              leading: ClipOval(
//                                child: Image.network(messageList[index][userName]['messageData']['storeLogo'],
//                                  height: 55,
//                                  width: 55,
//                                ),
//                              ),
//                              title: Text(messageList[index][userName]['messageData']['storeName'],
//                                maxLines: 1,
//                                overflow: TextOverflow.ellipsis,
//                                style: TextStyle(fontWeight: FontWeight.bold),
//                              ),
//                              subtitle: Text(messageList[index][userName]['messageData']['message'],
//                                maxLines: 1,
//                                overflow: TextOverflow.ellipsis,
//                              ),
//                              trailing: ClipOval(
//                                  child: Container(
//                                      alignment: Alignment.center,
//                                      height: 25,
//                                      width: 25,
//                                      color: mColor,
//                                      child: Text(messageList[index][userName]['messageData']['msgCounter'],
//                                        style: TextStyle(color: Colors.white),
//                                      ))),
//                            ),
//                          ),
//                          Positioned(
//                              bottom: 16,
//                              right: 16,
//                              child: Text(DateFormat('hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(int.parse(messageList[index][userName]['messageData']['chatTime']))),
//                                style: TextStyle(color: mColor),
//                              )
//                          )
//                        ],
//                      ),
//                    );
//                  },
//                ),
//              );
//            }else if(snapshot.hasError){
//              return Center(child: Text(snapshot.error.toString()));
//            }else if(!snapshot.hasData){
//              return Center(
//                child: Image.asset('drawables/neomart_loader.gif', height: 50,),
//              );
//            }else{
//              return Center(child: Text("No Chats Available",
//                style: TextStyle(fontSize: 19),
//              ),);
//            }
//          }
//        ),
                    ],
                  ));
  }
}
