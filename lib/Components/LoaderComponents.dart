import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import 'CommonUtility.dart';
import 'MealCard.dart';

Widget productLoader() {
  return Shimmer.fromColors(
    baseColor: Colors.grey[300],
    highlightColor: Colors.grey[100],
    enabled: true,
    child: GridView.builder(
        addAutomaticKeepAlives: true,
        itemCount: 2,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 0.95,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8),
        itemBuilder: (context, index) {
          return Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey[400]),
                color: Colors.black26,
                borderRadius: BorderRadius.all(Radius.circular(18))),
          );
        }),
  );
}

Widget categoryLoader() {
  return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      enabled: true,
      child: Container(
        padding: EdgeInsets.all(8),
        height: 120,
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey.withOpacity(.4)),
            borderRadius: BorderRadius.all(Radius.circular(30))),
        child: ListView.separated(
            addAutomaticKeepAlives: true,
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            padding: EdgeInsets.symmetric(horizontal: 16),
            separatorBuilder: (context, index) {
              return SizedBox(
                width: 16,
              );
            },
            scrollDirection: Axis.horizontal,
            itemCount: 4,
            itemBuilder: (context, index) {
              return Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.grey.withOpacity(.4)),
                  color: mColor.withOpacity(.4),
                ),
              );
            }),
      ));
}

Widget listTileLoader() {
  return Shimmer.fromColors(
    baseColor: Colors.grey[300],
    highlightColor: Colors.grey[100],
    enabled: true,
    child: Column(
      children: <Widget>[
        ListTile(
          leading: Container(
            height: 50,
            width: 50,
            color: Colors.black26,
          ),
          title: Container(
            height: 10,
            width: 50,
            color: Colors.black26,
          ),
          subtitle: Container(
            height: 10,
            width: 50,
            color: Colors.black26,
          ),
        ),
      ],
    ),
  );
}

Widget containerLoader() {
  return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      enabled: true,
      child: Container(
        height: 60,
        width: 60,
      ));
}

Widget multipleListTileLoader() {
  return Shimmer.fromColors(
    baseColor: Colors.grey[300],
    highlightColor: Colors.grey[100],
    enabled: true,
    child: Column(
      children: <Widget>[
        ListTile(
          leading: Container(
            height: 50,
            width: 50,
            color: Colors.black26,
          ),
          title: Container(
            height: 10,
            width: 50,
            color: Colors.black26,
          ),
          subtitle: Container(
            height: 10,
            width: 50,
            color: Colors.black26,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        ListTile(
          leading: Container(
            height: 100,
            width: 100,
            color: Colors.black26,
          ),
          title: Container(
            height: 10,
            width: 50,
            color: Colors.black26,
          ),
          subtitle: Container(
            height: 10,
            width: 50,
            color: Colors.black26,
          ),
        ),
      ],
    ),
  );
}
