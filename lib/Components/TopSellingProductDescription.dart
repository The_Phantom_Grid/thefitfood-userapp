import 'dart:convert';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:userapp/Getters/GetNearbyStores.dart';
import 'package:userapp/Getters/GetProducts.dart';
import 'package:userapp/Getters/GetSearchProduct.dart';
import 'package:userapp/Getters/GetTopSellingProducts.dart';
import 'package:userapp/Getters/GetUser.dart';

import '../Getters/GetStoreDetails.dart';
import '../Screens/MyCart.dart';
import '../Screens/StoreDetail.dart';
import '../Network/httpRequests.dart';
import 'CommonUtility.dart';

class TopSellingProductDescription extends StatefulWidget {
  User user;
  int _itemCount;
  String storeId;
  String merchantId;
  TopSellingProducts topSellingProduct;

  TopSellingProductDescription(this.user, this.topSellingProduct,
      this._itemCount, this.storeId, this.merchantId);

  @override
  _TopSellingProductDescriptionState createState() =>
      _TopSellingProductDescriptionState();
}

class _TopSellingProductDescriptionState
    extends State<TopSellingProductDescription> {

//  var mAccentColor = Color(0xFFf82d70);
  bool addButton = true, loading = true;
  int itemCount, cartItem;
  var offer_id = "", offer_price = "";
  HttpRequests auth = HttpRequests();
  StoreDetails storeDetails;

  addToCart(itemCount) async {
    await auth.addItemToCart(
        widget.topSellingProduct.masterproductid,
        widget.topSellingProduct.merchantProductListId,
        offer_id,
        offer_price,
        itemCount,
        widget.merchantId,
        widget.storeId,
        widget.user.data.loginDetails.userId,
        widget.user.data.loginDetails.authKey);

//    Fluttertoast.showToast(
//        msg: "Cart Updated",
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        timeInSecForIosWeb: 1,
//        backgroundColor: mColor,
//        textColor: Colors.white,
//        fontSize: 16.0
//    );

//    Flushbar(
//      message: "Cart updated",
//      duration: Duration(seconds: 1),
//      flushbarPosition: FlushbarPosition.BOTTOM,
//    )
//      ..show(context);
    getShoppingListCount();
  }

  addItem() {
    if (itemCount == widget.topSellingProduct.itemLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color:  mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color:  mColor),
        ),
      )..show(context);
    } else {
      setState(() {
        itemCount++;
      });
      addToCart(itemCount);
    }
  }

  getShoppingListCount() async {
    var r =
        await auth.getShoppingListCount(widget.user.data.loginDetails.authKey);
    setState(() {
      cartItem = r['data'];
      loading = false;
    });
  }

//  getStoreDetails() async {
//    var r = await auth.getStoreDetail(
//        widget.storeId, widget.user.data.loginDetails.authKey);
//    setState(() {
//      var jData = json.decode(r.body);
//      storeDetails = StoreDetails.fromJson(jData);
//      setState(() {
//        loading = false;
//      });
//    });
//  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    itemCount = widget._itemCount;
    if (itemCount != 0) addButton = false;
    getShoppingListCount();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
              color: Colors.black,
            ),
            centerTitle: true,
            backgroundColor: mColor,
            title: Text(
              "${widget.topSellingProduct.storeName}",
              style: TextStyle(color: Colors.white),
            ),
            actions: <Widget>[
              Center(
                  child: Padding(
                padding: EdgeInsets.only(right: 12.0),
                child: Container(
                  height: 40,
                  width: 40,
                  padding: EdgeInsets.all(1.0),
                  decoration: BoxDecoration(shape: BoxShape.circle),
                  child: GestureDetector(
                    onTap: () async {
                      await Navigator.push(context,
                          MaterialPageRoute(builder: (context) => MyCart()));
                      getShoppingListCount();
                    },
                    child: Stack(
                      children: <Widget>[
                        Container(
                            child: Icon(
                          Icons.shopping_cart,
                          color: Colors.white,
                          size: 35,
                        )),
                        Positioned(
                            top: 5,
                            right: 15,
                            child: cartItem == null
                                ? SizedBox()
                                : Text(
                                    "$cartItem",
                                    style: TextStyle(
                                        color: mColor,
                                        fontSize: 11,
                                        fontWeight: FontWeight.bold),
                                  ))
                      ],
                    ),
                  ),
                ),
              ))
            ]),
        body: loading
            ? Center(child: CircularProgressIndicator())
            : SingleChildScrollView(
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Card(
                        child: Container(
                          width: double.infinity,
                          padding: EdgeInsets.only(left: 2, right: 2),
                          child: CarouselSlider(
                            height: 200,
                            initialPage: 0,
                            enlargeCenterPage: true,
                            pauseAutoPlayOnTouch: Duration(seconds: 5),
                            autoPlayAnimationDuration:
                                Duration(milliseconds: 800),
                            autoPlayCurve: Curves.fastOutSlowIn,
                            autoPlay: true,
                            autoPlayInterval: Duration(seconds: 3),
                            enableInfiniteScroll: true,
                            items: <Widget>[
                              Image.network(
                                  widget.topSellingProduct.productImage)
                            ],
                          ),
                        ),
                      ),
                      Divider(),
                      Card(
                          elevation: 0.0,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text("Seller : ",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold)),
                                GestureDetector(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                StoreDetail(widget.storeId)));
                                  },
                                  child: Text(
                                    "${widget.topSellingProduct.storeName}",
                                    style: TextStyle(
                                        color: Colors.blue,
                                        decoration: TextDecoration.underline),
                                  ),
                                ),
                              ],
                            ),
                          )),
                      Card(
                        elevation: 0.0,
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              title: Text(
                                "${widget.topSellingProduct.productName}",
                                style: TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Divider(),
                            ListTile(
                              title: Text(
                                "Description:\n",
                                style: TextStyle(fontSize: 18),
                              ),
                              subtitle: Text(
                                  "${widget.topSellingProduct.productDescr}"),
                            ),
                            Divider(),
                            Card(
                              elevation: 0.0,
                              child: Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: ListTile(
                                          title: Text("Company Name"),
                                          subtitle: Text(
                                              "${widget.topSellingProduct.manufacture}"),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListTile(
                                          title: Text(
                                            "Sub Category",
                                            textAlign: TextAlign.end,
                                          ),
                                          subtitle: Text(
                                            "${widget.topSellingProduct.subcatName}",
                                            textAlign: TextAlign.right,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: ListTile(
                                          title: Text("Parent Category"),
                                          subtitle: Text(
                                              "${widget.topSellingProduct.catName}"),
                                        ),
                                      ),
                                      Expanded(
                                        child: ListTile(
                                          title: Text(
                                            "MRP",
                                            textAlign: TextAlign.end,
                                          ),
                                          subtitle: Text(
                                            "₹ ${widget.topSellingProduct.mrp}",
                                            textAlign: TextAlign.right,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: ListTile(
                                          title: Text("Unit"),
                                          subtitle: Text(
                                              "₹ ${widget.topSellingProduct.productUnit}"),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
        bottomNavigationBar: SafeArea(
          child: Stack(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  FlatButton(
                    color: mColor,
                    onPressed: () {
                      setState(() {
                        if (itemCount <= 1) {
                          addButton = true;
                          itemCount--;
                          addToCart(itemCount);
                        } else
                          itemCount--;
                        addToCart(itemCount);
                      });
                    },
                    child: Icon(
                      Icons.remove,
                      color: Colors.white,
                    ),
                  ),
                  Text(
                    "${itemCount}",
                    style: TextStyle(color: Colors.black),
                  ),
                  FlatButton(
                    color: mColor,
                    onPressed: () {
                      addItem();
                    },
                    child: Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
              Visibility(
                visible: addButton,
                child: ButtonTheme(
                  minWidth: double.infinity,
                  height: 50.0,
                  child: RaisedButton(
                    child: Text("Add to Cart"),
                    onPressed: () {
                      setState(() {
                        addButton = false;
                        addItem();
                      });
                    },
                    textColor: Colors.white,
                    color: mColor,
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
