import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Getters/GetProductDetail.dart';
import 'package:userapp/Getters/GetSearchProduct.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:userapp/Screens/MyCart.dart';
import 'package:userapp/checkout.dart';

import '../constants.dart';
import 'CommonUtility.dart';
import 'CommonUtility.dart';
import 'CommonUtility.dart';
import 'CommonUtility.dart';
import 'ProductDetail.dart';
import 'ProductDetail.dart';
import 'SearchProductDescription.dart';

_ProductSearchState productSearchState;

class ProductSearch extends StatefulWidget {
  @override
  _ProductSearchState createState() {
    productSearchState = _ProductSearchState();
    return productSearchState;
  }
}

class _ProductSearchState extends State<ProductSearch> {
  TextEditingController searchController = TextEditingController();
  HttpRequests auth = HttpRequests();
  SearchProduct product;
  bool loading = false,
      loadingCart = true,
      notFound = false,
      showContainer = false;
  bool showCloseIcon = false, showSearchContainer = true;
  String searchByValue = "Search for Products";
  List<String> searchBy = [
    "Search for Products",
    "Search for Brands",
  ];
  int sortValue = 1;

  int itemCount = 0;

  searchProduct() async {
    print("SEARCH HiT");
    setState(() {
      loading = true;
    });
    product = null;
    var r = await auth.searchProduct(STORE_ID, searchController.text, "");
    setState(() {
      product = SearchProduct.fromJson(r);
    });
    print(product.data.length);
    if (product.data.length == 0) {
      setState(() {
        print("${product.data.length} == $loading }} 0");
        loading = false;
        notFound = true;
        showContainer = false;
      });
    } else {
      setState(() {
        print("${product.data.length} == $loading");
        loading = false;
        notFound = false;
        showContainer = true;
      });
      selectSortValue(sortValue);
    }
  }

  selectSortValue(int val) {
    setState(() {
      sortValue = val;
    });
    switch (sortValue) {
      case 1:
        high2low();
        break;
      case 2:
        low2high();
        break;
    }
  }

  showSortFilter() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          "SORT",
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                      Divider(
                        height: 0,
                      ),
                      RadioListTile(
                        onChanged: (val) {
                          selectSortValue(val);
                          Navigator.pop(context);
                        },
                        value: 1,
                        groupValue: sortValue,
                        title: Text("Price  -  High to Low"),
                      ),
                      RadioListTile(
                        onChanged: (val) {
                          selectSortValue(val);
                          Navigator.pop(context);
                        },
                        value: 2,
                        groupValue: sortValue,
                        title: Text("Price  -  Low to High"),
                      ),
                    ],
                  ),
                );
              });
        });
  }

  low2high() {
    if (!notFound) {
      Comparator<SearchProductData> priceComparator = (a, b) =>
          double.tryParse(a.sellingPrice.toString())
              .compareTo(double.tryParse(b.sellingPrice.toString()));
      setState(() {
        product.data.sort(priceComparator);
      });
    }
  }

  high2low() {
    if (!notFound) {
      Comparator<SearchProductData> priceComparator = (b, a) =>
          double.tryParse(a.sellingPrice.toString())
              .compareTo(double.tryParse(b.sellingPrice.toString()));
      setState(() {
        product.data.sort(priceComparator);
      });
    }
  }

  initFunc() async {
    int i = await getCartCount();
    setState(() {
      itemCount = i;
      loadingCart = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    initFunc();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final StreamVars streamVars = Get.put(StreamVars());
    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: Scaffold(
        appBar: AppBar(
            backgroundColor: mColor,
            elevation: 0.0,
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            title: Text("Search in ${STORE_INFO.info[0].storeName}")),
        body: Stack(
          children: <Widget>[
            loading
                ? neoLoader()
                : !showSearchContainer
                ? SizedBox()
                : Padding(
              padding: const EdgeInsets.symmetric(vertical: 4.0),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(4),
                    color: Colors.white,
                    height: 40,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: TextField(
                            controller: searchController,
                            autofocus: false,
                            onChanged: (text) {
                              if (searchController.text.isNotEmpty) {
                                setState(() {
                                  showCloseIcon = true;
                                });
                              }
                            },
                            onSubmitted: (text) {
                              if (searchController.text.isNotEmpty) {
                                searchController.text = text;
                                setState(() {
                                  loading = true;
                                  showSearchContainer = true;
                                });
                                searchProduct();
                              }
                            },
                            decoration: InputDecoration(
                              // suffixIcon: showCloseIcon
                              //     ? GestureDetector(
                              //         onTap: () {
                              //           setState(() {
                              //             searchController.text = "";
                              //             showCloseIcon = false;
                              //             showSearchContainer = true;
                              //             notFound = false;
                              //           });
                              //         },
                              //         child: Icon(
                              //           Icons.close,
                              //           color: Colors.black,
                              //           size: 18,
                              //         ),
                              //       )
                              //     : PopupMenuButton<String>(
                              //         itemBuilder: (context) {
                              //           return searchBy.map((str) {
                              //             return PopupMenuItem(
                              //               value: str,
                              //               child: Text(str),
                              //             );
                              //           }).toList();
                              //         },
                              //         child: Icon(
                              //             Icons.arrow_drop_down,
                              //             color: Colors.black),
                              //         onSelected: (v) {
                              //           setState(() {
                              //             searchByValue = v;
                              //             print(searchByValue);
                              //             searchController.text = "";
                              //           });
                              //         },
                              //       ),
                              filled: true,
                              fillColor: Colors.black12,
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black12,
                                      width: 0),
                                  borderRadius:
                                  BorderRadius.circular(12)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.black12,
                                      width: 0),
                                  borderRadius:
                                  BorderRadius.circular(12)),
                              contentPadding: EdgeInsets.all(4),
                              hintStyle: TextStyle(fontSize: 13),
                              labelStyle: TextStyle(fontSize: 13),
                              prefixIcon: Icon(Icons.search,
                                  color: Colors.black),
//                    suffixIcon: Text(resultCount),
                              enabled: true,
                              hintText: "Search for products",
                            ),
                            cursorColor: Colors.black,
                            showCursor: true,
                          ),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        GestureDetector(
                          onTap: () {
                            showSortFilter();
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Row(
                              children: <Widget>[
                                Text("Sort"),
                                SizedBox(
                                  width: 5,
                                ),
                                Icon(
                                  Icons.sort,
                                  color: Colors.black,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  notFound
                      ? SingleChildScrollView(
                    child: Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment:
                      CrossAxisAlignment.center,
                      mainAxisAlignment:
                      MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        SizedBox(
                          height: 50,
                        ),
                        Image.asset(
                          'drawables/no_result_found.png',
                          height: 206,
                          width: 172,
                        ),
                        Text(
                          "Oops!!!",
                          style: TextStyle(fontSize: 20),
                        ),
                        Text(
                          "No result found.",
                          style: TextStyle(fontSize: 20),
                        ),
                      ],
                    ),
                  )
                      : showContainer
                      ? Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 16),
                      child: ListView.separated(
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 20,
                          );
                        },
                        padding: EdgeInsets.only(top: 0),
                        shrinkWrap: true,
                        physics: AlwaysScrollableScrollPhysics(),
                        itemCount: product.data.length,
                        itemBuilder: (context, index) {
                          return ProductCard(
                              UniqueKey(), product.data[index]);
                        },
                      ),
                    ),
                    // child: ListView.builder(
                    //     padding:
                    //         EdgeInsets.only(bottom: 120),
                    //     itemCount: product.data.length,
                    //     itemBuilder: (context, index) {
                    //       return ProductCard(product.data[index]);
                    //     }),
                  )
                      : SizedBox()
                ],
              ),
            ),
            neoLoaderStack()
          ],
        ),
        floatingActionButton: Stack(
          fit: StackFit.loose,
          children: <Widget>[
            FloatingActionButton(
              elevation: 8.0,
              heroTag: 'cart',
              tooltip: 'Cart',
              backgroundColor: mColor,
              onPressed: () async {
                await Get.to(checkout());
                getShoppingListCount();
                // onBackPressed();
//                  offerState.getComboOffers();
              },
              child: Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
            ),
            itemCount == null || itemCount < 1
                ? SizedBox()
                : Positioned(
                right: 0,
                top: 0,
                child: AnimatedOpacity(
                  opacity: loadingCart ? 0 : 1.0,
                  duration: Duration(milliseconds: 0),
                  child: Container(
                    padding: EdgeInsets.all(2),
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: mColor),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    constraints: BoxConstraints(
                      minWidth: 20,
                      minHeight: 20,
                    ),
                    child: loadingCart
                        ? SizedBox()
                        : Obx(() =>
                        Text(
                          '${v.cartCount.value}',
                          style: TextStyle(
                            color: mColor,
                            fontSize: 12,
                          ),
                          textAlign: TextAlign.center,
                        )),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}

class ProductCard extends StatefulWidget {
  SearchProductData data;

  ProductCard(Key key, this.data) : super(key: key);

  @override
  _ProductCardState createState() => _ProductCardState();
}

class _ProductCardState extends State<ProductCard> {
  int itemCount = 0,
      totalLeft = 0;
  HttpRequests requests = HttpRequests();

  increment() {
    if (itemCount != totalLeft) {
      itemCount++;
      addItemToCart(itemCount);
    }
  }

  decrement() {
    if (itemCount <= 1) {
      setState(() {
        itemCount--;
      });

      print("$itemCount");
      addItemToCart(itemCount);
    } else {
      itemCount--;
      addItemToCart(itemCount);
    }
  }

  addItemToCart(int qty) async {
    print("F $qty");
    var response = await requests.addToCart(
      widget.data.masterProductId,
      widget.data.merchantproductList,
      "",
      "",
      qty,);

    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      setState(() {
        print("CART UPDATED!");
        // checkOutState.getCartProducts();
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    itemCount = widget.data.shoppingListQty;
    totalLeft = widget.data.totalLeft;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.to(ProductDetailPage(widget.data.merchantproductList.toString()));
      },
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 55,
                    width: 80,
                    decoration: BoxDecoration(
                        color: mColor.withOpacity(.4),
                        borderRadius: BorderRadius.all(Radius.circular(18))),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(18)),
                      child: CachedNetworkImage(
                        placeholder: (context, _) =>
                            Image.asset('assets/default-image.jpg'),
                        imageUrl: widget.data.productImage,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(4),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "${widget.data.productName}",
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: mediumTextStyleBlack,
                          ),
                          Image.asset(
                            'assets/food-category.png',
                            height: 12,
                            color: Colors.green,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Text(
                    "₹ ${widget.data.sellingPrice}",
                    style: largeTextStyleBlack,
                  ),
                ],
              ),
            ),
            // Column(
            //   crossAxisAlignment: CrossAxisAlignment.end,
            //   children: <Widget>[
            //     Container(
            //       width: 80,
            //       height: 30,
            //       decoration: BoxDecoration(
            //           color: Colors.white,
            //           boxShadow: [
            //             BoxShadow(
            //               color: Colors.grey.withOpacity(0.5),
            //               spreadRadius: 1,
            //               blurRadius: 1,
            //               offset: Offset(0, 1), // changes position of shadow
            //             ),
            //           ],
            //           borderRadius: BorderRadius.all(Radius.circular(3))),
            //       child: Container(
            //         child: Row(
            //           mainAxisSize: MainAxisSize.min,
            //           mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //           children: <Widget>[
            //             Expanded(
            //               child: GestureDetector(
            //                 onTap: (){decrement();},
            //                 child: Container(
            //                   child: Icon(
            //                     Icons.remove,
            //                     size: 16,
            //                     color: Colors.black,
            //                   ),
            //                 ),
            //               ),
            //             ),
            //             Expanded(
            //               child: Center(child: Text("$itemCount")),
            //             ),
            //             Expanded(
            //               child: GestureDetector(
            //                 onTap: (){increment();},
            //                 child: Container(
            //                   child: Icon(
            //                     Icons.add,
            //                     size: 16,
            //                     color: Colors.black,
            //                   ),
            //                 ),
            //               ),
            //             ),
            //           ],
            //         ),
            //       ),
            //     ),
            //     SizedBox(
            //       height: 8,
            //     ),
            //     Text(
            //       "₹ ${widget.data.sellingPrice}",
            //       style: largeTextStyleBlack,
            //     ),
            //   ],
            // )
          ],
        ),
      ),
    );
  }
}
