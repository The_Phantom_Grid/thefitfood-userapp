import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:userapp/Screens/LoginRegister/LoginPage.dart';

import '../Components/CommonUtility.dart';

class HttpRequests {
//  String devUrl = "$baseUrl";
//  String liveUrl = "$baseUrl";

  String baseUrl = "http://bm.retailmagik.com";

  clearData() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences?.setBool("isLogin", null);
    preferences?.setString("username", null);
    preferences?.setString("password", null);
  }

  sessionExpire() async {
    await clearData();
    Get.offAll(LoginPage());
    Get.snackbar(
      "SESSION EXPIRED",
      "Token expired, Login again.",
      isDismissible: true,
      duration: Duration(seconds: 2),
      snackPosition: SnackPosition.TOP,
    );
    // mainAppState.timer.cancel();
  }

  showError(http.Response response) async {
    Get.back();
    Get.snackbar(
      "Something went wrong!",
      "Error code: ${response.statusCode}",
      isDismissible: true,
      duration: Duration(seconds: 2),
      snackPosition: SnackPosition.TOP,
    );
    // mainAppState.timer.cancel();
  }

  Future LoginAuth(String number, String password) async {
    String url = "$baseUrl/rest/web/index.php/v1/loginwith_phone";
    try {
      var response = await http.post(
        url,
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: json.encode({"number": number, "password": password}),
        encoding: Encoding.getByName("utf-8"),
      );
      if (response.statusCode == 401)
        sessionExpire();
      else if (response.statusCode != 200) {
        showError(response);
        // else if (response.toLowerCase().contains("failed host lookup")) {
        //   Get.snackbar("No Internet", "Make sure you are connected to internet.",
        //       isDismissible: true,
        //       duration: Duration(seconds: 10),
        //       snackPosition: SnackPosition.TOP,
        //       backgroundColor: Colors.white54);
      } else {
        var jData = json.decode(response.body);
        return jData;
      }
    } catch (e) {
      print("ERROR ->> $e");
      if (e.toString().contains("Failed host lookup")) {
        Get.snackbar("No Internet",
            "Make sure you are connected to internet and try again.",
            isDismissible: true,
            duration: Duration(seconds: 60),
            snackPosition: SnackPosition.TOP,
            backgroundColor: Colors.white54);
      }
    }
    // print("ERROR2 ->> $response");
  }

  Future registeringUser(var lat, var lng, var mobile) async {
    String url = "$baseUrl/rest/web/index.php/v1/user";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: json.encode({"lat": lat, "lng": lng, "mobile": mobile}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getReferralCode(var number) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_referral_code_from_number";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: json.encode({"number": number}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getToken(
      var userId, var deviceUniqueId, var fcmToken, var deviceType) async {
    String url = "$baseUrl/rest/web/index.php/v1/push/gettoken";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      body: json.encode({
        "info": {
          "deveiceUniqId": deviceUniqueId,
          "deviceTokenId": fcmToken,
          "deviceType": 3,
          "user_type": "user"
        },
        "userid": userId
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getNearbyStores(
      int distance, var max, var min, var userId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/merchant/shownearestmerchant";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode(
          {"distance": distance, "max": max, "min": min, "userid": userId}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return response;
    }
  }

  Future getStoreDetail(var storeId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_store_data";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "store_id": storeId,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return response;
    }
  }

  Future getStoreInfo(String userId, String lat, String lng) async {
    String url = "$baseUrl/rest/web/index.php/v1/merchant/get_nearest_store";
    var response = await http
        .post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({"userid": userId, "lat": lat, "lng": lng}),
      encoding: Encoding.getByName("utf-8"),
    )
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getInventory(
      var cat_id,
      var max,
      var min,
      var storeId,
      var token,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/product_by_cat";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode(
          {"cat_id": cat_id, "max": max, "min": min, "storeid": storeId}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getAllProducts(
      var max,
      var min,
      var storeId,
      var token,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/product_by_store";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({"max": max, "min": min, "storeid": storeId}),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getProductsByCategory(
      String cat_id,
      String max,
      String min,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/product_by_cat";
    var response = await http
        .post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode(
          {"cat_id": cat_id, "max": max, "min": min, "storeid": STORE_ID}),
      encoding: Encoding.getByName("utf-8"),
    )
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getProducts(
      String max,
      String min,
      String storeId,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/product_by_store";
    var response = await http
        .post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({"max": max, "min": min, "storeid": storeId}),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getProductDetail(String merchantListId) async {
    String url = "$baseUrl/rest/web/index.php/v1/product_detail";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({
        "id": USER_ID,
        "storeid": STORE_ID,
        "product_id": merchantListId
      }),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future addItemToCart(var masterProduct_id,
      var merchantlist_id,
      var offer_id,
      var offer_price,
      var qty,
      var merchant_id,
      var storeId,
      var user_id,
      var token,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/addalltoshoppingcart";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "cart": [
          {
            "masterproductid": masterProduct_id,
            "merchantlist_id": merchantlist_id,
            "offer_id": 0,
            "offer_price": offer_price,
            "qty": qty
          }
        ],
        "merchant_id": merchant_id,
        "store_id": storeId,
        "user_id": user_id
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    print(json.decode(response.body));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getShoppingCartId(var shopping_list_id) async {
    String url = "$baseUrl/rest/web/index.php/v1/copy_shopping_list_to_cart";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $TOKEN'
        },
        body: json.encode({
          "merchant_id": MERCHANT_ID,
          "shopping_list_id": shopping_list_id,
          "store_id": STORE_ID
        })).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future placeCustomerOrder(
      String cart_id,
      String deliveryAddress,
      String deliverydate,
      String deliverytype,
      String max_time_to_deliver,
      String order_instruction,
      String paymentmode,
      String promocode,
      String discountAmount,
      String response,
      String shopping_list_id,
      String time_slot,
      String website) async {
    print("hitting...");
    String url = "$baseUrl/rest/web/index.php/v1/place_my_order";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN'
      },
      body: json.encode({
        "cart_id": cart_id,
        "deliveryaddress": deliveryAddress,
        "deliverydate": deliverydate,
        "deliverytype": deliverytype,
        "max_time_to_deliver": "",
        "merchant_id": MERCHANT_ID,
        "order_instruction": order_instruction,
        "paymentmode": paymentmode,
        "promocode": promocode,
        "promocode_discount": discountAmount,
        "response": "1",
        "shopping_list_id": shopping_list_id,
        "store_id": STORE_ID,
        "time_slot": time_slot,
        "userid": USER_ID,
        "website": "APPPROD"
      }),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getStoreDetails() async {
    String url = "$baseUrl/rest/web/index.php/v1/get_store_data";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({
        "store_id": STORE_ID,
      }),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getPaymentMethod() async {
    String url = "$baseUrl/rest/web/index.php/v1/viewcart_new";
    print(json.encode(
        {"merchantid": MERCHANT_ID, "storeid": STORE_ID, "token": TOKEN}));
    var response = await http
        .post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $TOKEN'
        },
        body: json.encode({
          "merchantid": MERCHANT_ID,
          "storeid": STORE_ID,
        }))
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      print(jData);
      return jData;
    }
  }

  Future addToCart(var masterProduct_id,
      var merchantlist_id,
      var offer_id,
      var offer_price,
      var qty,) async {
    String url = "$baseUrl/rest/web/index.php/v1/addalltoshoppingcart";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({
        "cart": [
          {
            "masterproductid": masterProduct_id,
            "merchantlist_id": merchantlist_id,
            "offer_id": 0,
            "offer_price": offer_price,
            "qty": qty
          }
        ],
        "merchant_id": MERCHANT_ID,
        "store_id": STORE_ID,
        "user_id": USER_ID
      }),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    print(json.decode(response.body));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future setOrderPaymentStatus(var paymentStatus, var orderId) async {
    String url = "$baseUrl/rest/web/index.php/v1/update_paytm_order";
    print("Payment Status${paymentStatus}");

    var response = await http
        .post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({
        "merchant_id": MERCHANT_ID,
        "user_id": USER_ID,
        "order_id": orderId,
        "payment_status": paymentStatus
      }),
      encoding: Encoding.getByName("utf-8"),
    )
        .timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      print("Payment Status${paymentStatus}");

      print("Payment Status${response.body}");
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getMyCart(var userId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/view_shopping_cart_by_store";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({"userid": userId}),
      encoding: Encoding.getByName("utf-8"),
    );
    print("STATUS CODE: ${response.statusCode}");
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future deleteItemFromCart(
      var itemId, var merchantId, var storeId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/delete_shopping_cart_item";

    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode(
          {"item_id": itemId, "merchant_id": merchantId, "store_id": storeId}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getShoppingListCount(var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/shopping_list_count";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: jsonEncode({"store_id": STORE_ID}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getCartProducts(
      var merchantId, var storeId, var userId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/view_shopping_cart";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "merchantid": merchantId,
        "storeid": storeId,
        "userid": userId,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    print(json.decode(response.body));
    print("STATUS CODE: ${response.statusCode}");
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      print(jData);
      return jData;
    }
  }

  Future getCart() async {
    String url = "$baseUrl/rest/web/index.php/v1/view_shopping_cart";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({
        "merchantid": MERCHANT_ID,
        "storeid": STORE_ID,
        "userid": USER_DETAILES.data.loginDetails.userId.toString(),
      }),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    print(json.decode(response.body));
    print("STATUS CODE: ${response.statusCode}");
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      print(jData);
      return jData;
    }
  }

  Future validatePromoCode(String totalAmount, String promoCode) async {
    String url = "$baseUrl/rest/web/index.php/v1/validate_promo_codes";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({
        "total_order_amount": totalAmount,
        "promocode": promoCode,
        "userid": USER_DETAILES.data.loginDetails.userId.toString(),
      }),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    print(json.decode(response.body));
    print("STATUS CODE: ${response.statusCode}");
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      print(jData);
      return jData;
    }
  }

  Future getPromoCodes() async {
    String url = "$baseUrl/rest/web/index.php/v1/get_promo_codes";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    print(json.decode(response.body));
    print("STATUS CODE: ${response.statusCode}");
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      print(jData);
      return jData;
    }
  }

  Future getOtp(var lat, var lng, var mobile) async {
    String url = "$baseUrl/rest/web/index.php/v1/user";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      body: json.encode({"lat": lat, "lng": lng, "mobile": mobile}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future sendComm(var userId) async {
    String url = "$baseUrl/rest/web/index.php/v1/send_comm";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
      },
      body: json.encode({"userid": userId}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getProfile(var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/register/viewprofile";
    var response =
    await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future removeStore(var store_id, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/remove_shopping_list";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({"store_id": store_id}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future addToFavourite(
      var user_id, var shop_id, var distance, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/make_shop_as_favourite";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "userid": user_id,
        "distance": distance,
        "shop_id": shop_id,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData['message'];
    }
  }

  Future getFavourites(
      var user_id, var max, var min, var distance, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_my_stores";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "userid": user_id,
        "max": max,
        "min": min,
        "distance": distance,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future removeFavStore(
      var user_id, var shop_id, var distance, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/remove_shop_as_favourite";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "userid": user_id,
        "distance": distance,
        "shop_id": shop_id,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData['message'];
    }
  }

  Future getOTP(var number, var otp) async {
    String url = "$baseUrl/rest/web/index.php/v1/check_user_exists";
    var response = await http.post(
      url,
      body: json.encode({
        "mobile": number,
        "otp": otp,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future verifyOtp(var number, var otp) async {
    String url = "$baseUrl/rest/web/index.php/v1/verifyotp";
    var response = await http.post(
      url,
      body: json.encode({
        "mobile": number,
        "otp": otp,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future updatePassword(var token, var password) async {
    String url = "$baseUrl/rest/web/index.php/v1/set_password";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "password": password,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getAddress(var token, var userid) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_all_address";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "user_id": userid,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getDeliveryAddress() async {
    String url = "$baseUrl/rest/web/index.php/v1/get_all_address";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN'
      },
      body: json.encode({
        "user_id": USER_ID,
      }),
      encoding: Encoding.getByName("utf-8"),
    ).timeout(Duration(seconds: 10), onTimeout: () {
      return null;
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
      return null;
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future addNewAddress(var address,
      var city,
      var colony_street,
      var flat_houseno,
      var full_name,
      var landmark,
      var lat,
      var lng,
      var pincode,
      var state,
      var token,
      var userid) async {
    String url = "$baseUrl/rest/web/index.php/v1/add_more_address";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "address": address,
        "city": city,
        "colony_street": colony_street,
        "flat_houseno": flat_houseno,
        "full_name": full_name,
        "landmark": landmark,
        "lat": lat,
        "lng": lng,
        "pincode": pincode,
        "state": state,
        "user_id": userid,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future updateAddress(
      var address,
      var addressId,
      var city,
      var colony_street,
      var flat_houseno,
      var full_name,
      var landmark,
      var lat,
      var lng,
      var pincode,
      var state,
      var token,
      var userid) async {
    String url = "$baseUrl/rest/web/index.php/v1/update_address_details";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "address": address,
        "address_id": addressId,
        "city": city,
        "colony_street": colony_street,
        "flat_houseno": flat_houseno,
        "full_name": full_name,
        "landmark": landmark,
        "lat": lat,
        "lng": lng,
        "pincode": pincode,
        "state": state,
        "user_id": userid,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
//
  }

  Future removeAddress(var user_id, var address_id, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/remove_address";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "address_id": address_id,
        "user_id": user_id,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future placeOrder(
      String cart_id,
      String deliveryAddress,
      String deliverydate,
      String deliverytype,
      String max_time_to_deliver,
      String merchant_id,
      String order_instruction,
      String paymentmode,
      String promocode,
      String response,
      String shopping_list_id,
      String store_id,
      String time_slot,
      String userid,
      String website,
      var token) async {
    print("hitting...");
    String url = "$baseUrl/rest/web/index.php/v1/place_my_order";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
      body: json.encode({
        "cart_id": cart_id,
        "deliveryaddress": deliveryAddress,
        "deliverydate": deliverydate,
        "deliverytype": deliverytype,
        "max_time_to_deliver": max_time_to_deliver,
        "merchant_id": merchant_id,
        "order_instruction": order_instruction,
        "paymentmode": paymentmode,
        "promocode": promocode,
        "response": "1",
        "shopping_list_id": shopping_list_id,
        "store_id": store_id,
        "time_slot": time_slot,
        "userid": userid,
        "website": "APPPROD"
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getOrders(var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/cart/myorder";
    var response =
    await http.get(url, headers: {'Authorization': 'Bearer $token'});
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      print(jData);
      return jData;
    }
  }

  Future getOrderDetail(var order_id, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/cartorder/orderdetails";
    var response = await http.post(url,
        headers: {'Authorization': 'Bearer $token'},
        body: json.encode({
          "orderid": order_id,
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getNotifications(var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_sent_notifications";
    var response = await http.post(
      url,
      headers: {'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getInvoice(var orderId, var token) async {
    String url = "$baseUrl/rest/web/v2/get_invoice";
    var response = await http.post(url,
        headers: {'Authorization': 'Bearer $token'},
        body: json.encode({
          "order_id": orderId,
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getUnreadNotifications(var token) async {
    String url =
        "$baseUrl/rest/web/index.php/v1/get_total_unread_notifications";
    var response = await http.post(
      url,
      headers: {'Authorization': 'Bearer $token'},
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future validatePromo(
      var promo, var total_amount, var userId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/validate_promo_codes";
    var response = await http.post(url,
        headers: {'Authorization': 'Bearer $token'},
        body: json.encode({
          "promocode": promo,
          "total_order_amount": total_amount,
          "userid": userId
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future updateLocation(var lat, var lng, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/register/updatelocation";
    http.Response response = await http.post(
      url,
      headers: {'Authorization': 'Bearer $token'},
      body: json.encode({"lat": "$lat", "lng": "$lng"}),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future updateProfile(
      var token, var address, var email, var image, var name) async {
    String url = "$baseUrl/rest/web/index.php/v1/updateprofile";
    print(json.encode({
      "address": address,
      "city": "",
      "email": email,
      "image": "image",
      "landmark": "",
      "name": name,
      "pincode": ""
    }));
    var response = await http.post(url,
        headers: {'Authorization': 'Bearer $token'},
        body: json.encode({
          "address": address,
          "city": "",
          "email": email,
          "image": image,
          "landmark": "",
          "name": name,
          "pincode": ""
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future viewCart(var token, var merchant_id, var store_id) async {
    String url = "$baseUrl/rest/web/index.php/v1/viewcart_new";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $token'
        },
        body: json.encode({
          "merchantid": merchant_id,
          "storeid": store_id,
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getCartId(
      var token, var merchant_id, var shopping_list_id, var store_id) async {
    String url = "$baseUrl/rest/web/index.php/v1/copy_shopping_list_to_cart";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $token'
        },
        body: json.encode({
          "merchant_id": merchant_id,
          "shopping_list_id": shopping_list_id,
          "store_id": store_id
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getOrderDetails(var token, var order_id) async {
    String url = "$baseUrl/rest/web/index.php/v1/cartorder/orderdetails";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $token'
        },
        body: json.encode({
          "orderid": order_id,
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      print(jData);

      return jData;
    }
  }

  Future getReferral(var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_referral_code";
    var response = await http.post(url, headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
      'Authorization': 'Bearer $token'
    });
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getComboOffer(var token, var distance, var storeId) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_combo_offers";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $token'
        },
        body: json.encode({
          "distance": distance,
          "store_id": STORE_ID,
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getOffersAndDeals(var distance, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_offers_and_deals";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $token'
        },
        body: json.encode({
          "store_id": STORE_ID,
          "distance": distance,
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getOffersByShops(
      var distance, String max, String min, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_offers_by_shops";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $token'
        },
        body: json.encode({"distance": distance, "max": max, "min": min}));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getRunningOffers(
      var distance, String max, String min, var token) async {
    String url =
        "$baseUrl/rest/web/index.php/v1/get_offers_by_running_discounts";
    var response = await http.post(url,
        headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
          'Authorization': 'Bearer $token'
        },
        body: json.encode({
          "store_id": STORE_ID,
          "distance": distance,
          "max": max,
          "max_percent": "100",
          "min": min,
          "min_percent": "25"
        }));
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      print("Running Discount${response.body}");

      var jData = json.decode(response.body);
      print(jData);
      return jData;
    }
  }

  Future addOfferToCart(
      String jBody,
      var token,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/addalltoshoppingcart";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: jBody,
      encoding: Encoding.getByName("utf-8"),
    );
    print(json.decode(response.body));
    print("Status CODE: ${response.statusCode}");
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future returnOrder(
      var comments,
      var orderId,
      var token,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/return_or_cancel_order";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "comments": comments,
        "order_id": orderId,
        "return_requested_or_order_canceled": "Return Requested"
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future cancelOrder(
      var comments,
      var orderId,
      var token,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/return_or_cancel_order";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "comments": comments,
        "order_id": orderId,
        "return_requested_or_order_canceled": "Order Canceled"
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future giveFeedback(
      var comments,
      var dimension,
      var orderId,
      var rating,
      var token,
      ) async {
    String url = "$baseUrl/rest/web/index.php/v1/customer/getfeedback";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "comments": comments,
        "order_id": orderId,
        "dimension": dimension,
        "rating": rating
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

//  Future generateCheckSum(
//      var mid,
//      var channelId,
//      var Industry_Type_Id,
//      var website,
//      var paytm_merchant_key,
//      var txn_amount,
//      var order_id,
//      var cust_id,
//      ) async {
//
//    String url = "https://us-central1-mrdishant-4819c.cloudfunctions.net/generateCheckSum";
//    var body = {
//      "mid": mid,
//      "CHANNEL_ID": channelId,
//      "INDUSTRY_TYPE_ID": Industry_Type_Id,
//      "WEBSITE": website,
//      "PAYTM_MERCHANT_KEY": paytm_merchant_key,
//      "TXN_AMOUNT": txn_amount,
//      "ORDER_ID": order_id,
//      "CUST_ID": cust_id,
//    };
//    var response = await http.post(
//      url,
//      headers: {
//        "Content-Type": "application/x-www-form-urlencoded",
//      },
//      body: body,
//      encoding: Encoding.getByName("utf-8"),
//    );
//    if(response.statusCode == 401)
//      sessionExpire();
//    else if(response.statusCode != 200) {
//      showError(response);
//    }
//    else {
//      var jData = json.decode(response.body);
//      return jData;
//
//    }
//    print("checksum == ${response.body.toString()}");
//    return response.body.toString();
//  }

  Future getPaytmDetails(
      var merchantId, var orderId, var userId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/get_paytm_details";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "merchant_id": merchantId,
        "order_id": orderId,
        "user_id": userId,
        "website": "APPROD"
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future searchProduct(var storeId, var tag, var searchBy) async {
    String url = "$baseUrl/rest/web/index.php/v1/search_product_store";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $TOKEN',
      },
      body: json.encode({
        "store_id": storeId,
        "tag": tag,
        "search_by": searchBy,
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getTopSellingProducts(var storeId, var tag, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/product_sku_history";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({"store_id": storeId, "tag": tag}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future setPaymentStatus(
      var merchantId, var orderId, var userId, var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/update_paytm_order";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode(
          {"merchant_id": merchantId, "user_id": userId, "order_id": orderId}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future searchStore(
      var storeName,
      var category,
      var pincode,
      var distance,
      var rating,
      var storeType,
      var max,
      var min,
      var locality,
      var token) async {
    String url = "$baseUrl/rest/web/v1/search_store_by_filter_new";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': 'Bearer $token',
      },
      body: json.encode({
        "search_by": "",
        "category": category,
        "distance": distance,
        "payment_mode": "",
        "rating": rating,
        "store_name": storeName,
        "max": max,
        "min": min,
        "pincode": pincode,
        "store_type": storeType,
        "locality": locality
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  //////////OTP//////////////
  Future sendOTP(
      var otp,
//      var lng,
      var mobile) async {
    String url = "$baseUrl/rest/web/index.php/v1/check_user_exists";
    var response = await http.post(
      url,
      body: json.encode({
        "otp": otp,
//        "lng": lng,
        "mobile": mobile
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future verifyOTP(var otp, var mobile) async {
    String url = "$baseUrl/rest/web/index.php/v1/verifyotp";
    var response = await http.post(
      url,
      body: json.encode({"mobile": mobile, "otp": otp}),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      print("Forget Password${response.body}");
      var jData = json.decode(response.body);
      return jData;
    }
  }

  Future getStoreTypeList(var token) async {
    String url = "$baseUrl/rest/web/index.php/v1/store_types";
    var response = await http.post(
      url,
      headers: {
//      "Accept": "application/json",
//      "Content-Type": "application/json",
        'Authorization': 'Bearer $token'
      },
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }

  ///////send chat notification/////////////////////////
  Future sendChatNotification(var title, var msg, var fcmToken, String orderId,
      String customerId) async {
    print("message::: $msg");
    String serverKey =
        "AAAAyZtkiuU:APA91bElBjW5LEzKI5Pm84Hux7EK6WGyRZtxWRLr-h-0Ya5tBBulxDo75orlTBS3PY9tkB39ZYaCCDtugL32rDSBHCJKp8PReZrfgUjDlK5mrY7orLcW3KRsLhO9eUI85EshzVqFTZw0";
    String url = "https://fcm.googleapis.com/fcm/send";
    var response = await http.post(
      url,
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json",
        'Authorization': "key=$serverKey",
      },
      body: json.encode({
        "notification": {"title": title.toString(), "body": msg.toString()},
        "data": {
          "subject": "",
          "on_click_event": "",
          "screen": "chat",
          "click_action": "FLUTTER_NOTIFICATION_CLICK",
          "message": "",
          "messageData": {"orderId": orderId, "customerId": customerId}
        },
        "to": fcmToken
      }),
      encoding: Encoding.getByName("utf-8"),
    );
    if (response.statusCode == 401)
      sessionExpire();
    else if (response.statusCode != 200) {
      showError(response);
    } else {
      var jData = json.decode(response.body);
      return jData;
    }
  }
////////////////end ///////////////
}
