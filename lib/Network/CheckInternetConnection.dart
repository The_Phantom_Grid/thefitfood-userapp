import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Screens/NoInternet.dart';
import 'package:userapp/Screens/SplashScreen.dart';

import '../constants.dart';

class CheckInternet {
  BuildContext context;

  CheckInternet(this.context);

  StreamVars streamVars = Get.put(StreamVars());

  bool snackBar = false;

  checkInternet() async {
    DataConnectionChecker().onStatusChange.listen(onStatusChange);

    return await DataConnectionChecker().connectionStatus;
  }

  void onStatusChange(DataConnectionStatus status) {
    print(status.toString());
    switch (status) {
      case DataConnectionStatus.connected:
        streamVars.noInternet.value = false;
        print('Data connection is available.');
        if (Get.currentRoute.toString().contains("/NoInternet")) {
          Get.back();
          Get.snackbar("Online", "We are back online.",
              backgroundColor: Colors.green,
              colorText: Colors.white,
              icon: Icon(Icons.signal_wifi_4_bar, color: Colors.white),
              isDismissible: true,
              duration: Duration(seconds: 2),
              snackPosition: SnackPosition.TOP,
              snackbarStatus: (SnackbarStatus status) {
            print(status);
          });
//          snackBar = false;
        }
        break;
      case DataConnectionStatus.disconnected:
        streamVars.noInternet.value = true;
        print('You are disconnected from the internet.');
        Get.to(NoInternet());
        print("Route:: ${Get.currentRoute.toString()}");
        print("Prev Route:: ${Get.previousRoute.toString()}");
//        Get.snackbar(
//          "Offline",
//          "Check your internet connection & try again.",
//          icon: Icon(Icons.perm_scan_wifi, color: Colors.white),
//          colorText: Colors.white,
//          backgroundColor:  mColor,
//          isDismissible: false,
//          duration: Duration(seconds: 999),
//          snackPosition: SnackPosition.TOP,
//          snackbarStatus: (SnackbarStatus status) {
//            print(status);
//            if (status.toString().contains("SnackbarStatus.OPEN") ||
//                status.toString().contains("SnackbarStatus.OPENING"))
//              snackBar = true;
//          },
//        );
//        Flushbar(
//          isDismissible: true,
//          flushbarPosition: FlushbarPosition.TOP,
//          flushbarStyle: FlushbarStyle.GROUNDED,
//          dismissDirection: FlushbarDismissDirection.HORIZONTAL,
//          title: "No Internet Connection",
//          message: "Check your internet connectivity!",
//          icon: Icon(
//            Icons.signal_wifi_off,
//            color: Colors.white,
//          ),
//          mainButton: FlatButton(
//            onPressed: () {
//              Navigator.pop(context);
//            },
//            textColor: Colors.redAccent,
//            child: Text("Cancel"),
//          ),
//        )..show(context);
        break;
    }
  }
}
