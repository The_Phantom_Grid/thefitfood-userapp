import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/Chats.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/ChatScreen.dart';
import 'package:userapp/Screens/OrderDetail.dart';

class NotificationHandler {
  FirebaseMessaging firebaseMessaging;
  BuildContext context;

  void setupFirebaseNotification() {
    firebaseMessaging = FirebaseMessaging();
    firebaseListner();
  }

  void firebaseListner() async {
    if (Platform.isIOS) iOS_Permission();
    firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print("on message: $message");
        // myBackgroundMessageHandler(message);
        return;
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
        // myBackgroundMessageHandler(message);
        if (Platform.isAndroid) {
          myBackgroundMessageHandler(message);
        } else if (Platform.isIOS) {
          myBackgroundMessageHandlerIOS(message);
        }
        return;
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
        // myBackgroundMessageHandler(message);
        if (Platform.isAndroid) {
          myBackgroundMessageHandler(message);
        } else if (Platform.isIOS) {
          myBackgroundMessageHandlerIOS(message);
        }
        return;
      },
    );
  }

  Future<dynamic> myBackgroundMessageHandler(
      Map<String, dynamic> message) async {
    print("MESSAGE -> $message");
    final dynamic data = message['data'];
    print("DATA -> $data");
    if (data['screen'].toString().toLowerCase() == "chat") {
      Map<String, dynamic> d = jsonDecode(data['messageData']);
      print("MESSAGE DATA -> $d");

      Map<String, dynamic> msgData = {
        'user': USER_DETAILES,
        'merchantId': d['merchantId'],
        'storeId': d['storeId'].toString(),
        'gTotal': d['grandTotalAmount'],
        'totalItems': d['totalItem'],
        'imageUrl': d['storeLogo'],
        'logo': d['storeLogo'],
        'deliveryCharge': d['deliveryCharges'],
        'subTotal': d['subTotal'],
        'phone': d['mobile'],
        'location': d['address'],
        'storeName': d['storeName'],
        'orderId': d['orderId'],
        'chatDate': d['chatDate'],
        'chatTime': d['chatTime'],
        'msg': d['message']
      };

      Get.to(ChatScreen(msgData, "Chats", USER_DETAILES));
    } else {
      String orderId = data['screen'].toString();
      print("ORDERID ->> $orderId");
      Get.to(OrderDetail(orderId));
    }

    // Or do other work.
  }

  Future<dynamic> myBackgroundMessageHandlerIOS(
      Map<String, dynamic> message) async {
    print("MESSAGE -> $message");
    if (message['screen'].toString().toLowerCase() == "chat") {
      Map<String, dynamic> d = jsonDecode(message['messageData']);
      print("MESSAGE DATA -> $d");

      Map<String, dynamic> msgData = {
        'user': USER_DETAILES,
        'merchantId': d['merchantId'],
        'storeId': d['storeId'].toString(),
        'gTotal': d['grandTotalAmount'],
        'totalItems': d['totalItem'],
        'imageUrl': d['storeLogo'],
        'logo': d['storeLogo'],
        'deliveryCharge': d['deliveryCharges'],
        'subTotal': d['subTotal'],
        'phone': d['mobile'],
        'location': d['address'],
        'storeName': d['storeName'],
        'orderId': d['orderId'],
        'chatDate': d['chatDate'],
        'chatTime': d['chatTime'],
        'msg': d['message']
      };

      Get.to(ChatScreen(msgData, "Chats", USER_DETAILES));
    } else {
      String orderId = message['screen'].toString();
      print("ORDERID ->> $orderId");
      Get.to(OrderDetail(orderId));
    }

    // Or do other work.
  }

  void iOS_Permission() {
    firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }
}
