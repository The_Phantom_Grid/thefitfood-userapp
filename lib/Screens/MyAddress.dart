import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Components/VariableController.dart';
import 'package:userapp/Getters/GetAddress.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/NewAddress.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:userapp/Screens/SearchLocationMap.dart';

import '../constants.dart';

class MyAddress extends StatefulWidget {
  @override
  _MyAddressState createState() => _MyAddressState();
}

class _MyAddressState extends State<MyAddress> {
//  var mAccentColor = Color(0xFFf82d70);
  HttpRequests auth = HttpRequests();
  GetMyAddress address;
  String addresses = "";
  String addressId;
  bool noAddress = false, loading = true;

  getAddress() async {
    print("hit...");
    var r = await auth.getAddress(USER_DETAILES.data.loginDetails.authKey,
        USER_DETAILES.data.loginDetails.userId);
    setState(() {
      address = GetMyAddress.fromJson(r);
      if (address.details.isEmpty) {
        print(address.details.length);
        setState(() {
          noAddress = true;
          loading = false;
        });
      } else {
        setState(() {
          print("${address.details.length} len");
          noAddress = false;
          loading = false;
        });
      }
    });
  }

  removeAddress(index) async {
    var r = await auth.removeAddress(USER_DETAILES.data.loginDetails.userId,
        address.details[index].id, USER_DETAILES.data.loginDetails.authKey);

    setState(() {
      if (r['details'] == 1) {
        print("success");
        address.details.removeAt(index);
        getAddress();
      } else {
        print("not done");
        print(r);
        print(address.details[index].id);
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAddress();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBody: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 4.0,
        backgroundColor: mColor,
        centerTitle: true,
        title: Text(
          "My Address",
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: loading
          ? neoLoader()
          : noAddress
              ? Center(
                  child: Text("Please Add a new address."),
                )
              : Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).padding.bottom),
                  child: Column(
                    children: <Widget>[
//          Text("Swipe for more options",
//            style: TextStyle(fontSize: 12, color: Colors.grey),),
                      Expanded(
                          flex: 1,
                          child: Container(
                            child: ListView.builder(
                              itemCount: address.details.length,
                              itemBuilder: (context, index) {
                                return Slidable(
                                    actionPane: SlidableDrawerActionPane(),
                                    actionExtentRatio: 0.25,
                                    movementDuration:
                                        (Duration(milliseconds: 500)),
                                    closeOnScroll: true,
                                    secondaryActions: <Widget>[
                                      IconSlideAction(
                                          caption: 'Edit',
                                          color: Colors.blue,
                                          icon: Icons.edit,
                                          onTap: () async {
                                            setState(() {
                                              addressId = address
                                                  .details[index].id
                                                  .toString();
                                            });
                                            await Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        SearchLocationMap(
                                                            addressId,
                                                            address
                                                                .details[index],
                                                            true)));
                                            getAddress();
                                          }
//                                removeFromCart(
//                            myCartItems.product[index].storeid, index),
                                          ),
                                      IconSlideAction(
                                          caption: 'Remove',
                                          color: Colors.redAccent,
                                          icon: Icons.delete,
                                          onTap: () {
                                            removeAddress(index);
                                          }
                                          //                                removeFromCart(
                                          //                            myCartItems.product[index].storeid, index),
                                          ),
                                    ],
                                    key: UniqueKey(),
                                    child: buildAddressList(
                                        address.details[index], USER_DETAILES));
                              },
                            ),
                          )),
                    ],
                  ),
                ),
      bottomNavigationBar: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ButtonTheme(
              minWidth: double.infinity,
              height: 50.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(27)),
              child: RaisedButton(
                child: Text("+ Add a new address"),
                onPressed: () async {
                  if (address.details.isEmpty) {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SearchLocationMap(
                                addressId,
                                address.details.isEmpty
                                    ? null
                                    : address.details[0],
                                false)));
                    getAddress();
                  } else {
                    setState(() {
                      addressId = null;
                    });
//                    await Navigator.push(
//                        context,
//                        MaterialPageRoute(
//                            builder: (context) =>
//                                NewAddress(USER_DETAILES, addressId,
//                                    address.details[0], false)));
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SearchLocationMap(
                                addressId, address.details[0], false)));
                    getAddress();
                  }
                },
                textColor: Colors.white,
                color: mColor,
              )),
        ),
      ),
    );
  }
}

class buildAddressList extends StatefulWidget {
  Details details;
  User user;

  buildAddressList(this.details, this.user);

  @override
  _buildAddressListState createState() => _buildAddressListState();
}

class _buildAddressListState extends State<buildAddressList> {
  HttpRequests auth = HttpRequests();
  DynamicVar v = Get.put(DynamicVar());

  updateLocation() async {
    print("city=== ${widget.details.city}");
    print("pincode=== ${widget.details.pincode}");
    print("lat-long=== ${widget.details.lat}--${widget.details.lng}");
    var addresses, subLocality;
    var lat = double.tryParse(widget.details.lat.toString());
    var lng = double.tryParse(widget.details.lng.toString());
    final coordinates = Coordinates(lat, lng);
    addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    var first = addresses.first;
    if (first.subLocality == null)
      subLocality = widget.details.city;
    else
      subLocality = first.subLocality;
    var r = await auth.updateLocation(
        double.tryParse(widget.details.lat),
        double.tryParse(widget.details.lng),
        USER_DETAILES.data.loginDetails.authKey);
    if (r['message'].toString().contains("Location updated successfully")) {
      Fluttertoast.showToast(
          msg: "Location updated successfully", toastLength: Toast.LENGTH_LONG);
    } else {
      Fluttertoast.showToast(
          msg: "${r['message'].toString()}", toastLength: Toast.LENGTH_LONG);
    }
    v.current_location.value = subLocality;
    v.current_postalCode.value = widget.details.pincode.toString();
    Navigator.pop(context, [
      subLocality,
      widget.details.pincode,
      double.tryParse(widget.details.lat.toString()),
      double.tryParse(widget.details.lng.toString())
    ]);
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 8.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6.0)),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            children: <Widget>[
              Text(
                "${widget.details.fullName}",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
              ),
              Divider(height: 0,),
              ListTile(
                dense: true,
                onTap: () {
                  print(
                      "LAT>> ${widget.details.lat.toString()} || LONG>>${widget.details.lng.toString()}");
                  setLocation(widget.details.lat.toString(),
                      widget.details.lng.toString());
                },
                leading: Icon(
                  Icons.location_on,
                  color: Colors.blue,
                ),
                title: Text(
                  "${widget.details.address}",
                  style: largeTextStyleBlack,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
