//import 'dart:ui';
//
//import 'package:flushbar/flushbar.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
//import 'package:geocoder/geocoder.dart';
//import 'package:geolocator/geolocator.dart';
//import 'package:userapp/Getters/GetAddress.dart';
//import 'package:userapp/Getters/GetUser.dart';
//import 'package:userapp/Network/httpRequests.dart';
//
//class NewAddress extends StatefulWidget {
//  User user;
//  String addressId;
//  Details details;
//  bool edit;
//
//  NewAddress(this.user, this.addressId, this.details, this.edit);
//
//  @override
//  _NewAddressState createState() => _NewAddressState();
//}
//
//class _NewAddressState extends State<NewAddress> {
//  var mColor = Color(0xFFe9991e);
//
////  var mAccentColor2 = Color(0xFFf82d70);
//  String full_address;
//  HttpRequests auth = HttpRequests();
//  var house = TextEditingController(),
//      streetName = TextEditingController(),
//      fullNAme = TextEditingController(),
//      pincode = TextEditingController(),
//      city = TextEditingController(),
//      state = TextEditingController(),
//      landmark = TextEditingController();
//  Position _currentPosition;
//  List<Address> addresses;
//  var first;
//  bool loading = false, addingAddress = false, ignoreButton = false;
//  double lat = 0.0, lng = 0.0;
//  bool editAddress = false;
//
//  validateFields() {
//    if (house.text.isEmpty ||
//        streetName.text.isEmpty ||
//        pincode.text.isEmpty ||
//        city.text.isEmpty ||
//        fullNAme.text.isEmpty ||
//        state.text.isEmpty) {
//      setState(() {
//        ignoreButton = false;
//        showDialog(
//            context: context,
//            builder: (_) =>
//                AlertDialog(
//                  content: Text(
//                      "Mandatory fields incomplete. Please review and resubmit"),
//                  actions: <Widget>[
//                    FlatButton(
//                      onPressed: () {
//                        Navigator.pop(context);
//                      },
//                      child: Text("OK"),
//                    )
//                  ],
//                ));
//      });
//    } else {
//      getCoordinates();
//    }
//  }
//
//  getCoordinates() async {
//    var result;
//    try {
//      result = await Geolocator().placemarkFromAddress(pincode.text);
//    } on PlatformException {
//      setState(() {
//        ignoreButton = false;
//      });
//      showDialog(
//          context: context,
//          builder: (_) =>
//              AlertDialog(
//                content: Text(
//                    "Please provide valid details."),
//                actions: <Widget>[
//                  FlatButton(
//                    onPressed: () {
//                      Navigator.pop(context);
//                    },
//                    child: Text("OK"),
//                  )
//                ],
//              ));
//    }
////    var addresses = await Geocoder.local.findAddressesFromQuery(
////        pincode.text);
//    lat = result[0].position.latitude;
//    lng = result[0].position.longitude;
//
//    full_address =
//        "${house.text}, ${streetName.text}, ${city.text} - ${pincode.text}, ${state.text}";
//
//    if (!editAddress) {
//      addNewAddress();
//    } else {
//      updateAddress();
//    }
//  }
//
//  updateAddress() async {
//    print(widget.addressId);
//    var r = await auth.updateAddress(
//        full_address,
//        widget.addressId,
//        city.text,
//        streetName.text,
//        house.text,
//        fullNAme.text,
//        landmark.text,
//        lat,
//        lng,
//        pincode.text,
//        state.text,
//        widget.user.data.loginDetails.authKey,
//        widget.user.data.loginDetails.userId);
//
//    if (r['message'].toString().contains(widget.addressId)) {
//      Flushbar(
//        duration: Duration(seconds: 2),
//        message: "Address Updated",
//      )
//        ..show(context);
//    }
//    Navigator.pop(context);
//  }
//
//  addNewAddress() async {
////    if(lat == 0.0 && lng == 0.0) {
////      addingAddress = true;
////      await getLocation();
////    }
//    print("$lat ===== $lng");
//    var r = await auth.addNewAddress(
//        full_address,
//        city.text,
//        streetName.text,
//        house.text,
//        fullNAme.text,
//        landmark.text,
//        lat,
//        lng,
//        pincode.text,
//        state.text,
//        widget.user.data.loginDetails.authKey,
//        widget.user.data.loginDetails.userId);
//    if (r['message'] == "Address already saved") {
//      Flushbar(
//        duration: Duration(seconds: 2),
//        message: "Address already saved",
//      )
//        ..show(context);
//      setState(() {
//        ignoreButton = false;
//      });
//    } else {
//      Navigator.pop(context);
//      Flushbar(
//        duration: Duration(seconds: 3),
//        message: "Address Saved",
//      )
//        ..show(context);
////      Navigator.pop(context);
//    }
////    setState(() {
////      showDialog(
////          context: context,
////          builder: (_) => AlertDialog(
////            content: Text("${r['Address already saved']}"),
////          ));
////    });
//  }
//
//
//  getFullAddress() async {
//    final coordinates = Coordinates(lat, lng);
//    addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
//    setState(() {
//      first = addresses.first;
//      print(addresses.length);
//      print("${first.addressLine}");
//      full_address = "${first.addressLine}";
//
//      print(' ${first.addressLine}, ||||  ${first.adminArea}, |||| ${first
//          .subLocality},  |||| ${first.subAdminArea}, |||| ${first
//          .locality},  |||| ${first.featureName}, |||| ${first
//          .thoroughfare}, ||||  ${first.subThoroughfare} |||| ${first
//          .countryName} |||| ${first.countryCode} |||| ${first
//          .subAdminArea} |||| ${first.postalCode} |||| ');
//
//      initTextControllers();
//    });
//  }
//
//  initTextControllers() {
//    setState(() {
//      house.text =
//          "${first.featureName} , ${first.subLocality}".replaceAll('null', "");
//      streetName.text =
//          "${first.thoroughfare} , ${first.locality}".replaceAll('null', "");
//      pincode.text = "${first.postalCode}".replaceAll('null', "");
//      city.text = "${first.subAdminArea}".replaceAll('null', "");
//      state.text = "${first.adminArea}".replaceAll('null', "");
//      loading = false;
//    });
//  }
//
//  getLocation() {
//    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
//    geolocator
//        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
//        .then((Position position) {
//      _currentPosition = position;
//        if(addingAddress){
//          lat = _currentPosition.latitude;
//          lng = _currentPosition.longitude;
//          print("locatin......$lat, $lng");
//          addNewAddress();
//        }else{
//          lat = _currentPosition.latitude;
//          lng = _currentPosition.longitude;
//          getFullAddress();
//        }
//    }).catchError((e) {
//      print(e);
//    });
//  }
//
//  fillAddress() {
//    setState(() {
//      if (widget.edit) {
////        var cAddress = widget.details.address.split(',');
////        int len = cAddress.length;
//        house.text = widget.details.flatHouseno;
//        streetName.text = widget.details.colonyStreet;
//        state.text = widget.details.state;
//        pincode.text = widget.details.pincode.toString();
//        fullNAme.text = widget.details.fullName;
//        city.text = widget.details.city;
//
////        pincode.text = cAddress.;
////        house.text = cAddress.split(',') as String;
//      }
//    });
//  }
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    if(widget.addressId != null)
//      editAddress = true;
//    fillAddress();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Stack(
//      children: <Widget>[
//        Scaffold(
//          appBar: AppBar(
//            centerTitle: true,
//            backgroundColor: mColor,
//            elevation: 8.0,
//            title: editAddress?
//            Text(
//              "Edit Address", style: TextStyle(color: Colors.white),):
//            Text(
//              "Create New Address", style: TextStyle(color: Colors.white),),
//            leading: IconButton(
//              onPressed: () {
//                Navigator.pop(context);
//              },
//              icon: Icon(Icons.arrow_back_ios),
//              color: Colors.white,
//            ),
//          ),
//          body: SingleChildScrollView(
//            child: Column(
//              children: <Widget>[
//                Center(
//                  child: Column(
//                    children: <Widget>[
//                      Row(
//                        children: <Widget>[
//                          Expanded(
//                            flex: 1,
//                            child: SizedBox(),
//                          ),
//                          Expanded(
//                            flex: 3,
//                            child: OutlineButton(
//                              borderSide: BorderSide(color: Colors.blue),
//                              onPressed: () {
//                                setState(() {
//                                  loading = true;
//                                });
//                                getLocation();
//                              },
//                              child: Row(
//                                mainAxisAlignment: MainAxisAlignment.center,
//                                children: <Widget>[
//                                  Icon(Icons.my_location, color: Colors.blue,),
//                                  SizedBox(width: 10,),
//                                  Text(
//                                    "Get Current Location",
//                                    style: TextStyle(color: Colors.blue),)
//                                ],),
//                            ),
//                          ),
//                          Expanded(
//                            flex: 1,
//                            child: SizedBox(),
//                          ),
//                        ],
//                      )
//                    ],),
//                ),
//                Card(
//                  child: Container(
//                    padding: EdgeInsets.all(15),
//                    child: Column(
//                      children: <Widget>[
//                        Align(
//                          alignment: Alignment.topRight,
//                          child: RichText(
//                            text: TextSpan(
//                                children: <TextSpan>[
//                                  TextSpan(text: "*",
//                                      style: TextStyle(color: mColor)),
//                                  TextSpan(text: " Mandatory fields",
//                                      style: TextStyle(color: Colors.black)),
//                                ]
//                            ),
//                          ),
//                        ),
//                        TextField(
//                          controller: house,
//                          maxLength: 50,
//                          scrollPadding: EdgeInsets.all(8.0),
//                          inputFormatters: [
////                            WhitelistingTextInputFormatter(RegExp('[\d-]')),
//                            WhitelistingTextInputFormatter(RegExp(
//                                "[a-zA-Z 0-9]"))
//                          ],
//                          decoration: InputDecoration(
//                            counterText: "",
////                            helperText: "House No. / Flat No./ Building Name*",
//                            labelText: "House No. / Flat No./ Building Name*",
//                            hintText: "House No. / Flat No./ Building Name*",
//                          ),
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                        TextField(
//                          maxLength: 40,
//                          controller: streetName,
//                          scrollPadding: EdgeInsets.all(8.0),
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter(RegExp(
//                                "[a-zA-Z 0-9 ,-/]"))
//                          ],
//                          decoration: InputDecoration(
//                            counterText: "",
//                            hintText: "Colony/ Street Name*",
//                          ),
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                        TextField(
//                          controller: pincode,
//                          maxLength: 6,
//                          onChanged: (value){
//                            if(value.length == 6) {
//                              FocusScope.of(context).requestFocus(FocusNode());
//                            }
//                          },
//                          maxLengthEnforced: true,
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter(RegExp("[0-9]"))
//                          ],
//                          keyboardType: TextInputType.numberWithOptions(),
//                          scrollPadding: EdgeInsets.all(8.0),
//                          decoration: InputDecoration(
//                            counterText: "",
//                            hintText: "Pincode*",
//                          ),
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                        Row(
//                          children: <Widget>[
//                            Expanded(
//                              flex: 1,
//                              child: TextField(
//                                controller: city,
//                                maxLength: 30,
//                                scrollPadding: EdgeInsets.all(8.0),
//                                inputFormatters: [
//                                  WhitelistingTextInputFormatter(
//                                      RegExp("[a-zA-Z 0-9 - ,]"))
//                                ],
//                                decoration: InputDecoration(
//                                  counterText: "",
//                                  hintText: "City*",
//                                ),
//                              ),
//                            ),
//                            SizedBox(
//                              width: 15,
//                            ),
//                            Expanded(
//                              flex: 1,
//                              child: TextField(
//                                maxLength: 30,
//                                controller: state,
//                                scrollPadding: EdgeInsets.all(8.0),
//                                inputFormatters: [
//                                  WhitelistingTextInputFormatter(
//                                      RegExp("[a-zA-Z 0-9 - ,]"))
//                                ],
//                                decoration: InputDecoration(
//                                  counterText: "",
//                                  hintText: "State*",
//                                ),
//                              ),
//                            )
//                          ],
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                        TextField(
//                          maxLength: 30,
//                          controller: landmark,
//                          scrollPadding: EdgeInsets.all(8.0),
//                          decoration: InputDecoration(
//                            counterText: "",
//                            hintText: "Landmark (optional)",
//                          ),
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                      ],
//                    ),
//                  ),
//                ),
//                Card(
//                  child: Container(
//                    padding: EdgeInsets.all(15),
//                    child: Column(
//                      children: <Widget>[
//                        TextField(
//                          maxLength: 30,
//                          controller: fullNAme,
//                          scrollPadding: EdgeInsets.all(8.0),
//                          inputFormatters: [
//                            WhitelistingTextInputFormatter(RegExp(
//                                "[a-zA-Z 0-9 - ,]"))
//                          ],
//                          decoration: InputDecoration(
//                            counterText: "",
//                            hintText: "Full Name*",
//                          ),
//                        ),
//                        SizedBox(
//                          height: 15,
//                        ),
//                      ],
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
//          bottomNavigationBar: SafeArea(
//            child: Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: ButtonTheme(
//                  shape: RoundedRectangleBorder(
//                      borderRadius: BorderRadius.circular(25)
//                  ),
//                  minWidth: double.infinity,
//                  height: 50.0,
//                  child: IgnorePointer(
//                    ignoring: ignoreButton,
//                    child: RaisedButton(
//                      child: Text("Submit"),
//                      onPressed: () {
//                        setState(() {
//                          ignoreButton = true;
//                        });
//                        print(house.text);
//                        validateFields();
//                      },
//                      textColor: Colors.white,
//                      color: mColor,
//                    ),
//                  )
//              ),
//            ),
//          ),
//        ),
//        Visibility(
//          visible: loading,
//          child: Container(
//            child: BackdropFilter(
//              filter: ImageFilter.blur(
//                sigmaX: 5.0,
//                sigmaY: 5.0,
//              ),
//              child: Center(child: CircularProgressIndicator()),
//            ),
//          ),
//        )
//      ],
//    );
//  }
//}
