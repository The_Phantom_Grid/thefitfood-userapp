import 'package:flutter/material.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetOrders.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/OrderDetail.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../constants.dart';
import 'MainApp.dart';

_MyOrdersState myOrdersState;

class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() {
    myOrdersState = _MyOrdersState();
    return myOrdersState;
  }
}

class _MyOrdersState extends State<MyOrders> {
  bool isloading = true;

//  var mAccentColor = Color(0xFFf82d70);
  HttpRequests auth = HttpRequests();
  Order order;
  Recent recentOrder;

  getRecentOrders() async {
//    try {
    var r = await auth.getOrders(USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      order = Order.fromJson(r);
//        recentOrder = Recent.fromJson(r);
//        isloading = false;
    });
    if (order.myorder == null) {
      setState(() {
        order = null;
        isloading = false;
      });
    } else if (order.myorder.recent.length == 0) {
      setState(() {
        order = null;
        isloading = false;
      });
    } else {
      setState(() {
        isloading = false;
      });
    }
//    }
//    catch (e) {
//      if (e.toString().contains(
//          "'String' is not a subtype of type 'Map<String, dynamic>'")) {
//        setState(() {
//          order = null;
//          isloading = false;
//        });
//      }
//      print(e.toString());
//    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getRecentOrders();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0.0,
          backgroundColor: mColor,
          centerTitle: true,
          title: Text(
            "My Orders",
            style: TextStyle(color: Colors.white),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => MainApp()),
                  (Route<dynamic> route) => false);
            },
          )),
      body: WillPopScope(
        onWillPop: () {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(builder: (context) => MainApp()),
              (Route<dynamic> route) => false);
        },
        child: isloading
            ? neoLoader()
            : order == null
                ? Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "No Order Yet",
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Image.asset(
                          'drawables/start_shopping.png',
                          height: 150,
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text("Please Place Item In Your Cart"),
                        SizedBox(
                          height: 10,
                        ),
                        ButtonTheme(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25)),
                          height: 40,
                          minWidth: 200,
                          child: RaisedButton(
                            onPressed: () {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => MainApp()));
                            },
                            child: Text("Start Shopping"),
                            color: mColor,
                            textColor: Colors.white,
                          ),
                        )
                      ],
                    ),
                  )
                : Center(
                    child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return Divider(
                        height: 0,
                        indent: 80,
                        endIndent: 10,
                      );
                    },
                    itemCount: order.myorder.recent.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: () async {
                          await Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OrderDetail(
                                      order.myorder.recent[index].id)));
                          getRecentOrders();
                        },
                        child: Container(
                          height: 100,
                          child: Center(
                            child: ListTile(
                              contentPadding:
                                  EdgeInsets.symmetric(horizontal: 8),
                              leading: ClipOval(
                                child: Container(
                                  height: 55,
                                  width: 55,
                                  decoration: BoxDecoration(
                                      border: order.myorder.recent[index]
                                                  .storeLogo
                                                  .toString()
                                                  .isEmpty ||
                                              order.myorder.recent[index]
                                                      .storeLogo ==
                                                  null
                                          ? Border.all(color: mColor, width: .4)
                                          : Border.all(
                                              color: Colors.white, width: .4),
                                      borderRadius: BorderRadius.circular(30)),
                                  child: order.myorder.recent[index].storeLogo
                                              .toString()
                                              .isEmpty ||
                                          order.myorder.recent[index]
                                                  .storeLogo ==
                                              null
                                      ? Image(
                                      height: 100,
                                      width:100,
                                          image: AssetImage(
                                              'drawables/logo_neo_mart_splash.png'),
                                        )
                                      : Image.network(
                                          order.myorder.recent[index].storeLogo,
                                          fit: BoxFit.cover,
                                        ),
                                ),
                              ),
                              title: order.myorder.recent[index].paymentMode ==
                                          "paytm" &&
                                      order.myorder.recent[index].paymentStatus
                                              .toLowerCase() ==
                                          "unpaid"
                                  ? Text(
                                      "Transaction Failed",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    )
                                  : Text(
                                      "${order.myorder.recent[index].orderStatus}",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    "${order.myorder.recent[index].storeName}",
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                  Text(
                                      "Order id: ${order.myorder.recent[index].id}",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 12)),
                                  Text(
                                    "Placed at ${order.myorder.recent[index].orderDate}",
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ],
                              ),
                              trailing: Icon(
                                Icons.arrow_forward_ios,
                                color: mColor,
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                  )),
      ),
    );
  }
}
