import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

import '../Components/CommonUtility.dart';
import 'LoginRegister/LoginPage.dart';

class IntroSreen extends StatefulWidget {
  @override
  _IntroSreenState createState() => _IntroSreenState();
}

class _IntroSreenState extends State<IntroSreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
      ),
      body: IntroductionScreen(
        pages: <PageViewModel>[
          PageViewModel(
            title: "Connect With Your Favorite Retailers",
            bodyWidget: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'drawables/logo_neo_mart_splash.png',
                    height: 100,
                    width:100                )
              ],
            ),
            image: Center(child: Image.asset('drawables/connect.gif')),
          ),
          PageViewModel(
            title: "Exciting Offers & Discounts",
            bodyWidget: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'drawables/logo_neo_mart_splash.png',
                  height: 60,
                )
              ],
            ),
            image: Center(child: Image.asset('drawables/offer.gif')),
          ),
        ],
        showSkipButton: true,
        skip: Text("Skip"),
        onSkip: () {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => LoginPage()));
        },
        next: Text("Next"),
        showNextButton: true,
        done: Text("Done", style: TextStyle(fontWeight: FontWeight.w600)),
        onDone: () {
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => LoginPage()));
        },
        globalBackgroundColor: Colors.white,
        curve: Curves.bounceOut,
        dotsDecorator: DotsDecorator(
            size: const Size.square(10.0),
            activeSize: const Size(20.0, 10.0),
            activeColor: mColor,
            color: Colors.black26,
            spacing: const EdgeInsets.symmetric(horizontal: 3.0),
            activeShape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.0))),
      ),
    );
  }
}
