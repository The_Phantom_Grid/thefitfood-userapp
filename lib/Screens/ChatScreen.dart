import 'dart:convert';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:string_validator/string_validator.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:userapp/Components/Chats.dart';
import 'package:userapp/Getters/GetOrderDetail.dart';
import 'package:userapp/Getters/GetProfile.dart';
import 'package:userapp/Getters/GetStoreDetails.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:userapp/Screens/MainApp.dart';

import '../Components/CommonUtility.dart';
import '../constants.dart';

class ChatScreen extends StatefulWidget {
//  User user;
//  String orderId;
//  ShopDetails shopDetails;
//  Orderdetails orderdetails;
  Map<String, dynamic> data;
  String source;
  User user;

  ChatScreen(this.data, this.source, this.user);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  TextEditingController chatController = TextEditingController();
  String chatWith, userName, chatUserName, childRef, fcmToken, msg, nTitle;

//  final dbInstance =  FirebaseDatabase.instance;
  DatabaseReference dbRef,
      dbRef2,
      registeredUsersRef,
      messageUsersRef,
      merchantRef;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  HttpRequests auth = HttpRequests();
  String chatDate, childRef2;
  int chatTime, msgCounter = 0;
  Profile profile;
  ProfileData profileData;
  User user;
  String orderId,
      merchantId,
      storeId,
      gTotal,
      totalItems,
      imageUrl,
      logo,
      deliveryCharge,
      subTotal,
      phone,
      location,
      storeName,
      lastMessage;
  ShopDetails shopDetails;
  Orderdetails orderdetails;
  bool loading = true;

//  getReference() {
//    dbRef = FirebaseDatabase.instance.reference().child("oneToOneChatUserModel").child("$childRef");
//  }

  initValues() {
    var values = [];
    values = widget.data.values.toList();
    user = values[0];
    orderdetails = values[1];
    shopDetails = values[2];
    orderId = orderdetails.orderId.toString();
    merchantId = shopDetails.merchantId.toString();
    storeId = shopDetails.id.toString();
    gTotal = orderdetails.grandtotal.toString();
    totalItems = orderdetails.orderedItems.length.toString();
    imageUrl = orderdetails.storeImage.toString();
    logo = shopDetails.logo.toString();
    deliveryCharge = orderdetails.shipping.toString();
    subTotal = orderdetails.subtotal.toString();
    phone = shopDetails.phoneNo.toString();
    location = shopDetails.location.toString();
    storeName = shopDetails.storeName.toString();

    initDbReference();
  }

  getValues() {
    var values = [];
    values = widget.data.values.toList();
    user = values[0];
    print("USER ->>>>> $user");
    merchantId = values[1];
    storeId = values[2];
    gTotal = values[3];
    totalItems = values[4];
    imageUrl = values[5];
    logo = values[6];
    deliveryCharge = values[7];
    subTotal = values[8];
    phone = values[9];
    location = values[10];
    storeName = values[11];
    orderId = values[12];
    chatDate = values[13];
    chatTime = int.parse(values[14]);
    lastMessage = values[15].toString();

    print("MESSAGE $lastMessage");

    initDbReference();
  }

  initDbReference() async {
    print("merchatnid: ${merchantId.toString()}");
    chatWith = merchantId.toString() + storeId.toString() + orderId.toString();
    print("chatWith : $chatWith");
    userName = user.data.loginDetails.userId.toString();
    print("userName : $userName");
    childRef = chatWith + "_" + userName;
    childRef2 = "$userName\_$chatWith";
    print("child : $childRef");
    dbRef = FirebaseDatabase.instance
        .reference()
        .child("oneToOneChatUserModel")
        .child("$childRef");
    dbRef2 = FirebaseDatabase.instance
        .reference()
        .child("oneToOneChatUserModel")
        .child("$childRef2");
    setState(() {
      loading = false;
    });
    registeredUsersRef = FirebaseDatabase.instance
        .reference()
        .child("registeredUser")
        .child(merchantId.toString())
        .child("fcmToken");
    messageUsersRef = FirebaseDatabase.instance
        .reference()
        .child("mesageUsers")
        .child("user");
    merchantRef = FirebaseDatabase.instance
        .reference()
        .child("mesageUsers")
        .child("merchant");
    await getMsgCount();
    if (!widget.source.contains("OrderDetails")) resetMsgCounter();
//    firebaseListner();
  }

  getMsgCount() async {
    msgCounter = 0;
    await messageUsersRef
        .child("${user.data.loginDetails.userId}$orderId")
        .child("${merchantId}${storeId}")
        .child("messageData")
        .child('msgCounter')
        .once()
        .then((DataSnapshot dataSnapshot) => msgCounter =
            dataSnapshot.value == null || dataSnapshot.value == ""
                ? 0
                : int.parse(dataSnapshot.value));
    print("Chat Count >> $msgCounter");
  }

  resetMsgCounter() {
    merchantRef
        .child("${merchantId}${storeId}${orderId}")
        .child("${userName}")
        .child("messageData")
        .set({
      'address': "${location}".toString(),
      'chatDate': chatDate.toString(),
      'chatTime': chatTime.toString(),
      'customerOrderId': user.data.loginDetails.userId.toString(),
      'deliveryCharges': deliveryCharge.toString(),
      'grandTotalAmount': gTotal.toString(),
      'imageUrl': imageUrl.toString(),
      'merchantId': merchantId.toString(),
      'message': lastMessage.toString(),
      'mobile': phone.toString(),
      'msgCounter': "0",
      'orderId': orderId.toString(),
      'storeId': int.parse(storeId),
      'storeLogo': logo.toString(),
      'storeName': storeName.toString(),
      'subTotal': subTotal.toString(),
      'totalItem': totalItems.toString(),
      'user': profileData.username.toString(),
      'userUid': user.data.loginDetails.userId,
    });

//    messageUsersRef
//        .child("${user.data.loginDetails.userId}$orderId")
//        .child("$merchantId$storeId")
//        .child("messageData")
//        .set({
//      'address': location.toString(),
//      'chatDate': chatDate.toString(),
//      'chatTime': chatTime.toString(),
//      'customerOrderId': user.data.loginDetails.userId.toString(),
//      'deliveryCharges': deliveryCharge.toString(),
//      'grandTotalAmount': gTotal.toString(),
//      'imageUrl': imageUrl.toString(),
//      'merchantId': merchantId.toString(),
//      'message': chatController.text.toString(),
//      'mobile': phone.toString(),
//      'msgCounter': "0",
//      'orderId': orderId.toString(),
//      'storeId': int.parse(storeId),
//      'storeLogo': logo.toString(),
//      'storeName': storeName.toString(),
//      'subTotal': subTotal.toString(),
//      'totalItem': totalItems.toString(),
//      'user': profileData.firstname.toString(),
//      'userUid': profileData.id.toString(),
//    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProfileData();
//    firebaseListner();
//    getReference();
//    chatUserName = user.data.loginDetails.
  }

  getProfileData() async {
    var r = await auth.getProfile(widget.user.data.loginDetails.authKey);
    setState(() {
      profile = Profile.fromJson(r);
      profileData = profile.prifiledata[0];
    });
    if (widget.source.contains("OrderDetails"))
      initValues();
    else
      getValues();
  }

  firebaseListner() async {
    if (Platform.isIOS) iOS_Permission();

    firebaseMessaging.configure(
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
//        mainAppState.myBackgroundMessageHandler(message, context);
      },
      onMessage: (Map<String, dynamic> message) async {
        print("on message: $message");
//        mainAppState.myBackgroundMessageHandler(message, context);
      },
//      onLaunch: (Map<String, dynamic> message) async {
//        print('on launch $message');
////        mainAppState.myBackgroundMessageHandler(message, context);
//      },
    );
  }

//  Future<dynamic> myBackgroundMessageHandler(
//      Map<String, dynamic> message) async {
//    if (message.containsKey('data')) {
//      print("Data -> ");
//      final dynamic data = message['data'];
//      print("data   ${data['screen']}");
////      if (data['screen'] == "Chat")
//        Navigator.push(
//            context, MaterialPageRoute(builder: (context) => Chats(user)));
////      print(data);
//    }
//
//    if (message.containsKey('notification')) {
//      print("notification -> ");
//      final dynamic notification = message['notification'];
//      print(notification);
//    }
//
//    // Or do other work.
//  }

  getData() async {
    print("hit");
    var data;
    dbRef.once().then((value) => (DataSnapshot ds) {
          data = json.encode(ds.value);
          print(data);
        });
  }

  void iOS_Permission() {
    firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });
  }

  sendNotification() async {
    nTitle = "New message from customer";
    await auth.sendChatNotification(nTitle, msg, fcmToken, orderId,
        user.data.loginDetails.userId.toString());
  }

  sendMessage() async {
    print("counter: $msgCounter");
    msgCounter++;
    chatDate = DateFormat('dd-mm-yyy').format(DateTime.now());
    chatTime = DateTime.now().millisecondsSinceEpoch;
    dbRef.push().set({
      'chatDate': chatDate,
      'chatTime': chatTime,
      'id': "",
      'file': {'size_file': "", 'type': "", 'name_file': "", 'url_file': ""},
      'imageUrl': "${logo.toString()}",
      'message': chatController.text,
      'user': userName
    });

    dbRef2.push().set({
      'chatDate': chatDate,
      'chatTime': chatTime,
      'id': "",
      'file': {'size_file': "", 'type': "", 'name_file': "", 'url_file': ""},
      'imageUrl': "${logo.toString()}",
      'message': chatController.text,
      'user': userName
    });
    updateChatList();
    await registeredUsersRef.once().then((DataSnapshot data) {
      fcmToken = data.value;
      print(data.value);
    });
    print("fcmtoken: $fcmToken");
    sendNotification();
  }

  updateChatList() {
    merchantRef
        .child("${merchantId}${storeId}${orderId}")
        .child("${userName}")
        .child("messageData")
        .set({
      'address': "${location}".toString(),
      'chatDate': chatDate.toString(),
      'chatTime': chatTime.toString(),
      'customerOrderId': user.data.loginDetails.userId.toString(),
      'deliveryCharges': deliveryCharge.toString(),
      'grandTotalAmount': gTotal.toString(),
      'imageUrl': imageUrl.toString(),
      'merchantId': merchantId.toString(),
      'message': chatController.text.toString(),
      'mobile': phone.toString(),
      'msgCounter': msgCounter.toString(),
      'orderId': orderId.toString(),
      'storeId': int.parse(storeId),
      'storeLogo': logo.toString(),
      'storeName': storeName.toString(),
      'subTotal': subTotal.toString(),
      'totalItem': totalItems.toString(),
      'user': profileData.firstname.toString(),
      'userUid': profileData.id.toString(),
    });

    messageUsersRef
        .child("${user.data.loginDetails.userId}$orderId")
        .child("$merchantId$storeId")
        .child("messageData")
        .set({
      'address': location.toString(),
      'chatDate': chatDate.toString(),
      'chatTime': chatTime.toString(),
      'customerOrderId': user.data.loginDetails.userId.toString(),
      'deliveryCharges': deliveryCharge.toString(),
      'grandTotalAmount': gTotal.toString(),
      'imageUrl': imageUrl.toString(),
      'merchantId': merchantId.toString(),
      'message': chatController.text.toString(),
      'mobile': phone.toString(),
      'msgCounter': msgCounter.toString(),
      'orderId': orderId.toString(),
      'storeId': int.parse(storeId),
      'storeLogo': logo.toString(),
      'storeName': storeName.toString(),
      'subTotal': subTotal.toString(),
      'totalItem': totalItems.toString(),
      'user': profileData.firstname.toString(),
      'userUid': profileData.id.toString(),
    });

    setState(() {
      msg = chatController.text.toString();
      chatController.text = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      extendBody: true,
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
          backgroundColor: mColor,
          centerTitle: true,
          title: Text(
            storeName != null ? "${storeName}" : "Loading...",
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(color: Colors.white),
          ),
          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios, color: Colors.white),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          bottom: PreferredSize(
            preferredSize: Size(double.infinity, 30.0),
            child: FlexibleSpaceBar(
              centerTitle: true,
              title: Text(
                orderId != null ? "ORDER ID: ${orderId}" : "Loading...",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          )),
      body: loading
          ?  neoLoader()
          : Column(
              children: <Widget>[
                Expanded(
                    child: StreamBuilder<Event>(
                  stream: dbRef.onValue,
                  builder: (context, snapshot) {
                    if (snapshot.hasData &&
                        !snapshot.hasError &&
                        snapshot.data.snapshot.value != null) {
                      Map data = snapshot.data.snapshot.value;
                      List item = [];
                      data.values.forEach((element) {
                        item.add(Map.of(element));
                      });
                      item.sort((x, y) => int.parse(x['chatTime'].toString())
                          .compareTo(int.parse(y['chatTime'].toString())));
                      item = item.reversed.toList();
                      if (item.isNotEmpty) getMsgCount();

                      return ListView.builder(
                          reverse: true,
                          itemCount: item.length,
                          itemBuilder: (context, index) {
//                        int timeStamp = int.parse(item[index]['chatTime']) * 1000;
                            return Container(
                              margin: EdgeInsets.symmetric(
                                  vertical: 3, horizontal: 5),
                              alignment: item[index]['user'] == userName
                                  ? Alignment.centerRight
                                  : Alignment.centerLeft,
                              width: MediaQuery.of(context).size.width,
                              child: Padding(
                                padding: item[index]['user'] == userName
                                    ? const EdgeInsets.only(left: 20)
                                    : const EdgeInsets.only(right: 20),
                                child: Container(
                                    decoration: item[index]['user'] == userName
                                        ? BoxDecoration(
                                            color: Colors.greenAccent,
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(8),
                                                topRight: Radius.circular(15),
                                                bottomRight: Radius.circular(0),
                                                bottomLeft: Radius.circular(8)))
                                        : BoxDecoration(
                                            color: Colors.tealAccent,
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(15),
                                                topRight: Radius.circular(8),
                                                bottomRight: Radius.circular(8),
                                                bottomLeft:
                                                    Radius.circular(0))),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 6),
                                    child: Wrap(
                                      alignment: WrapAlignment.end,
                                      crossAxisAlignment:
                                          WrapCrossAlignment.end,
                                      children: <Widget>[
                                        isURL(item[index]['message']
                                                .toString()
                                                .replaceFirst("\n", ""))
                                            ? item[index]['message']
                                                    .toString()
                                                    .replaceFirst("\n", "")
                                                    .contains('maps')
                                                ? ListTile(
                                                    onTap: () async {
                                                      UrlLauncher.launch(
                                                          "${item[index]['message'].toString().replaceFirst("\n", "")}");
                                                    },
                                                    title: Text("Location"),
                                                    subtitle:
                                                        Text("Tap to open"),
                                                    leading: Icon(
                                                      Icons.location_on,
                                                      color:  mColor,
                                                    ),
                                                  )
                                                : GestureDetector(
                                                    onTap: () {
                                                      print(
                                                          "${Uri.parse(item[index]['message']).toString()}");
                                                      UrlLauncher.launch(
                                                          "https:${item[index]['message'].toString().replaceAll("https://", "")}");
                                                    },
                                                    child: Text(
                                                        "${item[index]['message']}",
                                                        style: TextStyle(
                                                            decoration:
                                                                TextDecoration
                                                                    .underline)))
                                            : Text("${item[index]['message']}"),
                                        SizedBox(width: 10),
                                        Text(
                                          "${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(int.parse(item[index]['chatTime'].toString())))}",
                                          style: TextStyle(fontSize: 9),
                                        ),
                                      ],
                                    )),
                              ),
                            );
                          });
                    } else if (snapshot.hasError) {
                      return Center(child: Text(snapshot.error.toString()));
                    } else if (!snapshot.hasData) {
                      return Center(child: Text("Loading..."));
                    } else {
                      return Center(child: Text("Start Conversation"));
                    }
                  },
                )),
                Container(
                  color: Colors.white70,
                  padding: EdgeInsets.all(8),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: TextField(
                          controller: chatController,
                          autofocus: false,
                          onSubmitted: (text) {
                            print("text");
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            enabledBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black12, width: 0),
                                borderRadius: BorderRadius.circular(25)),
                            focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Colors.black12, width: 0),
                                borderRadius: BorderRadius.circular(25)),
                            contentPadding: EdgeInsets.fromLTRB(12, 8, 8, 8),
                            hintStyle: TextStyle(fontSize: 14),
                            labelStyle: TextStyle(fontSize: 14),
//                    suffixIcon: Text(resultCount),
                            enabled: true,
                            hintText: "Type a message",
                          ),
                          cursorColor: Colors.black,
                          showCursor: true,
                        ),
                      ),
                      IconButton(
                        onPressed: () {
                          if (chatController.text != "") sendMessage();
                        },
                        icon: Icon(
                          Icons.send,
                          color: mColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
    );
  }
}
