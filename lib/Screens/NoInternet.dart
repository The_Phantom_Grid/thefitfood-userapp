import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NoInternet extends StatefulWidget {
  @override
  _NoInternetState createState() => _NoInternetState();
}

class _NoInternetState extends State<NoInternet> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      appBar: AppBar(
//        elevation: 0.0,
//        backgroundColor: Colors.white,
//        title: Image.asset('drawables/logo_neo_mart_splash.png', height: 40,),
//        centerTitle: true,
//        automaticallyImplyLeading: false,
//      ),

      body: WillPopScope(
        onWillPop: () async => false,
        child: Container(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 60,
              ),
              Image.asset(
                'drawables/logo_neo_mart_splash.png',
                height: 40,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Image.asset(
                      'drawables/no_internet.png',
                      height: 180,
                    ),
                    Text(
                      "Oh Shucks!",
                      style: TextStyle(color: Color(0xFF474747), fontSize: 20),
                    ),
                    Text(
                      "Slow or no internet connection.",
                      style: TextStyle(color: Color(0xFF474747), fontSize: 13),
                    ),
                    Text(
                      "Please check your internet settings",
                      style: TextStyle(color: Color(0xFF474747), fontSize: 13),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
