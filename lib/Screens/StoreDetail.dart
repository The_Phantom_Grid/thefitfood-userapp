import 'dart:convert';
import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:marquee/marquee.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetStoreDetails.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import '../constants.dart';

class StoreDetail extends StatefulWidget {
  String storeId;

  StoreDetail(this.storeId);

  @override
  _StoreDetailState createState() => _StoreDetailState();
}

class _StoreDetailState extends State<StoreDetail> {
  StoreDetails storeDetails;
  var auth = HttpRequests();

  List<Widget> images = [];
  bool getImage = false;
  Position _currentPosition;

  getStoreDetails() async {
    var r = await auth.getStoreDetail(
        widget.storeId, USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      var jData = json.decode(r.body);
      storeDetails = StoreDetails.fromJson(jData);
      print(storeDetails.data.shopDetails.id);
      if (storeDetails.data.storeImages.length != null) {
//        images = List(storeDetails.data.storeImages.length);
        initImages();
      }
    });
    print("Date: ${storeDetails.data.shopDetails.storeOpenTime}");
  }

  double lat, lng;

  getLatLng() async {
    var r = await auth.getProfile(USER_DETAILES.data.loginDetails.authKey);
//    ProfileData profileData = ProfileData.fromJson(r);
    lat = r['prifiledata'][0]['lat'];
    lng = r['prifiledata'][0]['lng'];
    print(" profile data LAT::: ${lat} || LONG::: ${lng}");
    setDirections();
  }

  initImages() {
    for (int x = storeDetails.data.storeImages.length - 1; x >= 0; x--) {
      if (storeDetails.data.storeImages[x].path.toString().isNotEmpty) {
        images.add(
          Image.network(
            storeDetails.data.storeImages[x].path.toString(),
            fit: BoxFit.fill,
          ),
        );
      }
    }
    setState(() {
      if (images.length == 1) getImage = true;
    });
  }

  setDirections() async {
    double storeLat = storeDetails.data.shopDetails.lat;
    double storeLng = storeDetails.data.shopDetails.lng;
    UrlLauncher.launch(
        "http://maps.google.com/maps?daddr=($storeLat,$storeLng)&saddr=(${lat},${lng})");
  }

//  getLocation() {
//    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
//    geolocator
//        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
//        .then((Position position) {
//          _currentPosition = position;
//          setDirections();
//    }).catchError((e) {
//      if (e.toString().contains("PERMISSION_DENIED")) {
//        showUpdateLocation();
//      }
//    });
//  }
//
//  showUpdateLocation() {
//    showModalBottomSheet(
//        context: context,
//        isDismissible: false,
//        isScrollControlled: true,
//        builder: (context) {
//          return Container(
//              padding: EdgeInsets.all(15),
////              height: MediaQuery
////                  .of(context)
////                  .size
////                  .height * .32,
//              width: MediaQuery
//                  .of(context)
//                  .size
//                  .width,
//              child: Column(
//                mainAxisSize: MainAxisSize.min,
//                children: <Widget>[
//                  Text("Cant't Access your location", style: TextStyle(
//                      fontSize: 22, fontWeight: FontWeight.bold)),
//                  SizedBox(height: 15,),
//                  Text(
//                      "In order to access your location you must provide location permission to application."),
//                  SizedBox(height: 15,),
//                  Row(children: <Widget>[
//                    Expanded(
//                      flex: 1,
//                      child: OutlineButton(
//                        onPressed: () {
//                          exit(0);
//                        },
//                        child: Text("Exit"),
//                      ),
//                    ),
//                    SizedBox(width: 15,),
//                    Expanded(
//                      flex: 1,
//                      child: FlatButton(
//                        onPressed: () {
//                          Navigator.pop(context);
//                          PermissionHandler().openAppSettings();
//                        },
//                        child: Text("Open Settings"),
//                        textColor: Colors.white,
//                        color: mColor,
//                      ),
//                    )
//                  ],)
//                ],)
//          );
//        });
//  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getStoreDetails();
  }

  @override
  Widget build(BuildContext context) {
    var controller = ScrollController();
    return Scaffold(
      body: storeDetails == null
          ? neoLoader()
          : CustomScrollView(
              slivers: <Widget>[
                SliverAppBar(
                  elevation: 8.0,
                  floating: false,
                  automaticallyImplyLeading: false,
                  pinned: true,
                  centerTitle: true,
                  flexibleSpace: FlexibleSpaceBar(
                    title: Marquee(
                        pauseAfterRound: Duration(seconds: 3),
                        crossAxisAlignment: CrossAxisAlignment.end,
                        velocity: 50,
                        blankSpace: 70.0,
                        style: TextStyle(color: Colors.white),
                        text: "${storeDetails.data.shopDetails.storeName}"),
                    background: CarouselSlider(
                        height: double.infinity,
                        aspectRatio: 16 / 9,
                        initialPage: 0,
                        scrollDirection: Axis.horizontal,
                        autoPlay: getImage ? false : true,
                        autoPlayInterval: Duration(seconds: 3),
                        enlargeCenterPage: false,
                        enableInfiniteScroll: getImage ? false : true,
                        items: images.isEmpty
                            ? [
                                Image(
                                  image: AssetImage(
                                    'drawables/logo_neo_mart_splash.png',
                                  ),
                                  height: 100,
                                  width:100,
                                )
                              ]
                            : images),
                    centerTitle: false,
                  ),
                  expandedHeight: 200,
                  backgroundColor: mColor,
                  leading: IconButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                  ),
                ),
                SliverList(
                  delegate: SliverChildListDelegate.fixed([
                    SizedBox(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Card(
                            margin: EdgeInsets.only(top: 8.0),
                            elevation: 2.0,
                            child: Column(
                              children: <Widget>[
                                ListTile(
                                  onTap: () {
                                    getLatLng();
                                  },
                                  dense: true,
                                  leading: Icon(Icons.location_on,
                                      color:  mColor),
                                  title: Text("Location",
                                      style: TextStyle(
                                          fontSize: 13,
                                          fontWeight: FontWeight.bold)),
                                  subtitle: Text(
                                      "${storeDetails.data.shopDetails.location}"),
                                ),
                                Divider(),
                                Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Owner",
                                        style: TextStyle(fontSize: 13),
                                      ),
                                      Divider(),
                                      Text(
                                        "${storeDetails.data.storeOwners.merchantOwner.toString()}",
                                        style: TextStyle(fontSize: 18),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Card(
                              margin: EdgeInsets.only(top: 8.0),
                              elevation: 2.0,
                              child: Container(
                                padding: EdgeInsets.all(12),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Store type",
                                      style: TextStyle(fontSize: 13),
                                    ),
                                    Divider(),
                                    Text(
                                      "${storeDetails.data.storeTypeName.toString()}",
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ],
                                ),
                              )),
                          Card(
                              margin: EdgeInsets.only(top: 8.0),
                              elevation: 2.0,
                              child: Container(
                                padding: EdgeInsets.all(12),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Store Manager",
                                      style: TextStyle(fontSize: 13),
                                    ),
                                    Divider(),
                                    Text(
                                      "${storeDetails.data.storeOwners.storeManager.toString()}",
                                      style: TextStyle(fontSize: 18),
                                    ),
                                  ],
                                ),
                              )),
                          Card(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "About",
                                        style: TextStyle(fontSize: 13),
                                      ),
                                      Divider(),
                                      Text(
                                        "${storeDetails.data.shopDetails.storeDescr.toString()}",
                                        style: TextStyle(fontSize: 15),
                                      ),
                                    ],
                                  ),
                                ),
                                Divider(),
                                Container(
                                  padding: EdgeInsets.all(12),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(
                                        "Store Opening time",
                                        style: TextStyle(fontSize: 13),
                                      ),
                                      Divider(),
                                      storeDetails.data.shopDetails
                                                  .storeOpenTime ==
                                              null
                                          ? Text("--")
                                          : Text(
                                              "${DateFormat('hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(DateTime.parse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${storeDetails.data.shopDetails.storeOpenTime}").millisecondsSinceEpoch))} to ${DateFormat('hh:mm a').format(DateTime.fromMillisecondsSinceEpoch(DateTime.parse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} ${storeDetails.data.shopDetails.storeCloseTime}").millisecondsSinceEpoch))}",
                                              style: TextStyle(fontSize: 18),
                                            ),
                                    ],
                                  ),
                                ),
                                Divider(),
                                ListTile(
                                  title: Text("Delivery Charges"),
                                  trailing: storeDetails.data.shopDetails
                                              .deliveryCharge ==
                                          null
                                      ? Text("--")
                                      : Text(
                                          "₹ ${storeDetails.data.shopDetails.deliveryCharge}",
                                          style: TextStyle(color: mColor)),
                                ),
                                ListTile(
                                  title: Text(
                                    "Minimum Order ",
                                  ),
                                  trailing: Text(
                                      "₹ ${storeDetails.data.shopDetails.minOrder}",
                                      style: TextStyle(color: mColor)),
                                ),
                                ListTile(
                                  title: Text("Delivery Time"),
                                  trailing: Text(
                                      "${storeDetails.data.shopDetails.deliveryTime} min"),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ], addRepaintBoundaries: true),
                )
              ],
            ),
    );
  }
}
