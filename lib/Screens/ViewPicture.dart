import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

class ViewPicture extends StatefulWidget {
  Uint8List image;

  ViewPicture(this.image);

  @override
  _ViewPictureState createState() => _ViewPictureState();
}

class _ViewPictureState extends State<ViewPicture> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
      ),
      body: Center(
        child: PhotoView(
          heroAttributes: PhotoViewHeroAttributes(tag: 'neomart_logo'),
          imageProvider: MemoryImage(widget.image),
        ),
      ),
    );
  }
}
