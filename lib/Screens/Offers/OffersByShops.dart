import 'package:flutter/material.dart';
import 'package:userapp/Screens/Offers/DiscountOffers.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../Components/CommonUtility.dart';
import '../../Getters/GetRunningOffer.dart';
import '../../Getters/GetShopOffer.dart';
import '../../constants.dart';

class OffersByShops extends StatefulWidget {
  User user;

  OffersByShops(this.user);

  @override
  _OffersByShopsState createState() => _OffersByShopsState();
}

class _OffersByShopsState extends State<OffersByShops> {
  var scrollController = ScrollController();
  double scrollPosition;
  HttpRequests auth = HttpRequests();
  int distance = 10;
  OfferByShop offerByShop, tempOfferByShop;
  bool loading = true, noOfferAvail = false;
  RunningOffer runningOffer;
  bool showMoreOffer = false;
  int MAX = 10, MIN = 0;

  _scrollListener() {
    setState(() {
      scrollPosition = scrollController.position.pixels;
      print(scrollPosition);
    });
  }

  getShopOffer() async {
    var r = await auth.getOffersByShops(
        distance, "10", "0", widget.user.data.loginDetails.authKey);
    setState(() {
      offerByShop = OfferByShop.fromJson(r);
      if (offerByShop.info.length == 0 && offerByShop.info.isEmpty) {
        print("empty");
        print(offerByShop.info);
        loading = false;
        noOfferAvail = true;
      } else {
        loading = false;
        noOfferAvail = false;
        print(offerByShop.info);
      }
    });
  }

  loadMore() async {
    MIN += MAX;
    var r = await auth.getOffersByShops(distance, MAX.toString(),
        MIN.toString(), widget.user.data.loginDetails.authKey);
    setState(() {
      tempOfferByShop = OfferByShop.fromJson(r);
      offerByShop.info.addAll(tempOfferByShop.info);
      showMoreOffer = false;
    });
  }

  Widget showMore() {
    return GestureDetector(
      onTap: () {
        setState(() {
          showMoreOffer = true;
        });
        loadMore();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.all(16),
        child: showMoreOffer
            ? Text(
                "Loading...",
                textAlign: TextAlign.center,
              )
            : Text(
                "Show more",
                textAlign: TextAlign.center,
              ),
      ),
    );
  }

//  getRunningOffer() async {
//    var r = await auth.getRunningOffers(
//        distance, widget.user.data.loginDetails.authKey);
//    setState(() {
//      runningOffer = RunningOffer.fromJson(r);
//      loading = false;
//    });
//  }

  @override
  void initState() {
    getShopOffer();
    scrollPosition = 0.0;
    scrollController = ScrollController();
    scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 8,
        backgroundColor: mColor,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        title: Text("Offers By Shops"),
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: loading
          ?  neoLoader()
          : noOfferAvail
              ? Center(
                  child: Text(
                    "No Offers Available",
                    style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                )
              : Container(
                  child: ListView.builder(
                      controller: scrollController,
                      scrollDirection: Axis.vertical,
                      itemCount: offerByShop.info.length < MIN
                          ? offerByShop.info.length
                          : offerByShop.info.length + 1,
                      itemBuilder: (context, index) {
                        return index == offerByShop.info.length
                            ? showMore()
                            : buildOfferList(offerByShop.info[index],
                                widget.user, runningOffer);
                      }),
                ),
      floatingActionButton: Visibility(
        visible: scrollPosition < 150.0 ? false : true,
        child: FloatingActionButton(
          onPressed: () {
            setState(() {
              scrollController.animateTo(0,
                  duration: Duration(milliseconds: 800),
                  curve: Curves.decelerate);
            });
          },
          child: Icon(Icons.arrow_upward),
        ),
      ),
    );
  }
}

class buildOfferList extends StatefulWidget {
  ShopOfferInfo shopOfferInfo;
  User user;
  RunningOffer runningOffer;

  buildOfferList(this.shopOfferInfo, this.user, this.runningOffer);

  @override
  _buildOfferListState createState() => _buildOfferListState();
}

class _buildOfferListState extends State<buildOfferList> {
  var mColor = Color(0xFFe9991e);
  int _itemCount = 0;
  bool _addButton = true, _plusMinus = false;
  int discount;
  String offer_id = "", offer_price = "";
  HttpRequests auth = HttpRequests();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Card(
          elevation: 8,
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    InkWell(
                      onTap: () {
                        print("ppppp");
                        print(widget.shopOfferInfo.logo);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => new DiscountOffers(
                                    widget.user, widget.shopOfferInfo)));
                      },
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(6),
                              child: Image.network(
                                widget.shopOfferInfo.logo,
                                height: 100,
                                width: 100,
                                fit: BoxFit.contain,
                              ),
                            ),
                            Expanded(
                              child: ListTile(
                                title: Text(
                                  "${widget.shopOfferInfo.storeName}",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                subtitle: Text(
                                  "${widget.shopOfferInfo.address}",
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
                color: mColor,
                width: MediaQuery.of(context).size.width,
                child: Text(
                  "${widget.shopOfferInfo.description}",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ],
          )),
    );
  }
}
