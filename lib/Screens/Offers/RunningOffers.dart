import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/OfferProduct.dart';
import 'package:userapp/Components/ProductDetail.dart';
import '../../Components/CommonUtility.dart';
import '../../Getters/GetRunningOffer.dart';
import '../../Getters/GetUser.dart';
import '../../Network/httpRequests.dart';
import '../../constants.dart';

_RunningOffersState runningOffersState;

class RunningOffers extends StatefulWidget {
  User user;

  RunningOffers(this.user);

  @override
  _RunningOffersState createState() {
    runningOffersState = _RunningOffersState();
    return runningOffersState;
  }
}

class _RunningOffersState extends State<RunningOffers> {
  var scrollController = ScrollController();
  double scrollPosition;
  HttpRequests auth = HttpRequests();
  int distance = 10;
  RunningOffer runningOffer, tempRunningOffer;
  bool loading = true,
      showButton = false,
      showMoreOffer = false,
      noOffer = false;
  int MAX = 10, MIN = 0;

  _scrollListener() {
    setState(() {
      scrollPosition = scrollController.position.pixels;
      print(scrollPosition);
//      if (scrollPosition >= scrollController.position.maxScrollExtent ||
//          scrollPosition < 150)
//        showButton = false;
//      else
      if (scrollPosition > 150) showButton = true;
    });
  }

  getRunningOffer() async {
    setState(() {
      loading = true;
      runningOffer = null;
    });
    var r = await auth.getRunningOffers(
        distance, "10", "0", widget.user.data.loginDetails.authKey);
    setState(() {
      runningOffer = RunningOffer.fromJson(r);
      loading = false;
      if (runningOffer == null)
        noOffer = true;
      else
        noOffer = false;
    });
  }

  loadMore() async {
    MIN += MAX;
    var r = await auth.getRunningOffers(distance, MAX.toString(),
        MIN.toString(), widget.user.data.loginDetails.authKey);
    setState(() {
      tempRunningOffer = RunningOffer.fromJson(r);
      runningOffer.info.addAll(tempRunningOffer.info);
      if (runningOffer == null)
        noOffer = true;
      else
        noOffer = false;
      showMoreOffer = false;
    });
  }

  Widget showMore() {
    return GestureDetector(
      onTap: () {
        setState(() {
          showMoreOffer = true;
        });
        loadMore();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(16),
        margin: EdgeInsets.all(16),
        child: showMoreOffer
            ? Text(
                "Loading...",
                textAlign: TextAlign.center,
              )
            : Text(
                "Show more",
                textAlign: TextAlign.center,
              ),
      ),
    );
  }

  @override
  void initState() {
    getRunningOffer();
    scrollPosition = 0.0;
    scrollController = ScrollController();
    scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final StreamVars streamVars = Get.put(StreamVars());
    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: Scaffold(
        appBar: AppBar(
          elevation: 8,
          backgroundColor: mColor,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
          title: Text("Running Discounts"),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            loading
                ?  neoLoader()
                : Container(
                    child: ListView.builder(
                        padding: EdgeInsets.only(bottom: 70),
                        controller: scrollController,
                        scrollDirection: Axis.vertical,
                        itemCount: runningOffer.info.length < MIN
                            ? runningOffer.info.length
                            : runningOffer.info.length + 1,
                        itemBuilder: (context, index) {
                          return index == runningOffer.info.length
                              ? showMore()
                              : buildOfferList(
                                  runningOffer.info[index], widget.user);
                        }),
                  ),
            neoLoaderStack()
          ],
        ),
        floatingActionButton: Visibility(
          visible: showButton,
          child: FloatingActionButton(
            onPressed: () {
              setState(() {
                scrollController.animateTo(0,
                    duration: Duration(milliseconds: 800),
                    curve: Curves.decelerate);
              });
            },
            child: Icon(Icons.arrow_upward),
          ),
        ),
      ),
    );
  }
}

class buildOfferList extends StatefulWidget {
  RunningOffersInfo runningOfferInfo;
  User user;

  buildOfferList(this.runningOfferInfo, this.user);

  @override
  _buildOfferListState createState() => _buildOfferListState();
}

class _buildOfferListState extends State<buildOfferList> {
  int _itemCount = 0;
  bool _addButton = true, _plusMinus = false;
  int discount;
  String offer_id = "", offer_price = "";
  HttpRequests auth = HttpRequests();
  StreamVars streamVars = Get.put(StreamVars());

  addItem() {
    if (_itemCount == widget.runningOfferInfo.totalLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color:  mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color:  mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
//      appState.increment(_itemCount);
      addToCart(_itemCount);
    }
  }

  addToCart(itemCount) async {
    streamVars.addingItem.value = true;
    print(widget.runningOfferInfo.masterProductId);
    print(widget.runningOfferInfo.merchantId);
    await auth.addItemToCart(
        widget.runningOfferInfo.masterProductId,
        widget.runningOfferInfo.merchantproductList,
        offer_id,
        offer_price,
        itemCount,
        widget.runningOfferInfo.merchantId,
        widget.runningOfferInfo.storeId,
        widget.user.data.loginDetails.userId,
        widget.user.data.loginDetails.authKey);

    streamVars.addingItem.value = false;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    discount =
        ((widget.runningOfferInfo.mrp - widget.runningOfferInfo.sellingPrice) /
                widget.runningOfferInfo.mrp *
                100)
            .toInt();
    _itemCount = widget.runningOfferInfo.shoppingListQty;
    if (widget.runningOfferInfo.shoppingListQty != 0) {
      _addButton = false;
      _plusMinus = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Card(
          elevation: 8,
          child: Container(
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () async {
                    await Get.to(ProductDetailPage(
                        widget.runningOfferInfo.id.toString()));
                    runningOffersState.getRunningOffer();
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Image.network(
                          widget.runningOfferInfo.productImage.toString(),
                          height: 100,
                          width: 100,
                          fit: BoxFit.contain,
                        ),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              "${widget.runningOfferInfo.productName}",
                              maxLines: 2,
                              style: TextStyle(fontSize: 14),
                              overflow: TextOverflow.ellipsis,
                            ),
                            subtitle: Text(
                              "${widget.runningOfferInfo.storeName}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                AutoSizeText(
                                  "₹ ${double.tryParse(widget.runningOfferInfo.sellingPrice.toString()).toStringAsFixed(2)}",
                                  style: TextStyle(color: mColor),
                                  maxFontSize: 13,
                                  minFontSize: 9,
                                ),
                                AutoSizeText(
                                  "₹ ${widget.runningOfferInfo.mrp} ",
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.black),
                                  maxFontSize: 13,
                                  minFontSize: 9,
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 4.0, right: 4.0),
                  alignment: Alignment.bottomRight,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(Icons.local_offer, color: mColor),
                          Text(" ${discount}% off"),
                        ],
                      ),
                      Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 120,
                              height: 40,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  border: Border.all(color: mColor)),
                              child: Visibility(
                                visible: _addButton,
                                child: OutlineButton(
                                  textColor: mColor,
                                  onPressed: () {
                                    setState(() {
                                      print("hit");
//                                                            _itemCount++;
                                      addItem();
                                      _addButton = false;
                                      _plusMinus = true;
                                    });
                                  },
                                  borderSide:
                                      BorderSide(color: Colors.transparent),
                                  child: Text("Add"),
                                ),
                              ),
                            ),
                            Visibility(
                              visible: _plusMinus,
                              child: Container(
                                height: 40,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    IconButton(
                                      color: mColor,
                                      onPressed: () {
                                        setState(() {
                                          if (_itemCount <= 1) {
                                            _plusMinus = false;
                                            _addButton = true;
                                            _itemCount--;
                                            addToCart(_itemCount);
                                          } else
                                            _itemCount--;
                                          addToCart(_itemCount);
                                        });
                                      },
//                                      borderSide: BorderSide(
//                                          color: Colors.transparent),
                                      icon: Icon(Icons.remove),
                                    ),
                                    Container(
                                        width: 25,
                                        child: Center(
                                            child: Text("${_itemCount}",
                                                style:
                                                    TextStyle(fontSize: 13)))),
                                    IconButton(
                                      color: mColor,
                                      onPressed: () {
                                        setState(() {
                                          addItem();
                                        });
                                      },
//                                      borderSide: BorderSide(
//                                          color: Colors.transparent),
                                      icon: Icon(Icons.add),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
