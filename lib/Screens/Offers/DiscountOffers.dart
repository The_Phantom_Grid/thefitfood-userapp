import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Getters/GetComboOffer.dart';
import 'package:userapp/Getters/GetRunningOffer.dart';
import 'package:userapp/Getters/GetShopOffer.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/StoreInventory.dart';

import '../../Components/CommonUtility.dart';
import '../../Network/httpRequests.dart';
import '../../constants.dart';

class DiscountOffers extends StatefulWidget {
  User user;
  ShopOfferInfo shopOfferInfo;

  DiscountOffers(this.user, this.shopOfferInfo);

  @override
  _DiscountOffersState createState() => _DiscountOffersState();
}

class _DiscountOffersState extends State<DiscountOffers> {
  var scrollController = ScrollController();
  double scrollPosition;
  HttpRequests auth = HttpRequests();
  int distance = 10;
  bool loading = true, noOffer = false;

  List<RunningOffersInfo> offerList = [];
  String src = "StoreInventory";

  ComboOffer comboOffer;

  getComboOffers() async {
    var r = await auth.getComboOffer(widget.user.data.loginDetails.authKey,
        distance, widget.shopOfferInfo.storeId);
    setState(() {
      comboOffer = ComboOffer.fromJson(r);
      if (comboOffer.info.length == 0 && comboOffer.info.isEmpty) {
        setState(() {
          loading = false;
          noOffer = true;
        });
      } else {
        print(comboOffer.info);
        setState(() {
          loading = false;
          noOffer = false;
        });
      }
    });
  }

//  initList() {
//    print("init...");
//    setState(() {
//      offerList.clear();
//      for (int x = 0; x < widget.runningOffer.info.length; x++) {
//        print("runing...");
//        if (widget.runningOffer.info[x].merchantId
//            .toString()
//            .contains(widget.shopOfferInfo.merchantId.toString())) {
//          offerList.add(widget.runningOffer.info[x]);
//          print("found...");
//        }
//      }
//      loading = false;
//      print("dead...");
//    });
//  }

  _scrollListener() {
    setState(() {
      scrollPosition = scrollController.position.pixels;
      print(scrollPosition);
    });
  }

  @override
  void initState() {
//    initList();
    getComboOffers();
    scrollPosition = 0.0;
    scrollController = ScrollController();
    scrollController.addListener(_scrollListener);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final StreamVars streamVars = Get.put(StreamVars());
    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: Scaffold(
        appBar: AppBar(
          elevation: 8,
          backgroundColor: mColor,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
          title: Text("${widget.shopOfferInfo.storeName}"),
          centerTitle: true,
        ),
        backgroundColor: Colors.white,
        body: Stack(
          children: <Widget>[
            loading
                ?  neoLoader()
                : noOffer
                    ? Center(
                        child: Text("No Offers Listed by Owner."),
                      )
                    : Container(
                        child: Offers(widget.shopOfferInfo.storeId.toString(),
                            widget.user)),
            neoLoaderStack()
          ],
        ),
        floatingActionButton: Visibility(
          visible: scrollPosition < 150.0 ? false : true,
          child: FloatingActionButton(
            onPressed: () {
              setState(() {
                scrollController.animateTo(0,
                    duration: Duration(milliseconds: 800),
                    curve: Curves.decelerate);
              });
            },
            child: Icon(Icons.arrow_upward),
          ),
        ),
      ),
    );
  }
}

//class buildOfferList extends StatefulWidget {
//  RunningOffersInfo runningOfferInfo;
//  User user;
//
//  buildOfferList(this.runningOfferInfo, this.user);
//
//  @override
//  _buildOfferListState createState() => _buildOfferListState();
//}
//
//class _buildOfferListState extends State<buildOfferList> {
//  
//  int _itemCount = 0;
//  bool _addButton = true, _plusMinus = false;
//  int discount;
//  String offer_id = "", offer_price = "";
//  HttpRequests auth = HttpRequests();
//
//  addItem() {
//    if (_itemCount == widget.runningOfferInfo.totalLeft) {
//      Flushbar(
//        icon: Icon(
//          Icons.cancel,
//          color: mColor,
//        ),
//        duration: Duration(seconds: 2),
//        messageText: Text(
//          "Out of stock",
//          style: TextStyle(color: mColor),
//        ),
//      )..show(context);
//    } else {
//      _itemCount++;
////      appState.increment(_itemCount);
//      addToCart(_itemCount);
//    }
//  }
//
//  addToCart(itemCount) async {
//    print(widget.runningOfferInfo.masterProductId);
//    print(widget.runningOfferInfo.merchantId);
//    await auth.addItemToCart(
//        widget.runningOfferInfo.masterProductId,
//        widget.runningOfferInfo.merchantproductList,
//        offer_id,
//        offer_price,
//        itemCount,
//        widget.runningOfferInfo.merchantId,
//        widget.runningOfferInfo.storeId,
//        widget.user.data.loginDetails.userId,
//        widget.user.data.loginDetails.authKey);
//
////    Flushbar(
////      message: "Cart Updated!",
////      icon: Icon(
////        Icons.check_circle,
////        color: Colors.green,
////      ),
////      duration: Duration(seconds: 1),
////      flushbarPosition: FlushbarPosition.BOTTOM,
////    )..show(context);
//  }
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    discount =
//        ((widget.runningOfferInfo.mrp - widget.runningOfferInfo.sellingPrice) /
//                widget.runningOfferInfo.mrp *
//                100)
//            .toInt();
//    _itemCount = widget.runningOfferInfo.shoppingListQty;
//    if (widget.runningOfferInfo.shoppingListQty != 0) {
//      _addButton = false;
//      _plusMinus = true;
//    }
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      width: MediaQuery.of(context).size.width - 20,
//      margin: EdgeInsets.symmetric(horizontal: 10),
//      child: Card(
//          elevation: 8,
//          child: Container(
//            padding: EdgeInsets.all(8),
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.spaceBetween,
//              children: <Widget>[
//                InkWell(
//                  onTap: () {
////                                Navigator.push(
////                                    context,
////                                    MaterialPageRoute(
////                                        builder: (context) => new ProductDescription(
////                                            widget.user, widget.dataP, widget.storeInfo,
////                                            _itemCount)));
//                  },
//                  child: Container(
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                      children: <Widget>[
//                        Image.network(
//                          widget.runningOfferInfo.productImage.toString(),
//                          height: 100,
//                          width: 100,
//                          fit: BoxFit.contain,
//                        ),
//                        Expanded(
//                          child: ListTile(
//                            title: Text(
//                              "${widget.runningOfferInfo.productName}",
//                              maxLines: 2,
//                              overflow: TextOverflow.ellipsis,
//                            ),
//                            subtitle:
//                                Text("${widget.runningOfferInfo.storeName}"),
//                            trailing: Column(
//                              crossAxisAlignment: CrossAxisAlignment.end,
//                              children: <Widget>[
//                                Text(
//                                  "₹ ${widget.runningOfferInfo.sellingPrice}",
//                                  style: TextStyle(fontSize: 18, color: mColor),
//                                ),
//                                Text(
//                                  " ${widget.runningOfferInfo.mrp} ",
//                                  style: TextStyle(
//                                      decoration: TextDecoration.lineThrough,
//                                      fontSize: 16,
//                                      color: Colors.black),
//                                )
//                              ],
//                            ),
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                ),
//                Container(
//                  padding: EdgeInsets.only(left: 4.0, right: 4.0),
//                  alignment: Alignment.bottomRight,
//                  width: MediaQuery.of(context).size.width,
//                  child: Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                    children: <Widget>[
//                      Row(
//                        children: <Widget>[
//                          Icon(Icons.local_offer, color: mColor),
//                          Text(" ${discount}% off"),
//                        ],
//                      ),
//                      Container(
//                        child: Stack(
//                          children: <Widget>[
//                            Container(
//                              width: 120,
//                              height: 40,
//                              decoration: BoxDecoration(
//                                  borderRadius:
//                                      BorderRadius.all(Radius.circular(3)),
//                                  border: Border.all(color: mColor)),
//                              child: Visibility(
//                                visible: _addButton,
//                                child: OutlineButton(
//                                  textColor: mColor,
//                                  onPressed: () {
//                                    setState(() {
//                                      print("hit");
////                                                            _itemCount++;
//                                      addItem();
//                                      _addButton = false;
//                                      _plusMinus = true;
//                                    });
//                                  },
//                                  borderSide:
//                                      BorderSide(color: Colors.transparent),
//                                  child: Text("Add"),
//                                ),
//                              ),
//                            ),
//                            Visibility(
//                              visible: _plusMinus,
//                              child: Container(
//                                height: 40,
//                                child: Row(
//                                  mainAxisAlignment:
//                                      MainAxisAlignment.spaceBetween,
//                                  children: <Widget>[
//                                    IconButton(
//                                      color: mColor,
//                                      onPressed: () {
//                                        setState(() {
//                                          if (_itemCount <= 1) {
//                                            _plusMinus = false;
//                                            _addButton = true;
//                                            _itemCount--;
//                                            addToCart(_itemCount);
//                                          } else
//                                            _itemCount--;
//                                          addToCart(_itemCount);
//                                        });
//                                      },
////                                      borderSide: BorderSide(
////                                          color: Colors.transparent),
//                                      icon: Icon(Icons.remove),
//                                    ),
//                                    Container(
//                                        width: 25,
//                                        child: Center(
//                                            child: Text("${_itemCount}", style: TextStyle(fontSize: 13)))),
//                                    IconButton(
//                                      color: mColor,
//                                      onPressed: () {
//                                        setState(() {
//                                          addItem();
//                                        });
//                                      },
////                                      borderSide: BorderSide(
////                                          color: Colors.transparent),
//                                      icon: Icon(Icons.add),
//                                    ),
//                                  ],
//                                ),
//                              ),
//                            )
//                          ],
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              ],
//            ),
//          )),
//    );
//  }
//}
