import 'package:auto_size_text/auto_size_text.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Components/ProductDetail.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/MainApp.dart';
import 'package:userapp/Screens/Offers/OffersByShops.dart';
import 'package:userapp/Screens/Offers/RunningOffers.dart';
import 'package:userapp/Screens/Offers/SeeAllOffers.dart';
import 'package:userapp/Components/TopSaverTodayProduct.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../constants.dart';
import '../Checkout.dart';
import '../../Getters/GetDeals.dart';

_OfferScreenState offerScreenState;

class OfferScreen extends StatefulWidget {
  @override
  _OfferScreenState createState() {
    offerScreenState = _OfferScreenState();
    return offerScreenState;
  }
}

class _OfferScreenState extends State<OfferScreen> {
  HttpRequests auth = HttpRequests();
  int distance = 5;
  Deals deals;
  bool loading = true, comingSoon = false;
  List<TopSaversToday> topSaversToday;
  getOffers() async {
    deals = null;
    loading = true;
    var r = await auth.getOffersAndDeals(
        distance, USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      deals = Deals.fromJson(r);
      if (deals.comingSoon == 1) {
        comingSoon = true;
        loading = false;
      } else {
        loading = false;
        getTopSavers();
      }
    });
  }

  getTopSavers() async {
    topSaversToday = null;
    var r = await auth.getOffersAndDeals(

        distance, USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      topSaversToday = Deals.fromJson(r).info.topSaversToday;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getOffers();
  }

  @override
  Widget build(BuildContext context) {
    final StreamVars streamVars = Get.put(StreamVars());
    return comingSoon
        ? Center(
            child: Text(
              "Coming Soon",
              style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold, color: mColor),
            ),
          )
        : loading || deals == null
            ? neoLoader()
            : IgnorePointer(
                ignoring: streamVars.addingItem.value,
                child: Scaffold(
                    appBar: AppBar(
                      automaticallyImplyLeading: false,
                      backgroundColor: mColor,
                      title: Text(
                        "Offers & Deals",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    body: Stack(
                      children: <Widget>[
                        SingleChildScrollView(
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  CarouselSlider(
                                      height: 150,
                                      initialPage: 0,
                                      enlargeCenterPage: true,
                                      enableInfiniteScroll: false,
                                      items:
                                      deals.info.topOfferBanner[0].bannerUrl ==
                                          null
                                          ? <Widget>[
                                        Image(
                                          image: AssetImage(
                                              'drawables/logo_neo_mart_splash.png'),
                                          height: 100,
                                          width:100,
                                        )
                                      ]
                                          : <Widget>[
                                        Image.network(
                                          deals.info.topOfferBanner[0]
                                              .bannerUrl,
                                          fit: BoxFit.cover,
                                        ),
                                      ]),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    height: 70,
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        // ButtonTheme(
                                        //   height: 70.0,
                                        //   child: Expanded(
                                        //     flex: 1,
                                        //     child: RaisedButton(
                                        //       color: Colors.white,
                                        //       textColor: mColor,
                                        //       shape: RoundedRectangleBorder(
                                        //           borderRadius:
                                        //               BorderRadius.circular(10),
                                        //           side: BorderSide(color: mColor)),
                                        //       onPressed: () async {
                                        //         await Navigator.push(
                                        //             context,
                                        //             MaterialPageRoute(
                                        //                 builder: (context) =>
                                        //                     OffersByShops(
                                        //                         USER_DETAILES)));
                                        //         getTopSavers();
                                        //
                                        //       },
                                        //       child: Text("Offers By Shops"),
                                        //     ),
                                        //   ),
                                        // ),
                                        // SizedBox(
                                        //   width: 10,
                                        // ),
                                        ButtonTheme(
                                          height: 70.0,
                                          child: Expanded(
                                            flex: 1,
                                            child: RaisedButton(
                                              color: Colors.white,
                                              textColor: mColor,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(10),
                                                  side: BorderSide(color: mColor)),
                                              onPressed: () async {
                                                await Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            RunningOffers(
                                                                USER_DETAILES)));
                                                getTopSavers();
                                              },
                                              child: Text("Running Discounts"),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Container(
                                    child: Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 1,
                                          child: Text(
                                            "Top Savers Today",
                                            style: TextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        OutlineButton(
                                          textColor: Colors.blue,
                                          borderSide:
                                          BorderSide(color: Colors.blue),
                                          onPressed: () async {
                                            await Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        SeeAllOffers(
                                                            USER_DETAILES)));
                                            getTopSavers();
                                          },
                                          child: Text("See all"),
                                        )
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  topSaversToday == null
                                      ?  neoLoader()
                                      : Container(
                                    height: 180,
                                    child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: topSaversToday.length,
                                        itemBuilder: (context, index) {
                                          return buildOfferList(
                                              topSaversToday[index],
                                              USER_DETAILES);
                                        }),
                                  ),
                                  SizedBox(
                                    height: 50,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        neoLoaderStack()
                      ],
                    )),
              );
  }
}

class buildOfferList extends StatefulWidget {
  TopSaversToday topSaversToday;
  User user;

  buildOfferList(this.topSaversToday, this.user);

  @override
  _buildOfferListState createState() => _buildOfferListState();
}

class _buildOfferListState extends State<buildOfferList> {
  int _itemCount = 0;
  bool _addButton = true, _plusMinus = false;
  int discount;
  String offer_id = "", offer_price = "";
  HttpRequests auth = HttpRequests();
  String storeId, merchantId;
  StreamVars streamVars = Get.put(StreamVars());

  addItem() {
    if (_itemCount == widget.topSaversToday.totalLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color:  mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color:  mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
//      appState.increment(_itemCount);
      addToCart(_itemCount);
    }
  }

  addToCart(itemCount) async {
    streamVars.addingItem.value = true;
    await auth.addItemToCart(
        widget.topSaversToday.masterProductId,
        widget.topSaversToday.merchantProductId,
        offer_id,
        offer_price,
        itemCount,
        widget.topSaversToday.merchantId,
        widget.topSaversToday.storeId,
        USER_DETAILES.data.loginDetails.userId,
        USER_DETAILES.data.loginDetails.authKey);
    streamVars.addingItem.value = false;
    offerScreenState.getTopSavers();

//    Flushbar(
//      message: "Cart Updated!",
//      icon: Icon(
//        Icons.check_circle,
//        color: Colors.green,
//      ),
//      duration: Duration(seconds: 1),
//      flushbarPosition: FlushbarPosition.BOTTOM,
//    )..show(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    discount = ((widget.topSaversToday.productMrp -
                widget.topSaversToday.sellingPrice) /
            widget.topSaversToday.productMrp *
            100)
        .toInt();
    _itemCount = widget.topSaversToday.shoppingListQty;
    if (widget.topSaversToday.shoppingListQty != 0) {
      _addButton = false;
      _plusMinus = true;
    }
    
    
    storeId = widget.topSaversToday.storeId.toString();
    
    
    print("Store id"+STORE_ID);
    merchantId = widget.topSaversToday.merchantId.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Card(
          elevation: 8,
          child: Container(
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () async {
                    await Get.to(ProductDetailPage(
                        widget.topSaversToday.merchantProductId.toString()));
                    offerScreenState.getTopSavers();
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Image.network(
                          widget.topSaversToday.productImage.toString(),
                          height: 100,
                          width: 100,
                          fit: BoxFit.contain,
                        ),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              "${widget.topSaversToday.productname}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontSize: 14),
                            ),
                            subtitle: Text(
                              "${widget.topSaversToday.storeName}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                AutoSizeText(
                                  "₹ ${double.tryParse(widget.topSaversToday.sellingPrice.toString()).toStringAsFixed(2)}",
                                  style: TextStyle(color: mColor),
                                  maxFontSize: 13,
                                  minFontSize: 9,
                                ),
                                AutoSizeText(
                                  "₹ ${widget.topSaversToday.productMrp} ",
                                  maxFontSize: 13,
                                  minFontSize: 9,
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      color: Colors.black),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 4.0, right: 4.0),
                  alignment: Alignment.bottomRight,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(Icons.local_offer, color: mColor),
                          Text(" ${discount}% off"),
                        ],
                      ),
                      Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 120,
                              height: 40,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  border: Border.all(color: mColor)),
                              child: Visibility(
                                visible: _addButton,
                                child: OutlineButton(
                                  textColor: mColor,
                                  onPressed: () {
                                    setState(() {
                                      addItem();
                                      _addButton = false;
                                      _plusMinus = true;
                                    });
                                  },
                                  borderSide:
                                      BorderSide(color: Colors.transparent),
                                  child: Text("Add"),
                                ),
                              ),
                            ),
                            Visibility(
                              visible: _plusMinus,
                              child: Container(
                                height: 40,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    IconButton(
                                      color: mColor,
                                      onPressed: () {
                                        setState(() {
                                          if (_itemCount <= 1) {
                                            _plusMinus = false;
                                            _addButton = true;
                                            _itemCount--;
                                            addToCart(_itemCount);
                                          } else
                                            _itemCount--;
                                          addToCart(_itemCount);
                                        });
                                      },
//                                      borderSide: BorderSide(
//                                          color: Colors.transparent),
                                      icon: Icon(Icons.remove),
                                    ),
                                    Container(
                                        width: 25,
                                        child: Center(
                                            child: Text("${_itemCount}",
                                                style:
                                                    TextStyle(fontSize: 13)))),
                                    IconButton(
                                      color: mColor,
                                      onPressed: () {
                                        setState(() {
                                          addItem();
                                        });
                                      },
//                                      borderSide: BorderSide(
//                                          color: Colors.transparent),
                                      icon: Icon(Icons.add),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
