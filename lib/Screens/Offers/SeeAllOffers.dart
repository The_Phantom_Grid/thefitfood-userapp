import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/ProductDetail.dart';
import 'package:userapp/Getters/GetDeals.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Components/TopSaverTodayProduct.dart';

import '../../Components/CommonUtility.dart';
import '../../Network/httpRequests.dart';
import '../../constants.dart';

_SeeAllOffersState seeAllOffersState;

class SeeAllOffers extends StatefulWidget {
  User user;

  SeeAllOffers(this.user);

  @override
  _SeeAllOffersState createState() {
    seeAllOffersState = _SeeAllOffersState();
    return seeAllOffersState;
  }
}

class _SeeAllOffersState extends State<SeeAllOffers> {
  var scrollController = ScrollController();
  double scrollPosition;
  bool showButton = false, loading = false;
  Deals deals;
  List<TopSaversToday> topSaversToday;
  HttpRequests auth = HttpRequests();

  _scrollListener() {
    setState(() {
      scrollPosition = scrollController.position.pixels;
      print(scrollPosition);
      if (scrollPosition >= scrollController.position.maxScrollExtent ||
          scrollPosition < 150)
        showButton = false;
      else if (scrollPosition > 150) showButton = true;
//      else if(scrollPosition == scrollController.position.maxScrollExtent)
//        showButton = false;
    });
  }

//  getOffers() async {
//    deals = null;
//    loading = true;
//    var r = await auth.getOffersAndDeals(
//        5, widget.user.data.loginDetails.authKey);
//    setState(() {
//      deals = Deals.fromJson(r);
//      loading = false;
//    });
//    getTopSavers();
//  }

  getTopSavers() async {
    setState(() {
      loading = true;
      showButton = false;
    });
    topSaversToday = null;
    var r =
        await auth.getOffersAndDeals(5, widget.user.data.loginDetails.authKey);
    setState(() {
      topSaversToday = Deals.fromJson(r).info.topSaversToday;
      loading = false;
      showButton = true;
    });
  }

  @override
  void initState() {
    scrollPosition = 0.0;
    scrollController = ScrollController();
    scrollController.addListener(_scrollListener);
//    topSaversToday = widget.deals.info.topSaversToday;
    getTopSavers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final StreamVars streamVars = Get.put(StreamVars());
    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.transparent,
          title: scrollPosition <= 0.0 ? Text("Top Savers Today") : SizedBox(),
          centerTitle: true,
          leading: scrollPosition <= 0.0
              ? IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: Colors.white,
                  ),
                )
              : SizedBox(),
        ),
        backgroundColor: mColor,
        body: Stack(
          children: <Widget>[
            loading
                ? Center(
                    child: CircularProgressIndicator(
                    backgroundColor: Colors.white,
                  ))
                : Container(
                    child: ListView.builder(
                        controller: scrollController,
                        scrollDirection: Axis.vertical,
                        itemCount: topSaversToday.length,
                        itemBuilder: (context, index) {
                          return buildOfferList(
                              topSaversToday[index], widget.user);
                        }),
                  ),
            neoLoaderStack()
          ],
        ),
        floatingActionButton: Visibility(
          visible: showButton,
          child: FloatingActionButton(
            onPressed: () {
              setState(() {
                scrollController.animateTo(0,
                    duration: Duration(milliseconds: 800),
                    curve: Curves.decelerate);
              });
            },
            child: Icon(Icons.arrow_upward),
          ),
        ),
      ),
    );
  }
}

class buildOfferList extends StatefulWidget {
  TopSaversToday topSaversToday;
  User user;

  buildOfferList(this.topSaversToday, this.user);

  @override
  _buildOfferListState createState() => _buildOfferListState();
}

class _buildOfferListState extends State<buildOfferList> {
  int _itemCount = 0;
  bool _addButton = true, _plusMinus = false;
  int discount;
  String offer_id = "", offer_price = "";
  HttpRequests auth = HttpRequests();
  String storeId, merchantId;
  StreamVars streamVars = Get.put(StreamVars());

  addItem() {
    if (_itemCount == widget.topSaversToday.totalLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color:  mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color:  mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
//      appState.increment(_itemCount);
      addToCart(_itemCount);
    }
  }

  addToCart(itemCount) async {
    streamVars.addingItem.value = true;
    print(widget.topSaversToday.masterProductId);
    print(widget.topSaversToday.merchantId);
    await auth.addItemToCart(
        widget.topSaversToday.masterProductId,
        widget.topSaversToday.merchantProductId,
        offer_id,
        offer_price,
        itemCount,
        widget.topSaversToday.merchantId,
        widget.topSaversToday.storeId,
        widget.user.data.loginDetails.userId,
        widget.user.data.loginDetails.authKey);
    seeAllOffersState.getTopSavers();

    streamVars.addingItem.value = false;

//    Flushbar(
//      message: "Cart Updated!",
//      icon: Icon(
//        Icons.check_circle,
//        color: Colors.green,
//      ),
//      duration: Duration(seconds: 1),
//      flushbarPosition: FlushbarPosition.BOTTOM,
//    )..show(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    discount = ((widget.topSaversToday.productMrp -
                widget.topSaversToday.sellingPrice) /
            widget.topSaversToday.productMrp *
            100)
        .toInt();
    _itemCount = widget.topSaversToday.shoppingListQty;
    if (widget.topSaversToday.shoppingListQty != 0) {
      _addButton = false;
      _plusMinus = true;
    }

    storeId = widget.topSaversToday.storeId.toString();
    merchantId = widget.topSaversToday.merchantId.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      margin: EdgeInsets.symmetric(horizontal: 10),
      child: Card(
          elevation: 8,
          child: Container(
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                InkWell(
                  onTap: () async {
                    await Get.to(ProductDetailPage(
                        widget.topSaversToday.merchantProductId.toString()));
                    seeAllOffersState.getTopSavers();
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Image.network(
                          widget.topSaversToday.productImage.toString(),
                          height: 100,
                          width: 100,
                          fit: BoxFit.contain,
                        ),
                        Expanded(
                          child: ListTile(
                            title: Text(
                              "${widget.topSaversToday.storeName}",
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style:
                                  TextStyle(color: Colors.grey, fontSize: 13),
                            ),
                            subtitle: Text(
                              "${widget.topSaversToday.productname}",
                              style: TextStyle(color: Colors.black),
                            ),
                            trailing: Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                AutoSizeText(
                                  "₹ ${double.tryParse(widget.topSaversToday.sellingPrice.toString()).toStringAsFixed(2)}",
                                  style: TextStyle(color: mColor),
                                  maxFontSize: 16,
                                  minFontSize: 11,
                                ),
                                Text(
                                  "₹ ${widget.topSaversToday.productMrp} ",
                                  style: TextStyle(
                                      decoration: TextDecoration.lineThrough,
                                      fontSize: 16,
                                      color: Colors.black),
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 4.0, right: 4.0),
                  alignment: Alignment.bottomRight,
                  width: MediaQuery.of(context).size.width,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
//                      Row(
//                        children: <Widget>[
//                          Icon(Icons.local_offer, color: mColor),
//                          Text(" ${discount}% off"),
//                        ],
//                      ),
                      SizedBox(),
                      Container(
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 120,
                              height: 40,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                  border: Border.all(color: mColor)),
                              child: Visibility(
                                visible: _addButton,
                                child: OutlineButton(
                                  textColor: mColor,
                                  onPressed: () {
                                    setState(() {
                                      print("hit");
//                                                            _itemCount++;
                                      addItem();
                                      _addButton = false;
                                      _plusMinus = true;
                                    });
                                  },
                                  borderSide:
                                      BorderSide(color: Colors.transparent),
                                  child: Text("Add"),
                                ),
                              ),
                            ),
                            Visibility(
                              visible: _plusMinus,
                              child: Container(
                                height: 40,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    IconButton(
                                      color: mColor,
                                      onPressed: () {
                                        setState(() {
                                          if (_itemCount <= 1) {
                                            _plusMinus = false;
                                            _addButton = true;
                                            _itemCount--;
                                            addToCart(_itemCount);
                                          } else
                                            _itemCount--;
                                          addToCart(_itemCount);
                                        });
                                      },
//                                      borderSide: BorderSide(
//                                          color: Colors.transparent),
                                      icon: Icon(Icons.remove),
                                    ),
                                    Container(
                                        width: 25,
                                        child: Center(
                                            child: Text(
                                          "$_itemCount",
                                          style: TextStyle(fontSize: 13),
                                        ))),
                                    IconButton(
                                      color: mColor,
                                      onPressed: () {
                                        setState(() {
                                          addItem();
                                        });
                                      },
//                                      borderSide: BorderSide(
//                                          color: Colors.transparentent),
                                      icon: Icon(Icons.add),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
