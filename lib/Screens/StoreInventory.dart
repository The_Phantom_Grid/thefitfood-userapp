import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:chips_choice/chips_choice.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetComboOffer.dart';
import 'package:userapp/Getters/GetMyCart.dart';
import 'package:userapp/Getters/GetNearbyStores.dart';
import 'package:userapp/Getters/GetProducts.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/MyCart.dart';
import 'package:userapp/Components/ProductDescription.dart';
import 'package:userapp/Screens/StoreDetail.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:userapp/constants.dart';

import 'Checkout.dart';
import '../Components/ProductSearch.dart';

_StoreInventoryState storeInventoryState;

class StoreInventory extends StatefulWidget {
  Info storeInfo;
  List<CategoryidArray> categories;
  NearbyStores nearbyStores;

  StoreInventory(this.nearbyStores, this.storeInfo, this.categories);

  @override
  _StoreInventoryState createState() {
    storeInventoryState = _StoreInventoryState();
    return storeInventoryState;
  }
}

class _StoreInventoryState extends State<StoreInventory> {

//  var mAccentColor = Color(0xFFf82d70);
//  var mAccentColor = Color(0xFFac1647);
//  var mAccentColor2 = Color(0xFFf82d70);

  int tag = 0, catId = 0;
  List<String> options = [];
  var auth = new HttpRequests();
  List itemList = [], searchList = [], searchItemList = [];
  var storeDetail;
  ProductList productList, searchProductList;
  int offer = 1;
  int min = 0, max = 10;
  var offer_id = "", offer_price = "";
  bool hideContainer = false;
  ProductList pList;
  var searchController = TextEditingController();
  MyCartItems myCartItems;
  int itemCount = 0;
  bool loadingCart = true,
      loading = true,
      showNotFound = false,
      Inventory = false;
  var actionButtonPos = FloatingActionButtonLocation.endFloat;

  initList() {
    options.add("All");
    for (int x = 0; x < widget.categories.length; x++) {
      options.add(widget.categories[x].categoryName);
    }
  }

  getShoppingListCount() async {
    var r = await auth
        .getShoppingListCount(USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      itemCount = r['data'];
      loadingCart = false;
    });
  }

  searchItem() {
    setState(() {
      searchList.clear();
      for (int x = 0; x < searchItemList.length; x++) {
        if (searchItemList[x]
            .productName
            .toString()
            .toLowerCase()
            .contains(searchController.text.toLowerCase())) {
          searchList.add(searchItemList[x]);
          print("length=== ${searchList.length}");
        }
      }
      if (searchList.isEmpty) showNotFound = true;
    });
  }

  getStoreDetails() async {
    var r = await auth.getStoreDetail(
      widget.storeInfo.storeId,
      USER_DETAILES.data.loginDetails.authKey,
    );
    setState(() {
      storeDetail = json.decode(r.body);
    });
    setState(() {
      loading = false;
    });
  }

//  getSearchList() async {
//    searchItemList.clear();
//    for (int x = 0; x < widget.categories.length; x++) {
//      var jData = await auth.getInventory(widget.categories[x].categoryId, 1000,
//          0, widget.storeInfo.storeId, USER_DETAILES.data.loginDetails.authKey);
//      print(jData);
//      setState(() {
//        searchProductList = ProductList.fromJson(jData);
//        for (int k = 0; k < searchProductList.data.length; k++) {
//          searchItemList.add(searchProductList.data[k]);
//        }
//      });
//    }
//  }

  getInventoryById() async {
    itemList.clear();
    var jData = await auth.getInventory(widget.categories[catId].categoryId, 10,
        0, widget.storeInfo.storeId, USER_DETAILES.data.loginDetails.authKey);
    print(jData);

    setState(() {
      productList = ProductList.fromJson(jData);
      itemList = productList.data;
      if (productList.data.length == 0) {
        Inventory = true;
      } else {
        Inventory = false;
      }
//      print(productList.data);
    });
    getStoreDetails();
  }

  getAllProducts() async {
    itemList.clear();
    var jData = await auth.getAllProducts(10, 0, widget.storeInfo.storeId,
        USER_DETAILES.data.loginDetails.authKey);
    print(jData);
    setState(() {
      productList = ProductList.fromJson(jData);
      itemList = productList.data;
      if (productList.data.length == 0) {
        Inventory = true;
      } else {
        Inventory = false;
      }
//      print(productList.data);
    });
    getStoreDetails();
  }

  int currentMax = 20;
  ScrollController scrollController = ScrollController();
  bool loadMore = false;

  getMoreProducts() async {
    setState(() {
      loadMore = true;
    });
    var tempList = [];
    var jData;
    if (tag == 0)
      jData = await auth.getAllProducts(currentMax, 0, widget.storeInfo.storeId,
          USER_DETAILES.data.loginDetails.authKey);
    else
      jData = await auth.getInventory(
          widget.categories[catId].categoryId,
          currentMax,
          0,
          widget.storeInfo.storeId,
          USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      productList = ProductList.fromJson(jData);
      print("LIST LENGTH::: ${productList.data.length}");
      tempList = productList.data;
      for (int x = currentMax - 10; x < currentMax; x++) {
        try {
          itemList.add(tempList[x]);
        } catch (e) {
          if (e.toString().contains("RangeError (index): Invalid value")) {
            loadMore = false;
            break;
          }
        }
      }

      print("LIST LENGTH::: ${itemList.length}");
      currentMax += 10;
      loadMore = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initList();
    print("aaaaaaaaaaaaa${widget.storeInfo.storeId}");
    getAllProducts();
    getShoppingListCount();
//    getSearchList();
    scrollController.addListener(() {
      if (!loadMore) {
        if (scrollController.position.pixels >=
            scrollController.position.maxScrollExtent) {
          getMoreProducts();
          print("Extent");
        }
      }
    });
  }

  var pageController = PageController(initialPage: 1);

  onBackPressed() {
    setState(() {
      tag = 0;
      offer = null;
      pageController.animateToPage(1,
          duration: Duration(milliseconds: 800), curve: Curves.decelerate);
    });
    getAllProducts();
    getShoppingListCount();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    var stsBarHeight = MediaQuery.of(context).padding.top;

//    BoxDecoration _boxGradient = BoxDecoration(
//        gradient: LinearGradient(
//          colors: [mColor, mColor],
//      begin: FractionalOffset(0.0, 0.2),
//      end: FractionalOffset(0.8, 0.0),
//      stops: [0.0, 1.0],
//    ));

    Widget expandedAppBar = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        // Container(
        //   child: ChipsChoice<int>.single(
        //     itemConfig: ChipsChoiceItemConfig(
        //         showCheckmark: false,
        //         selectedColor: mColor,
        //         unselectedColor: Colors.black),
        //     value: offer,
        //     options: ChipsChoiceOption.listFrom<int, String>(
        //         source: ["Offers"], value: (i, v) => i, label: (i, v) => v),
        //     onChanged: (val) {
        //       setState(() {
        //         pageController.animateToPage(0,
        //             duration: Duration(milliseconds: 800),
        //             curve: Curves.decelerate);
        //         offer = val;
        //         tag = null;
        //         print("offers");
        //       });
        //     },
        //   ),
        // ),
        Expanded(
          flex: 3,
          child: Container(
            height: 60,
//            decoration: BoxDecoration(
//              border: Border(left: BorderSide(color: Colors.white, width: 3))
//            ),
            child: Padding(
              padding: EdgeInsets.all(4.0),
              child: ListView.builder(
                  itemCount: 1,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return Container(
                      child: ChipsChoice<int>.single(
                        itemConfig: ChipsChoiceItemConfig(
                            selectedColor: mColor,
                            unselectedColor: Colors.black),
                        value: tag,
                        options: ChipsChoiceOption.listFrom<int, String>(
                            source: options,
                            value: (i, v) => i,
                            label: (i, v) => v),
                        onChanged: (val) {
                          setState(() {
                            pageController.animateToPage(1,
                                duration: Duration(milliseconds: 800),
                                curve: Curves.decelerate);
                            offer = null;
                            tag = val;
                            print("TAG:: $tag");
                          });
                          if (tag == 0)
                            getAllProducts();
                          else {
                            catId = tag - 1;
                            getInventoryById();
                          }
                        },
                      ),
                    );
                  }),
            ),
          ),
        ),
      ],
    );
    final StreamVars streamVars = Get.put(StreamVars());

    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: mColor,
            elevation: 0.0,
            centerTitle: false,
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            StoreDetail(widget.storeInfo.storeId)));
              },
              child: Text(
                "${widget.storeInfo.storeName}",
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white),
              ),
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () async {
                  await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProductSearch()));
                  onBackPressed();
//                setState(() {
//                  if (hideContainer) {
//                    hideContainer = false;
//                    if(searchList.isEmpty){
//                      searchController.text = "";
//                      searchItem();
//                    }
//                  } else {
//                    getSearchList();
//                    hideContainer = true;
//                  }
//                });
                },
                icon: Icon(
                  hideContainer ? Icons.close : Icons.search,
                  color: Colors.white,
                ),
              )
            ],
          ),
          resizeToAvoidBottomInset: false,
          body: Stack(
            children: <Widget>[
              loading
                  ? neoLoader()
                  : Column(
                      children: <Widget>[
                        AnimatedContainer(
                          duration: Duration(milliseconds: 300),
                          curve: Curves.decelerate,
                          height: hideContainer ? 0 : 60,
                          color: mColor,
                          child: expandedAppBar,
                        ),
                        Center(
                          child: AnimatedContainer(
                            margin: EdgeInsets.all(0),
                            duration: Duration(milliseconds: 800),
                            height: MediaQuery.of(context).size.height -
                                (stsBarHeight + 116),
                            child: PageView(
                              physics: NeverScrollableScrollPhysics(),
                              controller: pageController,
                              scrollDirection: Axis.horizontal,
                              children: <Widget>[
                                // Offers(widget.storeInfo.storeId.toString(),
                                //     USER_DETAILES),
                                Column(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 6,
                                      child: Container(
                                        margin: EdgeInsets.all(0),
                                        child:
                                            storeDetail == null ||
                                                    itemList == null
                                                ? Center(
                                                    child:
                                                        CircularProgressIndicator(),
                                                  )
                                                : Container(
                                                    child: Center(
                                                        child: Container(
                                                      margin: EdgeInsets.only(
                                                          top: 0),
                                                      width: double.infinity,
                                                      child: showNotFound
                                                          ? Container(
                                                              child:
                                                                  SingleChildScrollView(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .center,
                                                                  mainAxisSize:
                                                                      MainAxisSize
                                                                          .min,
                                                                  children: <
                                                                      Widget>[
                                                                    SizedBox(
                                                                      height:
                                                                          50,
                                                                    ),
                                                                    Image.asset(
                                                                      'drawables/no_result_found.png',
                                                                      height:
                                                                          206,
                                                                      width:
                                                                          172,
                                                                    ),
                                                                    Text(
                                                                      "Oops!!!",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              20),
                                                                    ),
                                                                    Text(
                                                                      "No result found.",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              20),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                            )
                                                          : Inventory
                                                              ? Center(
                                                                  child: Padding(
                                                                      padding: EdgeInsets.all(50),
                                                                      child: Text(
                                                                        "The merchant has not added products in his inventory",
                                                                        textAlign:
                                                                            TextAlign.center,
                                                                      )),
                                                                )
                                                              : Stack(
                                                                  children: <
                                                                      Widget>[
                                                                    ListView.builder(
                                                                        controller: scrollController,
                                                                        padding: EdgeInsets.only(bottom: 70),
                                                                        itemCount: itemList.length,
                                                                        itemBuilder: (context, index) {
                                                                          return SingleCartItem(
                                                                            USER_DETAILES,
                                                                            productList.data[index],
                                                                            widget.storeInfo,
                                                                          );
                                                                        }),
                                                                    Visibility(
                                                                      visible:
                                                                          loadMore,
                                                                      child: neoLoader(),
                                                                    )
                                                                  ],
                                                                ),
                                                    )),
                                                  ),
                                      ),
                                    ),
//                              Visibility(
//                                visible: loadMore,
//                                child: Expanded(
//                                  flex: 1,
//                                  child: Center(
//                                    child: CircularProgressIndicator(),
//                                  ),
//                                ),
//                              )
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
              neoLoaderStack()
            ],
          ),
          floatingActionButton: GestureDetector(
            onHorizontalDragStart: (details) {
              print("dragging...${details.globalPosition}");
              setState(() {
                if (actionButtonPos == FloatingActionButtonLocation.centerFloat)
                  actionButtonPos = FloatingActionButtonLocation.endFloat;
                else
                  actionButtonPos = FloatingActionButtonLocation.centerFloat;
              });
            },
            child: Stack(
              fit: StackFit.loose,
              children: <Widget>[
                FloatingActionButton(
                  elevation: 8.0,
                  heroTag: 'cart',
                  tooltip: 'Cart',
                  backgroundColor: mColor,
                  onPressed: () async {
                    await Navigator.push(context,
                        MaterialPageRoute(builder: (context) => MyCart()));
                    onBackPressed();
//                  offerState.getComboOffers();
                  },
                  child: Icon(
                    Icons.shopping_cart,
                    color: Colors.white,
                  ),
                ),
                itemCount < 1
                    ? SizedBox()
                    : Positioned(
                        right: 0,
                        top: 0,
                        child: AnimatedOpacity(
                          opacity: loadingCart ? 0 : 1.0,
                          duration: Duration(milliseconds: 0),
                          child: Container(
                            padding: EdgeInsets.all(2),
                            decoration: new BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: mColor),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            constraints: BoxConstraints(
                              minWidth: 20,
                              minHeight: 20,
                            ),
                            child: loadingCart
                                ? SizedBox()
                                : Text(
                                    '${itemCount}',
                                    style: TextStyle(
                                      color: mColor,
                                      fontSize: 12,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                          ),
                        )),
              ],
            ),
          ),
          floatingActionButtonLocation: actionButtonPos),
    );
  }
}

class SingleCartItem extends StatefulWidget {
  User user;
  DataP dataP;
  Info storeInfo;

  SingleCartItem(
    this.user,
    this.dataP,
    this.storeInfo,
  );

  @override
  _SingleCartItemState createState() => _SingleCartItemState();
}

class _SingleCartItemState extends State<SingleCartItem> {
  var mColor = Color(0xFFe9991e);

//  var mAccentColor = Color(0xFFac1647);
//
//  var mAccentColor2 = Color(0xFFf82d70);
  var offer_id = "", offer_price = "";

  int _itemCount;

  bool _addButton = true, _plusMinus = false;
  HttpRequests auth = HttpRequests();
  StreamVars streamVars = Get.put(StreamVars());

  addToCart(itemCount) async {
    streamVars.addingItem.value = true;
    print(widget.dataP.masterProductId);
    print(widget.storeInfo.merchantId);
    await auth.addItemToCart(
        widget.dataP.masterProductId,
        widget.dataP.merchantproductList,
        offer_id,
        offer_price,
        itemCount,
        widget.storeInfo.merchantId,
        widget.storeInfo.storeId,
        USER_DETAILES.data.loginDetails.userId,
        USER_DETAILES.data.loginDetails.authKey);
    storeInventoryState.getShoppingListCount();

    streamVars.addingItem.value = false;

//    Fluttertoast.showToast(
//        msg: "Cart Updated",
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        timeInSecForIosWeb: 1,
//        backgroundColor: mColor,
//        textColor: Colors.white,
//        fontSize: 16.0);

//    Flushbar(
//      message: "Cart Updated!",
//      icon: Icon(
//        Icons.check_circle,
//        color: Colors.green,
//      ),
//      duration: Duration(seconds: 1),
//      flushbarPosition: FlushbarPosition.BOTTOM,
//    )..show(context);
  }

  addItem() {
    if (_itemCount == widget.dataP.totalLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color: mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color: mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
      addToCart(_itemCount);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _itemCount = widget.dataP.shoppingListQty;
    if (widget.dataP.shoppingListQty != 0) {
      _addButton = false;
      _plusMinus = true;
    }
  }

//  Widget storeCard(){
//    return Card(
//      child: Container(
//        padding: EdgeInsets.all(6),
//        child: Row(
//          children: <Widget>[
//            Container(
//              height: 75,
//              width: 75,
//              child: Placeholder(fallbackHeight: 75, fallbackWidth: 75,),
//            ),
//            Expanded(
//              child: Container(
//                padding: EdgeInsets.all(8),
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Text("${widget.topSellingProduct.storeName}", style: TextStyle(fontSize: 14, color: mColor, fontWeight: FontWeight.bold),),
//                        Text("₹ ${widget.topSellingProduct.mrp}",  style: TextStyle(fontSize: 16, color: mColor, fontWeight: FontWeight.bold)),
//                      ],
//                    ),
//                    Text("${widget.topSellingProduct.productName}"),
//                    Container(
//                        padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
//                        color: mColor,
//                        child: Text("${widget.topSellingProduct.productUnit}", style: TextStyle(fontSize: 10, color: Colors.white, fontWeight: FontWeight.bold))
//                    ),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Text("Hurry only ${widget.topSellingProduct.itemLeft} left",  style: TextStyle(fontSize: 12,fontStyle: FontStyle.italic)),
//                        Container(
//                          child: Stack(
//                            children: <Widget>[
//                              Container(
//                                width: 110,
//                                height: 35,
//                                decoration: BoxDecoration(
//                                    borderRadius:
//                                    BorderRadius.all(Radius.circular(25)),
//                                    border: Border.all(color: widget.topSellingProduct.itemLeft == 0? Colors.grey: mColor)),
//                                child: Visibility(
//                                  visible: _addButton,
//                                  child: OutlineButton(
//                                    textColor: mColor,
//                                    onPressed: widget.topSellingProduct.itemLeft == 0? null:  () {
//                                      setState(() {
//                                        addItem();
//                                        _addButton = false;
//                                        _plusMinus = true;
//                                      });
//                                    },
//                                    borderSide:
//                                    BorderSide(color: Colors.transparent),
//                                    child: Text("Add"),
//                                  ),
//                                ),
//                              ),
//                              Visibility(
//                                visible: _plusMinus,
//                                child: Container(
//                                  height: 35,
//                                  width: 110,
//                                  child: Row(
//                                    mainAxisSize: MainAxisSize.min,
//                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                    children: <Widget>[
//                                      Expanded(
//                                        child: Container(
//                                          height: 40,
//                                          width: 30,
//                                          child: IconButton(
////                                            textColor: mColor,
//                                            onPressed: widget.topSellingProduct.itemLeft == 0? null: () {
//                                              setState(() {
//                                                if (_itemCount <= 1) {
//                                                  _plusMinus = false;
//                                                  _addButton = true;
//                                                  _itemCount--;
//                                                  addToCart(_itemCount);
//                                                  productsState.addingItem = true;
//                                                } else
//                                                  _itemCount--;
//                                                addToCart(_itemCount);
//                                                productsState.addingItem = true;
//                                              });
//                                            },
////                                            borderSide:
////                                            BorderSide(color: Colors.transparent),
//                                            icon: Icon(Icons.remove, size: 16, color: mColor,),
//                                          ),
//                                        ),
//                                      ),
//                                      Expanded(
//                                        child: Container(
//                                            width: 20,
//                                            child:
//                                            Center(child: Text("$_itemCount"))),
//                                      ),
//                                      Expanded(
//                                        child: Container(
//                                          height: 40,
//                                          width: 30,
//                                          child: IconButton(
////                                            textColor: mColor,
//                                            onPressed: widget.topSellingProduct.itemLeft == 0? null:  () {
//                                              setState(() {
//                                                addItem();
//                                                productsState.addingItem = true;
//                                              });
//                                            },
////                                            borderSide:
////                                            BorderSide(color: Colors.transparent),
//                                            icon: Icon(Icons.add, size: 16, color: mColor,),
//                                          ),
//                                        ),
//                                      ),
//                                    ],
//                                  ),
//                                ),
//                              )
//                            ],
//                          ),
//                        ),
//                      ],
//                    ),
//
//                  ],
//                ),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }

  Widget newProductCard() {
    return Card(
      elevation: 0.0,
      child: InkWell(
        onTap: () async {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => new ProductDescription(
                      USER_DETAILES,
                      widget.dataP,
                      _itemCount,
                      widget.storeInfo.storeId,
                      widget.storeInfo.merchantId)));
          storeInventoryState.onBackPressed();
//          storeInventoryState.getShoppingListCount();
//          storeInventoryState.getInventoryById();
        },
        child: Container(
          padding: EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: 75,
                width: 75,
                child: Opacity(
                  opacity: widget.dataP.totalLeft == 0 ? .6 : 1,
                  child: Image.network(
                    widget.dataP.productImage,
                    height: 75,
                    width: 75,
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Text(
                              "${widget.dataP.storename}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: mColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Expanded(
                            child: AutoSizeText(
                                "₹ ${double.tryParse(widget.dataP.sellingPrice.toString()).toStringAsFixed(2)}",
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.end,
                                maxFontSize: 13,
                                minFontSize: 9,
                                style: TextStyle(
                                    color: mColor,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
//                      Text("${(widget.dataP.distance * 1.60934).toStringAsFixed(2)} Km",
//                          maxLines: 2,
//                          overflow: TextOverflow.ellipsis,
//                          style: TextStyle(fontSize: 10, color: mColor)
//                      ),
                      Text(
                        "${widget.dataP.productName}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Container(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                          color: mColor,
                          child: Text("${widget.dataP.productUnit}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold))),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          widget.dataP.totalLeft == 0
                              ? Text("Out of stock!",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: mColor))
                              : widget.dataP.totalLeft <= 5 &&
                                      widget.dataP.totalLeft > 0
                                  ? Text(
                                      "Hurry only ${widget.dataP.totalLeft} left",
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontStyle: FontStyle.italic))
                                  : SizedBox(),
                          Container(
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  width: 110,
                                  height: 35,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(25)),
                                      border: Border.all(
                                          color: widget.dataP.totalLeft == 0
                                              ? Colors.grey
                                              : mColor)),
                                  child: Visibility(
                                    visible: _addButton,
                                    child: IgnorePointer(
                                      ignoring: widget.dataP.totalLeft == 0
                                          ? true
                                          : false,
                                      child: OutlineButton(
                                        textColor: mColor,
                                        onPressed: () {
                                          setState(() {
                                            addItem();
                                            _addButton = false;
                                            _plusMinus = true;
                                          });
                                        },
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                        child: Text(
                                          "Add",
                                          style: TextStyle(
                                              color: widget.dataP.totalLeft == 0
                                                  ? Colors.grey
                                                  : mColor),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: _plusMinus,
                                  child: Container(
                                    height: 35,
                                    width: 110,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            height: 40,
                                            width: 30,
                                            child: IconButton(
//                                            textColor: mColor,
                                              onPressed: widget
                                                          .dataP.totalLeft ==
                                                      0
                                                  ? null
                                                  : () {
                                                      setState(() {
                                                        if (_itemCount <= 1) {
                                                          _plusMinus = false;
                                                          _addButton = true;
                                                          _itemCount--;
                                                          addToCart(_itemCount);
//                                                    productsState.addingItem = true;
                                                        } else
                                                          _itemCount--;
                                                        addToCart(_itemCount);
//                                                  productsState.addingItem = true;
                                                      });
                                                    },
//                                            borderSide:
//                                            BorderSide(color: Colors.transparent),
                                              icon: Icon(
                                                Icons.remove,
                                                size: 16,
                                                color: mColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                              width: 20,
                                              child: Center(
                                                  child: Text("$_itemCount"))),
                                        ),
                                        Expanded(
                                          child: Container(
                                            height: 40,
                                            width: 30,
                                            child: IconButton(
//                                            textColor: mColor,
                                              onPressed:
                                                  widget.dataP.totalLeft == 0
                                                      ? null
                                                      : () {
                                                          setState(() {
                                                            addItem();
//                                                  productsState.addingItem = true;
                                                          });
                                                        },
//                                            borderSide:
//                                            BorderSide(color: Colors.transparent),
                                              icon: Icon(
                                                Icons.add,
                                                size: 16,
                                                color: mColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget oldProductCard() {
    return Card(
        elevation: 2.0,
        margin: EdgeInsets.only(top: 8.0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => new ProductDescription(
                              USER_DETAILES,
                              widget.dataP,
                              _itemCount,
                              widget.storeInfo.storeId,
                              widget.storeInfo.merchantId)));
                  storeInventoryState.getShoppingListCount();
                  storeInventoryState.getInventoryById();
                },
                child: Container(
                  padding: EdgeInsets.only(top: 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      widget.dataP.productImage == null
                          ? Image(
                              image: AssetImage(
                                  'drawables/logo_neo_mart_splash.png'),
                              height: 100,
                              width: 100,
                            )
                          : Image.network(
                              widget.dataP.productImage.toString(),
                              height: 100,
                              width: 100,
                            ),
                      Expanded(
//                        alignment: Alignment.centerLeft,
//                        width: 250,
                        child: ListTile(
                          title: Text(
                            "${widget.dataP.productName}",
                            maxLines: 2,
                            style: TextStyle(fontSize: 15),
                            overflow: TextOverflow.ellipsis,
                          ),
                          subtitle: Text("${widget.dataP.productUnit}"),
                          trailing: Text(
                            "₹ ${double.tryParse(widget.dataP.sellingPrice.toString()).toStringAsFixed(2)}",
                            style: TextStyle(color: mColor),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 4.0, bottom: 4.0),
                alignment: Alignment.bottomRight,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        SizedBox(),
                        SizedBox(),
                      ],
                    ),
                    Container(
                      child: Stack(
                        children: <Widget>[
                          Container(
                            width: 120,
                            height: 40,
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(3)),
                                border: Border.all(color: mColor)),
                            child: Visibility(
                              visible: _addButton,
                              child: OutlineButton(
                                textColor: mColor,
                                onPressed: () {
                                  setState(() {
                                    addItem();
                                    _addButton = false;
                                    _plusMinus = true;
                                  });
                                },
                                borderSide:
                                    BorderSide(color: Colors.transparent),
                                child: Text("Add"),
                              ),
                            ),
                          ),
                          Visibility(
                            visible: _plusMinus,
                            child: Container(
                              height: 40,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  IconButton(
                                    color: mColor,
                                    onPressed: () {
                                      setState(() {
                                        if (_itemCount <= 1) {
                                          _plusMinus = false;
                                          _addButton = true;
                                          _itemCount--;
                                          addToCart(_itemCount);
                                        } else
                                          _itemCount--;
                                        addToCart(_itemCount);
                                      });
                                    },
//                                      borderSide:
//                                          BorderSide(color: Colors.transparent),
                                    icon: Icon(Icons.remove),
                                  ),
                                  Container(
                                      width: 25,
                                      child:
                                          Center(child: Text("$_itemCount"))),
                                  IconButton(
                                    color: mColor,
                                    onPressed: () {
                                      setState(() {
                                        addItem();
                                      });
                                    },
//                                    borderSide:
//                                        BorderSide(color: Colors.transparent),
                                    icon: Icon(Icons.add),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return newProductCard();
  }
}

_OffersState offerState;

class Offers extends StatefulWidget {
  String storeId;
  User user;

  Offers(this.storeId, this.user);

  @override
  _OffersState createState() {
    offerState = _OffersState();
    return offerState;
  }
}

class _OffersState extends State<Offers> {
  var mColor = Color(0xFFe9991e);

  HttpRequests auth = HttpRequests();
  int distance = 5;
  ComboOffer comboOffer;
  bool loading = true;

  getComboOffers() async {
    var r = await auth.getComboOffer(
        USER_DETAILES.data.loginDetails.authKey, distance, widget.storeId);
    setState(() {
      comboOffer = ComboOffer.fromJson(r);
      loading = false;
    });
  }

  String src = "StoreInventory";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (comboOffer == null) getComboOffers();
  }

  @override
  Widget build(BuildContext context) {
    final StreamVars streamVars = Get.put(StreamVars());
    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: loading == true
          ?  neoLoader()
          : comboOffer.info.length == 0
              ? Center(
                  child: Text("No Offers Yet!"),
                )
              : Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                    margin: EdgeInsets.only(top: 0),
                    width: double.infinity,
                    child: ListView.builder(
                        itemCount: comboOffer.info.length,
                        itemBuilder: (context, index) {
                          return buildComboList(comboOffer.info[index],
                              USER_DETAILES, comboOffer, index, src);
//                    SingleCartItem(
//                    USER_DETAILES,
//                    productList.data[index],
//                    widget.storeInfo,
//                  );
                        }),
                  ),
                ),
    );
  }
}

class buildComboList extends StatefulWidget {
  ComboInfo info;
  User user;
  ComboOffer comboOffer;
  int index;
  String src;

  buildComboList(this.info, this.user, this.comboOffer, this.index, this.src);

  @override
  _buildComboListState createState() => _buildComboListState();
}

class _buildComboListState extends State<buildComboList> {
  var mColor = Color(0xFFe9991e);

  HttpRequests auth = HttpRequests();
  bool hide = false;
  List<CollectionProducts> collectionProducts;
  List<CollectionImages> collectionImages;
  int _itemCount;
  bool _addButton = true, _plusMinus = false, disableButton = false;

  StreamVars streamVars = Get.put(StreamVars());
  var offer_id, offer_price, itemCount;
  List<String> cartList = [];
  String jsonBodyString;

  ////////Creating String of Json Body which to be POST//////////

  addToCart() async {
    streamVars.addingItem.value = true;
    print("$_itemCount item count");
    var resBody = {};
    var jBody = {};

    resBody.clear();
    resBody["masterproductid"] = "0";
    resBody["merchantlist_id"] = "0";
    resBody["offer_id"] = widget.comboOffer.info[widget.index].id;
    resBody["offer_price"] = widget.comboOffer.info[widget.index].comboPrice;
    resBody["qty"] = _itemCount;
//    var user = [];
//    user.add(resBody);
    String str = json.encode(resBody);
    cartList.clear();
    cartList.add(str);
    for (int x = 0;
        x < widget.comboOffer.info[widget.index].collectionProducts.length;
        x++) {
      resBody.clear();
//      user.clear();
      resBody["masterproductid"] = widget
          .comboOffer.info[widget.index].collectionProducts[x].masterProductId;
      resBody["merchantlist_id"] = widget.comboOffer.info[widget.index]
          .collectionProducts[x].merchantProductId;
      resBody["offer_id"] = widget
          .comboOffer.info[widget.index].collectionProducts[x].collectionId;
      resBody["offer_price"] = offer_price;
      resBody["qty"] = "$_itemCount";
//      user.add(resBody);
      String str = json.encode(resBody);
      cartList.add(str);
    }
//    print("${cartList} ==== cartList");

    jBody["\"cart\""] = cartList;
    jBody["\"merchant_id\""] = widget.comboOffer.info[0].merchantId;
    jBody["\"store_id\""] = widget.comboOffer.info[0].storeId;
    jBody["\"user_id\""] = USER_DETAILES.data.loginDetails.userId;

    jsonBodyString = "";
    jsonBodyString = jBody.toString();
    print("${jsonBodyString} ======== json body");
    await auth.addOfferToCart(
        jsonBodyString, USER_DETAILES.data.loginDetails.authKey);
    streamVars.addingItem.value = false;
    storeInventoryState.getShoppingListCount();
    // checkOutState.getProducts();
//    streamVars.addingItem.value = false;
//    if(!widget.src.contains("Checkout"))
//      Fluttertoast.showToast(
//          msg: "Cart Updated",
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.CENTER,
//          timeInSecForIosWeb: 1,
//          backgroundColor: mColor,
//          textColor: Colors.white,
//          fontSize: 16.0
//      );
  }

///////////////////////END//////////////////////////////////////

  addItem() {
    if (_itemCount == widget.info.inventory) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color: mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color: mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
      addToCart();
    }
  }

  removeItem() async {
    // int itemId;
    // if (checkOutState.cartProduct.product.isEmpty)
    //   itemId = widget.comboOffer.info[widget.index].id;
    // else
    //   for (int x = 0; x < checkOutState.cartProduct.product.length; x++) {
    //     // if (checkOutState.cartProduct.product[x].offerId ==
    //     //     widget.comboOffer.info[widget.index].id)
    //     //   itemId = checkOutState.cartProduct.product[x].shoppingListItemId;
    //   }
    // var r = await auth.deleteItemFromCart(
    //     itemId,
    //     widget.comboOffer.info[widget.index].merchantId,
    //     widget.comboOffer.info[widget.index].storeId,
    //     USER_DETAILES.data.loginDetails.authKey);

    // if (r['message'].toString().contains("Product Deleted Sucessfully")) {
    //   // checkOutState.getStoreDetails();
    //   print("item removed");
    // } else {
    //   Fluttertoast.showToast(
    //       msg: "Something went wrong!",
    //       toastLength: Toast.LENGTH_SHORT,
    //       gravity: ToastGravity.CENTER,
    //       timeInSecForIosWeb: 1,
    //       textColor: Colors.white,
    //       fontSize: 16.0);
    //   Fluttertoast.showToast(
    //       msg: "${r['message'].toString()}",
    //       toastLength: Toast.LENGTH_LONG,
    //       gravity: ToastGravity.CENTER,
    //       timeInSecForIosWeb: 1,
    //       textColor: Colors.white,
    //       fontSize: 16.0);
    // }
  }

  removeOffer() {
    if (_itemCount > 1) {
      --_itemCount;
      addToCart();
    } else {
      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                content:
                    Text("Do you want to remove this offer from your cart?"),
                actions: <Widget>[
                  FlatButton(
                    onPressed: () {
                      setState(() {
                        if (widget.src.contains("Checkout")) {
                          removeItem();
                        } else {
                          _itemCount--;
                          _plusMinus = false;
                          _addButton = true;
                          addToCart();
                        }
                      });

                      Navigator.pop(context);
//                      checkOutState.removeItem(widget.index);
                    },
                    child: Text("Remove"),
                  ),
                  FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("Cancel"),
                  )
                ],
              ));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("${widget.index} indexxxxxxxoooo");
    collectionImages = widget.info.collectionImages;
    collectionProducts = widget.info.collectionProducts;
    offer_id = widget.info.id;
    offer_price = widget.info.comboPrice;
    _itemCount = widget.info.shoppingListQty;
    if (_itemCount != 0) {
      _addButton = false;
      _plusMinus = true;
    }
    print(collectionProducts.length);
//    if (widget.src.contains("Checkout")) {
//      if (widget.info.shoppingListQty == 1)
//        setState(() {
//          disableButton = true;
//        });
//    }
  }

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Card(
      elevation: 0,
      child: Container(
        child: Column(
//          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 15,
                ),
//              Padding(
//                padding: const EdgeInsets.only(left: 8),
//                child: Text("Combo Offer Name", textAlign: TextAlign.left, style: TextStyle(fontWeight: FontWeight.bold),),
//              ),
                ListTile(
                  leading: collectionImages[0].imageUrl.toString().isEmpty
                      ? Image(
                          image:
                              AssetImage('drawables/logo_neo_mart_splash.png'),
                          height: 100,
                          width: 100,
                        )
                      : Image.network(
                          collectionImages[0].imageUrl.toString(),
                          fit: BoxFit.cover,
                          height: 200,
                          width: 100,
                        ),
                  title: Text("${widget.info.collectionName}"),
                  subtitle: Text("${widget.info.descr}"),
                  trailing: AutoSizeText(
                    "₹ ${widget.info.comboPrice}",
                    maxFontSize: 17,
                    minFontSize: 11,
                    style: TextStyle(color: mColor),
                  ),
                ),
                widget.src.contains("OrderDetails")
                    ? SizedBox()
                    : Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Valid till:",
                                  style: TextStyle(fontSize: 12),
                                ),
                                Text(
                                  "${widget.info.validTo}",
                                  style: TextStyle(fontSize: 12),
                                )
                              ],
                            ),
                            Container(
                              child: Stack(
                                children: <Widget>[
                                  Container(
                                    width: 120,
                                    height: 40,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(3)),
                                        border: Border.all(color: mColor)),
                                    child: Visibility(
                                      visible: _addButton,
                                      child: OutlineButton(
                                        textColor: mColor,
                                        onPressed: () {
                                          setState(() {
                                            _itemCount++;
                                            _addButton = false;
                                            _plusMinus = true;
                                            addToCart();
                                            print("${height * .16} height");
                                          });
                                        },
                                        borderSide: BorderSide(
                                            color: Colors.transparent),
                                        child: Text("Add"),
                                      ),
                                    ),
                                  ),
                                  Visibility(
                                    visible: _plusMinus,
                                    child: Container(
                                      height: 40,
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                            height: 40,
                                            width: 50,
                                            child: OutlineButton(
                                              disabledBorderColor:
                                                  Colors.transparent,
                                              textColor: mColor,
                                              onPressed: () {
                                                removeOffer();
                                              },
                                              borderSide: BorderSide(
                                                  color: Colors.transparent),
                                              child: Icon(Icons.remove),
                                            ),
                                          ),
                                          Container(
                                              width: 20,
                                              child: Center(
                                                  child:
                                                      Text("${_itemCount}"))),
                                          Container(
                                            height: 40,
                                            width: 50,
                                            child: OutlineButton(
                                              textColor: mColor,
                                              onPressed: () {
                                                addItem();
                                              },
                                              borderSide: BorderSide(
                                                  color: Colors.transparent),
                                              child: Icon(Icons.add),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      )
              ],
            ),
            Visibility(visible: hide, child: Divider()),
            AnimatedContainer(
              padding: EdgeInsets.all(4),
              height: hide ? (collectionProducts.length * 90).toDouble() : 0,
              duration: Duration(milliseconds: 300),
              child: ListView.separated(
                separatorBuilder: (context, index) {
                  return Divider();
                },
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: collectionProducts.length,
                itemBuilder: (context, index) {
                  return Card(
                    elevation: 0.0,
                    child: ListTile(
                      leading: collectionProducts[index]
                              .productImage
                              .toString()
                              .isEmpty
                          ? Image(
                              image: AssetImage(
                                  'drawables/logo_neo_mart_splash.png'),
                              fit: BoxFit.contain,
                              height: 100,
                              width: 100,
                            )
                          : Image.network(
                              collectionProducts[index].productImage.toString(),
                              fit: BoxFit.contain,
                              height: 100,
                              width: 100,
                            ),
                      title: Text(
                        "${collectionProducts[index].productName}",
                        maxLines: 3,
                        overflow: TextOverflow.ellipsis,
                      ),
//                          subtitle: Text(
//                              "${collectionProducts[index].productDescr}"),
                    ),
                  );
                },
              ),
            ),
            Divider(),
            InkWell(
                onTap: () {
                  setState(() {
                    if (hide)
                      hide = false;
                    else
                      hide = true;
                  });
                },
                child: Container(
                    width: double.infinity,
                    child: hide == false
                        ? Icon(
                            Icons.keyboard_arrow_down,
                            color: mColor,
                          )
                        : Icon(
                            Icons.keyboard_arrow_up,
                            color: mColor,
                          ))),
          ],
        ),
      ),
    );
  }
}

class SearchItem extends StatefulWidget {
  User user;
  DataP dataP;
  Info storeInfo;

  SearchItem(
    this.user,
    this.dataP,
    this.storeInfo,
  );

  @override
  _SearchItemState createState() => _SearchItemState();
}

class _SearchItemState extends State<SearchItem> {
  var mColor = Color(0xFFe9991e);

//  var mAccentColor = Color(0xFFac1647);
//
//  var mAccentColor2 = Color(0xFFf82d70);
  var offer_id = "", offer_price = "";

  int _itemCount;

  bool _addButton = true, _plusMinus = false;
  HttpRequests auth = HttpRequests();

  addToCart(itemCount) async {
    print(widget.dataP.masterProductId);
    print(widget.storeInfo.merchantId);
    await auth.addItemToCart(
        widget.dataP.masterProductId,
        widget.dataP.merchantproductList,
        offer_id,
        offer_price,
        itemCount,
        widget.storeInfo.merchantId,
        widget.storeInfo.storeId,
        USER_DETAILES.data.loginDetails.userId,
        USER_DETAILES.data.loginDetails.authKey);
    storeInventoryState.getShoppingListCount();
    storeInventoryState.getInventoryById();

//    Flushbar(
//      message: "Cart Updated!",
//      icon: Icon(
//        Icons.check_circle,
//        color: Colors.green,
//      ),
//      duration: Duration(seconds: 1),
//      flushbarPosition: FlushbarPosition.BOTTOM,
//    )..show(context);
  }

  addItem() {
    if (_itemCount == widget.dataP.totalLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color: mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color: mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
      addToCart(_itemCount);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _itemCount = widget.dataP.shoppingListQty;
    if (widget.dataP.shoppingListQty != 0) {
      _addButton = false;
      _plusMinus = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 0.0,
        margin: EdgeInsets.only(top: 8.0),
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                onTap: () async {
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => new ProductDescription(
                              USER_DETAILES,
                              widget.dataP,
                              _itemCount,
                              widget.storeInfo.storeId,
                              widget.storeInfo.merchantId)));
                  storeInventoryState.getShoppingListCount();
                  storeInventoryState.getInventoryById();
                },
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      widget.dataP.productImage == null
                          ? Image(
                              image: AssetImage(
                                  'drawables/logo_neo_mart_splash.png'),
                              height: 100,
                              width: 100,
                            )
                          : Image.network(
                              widget.dataP.productImage.toString(),
                              height: 100,
                              width: 100,
                            ),
                      Expanded(
//                        alignment: Alignment.centerLeft,
//                        width: 250,
                        child: ListTile(
                          title: Text(
                            "${widget.dataP.productName}",
                            maxLines: 2,
                            style: TextStyle(fontSize: 13),
                            overflow: TextOverflow.ellipsis,
                          ),
                          subtitle: Text("${widget.dataP.productUnit}"),
                          trailing: AutoSizeText(
                            "₹ ${double.tryParse(widget.dataP.sellingPrice.toString()).toStringAsFixed(2)}",
                            style: TextStyle(color: mColor),
                            minFontSize: 9,
                            maxFontSize: 13,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
