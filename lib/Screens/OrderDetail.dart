import 'dart:convert';
import 'dart:io';

//import 'package:dio/dio.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;

//import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:path_provider/path_provider.dart';
import 'package:paytm/paytm.dart';
import 'package:userapp/Components/CommonUtility.dart';

//import 'package:paytm/paytm.dart';
import 'package:userapp/Screens/ChatScreen.dart';
import 'package:userapp/Getters/GetOrderDetail.dart';
import 'package:userapp/Getters/GetOrders.dart';
import 'package:userapp/Getters/GetPaytm.dart';
import 'package:userapp/Getters/GetStoreDetails.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Getters/GetViewCart.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:userapp/Screens/WebViewScreen.dart';

import '../Getters/GetComboOffer.dart';
import '../constants.dart';
import 'StoreInventory.dart';
import 'TxnComplete.dart';
import 'TxnFail.dart';

class OrderDetail extends StatefulWidget {
  String orderId;

  OrderDetail(this.orderId);

  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {
//  var mColor = Color(0xFFf82d70);
  String path, pdfUrl;
  HttpRequests auth = HttpRequests();
  MyOrderDetails od;
  Orderdetails myOrderDetail;
  String payment_response = '';
  double totalTax = 0.0;

//  FlutterDownloader f;
  double orderStatusValue = 1;
  var taskId;
  Directory appdoc;
  String invoicePath = "", dimension, src = "OrderDetails", orderStatus = "";
  var commentsController = TextEditingController();
  bool cancelOrder = false,
      returnOrder = false,
      loading = true,
      showInfo = false;
  int grandtotal, orderId;
  PaytmWallet paytmWallet;
  PaytmResponse pResponse;

  setOrderStatusValue() {
    setState(() {
      print("${myOrderDetail.orderStatus}...");
      if (myOrderDetail.orderStatus.contains("Placed") ||
          myOrderDetail.orderStatus.contains("Received")) {
        orderStatusValue = 1;
        orderStatus = "Order Placed";
        print("Packed");
      } else if (myOrderDetail.orderStatus
              .toLowerCase()
              .contains("confirmed") ||
          myOrderDetail.orderStatus.contains("Packed")) {
        orderStatus = "Order Confirmed/Packed";
        orderStatusValue = 2;
      }
      if (myOrderDetail.orderStatus.toLowerCase().contains("out")) {
        orderStatus = "Out for delivery";
        orderStatusValue = 3;
      }
      if (myOrderDetail.orderStatus.toLowerCase().contains("complete") ||
          myOrderDetail.orderStatus.toLowerCase().contains("delivered")) {
        orderStatus = "Order Delivered";
        orderStatusValue = 4;
      }
      if (myOrderDetail.orderStatus.contains("Cancel")) cancelOrder = true;
      if (myOrderDetail.orderStatus.contains("Return")) returnOrder = true;
    });
  }

  getOrderDetail() async {
    setState(() {
      loading = true;
    });
    var r = await auth.getOrderDetails(
        USER_DETAILES.data.loginDetails.authKey, widget.orderId);
    setState(() {
      od = MyOrderDetails.fromJson(r);
      print(od);
      if (od.orderdetails == null) {
        loading = false;
        showInfo = true;
      } else {
        myOrderDetail = od.orderdetails[0];
        getStoreDetails();
      }
    });
  }

  StoreDetails storeDetails;
  double homeDeliveryCharge, shippingP;

  getStoreDetails() async {
    print("getting store details");
    var r = await auth.getStoreDetail(
      myOrderDetail.storeId,
      USER_DETAILES.data.loginDetails.authKey,
    );
    setState(() {
      var jData = json.decode(r.body);
      storeDetails = StoreDetails.fromJson(jData);
      shippingP = double.parse(
          storeDetails.data.shopDetails.deliveryCharge.toStringAsFixed(3));
      print("${storeDetails}getting store details - 2");
      print("${shippingP}getting store details - 2");
      if (storeDetails.data.shopDetails.minOrder > 0)
        homeDeliveryCharge = myOrderDetail.subtotal + shippingP;
      else
        homeDeliveryCharge = myOrderDetail.subtotal;
//      loading = false;
    });
    removeExtraOfferFunction();
    setOrderStatusValue();
//    getComboOffers();
  }

  int distance = 5;
  ComboOffer comboOffer;
  int comboIndex;

  removeExtraOfferFunction() {
    int oId;
    for (int x = 0; x < myOrderDetail.orderedItems.length; x++) {
      if (myOrderDetail.orderedItems[x].offerId != 0) {
        oId = myOrderDetail.orderedItems[x].offerId;
        for (int y = x + 1; y < myOrderDetail.orderedItems.length; y++) {
          if (myOrderDetail.orderedItems[y].offerId == oId)
            setState(() {
              myOrderDetail.orderedItems.removeAt(y);
              y--;
            });
        }
      }
    }
    setState(() {
      loading = false;
      showInfo = false;
    });
//    loading = false;
    getTotalTax();
    getPaytmdetails();
  }

  getPaytmdetails() async {
    var r = await auth.getPaytmDetails(
        storeDetails.data.shopDetails.merchantId.toString(),
        myOrderDetail.orderId,
        USER_DETAILES.data.loginDetails.userId,
        USER_DETAILES.data.loginDetails.authKey);

    paytmWallet = PaytmWallet.fromJson(r);
    print("paytmdetailes:::: ${paytmWallet.paytm}");

    setState(() {
      loading = false;
      showInfo = false;
    });
  }

  getTotalTax() {
    for (int x = 0; x < myOrderDetail.orderedItems.length; x++) {
      totalTax += double.tryParse(myOrderDetail.orderedItems[x].tax);
    }
  }

  void retryPayment() async {
    String mid = paytmWallet.paytm.mID,
        channelId = paytmWallet.paytm.cHANNELID,
        industryTypeId = paytmWallet.paytm.iNDUSTRYTYPEID,
        website = paytmWallet.paytm.wEBSITE,
        merchantKey = paytmWallet.paytm.MERCHANT_KEY.toString(),
        amount = paytmWallet.paytm.tXNAMOUNT,
        orderId = DateTime.now().millisecondsSinceEpoch.toString(),
        custId = paytmWallet.paytm.cUSTID,
        callbackUrl = paytmWallet.paytm.cALLBACKURL,
        checkSum = paytmWallet.paytm.checksum;
//
//    print(callBackUrl);
    merchantKey = Uri.encodeComponent(merchantKey);
    print("merchant KEy = $merchantKey");
    var url = 'https://sleepy-dusk-58079.herokuapp.com/generateToken' +
        "?mid=" +
        mid +
        "&key_secret=" +
        merchantKey +
        "&website=" +
        website +
        "&orderId=" +
        orderId +
        "&amount=" +
        double.tryParse(amount).toStringAsFixed(2) +
        "&callbackUrl=" +
        callbackUrl +
        "&custId=" +
        custId +
        "&mode=" +
        "0";
    print("url::: $url");
    print("orderId::: $orderId");
    final response = await http.get(url);
    String txnToken = response.body;
    print("token::: $txnToken");
    if (txnToken.isEmpty) {
      String resMsg = "Something went wrong.";
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => TxnFail(
                  DateTime.now(),
                  resMsg,
                  storeDetails.data.shopDetails.merchantId.toString(),
                  orderId,
                  paytmWallet.paytm.tXNAMOUNT.toString(),
                  paytmWallet)),
          (Route<dynamic> route) => false);
    } else {
      var paytmResponse = await Paytm.payWithPaytm(mid, orderId, txnToken,
          double.tryParse(amount).toStringAsFixed(2), callbackUrl);
      print("payment_resp:  $paytmResponse");
      if (paytmResponse['error'].toString().contains("true")) {
        String resMsg = "User cancelled the transaction.";
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => TxnFail(
                    DateTime.now(),
                    resMsg,
                    storeDetails.data.shopDetails.merchantId.toString(),
                    paytmWallet.paytm.oRDERID,
                    paytmWallet.paytm.tXNAMOUNT.toString(),
                    paytmWallet)),
            (Route<dynamic> route) => false);
      } else if (paytmResponse['error'].toString().contains("false") ||
          paytmResponse.toString().contains("TXN_SUCCESS")) {
        await auth.setPaymentStatus(
          storeDetails.data.shopDetails.merchantId.toString(),
          paytmWallet.paytm.oRDERID,
          USER_DETAILES.data.loginDetails.userId,
          USER_DETAILES.data.loginDetails.authKey,
        );
        String resMsg = "Transaction Successful";
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => TxnComplete(DateTime.now(), resMsg,
                    paytmWallet.paytm.oRDERID, paytmWallet)),
            (Route<dynamic> route) => false);
      }
    }
  }

  Widget buildComboListFunction(int index) {
    print("${comboOffer.info.length} combo offer length");
    for (int x = 0; x < comboOffer.info.length; x++) {
      if (comboOffer.info[x].id == myOrderDetail.orderedItems[index].offerId) {
        comboIndex = x;
        break;
      }
    }
    return buildComboList(comboOffer.info[comboIndex], USER_DETAILES,
        comboOffer, comboIndex, src);
  }

  double rating = 3;
  Widget rateIcon = Image.asset(
    'drawables/good.png',
    height: 80,
    width: 80,
  );

  setRating(int val) {
    setState(() {
      if (val == 0) {
        rateIcon = Image.asset(
          'drawables/miserable.png',
          height: 80,
          width: 80,
        );
        dimension = "miserable";
      } else if (val < 2) {
        rateIcon = Image.asset(
          'drawables/sad.png',
          height: 80,
          width: 80,
        );
        dimension = "sad";
      } else if (val < 3) {
        rateIcon = Image.asset(
          'drawables/good.png',
          height: 80,
          width: 80,
        );
        dimension = "good";
      } else if (val < 4) {
        rateIcon = Image.asset(
          'drawables/awesome.png',
          height: 80,
          width: 80,
        );
        dimension = "awesome";
      } else if (val < 5) {
        rateIcon = Image.asset(
          'drawables/marvellous.png',
          height: 80,
          width: 80,
        );
        dimension = "marvellous";
      }
    });
  }

  Widget offerCard(int index) {
    String price = myOrderDetail.orderedItems[index].offerPrice == ""
        ? "0"
        : (double.tryParse(myOrderDetail.orderedItems[index].offerPrice) *
                myOrderDetail.orderedItems[index].quantity)
            .toStringAsFixed(2);
    return ClipRRect(
      child: Banner(
        location: BannerLocation.topStart,
        color: Colors.green,
        message: "Offer",
        textDirection: TextDirection.ltr,
        layoutDirection: TextDirection.ltr,
        child: ListTile(
          leading: ClipOval(
            child:
                myOrderDetail.orderedItems[index].offerImage.toString().isEmpty
                    ? Image.asset('drawables/logo_neo_mart_splash')
                    : Image.network(
                        myOrderDetail.orderedItems[index].offerImage,
                        height: 50,
                        width: 50,
                        fit: BoxFit.fitHeight,
                      ),
          ),
          title: Text(
            "${myOrderDetail.orderedItems[index].offerName}",
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          subtitle: Text(
              "₹ ${myOrderDetail.orderedItems[index].offerPrice} x ${myOrderDetail.orderedItems[index].quantity}"),
          trailing: Text(
            "₹ $price",
            style: TextStyle(color: mColor),
          ),
        ),
      ),
    );
  }

  showRateAlert() {
    showDialog(
        context: context,
        builder: (_) => StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return AlertDialog(
                  title: Text(
                    "Rate your experience",
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center,
                  ),
                  content: Container(
                      height: 150,
                      width: MediaQuery.of(context).size.width - 100,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          rateIcon,
                          Slider(
                            min: 0,
                            max: 4,
                            value: rating,
                            onChanged: (val) {
                              print(val.toInt());
                              setState(() {
                                rating = val;
                              });
                              setRating(val.toInt());
                            },
                          ),
                        ],
                      )),
                  actions: <Widget>[
                    ButtonTheme(
                      height: 40,
                      minWidth: MediaQuery.of(context).size.width,
                      child: OutlineButton(
                        onPressed: () {
                          Navigator.pop(context);
                          giveFeedback();
                        },
                        child: Text("Submit"),
                        borderSide: BorderSide(color: mColor),
                        textColor: mColor,
                      ),
                    )
                  ],
                );
              },
            ));
  }

  showReturnAlert() {
    showDialog(
        context: context,
        builder: (_) => StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return AlertDialog(
                  title: Text(
                    "Return Order",
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.start,
                  ),
                  content: Container(
                    padding: EdgeInsets.only(left: 16, right: 16, bottom: 16),
                    child: TextField(
                      scrollPadding: EdgeInsets.all(8.0),
                      controller: commentsController,
                      maxLines: 2,
                      maxLength: 100,
                      inputFormatters: [
                        WhitelistingTextInputFormatter(
                            RegExp("[0-9, A-Z, a-z]"))
                      ],
                      decoration: InputDecoration(
                        hintText: "Any Comments (optional)",
                      ),
                    ),
                  ),
                  actions: <Widget>[
                    ButtonTheme(
                      height: 40,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text("Cancel"),
//                        borderSide: BorderSide(color: mColor),
                        textColor: Colors.black,
                        color: Colors.white30,
                      ),
                    ),
                    ButtonTheme(
                      height: 40,
                      child: FlatButton(
                        onPressed: () {
                          Navigator.pop(context);
                          requestReturn();
                        },
                        child: Text("Submit"),
//                        borderSide: BorderSide(color: mColor),
                        textColor: Colors.white,
                        color: mColor,
                      ),
                    ),
                  ],
                );
              },
            ));
  }

  showCancelAlert() {
    showDialog(
        context: context,
        builder: (_) => StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return Container(
                  width: double.infinity,
                  child: AlertDialog(
                    title: Text(
                      "Cancel Order",
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.start,
                    ),
                    content: TextField(
                      scrollPadding: EdgeInsets.all(8.0),
                      controller: commentsController,
                      maxLines: 3,
                      maxLength: 100,
                      inputFormatters: [
                        WhitelistingTextInputFormatter(
                            RegExp("[0-9, A-Z, a-z]"))
                      ],
                      decoration: InputDecoration(
                        hintText: "Any Comments (optional)",
                      ),
                    ),
                    actions: <Widget>[
                      ButtonTheme(
                        height: 40,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Cancel"),
//                        borderSide: BorderSide(color: mColor),
                          textColor: Colors.black,
                          color: Colors.white30,
                        ),
                      ),
                      ButtonTheme(
                        height: 40,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            requestCancel();
                          },
                          child: Text("Submit"),
//                        borderSide: BorderSide(color: mColor),
                          textColor: Colors.white,
                          color: mColor,
                        ),
                      ),
                    ],
                  ),
                );
              },
            ));
  }

  requestReturn() async {
    var r = await auth.returnOrder(commentsController.text,
        myOrderDetail.orderId, USER_DETAILES.data.loginDetails.authKey);

    if (r['message'].toString().contains("Return Requested Successfully!")) {
      print("Return Requested Successfully!");
      Flushbar(
        icon: Icon(Icons.check_circle, color: Colors.green),
        messageText: Text("Order Return has been requested!",
            style: TextStyle(color: Colors.white)),
        duration: Duration(seconds: 2),
      )..show(context);
      getOrderDetail();
    } else
      print("Something went wrong!");
  }

  requestCancel() async {
    var r = await auth.cancelOrder(commentsController.text,
        myOrderDetail.orderId, USER_DETAILES.data.loginDetails.authKey);

    if (r['message'].toString().contains("Cancel Requested Successfully!")) {
      print("Cancel Requested Successfully!");
      Flushbar(
        icon: Icon(Icons.check_circle, color: Colors.green),
        messageText: Text("Order Cancellation has been requested!",
            style: TextStyle(color: Colors.white)),
        duration: Duration(seconds: 2),
      )..show(context);
      getOrderDetail();
    } else
      print("Something went wrong!");
  }

  giveFeedback() async {
    var r = await auth.giveFeedback(commentsController.text, dimension,
        myOrderDetail.orderId, rating, USER_DETAILES.data.loginDetails.authKey);

    if (r['message'].toString().contains("Thank you for your feedback.")) {
      Flushbar(
        icon: Icon(Icons.insert_emoticon, color: mColor),
        messageText: Text(
          "Thank you for your feedback.",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(seconds: 2),
      )..show(context);
    }
  }

  getInvoice() async {
    print('invoice');
    var r = await auth.getInvoice(
        myOrderDetail.orderId, USER_DETAILES.data.loginDetails.authKey);
    if (r['info'].toString().isNotEmpty) {
      pdfUrl = r['info'];
      print('$pdfUrl');
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  ShowInvoice(pdfUrl, myOrderDetail.orderId.toString())));
    } else {
      Fluttertoast.showToast(
          msg: "Something went wrong!",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIosWeb: 1,
          textColor: Colors.white,
          fontSize: 15.0);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getOrderDetail();
  }

  Widget wentWrong() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'drawables/sad.png',
            height: 80,
            width: 80,
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            "Something Went Wrong!",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 19),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Need help?"),
              SizedBox(
                width: 15,
              ),
              OutlineButton(
                borderSide: BorderSide(color: Colors.blueAccent),
                textColor: Colors.blueAccent,
                color: Colors.blueAccent,
                splashColor: Colors.blueAccent,
                onPressed: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ContactUs()));
                },
                child: Text("Contact Us"),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        backgroundColor: mColor,
        title: showInfo
            ? Text(
                "Error",
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white),
              )
            : loading
                ? Text(
                    "Loading...",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.white),
                  )
                : Text(
                    "${storeDetails.data.shopDetails.storeName}",
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(color: Colors.white),
                  ),
      ),
      body: loading
          ? neoLoader()
          : showInfo
              ? wentWrong()
              : SingleChildScrollView(
                  child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[
                        Card(
                          margin: EdgeInsets.only(top: 2),
                          elevation: 2.0,
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                dense: true,
                                title: Text(
                                    "Bill Number: ${myOrderDetail.invoiceNumber}"),
                                subtitle: Text(
                                    "Order Placed: ${myOrderDetail.orderDate}"),
                              ),
                              Divider(),
                              ListTile(
                                title: Text(
                                  "${myOrderDetail.orderStatus}",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                trailing: OutlineButton(
                                  borderSide: BorderSide(color: Colors.green),
                                  onPressed: () {
//                          openFile();
                                    print(
                                        "DELIVERY TYPE: ${myOrderDetail.deliveryType}");
                                    print(
                                        "SHIPPING CHARGE: ${myOrderDetail.shipping}");
                                    print(
                                        "phn num:  ${storeDetails.data.shopDetails.phoneNo}");
                                    UrlLauncher.launch(
                                        "tel:${storeDetails.data.shopDetails.phoneNo}");
                                    print("launcheeeee");
                                  },
                                  splashColor: Colors.green,
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      Icon(
                                        Icons.call,
                                        size: 15,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text("Call")
                                    ],
                                  ),
                                  textColor: Colors.green,
                                  color: Colors.green,
                                  disabledBorderColor: Colors.green,
                                ),
                              ),
                              cancelOrder || returnOrder
                                  ? SizedBox()
                                  : Divider(),
                              cancelOrder || returnOrder
                                  ? SizedBox()
                                  : Column(
                                      children: <Widget>[
                                        SliderTheme(
                                          data: SliderThemeData(
                                            trackHeight: 5,
                                            inactiveTrackColor: Colors.grey,
                                          ),
                                          child: Slider(
                                            min: 1,
                                            max: 4,
                                            value: orderStatusValue,
                                            activeColor: mColor,
                                            onChanged: (val) {},
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(8.0),
                                          child: Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      radius: 15,
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      child: Image(
                                                        image: AssetImage(
                                                            'drawables/order_placed.png'),
                                                        color: mColor,
                                                      ),
                                                    ),
                                                    Text(
                                                      "Order Placed",
                                                      textScaleFactor: .8,
                                                      style: TextStyle(
                                                          color: mColor,
                                                          fontSize: 11),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      radius: 15,
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      child: Image(
                                                        image: AssetImage(
                                                            'drawables/order_confirmed.png'),
                                                        color: mColor,
                                                      ),
                                                    ),
                                                    Text(
                                                      "Order Confirmed",
                                                      textScaleFactor: .8,
                                                      style: TextStyle(
                                                          color: mColor,
                                                          fontSize: 11),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      radius: 15,
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      child: Image(
                                                        image: AssetImage(
                                                            'drawables/out_for_delivery.png'),
                                                        color: mColor,
                                                      ),
                                                    ),
                                                    Text(
                                                      "Out for Delivery",
                                                      textScaleFactor: .8,
                                                      style: TextStyle(
                                                          color: mColor,
                                                          fontSize: 11),
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                child: Column(
                                                  children: <Widget>[
                                                    CircleAvatar(
                                                      radius: 15,
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      child: Image(
                                                        image: AssetImage(
                                                            'drawables/order_complete.png'),
                                                        color: mColor,
                                                      ),
                                                    ),
                                                    Text(
                                                      "Order Complete",
                                                      textScaleFactor: .8,
                                                      style: TextStyle(
                                                          color: mColor,
                                                          fontSize: 11),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                            ],
                          ),
                        ),
                        Visibility(
                          visible: cancelOrder || returnOrder ? false : true,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                ButtonTheme(
                                  minWidth: MediaQuery.of(context).size.width,
                                  child: RaisedButton(
                                    elevation: 4.0,
                                    onPressed: () {
                                      print(myOrderDetail.paymentStatus);
                                      if (orderStatusValue == 4) {
                                        showReturnAlert();
                                      } else
                                        showCancelAlert();
                                    },
                                    child: orderStatusValue == 4
                                        ? Text("Return")
                                        : Text("Cancel Order"),
                                    textColor: Colors.white,
                                    color: orderStatusValue == 4
                                        ? Colors.teal
                                        : Colors.redAccent,
                                  ),
                                ),
                                Visibility(
                                  visible: orderStatusValue == 4 ? true : false,
                                  child: ButtonTheme(
                                    minWidth: MediaQuery.of(context).size.width,
                                    child: RaisedButton(
                                      elevation: 4.0,
                                      onPressed: () {
                                        showRateAlert();
                                      },
                                      child: Text("Feedback"),
                                      textColor: Colors.black,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Card(
                            margin: EdgeInsets.only(top: 8),
                            elevation: 2.0,
                            child: Container(
                              child: Row(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    child: ListTile(
                                      onTap: () {
                                        getInvoice();
                                      },
                                      leading: Icon(
                                        Icons.assignment,
                                        color: mColor,
                                      ),
                                      title: Text("Invoice"),
                                    ),
                                  ),
                                  Expanded(
                                    child: ListTile(
                                      onTap: () {
                                        Map<String, dynamic> data = {
                                          'user': USER_DETAILES,
                                          'orderDetail': myOrderDetail,
                                          'shopDetail':
                                              storeDetails.data.shopDetails,
                                        };
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (contex) => ChatScreen(
                                                    data,
                                                    "OrderDetails",
                                                    USER_DETAILES)));
                                      },
                                      leading: Icon(
                                        Icons.chat,
                                        color: mColor,
                                      ),
                                      title: Text("Chat"),
                                    ),
                                  )
                                ],
                              ),
                            )),
                        Card(
                          margin: EdgeInsets.only(top: 8),
                          elevation: 2.0,
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                leading: Text(
                                  "Shipping Details",
                                  style: mediumTextStyleBlack,
                                ),
                                dense: true,
                              ),
                              Divider(
                                height: 0,
                                thickness: 1,
                              ),
                              ListTile(
                                title: myOrderDetail.deliveryType
                                        .toString()
                                        .contains("homedelivery")
                                    ? Text(
                                  "Delivery Address",
                                  style: mediumTextStyleBlack,
                                      )
                                    : Text(
                                  "Pickup Address",
                                  style: mediumTextStyleBlack,
                                      ),
                                subtitle: Text(
                                  "${myOrderDetail.deliveryAddress}",
                                  style: TextStyle(fontSize: 13, color: Colors
                                      .black),
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              Divider(
                                indent: 20,
                                endIndent: 20,
                              ),
                              ListTile(
                                title: myOrderDetail.deliveryType
                                    .toString()
                                    .contains("homedelivery")
                                    ? Text("Delivery Date & Time",
                                    style: mediumTextStyleBlack)
                                    : Text("Pickup Date & Time",
                                    style: mediumTextStyleBlack),
                                subtitle:
                                Text("${myOrderDetail.totalTimeToDeliver}"),
                              ),
                              myOrderDetail.orderInstructions.isNotEmpty ||
                                      myOrderDetail.orderInstructions
                                          .toString()
                                          .contains("null")
                                  ? SizedBox()
                                  : Divider(
                                      indent: 20,
                                      endIndent: 20,
                                    ),
                              myOrderDetail.orderInstructions.isEmpty ||
                                      myOrderDetail.orderInstructions
                                          .toString()
                                          .contains("null")
                                  ? SizedBox()
                                  : ListTile(
                                title: Text("Order Instructions",
                                    style: mediumTextStyleBlack),
                                subtitle: Text(
                                  "${myOrderDetail.orderInstructions
                                      .toString()}",
                                  style: TextStyle(
                                      fontStyle: FontStyle.italic,
                                      color: Colors.black),
                                ),
                                    ),
                              Visibility(
                                  visible: myOrderDetail.orderStatus ==
                                          "Order Cancelled" ||
                                      myOrderDetail.orderStatus ==
                                          "Cancel Requested",
                                  child: ListTile(
                                    title: Text("Cancellation Reason"),
                                    subtitle: Text(
                                      "${myOrderDetail.comments.toString()}",
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black),
                                    ),

                                    //                 child:ListTile(
                                    //                 title: Text("Order Instructions"),
                                    // subtitle: Text(
                                    //   "${myOrderDetail.orderInstructions.toString()}",
                                    //   style: TextStyle(
                                    //       fontStyle: FontStyle.italic,
                                    //       color: Colors.black),
                                    // ),
                                  )),
                              Visibility(
                                  visible: myOrderDetail.orderStatus ==
                                          "Order Returned" ||
                                      myOrderDetail.orderStatus ==
                                          "Return Requested",
                                  child: ListTile(
                                    title: Text("Returned Reason"),
                                    subtitle: Text(
                                      "${myOrderDetail.comments.toString()}",
                                      style: TextStyle(
                                          fontStyle: FontStyle.italic,
                                          color: Colors.black),
                                    ),

                                    //                 child:ListTile(
                                    //                 title: Text("Order Instructions"),
                                    // subtitle: Text(
                                    //   "${myOrderDetail.orderInstructions.toString()}",
                                    //   style: TextStyle(
                                    //       fontStyle: FontStyle.italic,
                                    //       color: Colors.black),
                                    // ),
                                  ))
                            ],
                          ),
                        ),
                        Card(
                          margin: EdgeInsets.only(top: 8),
                          elevation: 2.0,
                          child: Column(
                            children: <Widget>[
                              Card(
                                margin: EdgeInsets.only(bottom: 0),
                                color: Colors.grey,
                                elevation: 0.0,
                                child: ListTile(
                                  leading: Container(
                                    decoration: BoxDecoration(
                                        border: Border.all(color: mColor),
                                        shape: BoxShape.circle),
                                    height: 60,
                                    width: 60,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(30),
                                      child: storeDetails.data.shopDetails.logo
                                              .toString()
                                              .isEmpty
                                          ? Image.asset(
                                              'drawables/logo_neo_mart_splash')
                                          : Image.network(
                                              storeDetails
                                                  .data.shopDetails.logo,
                                            ),
                                    ),
                                  ),
                                  title: Text(
                                    "${storeDetails.data.shopDetails
                                        .storeName}",
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: largeTextStyleBlack,
                                  ),
                                  subtitle: Text(
                                      "${myOrderDetail.orderedItems.length} items"),
                                ),
                              ),
                              Card(
                                margin: EdgeInsets.only(top: 8),
                                elevation: 2.0,
                                child: Container(
                                  child: ListView.separated(
                                    separatorBuilder: (context, index) {
                                      return Divider();
                                    },
                                    padding: EdgeInsets.all(5),
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemCount:
                                        myOrderDetail.orderedItems.length,
                                    itemBuilder: (context, index) {
                                      return myOrderDetail.orderedItems[index]
                                                  .offerId !=
                                              0
                                          ? offerCard(index)
                                          : Container(
                                              child: ListTile(
                                                leading: ClipOval(
                                                  child: myOrderDetail
                                                          .orderedItems[index]
                                                          .productImage
                                                          .toString()
                                                          .isEmpty
                                                      ? Image.asset(
                                                          'drawables/logo_neo_mart_splash')
                                                      : Image.network(
                                                          myOrderDetail
                                                              .orderedItems[
                                                                  index]
                                                              .productImage,
                                                          height: 50,
                                                          width: 50,
                                                    fit: BoxFit.fitHeight,
                                                  ),
                                                ),
                                                title: Text(
                                                  "${myOrderDetail
                                                      .orderedItems[index]
                                                      .productname}",
                                                  style: mediumTextStyleBlack,
                                                  maxLines: 2,
                                                  overflow:
                                                  TextOverflow.ellipsis,
                                                ),
                                                subtitle: Row(
                                                  children: <Widget>[

                                                    SizedBox(width: 4,),
                                                    Text(
                                                      "₹${myOrderDetail
                                                          .orderedItems[index]
                                                          .pricewithtax
                                                          .toStringAsFixed(
                                                          2)} x ${myOrderDetail
                                                          .orderedItems[index]
                                                          .quantity}",

                                                    ),
                                                  ],
                                                ),
                                                trailing: Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                                  mainAxisSize:
                                                  MainAxisSize.min,
                                                  children: <Widget>[
                                                    AutoSizeText(
                                                      "₹ ${(myOrderDetail
                                                          .orderedItems[index]
                                                          .pricewithtax *
                                                          myOrderDetail
                                                              .orderedItems[index]
                                                              .quantity)
                                                          .toStringAsFixed(2)}",
                                                      style: TextStyle(
                                                          color: mColor),
                                                      maxFontSize: 13,
                                                      minFontSize: 9,
                                                    ),
                                                    double.tryParse(myOrderDetail
                                                                .orderedItems[
                                                                    index]
                                                                .tax) >
                                                            0.0
                                                        ? AutoSizeText(
                                                            "Tax: ₹ ${double.tryParse(myOrderDetail.orderedItems[index].tax).toStringAsFixed(2)}",
                                                            style: TextStyle(
                                                                color: mColor),
                                                            maxFontSize: 13,
                                                            minFontSize: 9,
                                                          )
                                                        : SizedBox()
                                                  ],
                                                ),
                                              ),
                                            );
                                    },
                                  ),
                                ),
                              ),
                              Card(
                                margin: EdgeInsets.only(top: 8),
                                elevation: 2.0,
                                child: Column(
                                  children: <Widget>[
                                    ListTile(
                                      leading: Text("Price Details"),
                                      trailing: Text(
                                          "Total Units: ${myOrderDetail.orderedItems.length}"),
                                      dense: true,
                                    ),
                                    Divider(
                                      height: 0,
                                      thickness: 1,
                                    ),
                                    ListTile(
                                      dense: true,
                                      title: Text("Sub Total"),
                                      trailing: Text(
                                          "₹ ${myOrderDetail.subtotal
                                              .toStringAsFixed(2)}"),
                                    ),
                                    ListTile(
                                        dense: true,
                                        title: Text("Delivery Charges"),
                                        trailing: myOrderDetail.deliveryType
                                            .toString()
                                            .contains("homedelivery") &&
                                            myOrderDetail.shipping > 0
                                            ? Text(
                                            "₹ ${myOrderDetail.shipping
                                                .toStringAsFixed(2)}")
                                            : Text("Free")),
                                    myOrderDetail.discountPer != 0
                                        ? ListTile(
                                        dense: true,
                                        title: Text("Discount"),
                                        trailing: Column(
                                          mainAxisAlignment:
                                          MainAxisAlignment.end,
                                          crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                          children: <Widget>[
                                            myOrderDetail.discountFlag
                                                .toLowerCase()
                                                .contains("false")
                                                ? Text(
                                                "₹ ${myOrderDetail.discount.toStringAsFixed(2)}")
                                                : Text(
                                                "₹ ${myOrderDetail.discountAmount.toStringAsFixed(2)}"),
                                            Text(
                                              "You Save: ${myOrderDetail.discountPer.toStringAsFixed(2)}%",
                                              style: TextStyle(
                                                  color: Colors.green),
                                            ),
                                          ],
                                        ))
                                        : SizedBox(),
                                    totalTax > 0.0
                                        ? ListTile(
                                        dense: true,
                                        title: Text("Total Tax"),
                                        trailing: Text(
                                            "₹ ${double.tryParse(
                                                myOrderDetail.totalTax)
                                                .toStringAsFixed(2)}"))
                                        : SizedBox(),
                                    Divider(
                                      height: 0,
                                      thickness: 1,
                                      indent: 15,
                                      endIndent: 15,
                                    ),
                                    ListTile(
                                        title: Text(
                                          "Total Amount",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        trailing: Text(
                                            "₹ ${myOrderDetail.grandtotal
                                                .toStringAsFixed(2)}",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14))),
                                    Divider(height: 0,),
                                    ListTile(
                                      dense: true,
                                      title: Text("Payment Mode"),
                                      trailing: myOrderDetail.paymentMode
                                          .contains("paytm")
                                          ? Image.asset(
                                        'drawables/paytm_logo.png',
                                        height: 25,
                                      )
                                          : FlatButton(
                                        disabledColor: Colors.green,
                                        child: Text(
                                            "${myOrderDetail.paymentMode.toUpperCase()}"),
                                        onPressed: null,
                                        disabledTextColor: Colors.white,
                                      ),
                                    ),
                                    myOrderDetail.paymentStatus
                                            .contains("Unpaid")
                                        ? myOrderDetail.paymentStatus
                                                    .contains("Unpaid") &&
                                                myOrderDetail.paymentMode
                                                    .contains("paytm")
                                            ? !myOrderDetail.orderStatus
                                                    .contains("Cancel")
                                                ? Column(
                                                    children: <Widget>[
                                                      Divider(),
                                                      ListTile(
                                                        leading: Icon(
                                                          Icons.info_outline,
                                                          color: mColor,
                                                        ),
                                                        title: Text(
                                                            "Transaction Failed"),
                                                        trailing: OutlineButton(
                                                          child: Text("Retry"),
                                                          onPressed: () {
//                                    Fluttertoast.showToast(msg: "This feature is under maintenance.", toastLength: Toast.LENGTH_LONG, timeInSecForIosWeb: 3);
                                                            setState(() {
                                                              loading = true;
                                                            });
                                                            retryPayment();
                                                          },
                                                          textColor:
                                                          Colors.blue,
                                                          borderSide:
                                                          BorderSide(
                                                              color: Colors
                                                                  .blue),
                                                        ),
                                                      ),
                                                    ],
                                    )
                                        : SizedBox()
                                        : SizedBox()
                                        : SizedBox(),
                                    SizedBox(height: 20,),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
    );
  }
}
