import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:userapp/Getters/GetPaytm.dart';
import 'package:userapp/Getters/GetUser.dart';

import 'MainApp.dart';
import 'MyOrders.dart';

class TxnComplete extends StatefulWidget {
  DateTime time;
  String response;
  String orderId;
  PaytmWallet paytmWallet;

  TxnComplete(this.time, this.response, this.orderId, this.paytmWallet);

  @override
  _TxnCompleteState createState() => _TxnCompleteState();
}

class _TxnCompleteState extends State<TxnComplete> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        title: Text("Transaction Complete"),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.close,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => MainApp()),
                (Route<dynamic> route) => false);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
//              Text(
//                "Transaction Failed",
//                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
//                textAlign: TextAlign.left,
//              ),
              Padding(
                padding: EdgeInsets.all(15),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Center(
                          child: Container(
                        height: 70,
                        width: 70,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(color: Colors.green, width: 2),
                            shape: BoxShape.circle),
                        child: Icon(
                          Icons.check,
                          color: Colors.green,
                          size: 50,
                        ),
                      )),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        "Order id: #${widget.orderId}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                      Container(
                        width: double.infinity,
                        child: Card(
                          color: Colors.white,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 5,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: ListTile(
                                      title: Text(
                                        "Order Number",
                                        textAlign: TextAlign.start,
                                      ),
                                      subtitle: Text("OD/${widget.orderId}/BM",
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: ListTile(
                                      title: Text(
                                        "Amount",
                                        textAlign: TextAlign.end,
                                      ),
                                      subtitle: Text(
                                          "₹ ${double.tryParse(widget.paytmWallet.paytm.tXNAMOUNT).toStringAsFixed(2)}",
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Colors.black)),
                                    ),
                                  ),
                                ],
                              ),
                              Text(
                                  "${DateFormat("dd-MM-yyyy | HH:mm:ss").format(widget.time)}"),
                              Divider(),
                              ListTile(
                                leading: Icon(
                                  Icons.info_outline,
                                  color: Colors.orange,
                                ),
                                title: Text("${widget.response}"),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      extendBody: true,
      bottomNavigationBar: BottomAppBar(
        elevation: 0.0,
        color: Colors.transparent,
        child: Container(
          height: 60,
          padding: EdgeInsets.all(8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: ButtonTheme(
                  height: 100,
                  child: RaisedButton(
                    child: Text("Home"),
                    onPressed: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => MainApp()),
                          (Route<dynamic> route) => false);
                    },
                    color: Colors.green,
                    textColor: Colors.white,
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(27)),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                flex: 1,
                child: ButtonTheme(
                  height: 100,
                  child: RaisedButton(
                    child: Text("My Orders"),
                    onPressed: () {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => MyOrders()),
                          (Route<dynamic> route) => false);
                    },
                    color: Colors.white,
                    textColor: Colors.green,
                    elevation: 8.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(27)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
