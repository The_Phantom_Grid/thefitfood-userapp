import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';

import 'package:email_validator/email_validator.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:userapp/Getters/GetProfile.dart';
import 'package:http/http.dart' as http;
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/CheckInternetConnection.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:userapp/Screens/ViewPicture.dart';

import '../Components/CommonUtility.dart';
import '../constants.dart';
import 'SearchLocation.dart';

class AccountInformation extends StatefulWidget {
  User user;
  String isNewUser;

  AccountInformation(this.user, this.isNewUser);

  @override
  _AccountInformationState createState() => _AccountInformationState();
}

class _AccountInformationState extends State<AccountInformation> {
  var auth = HttpRequests();
  ProfileData profileData;
  Profile profile;
  File image;
  String profileImage, fullName, eMail, locality;
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var phoneController = TextEditingController();
  var localityController = TextEditingController();
  String nameError, emailError, addressError;
  var status;
  bool loading = false,
      newUser = false,
      _loading = true,
      textChange = false,
      imageChange = false;
  String base64Image;
  Uint8List decodedBase64Image;
  int imageQuality = 80;

  getProfileData() async {
    var r = await auth.getProfile(widget.user.data.loginDetails.authKey);
    setState(() {
      profile = Profile.fromJson(r);
      profileData = profile.prifiledata[0];
      if (profileData != null) {
        if (profileData.email != null &&
            profileData.address != "" &&
            profileData.avatarPath != null) {
          initValues();
          newUser = false;
        } else {
          _loading = false;
          newUser = true;
        }
      }
    });
  }

  initValues() async {
    print(
        "LATTT: ${profileData.avatarBaseUrl} || LONGG: ${profileData.username}");
    setState(() {
      nameController.text = profileData.firstname;
      emailController.text = profileData.email;
      localityController.text = profileData.address;
      phoneController.text = profileData.username;

//      setState(() {
      _loading = false;
//      });
    });
    if (profileData.avatarPath.contains('jpg') ||
        profileData.avatarPath.contains('jpeg') ||
        profileData.avatarPath.contains('png')) {
      profileImage = "${profileData.avatarBaseUrl}${profileData.avatarPath}";
      var res = await http.get(profileImage);
      base64Image = base64Encode(res.bodyBytes);
      print("Uint8List => $decodedBase64Image");
      decodedBase64Image = base64Decode(base64Image);
      print("Uint8List => $decodedBase64Image");
      print("BASE64: $base64Image");
    } else
      profileImage = null;

    print("dp ===== $profileImage");
  }

  getImageFromCamera() async {
    var i = await ImagePicker.pickImage(source: ImageSource.camera);
    i = await ImageCropper.cropImage(
        sourcePath: i.path,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: imageQuality,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: mColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));
    setState(() {
      if (i != null) {
        image = i;
        imageChange = true;
      }
    });
  }

  getImageFromGallery() async {
    var i = await ImagePicker.pickImage(source: ImageSource.gallery);
    i = await ImageCropper.cropImage(
        sourcePath: i.path,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: imageQuality,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: mColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));
    setState(() {
      if (i != null) {
        image = i;
        imageChange = true;
      }
    });
  }

  updateProfile() async {
    setState(() {
      loading = true;
    });

    if (profileImage != null && image != null) {
      setState(() {
        List<int> imageBytes = image.readAsBytesSync();
        base64Image = base64Encode(imageBytes);
      });
    } else if (image != null && profileImage == null) {
      setState(() {
        List<int> imageBytes = image.readAsBytesSync();
        base64Image = base64Encode(imageBytes);
      });
    } else if (image == null && profileImage == null) base64Image = "";

    Stopwatch stopwatch = Stopwatch()..start();
//    debugPrint("BASE64IMAGE CODE =:::> $base64Image", wrapWidth: 1024);
    var r = await auth.updateProfile(
        widget.user.data.loginDetails.authKey,
        localityController.text,
        emailController.text,
        base64Image,
        nameController.text);
    if (r['status'].toString().toLowerCase().contains("success")) {
      Flushbar(
        message: "${r['message']}",
        duration: Duration(seconds: 2),
      )..show(context);
      print("BASE64 STRING LENGTH: ${base64Image.length}");
      print(
          "UPLOAD TIME FOR QUALITY [$imageQuality%]: ${stopwatch.elapsed.inMilliseconds}ms");
      convertImage();
    } else {
      Flushbar(
        message: "${r['message']}",
        duration: Duration(seconds: 2),
      )..show(context);
      setState(() {
        loading = false;
        imageChange = false;
        textChange = false;
      });
    }
  }

  convertImage() {
    setState(() {
      decodedBase64Image = base64Decode(base64Image);
      loading = false;
      newUser = false;
      widget.isNewUser = "";
      imageChange = false;
      textChange = false;
    });
  }

  validateInfo() {
    setState(() {
      print("Validating...");
      if (nameController.text.isEmpty)
        nameError = "Name Field cannot be empty!";
      else if (!EmailValidator.validate(emailController.text))
        emailError = "Email address is not valid";
      else if (localityController.text.isEmpty)
        addressError = "Enter your location!";
      else if (imageChange || textChange) updateProfile();
    });
  }

  openBottomSheet() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ListTile(
                    onTap: () {
                      getImageFromCamera();
                      Navigator.pop(context);
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () {
                      getImageFromGallery();
                      Navigator.pop(context);
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                  profileImage == null
                      ? SizedBox()
                      : ListTile(
                          onTap: () {
                            setState(() {
                              profileImage = null;
                              image = null;
                              decodedBase64Image = null;
                              imageChange = true;
                            });
                            Navigator.pop(context);
                          },
                          leading: Icon(Icons.delete_forever),
                          title: Text("Remove"),
                        ),
                ],
              ));
        });
  }

  getCurrentLocation() async {
    var result = await Geolocator().getCurrentPosition(
        desiredAccuracy: LocationAccuracy.best,
        locationPermissionLevel: GeolocationPermission.location);
    Coordinates coordinates = Coordinates(result.latitude, result.longitude);
    var addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      localityController.text = addresses[0].locality.toString();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.user.data.loginDetails.userId);
    if (widget.isNewUser.contains("NewUser"))
      newUser = true;
    else
      newUser = false;
    getProfileData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: newUser
          ? null
          : () {
              Navigator.pop(context);
            },
      child: Scaffold(
          appBar: AppBar(
            title: newUser
                ? Text(
                    "Complete Profile",
                    style: TextStyle(color: Colors.white),
                  )
                : Text(
                    "Account",
                    style: TextStyle(color: Colors.white),
                  ),
            leading: newUser
                ? SizedBox()
                : IconButton(
                    icon: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                    ),
                    onPressed: loading
                        ? null
                        : () {
                            Navigator.pop(context);
                          },
                  ),
            backgroundColor: mColor,
            centerTitle: true,
          ),
          body: _loading
              ? neoLoader()
              : SingleChildScrollView(
                  child: Center(
                      child: Container(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            child: Column(
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    GestureDetector(
                                      onTap: profileImage == null &&
                                              decodedBase64Image == null
                                          ? null
                                          : () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          ViewPicture(
                                                              decodedBase64Image)));
                                            },
                                      child: Container(
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(color: mColor)),
                                        width: 200,
                                        height: 200,
                                        child: Hero(
                                          tag: 'neomart_logo',
                                          child: ClipOval(
                                            child: image != null
                                                ? Image.file(
                                                    image,
                                                    fit: BoxFit.cover,
                                                  )
                                                : profileImage == null
                                                    ? Icon(
                                                        Icons.account_circle,
                                                        size: 200,
                                                        color: Colors.grey,
                                                      )
                                                    : Image.network(
                                                        profileImage,
                                                        fit: BoxFit.cover,
                                                      ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: 0,
                                      right: 15,
                                      child: GestureDetector(
                                        onTap: openBottomSheet,
                                        child: CircleAvatar(
                                          radius: 25,
                                          child: Icon(
                                            Icons.camera_alt,
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                TextFormField(
                                  onChanged: (value) {
                                    setState(() {
                                      textChange = true;
                                    });
                                  },
                                  controller: nameController,
//                                    "${profile.prifiledata[0].firstname}",
                                  enabled: true,
                                  maxLength: 30,
                                  maxLengthEnforced: true,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    errorText: nameError,
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: mColor, width: 5.0)),
                                    labelText: "Name",
                                  ),
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                                TextFormField(
//                                initialValue: "${profile.prifiledata[0].email}",
                                  enabled: true,
                                  onChanged: (value) {
                                    setState(() {
                                      textChange = true;
                                    });
                                  },
                                  maxLength: 30,
                                  maxLengthEnforced: true,
                                  controller: emailController,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    errorText: emailError,
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: mColor, width: 5.0)),
                                    labelText: "E-mail address",
                                  ),
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                                TextFormField(
//                                initialValue: "${profile.prifiledata[0].email}",
                                  enabled: false,
                                  // onChanged: (value) {
                                  //   setState(() {
                                  //     textChange = true;
                                  //   });
                                  // },
                                  maxLength: 30,
                                  maxLengthEnforced: true,
                                  controller: phoneController,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    // errorText: emailError,
                                    isDense: true,
                                    hasFloatingPlaceholder: true,
                                    border: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: mColor, width: 5.0)),
                                    labelText: "Phone Number",
                                  ),
                                ),
                                SizedBox(
                                  height: 25,
                                ),
                                TextFormField(
                                  controller: localityController,
                                  enabled: true,
                                  onChanged: (val) {
                                    setState(() {
                                      textChange = true;
                                    });
                                  },
                                  onTap: () async {
                                    var loc = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                SearchLocation(
                                                    widget.user.data
                                                        .loginDetails.authKey,
                                                    true)));
                                    if (loc != null)
                                      setState(() {
                                        locality = loc[0];
                                        localityController.text = locality;
                                        textChange = true;
                                      });
                                  },
                                  maxLength: 30,
                                  maxLengthEnforced: true,
                                  decoration: InputDecoration(
                                      counterText: "",
                                      suffixIcon: IconButton(
                                        onPressed: () async {
//                                  getLocation();
                                          var loc = await Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      SearchLocation(
                                                          widget
                                                              .user
                                                              .data
                                                              .loginDetails
                                                              .authKey,
                                                          true)));
                                          if (loc != null)
                                            setState(() {
                                              locality = loc[0];
                                              localityController.text =
                                                  locality;
                                              textChange = true;
                                            });
                                        },
                                        icon: Icon(Icons.my_location,
                                            color: Colors.blue),
                                      ),
                                      errorText: addressError,
                                      isDense: true,
                                      hasFloatingPlaceholder: true,
                                      border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: mColor, width: 5.0)),
                                      labelText: "Your Location",
                                      enabled: true),
                                )
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: ButtonTheme(
                                disabledColor: mColor,
                                minWidth: double.infinity,
                                height: 50.0,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(27)),
                                child: RaisedButton(
                                  disabledColor: mColor.withOpacity(.4),
                                  disabledTextColor: Colors.white,
                                  child: loading
                                      ? Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            Container(
                                              height: 15,
                                              width: 15,
                                              child: CircularProgressIndicator(
                                                strokeWidth: 2,
                                                backgroundColor: Colors.white,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text("Updating Profile..."),
                                          ],
                                        )
                                      : Text("Save"),
                                  onPressed: loading
                                      ? null
                                      : !imageChange && !textChange
                                          ? null
                                          : () {
                                              validateInfo();
                                            },
                                  textColor: Colors.white,
                                  color: mColor,
                                )),
                          )
                        ],
                      ),
                    ),
                  )),
                )),
    );
  }
}
