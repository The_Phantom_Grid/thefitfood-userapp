import 'dart:convert';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:paytm/paytm.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetPaytm.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/TxnComplete.dart';
import 'package:http/http.dart' as http;

import 'MainApp.dart';
import 'MyOrders.dart';
import 'OrderPlaced.dart';
import '../Network/httpRequests.dart';

class TxnFail extends StatefulWidget {
  DateTime time;
  String response;
  String merchantId, orderId;
  String amount;
  PaytmWallet paytmWallet;

  TxnFail(this.time, this.response, this.merchantId, this.orderId, this.amount,
      this.paytmWallet);

  @override
  _TxnFailState createState() => _TxnFailState();
}

class _TxnFailState extends State<TxnFail> {
  HttpRequests auth = HttpRequests();
  PaytmResponse pResponse;
  String orderId;
  bool _loading = false;
  String payment_response = '';

  void retryPayment() async {
    String mid = widget.paytmWallet.paytm.mID,
        channelId = widget.paytmWallet.paytm.cHANNELID,
        industryTypeId = widget.paytmWallet.paytm.iNDUSTRYTYPEID,
        website = widget.paytmWallet.paytm.wEBSITE,
        merchantKey = widget.paytmWallet.paytm.MERCHANT_KEY.toString(),
        amount = widget.paytmWallet.paytm.tXNAMOUNT,
        orderId = DateTime.now().millisecondsSinceEpoch.toString(),
        custId = widget.paytmWallet.paytm.cUSTID,
        callbackUrl = widget.paytmWallet.paytm.cALLBACKURL,
        checkSum = widget.paytmWallet.paytm.checksum;
//
//    print(callBackUrl);
    merchantKey = Uri.encodeComponent(merchantKey);
    print("merchant KEy = $merchantKey");
    print("merchant KEy = ${widget.paytmWallet.paytm.oRDERID}");
    var url = 'https://sleepy-dusk-58079.herokuapp.com/generateToken' +
        "?mid=" +
        mid +
        "&key_secret=" +
        merchantKey +
        "&website=" +
        website +
        "&orderId=" +
        orderId +
        "&amount=" +
        double.tryParse(amount).toStringAsFixed(2) +
        "&callbackUrl=" +
        callbackUrl +
        "&custId=" +
        custId +
        "&mode=" +
        "0";
    print("url::: $url");
    print("orderId::: $orderId");
    final response = await http.get(url);
    String txnToken = response.body;
    print("token::: $txnToken");
    if (txnToken.isEmpty) {
      String resMsg = "Something went wrong.";
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => TxnFail(
                  DateTime.now(),
                  resMsg,
                  widget.merchantId,
                  orderId,
                  widget.paytmWallet.paytm.tXNAMOUNT.toString(),
                  widget.paytmWallet)),
          (Route<dynamic> route) => false);
    } else {
      var paytmResponse = await Paytm.payWithPaytm(mid, orderId, txnToken,
          double.tryParse(amount).toStringAsFixed(2), callbackUrl);
      print("payment_resp:  $paytmResponse");
      if (paytmResponse['error'].toString().contains("false") ||
          paytmResponse.toString().contains("TXN_SUCCESS")) {
        await auth.setPaymentStatus(
          widget.merchantId,
          widget.paytmWallet.paytm.oRDERID,
          USER_DETAILES.data.loginDetails.userId,
          USER_DETAILES.data.loginDetails.authKey,
        );
        String resMsg = "Transaction Successful";
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => TxnComplete(
                    DateTime.now(),
                    resMsg,
                    widget.paytmWallet.paytm.oRDERID.toString(),
                    widget.paytmWallet)),
            (Route<dynamic> route) => false);
        print(paytmResponse['response']['RESPMSG'].toString());
      } else {
        String resMsg = paytmResponse['response']['RESPMSG'].toString();
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) => TxnFail(
                    DateTime.now(),
                    resMsg,
                    widget.merchantId,
                    widget.paytmWallet.orderid.toString(),
                    widget.paytmWallet.paytm.tXNAMOUNT.toString(),
                    widget.paytmWallet)),
            (Route<dynamic> route) => false);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => MainApp()),
            (Route<dynamic> route) => false);
      },
      child: Stack(
        children: <Widget>[
          Scaffold(
            body: SingleChildScrollView(
              child: Container(
                child: Column(
                  children: <Widget>[
                    Stack(
                      overflow: Overflow.visible,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).padding.top),
                          color: Colors.redAccent,
                          child: Column(
                            children: <Widget>[
                              Center(
                                  child: Container(
                                height: 70,
                                width: 70,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: Colors.redAccent, width: 2),
                                    shape: BoxShape.circle),
                                child: Icon(
                                  Icons.close,
                                  color: Colors.redAccent,
                                  size: 50,
                                ),
                              )),
                              SizedBox(
                                height: 15,
                              ),
                              Text(
                                "Transaction Failed",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 20,
                                    color: Colors.white),
                                textAlign: TextAlign.left,
                              ),
                              Text(
                                "Order id: #${widget.orderId}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14,
                                    color: Colors.white),
                              ),
                              SizedBox(
                                height: 60,
                              )
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: 150 + MediaQuery.of(context).padding.top,
                              left: 16,
                              right: 16),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Container(
                                width: double.infinity,
                                child: Card(
                                  color: Colors.white,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: <Widget>[
                                          Expanded(
                                            flex: 1,
                                            child: ListTile(
                                              title: Text(
                                                "Order Number",
                                                style: mediumTextStyleBlack,
                                                textAlign: TextAlign.start,
                                              ),
                                              subtitle: Text(
                                                  "OD/${widget.paytmWallet.paytm.oRDERID}/BM",
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                      fontSize: 13,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.black)),
                                            ),
                                          ),
                                          Expanded(
                                            flex: 1,
                                            child: ListTile(
                                              title: Text(
                                                "Amount",
                                                textAlign: TextAlign.end,
                                                style: mediumTextStyleBlack,
                                              ),
                                              subtitle: Text(
                                                  "₹ ${double.tryParse(widget.paytmWallet.paytm.tXNAMOUNT == "" ? 0.toString() : widget.paytmWallet.paytm.tXNAMOUNT).toStringAsFixed(2)}",
                                                  textAlign: TextAlign.end,
                                                  style: TextStyle(
                                                      fontSize: 13,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.black)),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Divider(),
                                      ListTile(
                                          subtitle: Text(
                                            "${DateFormat("dd-MMM-yyyy | HH:mm:ss").format(widget.time)}",
                                            style: mediumTextStyleBlack,
                                          ),
                                          leading: Icon(
                                            Icons.info_outline,
                                            color: Colors.orange,
                                          ),
                                          title: Text(
                                            "${widget.response}",
                                            style: mediumTextStyleBlack,
                                          )),
                                      SizedBox(
                                        height: 15,
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                  ],
                ),
              ),
              // child: Container(

//                 child: Column(
//                   children: <Widget>[
//                     SizedBox(
//                       height: 15,
//                     ),
// //              Text(
// //                "Transaction Failed",
// //                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
// //                textAlign: TextAlign.left,
// //              ),
//                     Padding(
//                       padding: EdgeInsets.all(15),
//                       child: Container(
//                         child: Column(
//                           children: <Widget>[
//                             Center(
//                                 child: Container(
//                               height: 70,
//                               width: 70,
//                               decoration: BoxDecoration(
//                                   color: Colors.white,
//                                   border:
//                                       Border.all(color: mColor, width: 2),
//                                   shape: BoxShape.circle),
//                               child: Icon(
//                                 Icons.close,
//                                 color: mColor,
//                                 size: 50,
//                               ),
//                             )),
//                             SizedBox(
//                               height: 15,
//                             ),
//                             Text(
//                               "Order id: #${widget.paytmWallet.paytm.oRDERID}",
//                               style: TextStyle(
//                                   fontWeight: FontWeight.bold, fontSize: 20),
//                             ),
//                             Container(
//                               width: double.infinity,
//                               child: Card(
//                                 color: Colors.white,
//                                 child: Column(
//                                   crossAxisAlignment: CrossAxisAlignment.center,
//                                   children: <Widget>[
//                                     SizedBox(
//                                       height: 5,
//                                     ),
//                                     Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.center,
//                                       crossAxisAlignment:
//                                           CrossAxisAlignment.center,
//                                       children: <Widget>[
//                                         Expanded(
//                                           flex: 1,
//                                           child: ListTile(
//                                             title: Text(
//                                               "Order Number",
//                                               textAlign: TextAlign.start,
//                                             ),
//                                             subtitle: Text(
//                                                 "OD/${widget.paytmWallet.paytm.oRDERID}/NEO",
//                                                 textAlign: TextAlign.start,
//                                                 style: TextStyle(
//                                                     fontWeight: FontWeight.bold,
//                                                     color: Colors.black)),
//                                           ),
//                                         ),
//                                         Expanded(
//                                           flex: 1,
//                                           child: ListTile(
//                                             title: Text(
//                                               "Amount",
//                                               textAlign: TextAlign.end,
//                                             ),
//                                             subtitle: Text(
//                                                 "₹ ${double.tryParse(widget.paytmWallet.paytm.tXNAMOUNT == ""? 0.toString(): widget.paytmWallet.paytm.tXNAMOUNT).toStringAsFixed(2)}",
//                                                 textAlign: TextAlign.end,
//                                                 style: TextStyle(
//                                                     fontWeight: FontWeight.bold,
//                                                     color: Colors.black)),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                     Text(
//                                         "${DateFormat("dd-MM-yyyy | HH:mm:ss").format(widget.time)}"),
//                                     Divider(),
//                                     ListTile(
//                                         leading: Icon(
//                                           Icons.info_outline,
//                                           color: Colors.orange,
//                                         ),
//                                         title: Text("${widget.response}")),
//                                     SizedBox(
//                                       height: 15,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     )
//                   ],
//                 ),
//               ),
            ),
            extendBody: true,
            bottomNavigationBar: BottomAppBar(
              elevation: 0.0,
              color: Colors.transparent,
              child: Container(
                height: 60,
                padding: EdgeInsets.all(8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: ButtonTheme(
                        height: 100,
                        child: RaisedButton(
                          child: Text("Retry"),
                          onPressed: () {
//                            Fluttertoast.showToast(msg: "This feature is under maintenance.", toastLength: Toast.LENGTH_LONG, timeInSecForIosWeb: 3);
                            setState(() {
                              _loading = true;
                            });
                            retryPayment();
                          },
                          color: Colors.redAccent,
                          textColor: Colors.white,
                          elevation: 8.0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(27)),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      flex: 1,
                      child: ButtonTheme(
                        height: 100,
                        child: RaisedButton(
                          child: Text("My Orders"),
                          onPressed: () {
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(
                                    builder: (context) => MyOrders()),
                                    (Route<dynamic> route) => false);
                          },
                          color: Colors.white,
                          textColor: Colors.redAccent,
                          elevation: 8.0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(27)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Visibility(
            visible: _loading,
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 5.0,
                sigmaY: 5.0,
              ),
              child: Center(child: CircularProgressIndicator()),
            ),
          )
        ],
      ),
    );
  }
}
