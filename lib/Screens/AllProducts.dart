import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Components/MealCard.dart';
import 'package:userapp/Getters/GetProducts.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../constants.dart';

class AllProducts extends StatefulWidget {
  ProductList productList;

  AllProducts(this.productList);

  @override
  _AllProductsState createState() => _AllProductsState();
}

class _AllProductsState extends State<AllProducts> {
  ProductList productList, temp;
  int MIN = 0, MAX = 10;
  HttpRequests requests = HttpRequests();
  bool loadProducts = false, noProducts = false;

  getProducts() async {
    print("hitttt");
    print("${STORE_INFO.info[0].storeId}");

    MIN += MAX;
    temp = null;

    var response = await requests.getProducts(
        MAX.toString(), MIN.toString(), STORE_INFO.info[0].storeId.toString());
    temp = ProductList.fromJson(response);
    if (response != null && response['data'] != null) {
      print("RESPONSE ->> $response");
      productList.data.addAll(temp.data);
      if (temp.data.isEmpty) {
        showToast("No more products", Colors.black);
        setState(() {
          loadProducts = false;
          // noProducts = false;
        });
      } else {
        setState(() {
          loadProducts = false;
          noProducts = false;
        });
      }
    } else {
      setState(() {
        loadProducts = false;
      });
    }
  }

  Widget showMore() {
    return GestureDetector(
      onTap: () {
        setState(() {
          loadProducts = true;
        });
        getProducts();
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(8)),
        padding: EdgeInsets.all(16),
        child: loadProducts
            ? Center(
                child: Text(
                  "Loading...",
                  textAlign: TextAlign.center,
                ),
              )
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Icon(Icons.add),
                    Text(
                      "Show more",
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    productList = widget.productList;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mColor,
        title: Text("All Products"),
        leading: IconButton(
          onPressed: () {
            Get.back();
          },
          icon: Icon(Icons.arrow_back),
          color: Colors.white,
        ),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 8),
        child: GridView.builder(
            addAutomaticKeepAlives: true,
            itemCount: productList.data.length < MIN
                ? productList.data.length
                : productList.data.length + 1,
            shrinkWrap: true,
            padding: EdgeInsets.only(top: 8, bottom: 100),
            physics: AlwaysScrollableScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio:
                    MediaQuery.of(context).size.width < 375 ? 0.85 : 0.95,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8),
            itemBuilder: (context, index) {
              return index == productList.data.length
                  ? showMore()
                  : MealCard(UniqueKey(), productList.data[index]);
            }),
      ),
    );
  }
}
