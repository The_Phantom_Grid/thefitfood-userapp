// import 'dart:convert';
// import 'dart:math';
// import 'dart:ui';
// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/services.dart';
// import 'package:quiver/collection.dart';
//
// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
// import 'package:fluttertoast/fluttertoast.dart';
//
// //import 'package:flutterpaytm/flutterpaytm.dart';
// import 'package:intl/intl.dart';
// import 'package:paytm/paytm.dart';
// import 'package:razorpay_flutter/razorpay_flutter.dart';
// import 'package:userapp/Components/CommonUtility.dart';
// import 'package:userapp/Screens/ApplyPromo.dart';
// import 'package:userapp/Network/CheckInternetConnection.dart';
// import 'package:userapp/Getters/GetAddress.dart';
// import 'package:userapp/Getters/GetCartProducts.dart';
// import 'package:userapp/Getters/GetMyCart.dart';
// import 'package:userapp/Getters/GetPaytm.dart';
// import 'package:userapp/Getters/GetStoreDetails.dart';
// import 'package:userapp/Getters/GetUser.dart';
// import 'package:userapp/Getters/GetViewCart.dart';
// import 'package:userapp/Screens/MainApp.dart';
// import 'package:userapp/Screens/NewAddress.dart';
// import 'package:userapp/Screens/OrderPlaced.dart';
// import 'package:userapp/Screens/SearchLocationMap.dart';
// import 'package:userapp/Screens/TxnComplete.dart';
// import 'package:userapp/Screens/TxnFail.dart';
// import 'package:userapp/Network/httpRequests.dart';
// import 'package:http/http.dart' as http;
//
// import '../constants.dart';
//
// class PlaceOrder extends StatefulWidget {
//   CartProduct cartProduct;
//   StoreDetails storeDetails;
//   StoreItemProduct storeItemProduct;
//   double subtotal;
//   int totalItems;
//
//   PlaceOrder(this.cartProduct, this.storeDetails, this.storeItemProduct,
//       this.subtotal, this.totalItems);
//
//   @override
//   _PlaceOrderState createState() => _PlaceOrderState();
// }
//
// class _PlaceOrderState extends State<PlaceOrder> {
//   Razorpay razorpay;
//   bool _loading = false, deliveryLocation = false, isHomeDeliveryAvail = false;
//   var paytm_response;
//   int orderId;
//
//   String _selectedTime = "Tap to select",
//       deliveryDay = "Today",
//       deliveryTitle = "Home delivery available from";
//   double subtotal = 0;
//   HttpRequests auth = HttpRequests();
//   GetMyAddress address;
//   int _selectedRadio = 0,
//       paytmPaymentMode = 2,
//       _paymentMode = 1,
//       deliveryTime = 0;
//   String activeAddress, deliveryStartTime, deliveryCloseTime;
//
// //  String  openTime, closeTime;
//   bool buttonVisiblity = false,
//       pickUp = false,
//       loadScreen = true,
//       paytmAvail = false,
//       razorpayAvail = false,
//       isIOS = false;
//
//   // ViewMyCart viewCart;
//   String cartId, shoppingListid;
//   String dDay,
//       tSlot,
//       deliveryType = "homedelivery",
//       paymentmode = "cash",
//       promocode = "",
//       response = "1";
//   var orderInstruction = TextEditingController();
//   int tItems;
//   double sTotal, gTotal, shippingP, deliveryRadius;
//   PaytmWallet paytmWallet;
//   String payment_response = '';
//
//   checkPaymentMode() async {
//     print("SELECTED DAY::: $selectedDay");
//     print("ACTIVE ADDRESS: $activeAddress");
//     if (activeAddress != null) {
// //      if (pickUp) {
// //        setState(() {
// //          activeAddress = widget.storeDetails.data.shopDetails.location;
// //        });
// //      }
//       if (_paymentMode == 1) {
//         placeOrder();
//       } else {
//         if (_selectedTime.contains("Tap to select") || selectedDay == -1) {
//           showDialog(
//               context: context,
//               builder: (_) => AlertDialog(
//                     title: Text(
//                       "Alert",
//                       style: TextStyle(fontWeight: FontWeight.bold),
//                     ),
//                     content: Text(
//                         "Please select delivery day & time before placing any order."),
//                     actions: <Widget>[
//                       FlatButton(
//                         color: Colors.white,
//                         textColor: mColor,
//                         onPressed: () {
//                           Navigator.pop(context);
//                         },
//                         child: Text("OK"),
//                       )
//                     ],
//                   ));
//         } else {
//           setState(() {
//             paymentmode = "paytm";
//             var f = DateFormat('dd MMM yyy');
//             dDay = "${DateFormat('dd-MM-yyy').format(wDays[selectedDay])}";
//             tSlot = "${f.format(wDays[selectedDay])}, $_selectedTime";
// //            if (deliveryDay.contains("Today")) {
// //              dDay = "${DateFormat('dd-MM-yyy').format(DateTime.now())}";
// //              tSlot = "${f.format(DateTime.now())}, $_selectedTime";
// //            } else {
// //              dDay = "${DateFormat('dd-MM-yyy').format(DateTime(
// //                  DateTime
// //                      .now()
// //                      .year,
// //                  DateTime
// //                      .now()
// //                      .month,
// //                  DateTime
// //                      .now()
// //                      .day
// //                      .toInt() + 1))}";
// //              tSlot = tSlot = "${f.format(DateTime(
// //                  DateTime
// //                      .now()
// //                      .year,
// //                  DateTime
// //                      .now()
// //                      .month,
// //                  DateTime
// //                      .now()
// //                      .day
// //                      .toInt() + 1))}, $_selectedTime";
// //            }
//             _loading = true;
//             print("$deliveryType...");
//             print("$dDay ===dilevery date");
//             print("$tSlot ===tslot date");
//           });
//           var r = await auth.placeOrder(
//               cartId,
//               activeAddress,
//               dDay,
//               deliveryType,
//               widget.storeDetails.data.shopDetails.deliveryTime.toString(),
//               widget.storeDetails.data.shopDetails.merchantId.toString(),
//               orderInstruction.text,
//               paymentmode,
//               promocode,
//               response,
//               widget.storeItemProduct.shoppingListId,
//               widget.storeDetails.data.shopDetails.id.toString(),
//               tSlot,
//               USER_DETAILES.data.loginDetails.userId.toString(),
//               "",
//               USER_DETAILES.data.loginDetails.authKey);
//
//           if (r['name'].toString().contains("PHP Warning")) {
//             print("Exception: ${r}");
//             Fluttertoast.showToast(
//                 msg: "Something went wrong!", timeInSecForIosWeb: 4);
//             Navigator.push(
//                 context, MaterialPageRoute(builder: (context) => MainApp()));
//           } else {
//             setState(() {
//               print("response: $r");
//               paytmWallet = PaytmWallet.fromJson(r);
// //            payFromPaytm();
//               generateTxnToken();
//             });
//           }
// //      pay();
//         }
//       }
//     } else {
//       showDialog(
//           context: context,
//           builder: (_) => isIOS
//               ? CupertinoAlertDialog(
//                   title: Text(
//                     "Alert",
//                     style: TextStyle(fontWeight: FontWeight.bold),
//                   ),
//                   content:
//                       Text("Please add an address before placing any order."),
//                   actions: <Widget>[
//                     CupertinoDialogAction(
//                       onPressed: () {
//                         Navigator.pop(context);
//                       },
//                       child: Text("OK"),
//                     )
//                   ],
//                 )
//               : AlertDialog(
//                   title: Text(
//                     "Alert",
//                     style: TextStyle(fontWeight: FontWeight.bold),
//                   ),
//                   content:
//                       Text("Please add an address before placing any order."),
//                   actions: <Widget>[
//                     FlatButton(
//                       color: Colors.white,
//                       textColor: mColor,
//                       onPressed: () {
//                         Navigator.pop(context);
//                       },
//                       child: Text("OK"),
//                     )
//                   ],
//                 ));
//     }
//   }
//
//   void generateTxnToken() async {
//     String mid = paytmWallet.paytm.mID,
//         channelId = paytmWallet.paytm.cHANNELID,
//         industryTypeId = paytmWallet.paytm.iNDUSTRYTYPEID,
//         website = paytmWallet.paytm.wEBSITE,
//         merchantKey = paytmWallet.paytm.MERCHANT_KEY.toString(),
//         amount = paytmWallet.paytm.tXNAMOUNT,
//         orderId = paytmWallet.paytm.oRDERID,
//         custId = paytmWallet.paytm.cUSTID,
//         callbackUrl = paytmWallet.paytm.cALLBACKURL,
//         checkSum = paytmWallet.paytm.checksum;
// //
// //    print(callBackUrl);
//     merchantKey = Uri.encodeComponent(merchantKey);
//     print("merchant KEy = $merchantKey");
//     String pMode = "0";
//     switch (paytmPaymentMode) {
//       case 1:
//         pMode = "0";
//         break;
//       case 2:
//         pMode = "2";
//         break;
//       case 3:
//         pMode = "3";
//         break;
//       case 4:
//         pMode = "1";
//         break;
//       default:
//         pMode = "0";
//     }
//     var url = 'https://sleepy-dusk-58079.herokuapp.com/generateToken' +
//         "?mid=" +
//         mid +
//         "&key_secret=" +
//         merchantKey +
//         "&website=" +
//         website +
//         "&orderId=" +
//         orderId +
//         "&amount=" +
//         double.tryParse(amount.toString()).toStringAsFixed(2) +
//         "&callbackUrl=" +
//         callbackUrl +
//         "&custId=" +
//         custId +
//         "&mode=" +
//         pMode;
//     print("url::: $url");
//     print("orderId::: $orderId");
//     final response = await http.get(url);
//     String txnToken = response.body;
//     print("token::: $txnToken");
//     print("RESPONSE CODE::: ${response.statusCode}");
//
//     if (response.statusCode != 200) {
//       String resMsg = "Something went wrong.";
//       Navigator.of(context).pushAndRemoveUntil(
//           MaterialPageRoute(
//               builder: (context) => TxnFail(
//                   DateTime.now(),
//                   resMsg,
//                   widget.storeDetails.data.shopDetails.merchantId.toString(),
//                   orderId,
//                   gTotal.toString(),
//                   paytmWallet)),
//           (Route<dynamic> route) => false);
//     } else {
//       var paytmResponse = await Paytm.payWithPaytm(mid, orderId, txnToken,
//           double.tryParse(amount.toString()).toStringAsFixed(2), callbackUrl);
//
//       print("payment_resp:  $paytmResponse");
//       if (paytmResponse['error'].toString().contains("false") ||
//           paytmResponse.toString().contains("TXN_SUCCESS")) {
//         await auth.setPaymentStatus(
//           widget.storeDetails.data.shopDetails.merchantId,
//           orderId,
//           USER_DETAILES.data.loginDetails.userId,
//           USER_DETAILES.data.loginDetails.authKey,
//         );
//         String resMsg = "Transaction Successful";
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(
//                 builder: (context) =>
//                     TxnComplete(DateTime.now(), resMsg, orderId, paytmWallet)),
//             (Route<dynamic> route) => false);
//       } else if (paytmResponse
//           .toString()
//           .contains('onBackPressedCancelTransaction')) {
//         print(paytmResponse.toString());
//         String resMsg = "User cancelled the transaction";
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(
//                 builder: (context) => TxnFail(
//                     DateTime.now(),
//                     resMsg,
//                     widget.storeDetails.data.shopDetails.merchantId.toString(),
//                     orderId,
//                     gTotal.toString(),
//                     paytmWallet)),
//             (Route<dynamic> route) => false);
//       } else {
//         print(paytmResponse['response']['RESPMSG'].toString());
//         String resMsg = paytmResponse['response']['RESPMSG'].toString();
//         Navigator.of(context).pushAndRemoveUntil(
//             MaterialPageRoute(
//                 builder: (context) => TxnFail(
//                     DateTime.now(),
//                     resMsg,
//                     widget.storeDetails.data.shopDetails.merchantId.toString(),
//                     orderId,
//                     gTotal.toString(),
//                     paytmWallet)),
//             (Route<dynamic> route) => false);
//       }
//     }
//   }
//
//   placeOrder() async {
//     if (_selectedTime.contains("Tap to select") || selectedDay == -1) {
//       showDialog(
//           context: context,
//           builder: (_) => isIOS
//               ? CupertinoAlertDialog(
//                   title: Text(
//                     "Alert",
//                     style: TextStyle(fontWeight: FontWeight.bold),
//                   ),
//                   content: Text(
//                       "Please select delivery day & time before placing any order."),
//                   actions: <Widget>[
//                     CupertinoDialogAction(
//                       onPressed: () {
//                         Navigator.pop(context);
//                       },
//                       child: Text("OK"),
//                     )
//                   ],
//                 )
//               : AlertDialog(
//                   title: Text(
//                     "Alert",
//                     style: TextStyle(fontWeight: FontWeight.bold),
//                   ),
//                   content: Text(
//                       "Please select delivery day & time before placing any order."),
//                   actions: <Widget>[
//                     FlatButton(
//                       color: Colors.white,
//                       textColor: mColor,
//                       onPressed: () {
//                         Navigator.pop(context);
//                       },
//                       child: Text("OK"),
//                     )
//                   ],
//                 ));
//     } else {
//       setState(() {
//         var f = DateFormat('dd MMM yyy');
//
//         dDay = "${DateFormat('dd-MM-yyy').format(wDays[selectedDay])}";
//         tSlot = "${f.format(wDays[selectedDay])}, $_selectedTime";
// //        if (deliveryDay.contains("Today")) {
// //          dDay = "${DateFormat('dd-MM-yyy').format(DateTime.now())}";
// //          tSlot = "${f.format(DateTime.now())}, $_selectedTime";
// //        } else {
// //          dDay = "${DateFormat('dd-MM-yyy').format(DateTime(
// //              DateTime
// //                  .now()
// //                  .year,
// //              DateTime
// //                  .now()
// //                  .month,
// //              DateTime
// //                  .now()
// //                  .day
// //                  .toInt() + 1))}";
// //          tSlot = tSlot = "${f.format(DateTime(
// //              DateTime
// //                  .now()
// //                  .year,
// //              DateTime
// //                  .now()
// //                  .month,
// //              DateTime
// //                  .now()
// //                  .day
// //                  .toInt() + 1))}, $_selectedTime";
// //        }
//         _loading = true;
//         print("$deliveryType...");
//         print("$gTotal...");
//         print("$dDay ===dilevery date");
//         print("$tSlot ===tslot date");
//       });
//       var r = await auth.placeOrder(
//           cartId,
//           activeAddress,
//           dDay,
//           deliveryType,
//           widget.storeDetails.data.shopDetails.deliveryTime.toString(),
//           widget.storeDetails.data.shopDetails.merchantId.toString(),
//           orderInstruction.text,
//           paymentmode,
//           promocode,
//           response,
//           widget.storeItemProduct.shoppingListId,
//           widget.storeDetails.data.shopDetails.id.toString(),
//           tSlot,
//           USER_DETAILES.data.loginDetails.userId.toString(),
//           "",
//           USER_DETAILES.data.loginDetails.authKey);
//       setState(() {
// //      var jData = jsonDecode(r.body);
// //      print("${jData}Loading...2");
//         if (r['orderid'] != null) {
// //      if (r['status'] == "success") {
//           orderId = r['orderid'];
//           Navigator.push(
//               context,
//               MaterialPageRoute(
//                   builder: (context) => OrderPlaced(orderId, activeAddress,
//                       tSlot, widget.storeDetails, pickUp)));
//         } else
//           Navigator.push(
//               context, MaterialPageRoute(builder: (context) => MainApp()));
//       });
//     }
//   }
//
// //
// //  getSubtotal() {
// //    for(int x = 0; x < widget.cartProduct.product.length; x++){
// //      subtotal += widget.cartProduct.product[x].sellingPrice * widget.cartProduct.product[x].quantity;
// //    }
// //  }
//
//   getAddress() async {
//     var r = await auth.getAddress(USER_DETAILES.data.loginDetails.authKey,
//         USER_DETAILES.data.loginDetails.userId);
//     setState(() {
//       address = GetMyAddress.fromJson(r);
// //      activeAddress =
// //      address.details.isEmpty ? null : address.details[0].address;
// //      checkDeliveryLocation();
//       if (address.details.isNotEmpty)
//         calculateDistance();
//       else
//         setState(() {
//           pickUp = true;
//           _selectedRadio = 1;
//           activeAddress = widget.storeDetails.data.shopDetails.location;
//           deliveryLocation = false;
//           deliveryType = "pickup";
//         });
//     });
//     getCartId();
//   }
//
// //  checkDeliveryLocation(){
// //    if(!activeAddress.toString().contains(widget.storeDetails.data.shopDetails.pincode)){
// //      deliveryLocation = true;
// //      showLocationAlert();
// //    }else deliveryLocation = false;
// //  }
//
//   selectRadio(int val) {
//     setState(() {
//       _selectedRadio = val;
//     });
//   }
//
//   paymentMode(int val) {
//     setState(() {
//       _paymentMode = val;
//     });
//   }
//
//   selectAddress(int index) {
//     setState(() {
//       activeAddress = addressInRadius[index].address;
//     });
//     if (keys[index].toString().contains("true")) {
//       setState(() {
//         deliveryLocation = false;
//       });
//     } else {
//       setState(() {
//         deliveryLocation = true;
//       });
//     }
//   }
//
// //  List<String> timeSlots = [];
// //
// //  createTimeSlots(){
// //    timeSlots.clear();
// //
// //    int start = DateTime.tryParse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} $openTime").millisecondsSinceEpoch;
// //    int end = DateTime.tryParse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} $closeTime").millisecondsSinceEpoch;
// //    int hour = 3600000;
// //
// //    print("Start time => $start ||| ${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(start))}" );
// //    print("End time => $end ||| ${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(end))}" );
// //
// //    for(int x = end - start; hour < x; x = x - hour){
// //      timeSlots.add(DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(start))
// //          + " to " + DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(start + hour)));
// //
// //      start = start + hour;
// //    }
// //
// //    print("TimeSlots ========");
// //    print(timeSlots);
// //
// //  }
//
//   List<Details> addressInRadius = [];
//   List<String> keys = [];
//
//   calculateDistance() {
//     addressInRadius.clear();
//     keys.clear();
//     print(
//         "delivery Radius: ${widget.storeDetails.data.shopDetails.deliveryRadius}");
//     double lat1 = widget.storeDetails.data.shopDetails.lat;
//     double lon1 = widget.storeDetails.data.shopDetails.lng;
//     if (lon1 == null || lon1 == null) {
//       Navigator.pop(context);
//       Fluttertoast.showToast(
//           msg: "Something went wrong!",
//           timeInSecForIosWeb: 2,
//           toastLength: Toast.LENGTH_LONG);
//     }
//     var p = 0.017453292519943295;
//     var c = cos;
//     var a;
//     double lat2, lon2;
//     double distanceInKm;
//     for (int x = 0; x < address.details.length; x++) {
//       lat2 = double.tryParse(address.details[x].lat.toString());
//       lon2 = double.tryParse(address.details[x].lng.toString());
//
//       print("user lat: ${lat2}||| user lng: ${lon2}");
//       a = 0.5 -
//           c((lat2 - lat1) * p) / 2 +
//           c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
//       distanceInKm = 12742 * asin(sqrt(a));
//
//       print("distance from less:   $distanceInKm");
//       print("distance from less:   $deliveryRadius");
// //     if(distanceInKm < radius)
// //       print("distance from less:   $distanceInKm");
// //     else print("distance from far:   $distanceInKm");
//       if (deliveryRadius > distanceInKm) {
//         addressInRadius.add(address.details[x]);
//         keys.add("true");
//       } else {
//         addressInRadius.add(address.details[x]);
//         keys.add("false");
//       }
//     }
//     if (deliveryRadius == 0.0) {
//       print("delivery is 0.....");
//       setState(() {
//         isHomeDeliveryAvail = false;
//         deliveryTitle = "Store pickup is available from";
//         activeAddress = widget.storeDetails.data.shopDetails.location;
//         deliveryLocation = false;
//         pickUp = true;
//         deliveryType = "pickup";
//       });
//     } else {
//       print("delivery is $deliveryRadius....");
//       setState(() {
//         activeAddress = addressInRadius[0].address;
//         if (keys[0].contains("true"))
//           deliveryLocation = false;
//         else
//           deliveryLocation = true;
//       });
//     }
//   }
//
//   Future showAddList() {
//     return showDialog(
//         context: context,
//         builder: (_) => AlertDialog(
//               title: Text(
//                 "Select Address",
//                 style: TextStyle(fontWeight: FontWeight.bold),
//               ),
//               content: Container(
//                 width: MediaQuery.of(context).size.width - 100,
//                 child: ListView.builder(
//                   scrollDirection: Axis.vertical,
//                   itemCount: addressInRadius.length,
//                   itemBuilder: (context, index) {
//                     return InkWell(
//                       onTap: () {
//                         Navigator.pop(context);
//                         selectAddress(index);
//                       },
//                       child: Card(
//                         child: ListTile(
//                           selected: keys[index].contains("true") ? true : false,
//                           title: Text("${addressInRadius[index].address}"),
//                         ),
//                       ),
//                     );
//                   },
//                 ),
//               ),
//             ));
//   }
//
//   showLocationAlert() {
//     showDialog(
//         context: context,
//         builder: (_) => isIOS
//             ? CupertinoAlertDialog(
//                 title: Text(
//                   "Sorry",
//                   style: TextStyle(fontWeight: FontWeight.bold),
//                 ),
//                 content:
//                     Text("Merchant does not deliver to selected location."),
//                 actions: <Widget>[
//                   CupertinoDialogAction(
//                     onPressed: () {
//                       Navigator.pop(context);
//                     },
//                     child: Text("OK"),
//                   )
//                 ],
//               )
//             : AlertDialog(
//                 title: Text(
//                   "Sorry",
//                   style: TextStyle(fontWeight: FontWeight.bold),
//                 ),
//                 content:
//                     Text("Merchant does not deliver to selected location."),
//                 actions: <Widget>[
//                   FlatButton(
//                     color: Colors.white,
//                     textColor: mColor,
//                     onPressed: () {
//                       Navigator.pop(context);
//                     },
//                     child: Text("OK"),
//                   )
//                 ],
//               ));
//   }
//
//   viewCartList() async {
//     var r = await auth.viewCart(
//         USER_DETAILES.data.loginDetails.authKey,
//         widget.storeDetails.data.shopDetails.merchantId,
//         widget.storeDetails.data.shopDetails.id);
//     print(widget.storeDetails.data.shopDetails.merchantId);
//     print(widget.storeDetails.data.shopDetails.id);
//     print("viewcart::: $r");
//     setState(() {
//       if (r['total']['razorpay_avail'] == 1) razorpayAvail = true;
//       if (r['total']['paytm_avail'] == 1) paytmAvail = true;
//       loadScreen = false;
//     });
//   }
//
//   getCartId() async {
//     var r = await auth.getCartId(
//         USER_DETAILES.data.loginDetails.authKey,
//         widget.storeDetails.data.shopDetails.merchantId,
//         widget.storeItemProduct.shoppingListId,
//         widget.storeDetails.data.shopDetails.id);
//     setState(() {
//       if (r['status'] == "success") {
//         cartId = r['cart_id']['cart_id'].toString();
//         print("${cartId} cartId");
//         shoppingListid = r['cart_id']['shopping_list_id'].toString();
//         print("${shoppingListid} shoppingListId");
//       }
//     });
// //    loadScreen = false;
//     viewCartList();
//   }
//
// //  placeOrder() async {
// //    var r = auth.placeOrder(cart_id, deliveryAddress, cdeliveryTyp, deliveryDate, deliveryType, max_time, merchantId, orderInstruction, paymentMode, promo, shoppingListId, timeSlot, storeId, userId, token)
// //  }
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     razorpay = Razorpay();
//     razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, handlePaymentSuccess);
//     razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, handlePaymentError);
// //    razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, handleExternalWallet);
// //    _selectedTime = timeSlots[0];
//     shippingP = widget.storeDetails.data.shopDetails.deliveryCharge;
//     sTotal = widget.subtotal;
//     tItems = widget.totalItems;
//     // if (sTotal > widget.storeDetails.data.shopDetails.minOrder)
//     //   shippingP = 0;
//     gTotal = widget.subtotal +
//         shippingP +
//         double.tryParse(widget.cartProduct.total.totaltax.toString());
//     deliveryTime = widget.storeDetails.data.shopDetails.deliveryTime;
//     deliveryRadius =
//         double.tryParse(widget.storeDetails.data.shopDetails.deliveryRadius) ??
//             0;
//     if (deliveryRadius == 0.0) {
//       _selectedRadio = 1;
//       isHomeDeliveryAvail = false;
//       deliveryTitle = "Store pickup is available from";
//       print("home del:::: $isHomeDeliveryAvail");
//     }
//     print("D RADIUS: $deliveryRadius");
//     getAddress();
//     _paymentMode = 1;
// //    openTime = widget.storeDetails.data.shopDetails.storeOpenTime;
// //    closeTime = widget.storeDetails.data.shopDetails.storeCloseTime;
//     setDeliveryTime();
//     print(
//         "STORE CLOSED DAYS:: ${widget.storeDetails.data.shopDetails.storeCloseDays}");
// //    getWD();
//   }
//
//   setDeliveryTime() {
//     if (widget.storeDetails.data.shopDetails.deliveryStartTime != null) {
//       isHomeDeliveryAvail = true;
//       deliveryTitle = "Home delivery available from";
// //      deliveryLocation = true;
//       deliveryStartTime =
//           widget.storeDetails.data.shopDetails.deliveryStartTime;
//       deliveryCloseTime =
//           widget.storeDetails.data.shopDetails.deliveryCloseTime;
//     } else {
//       isHomeDeliveryAvail = false;
// //      deliveryLocation = false;
//       _selectedRadio = 1;
// //      deliveryLocation = false;
//       deliveryTitle = "Store pickup is available from";
//       deliveryStartTime = widget.storeDetails.data.shopDetails.storeOpenTime;
//       deliveryCloseTime = widget.storeDetails.data.shopDetails.storeCloseTime;
//     }
//     //    createTimeSlots();
// //    print("OPEN TIME:: $openTime");
// //    print("CLOSE TIME:: $closeTime");
//     print("START D TIME:: $deliveryStartTime");
//     print("CLOSE D TIME:: $deliveryCloseTime");
// //    getWD();
//
//     print("home del:::SET: $isHomeDeliveryAvail");
//     if (_selectedRadio == 0)
//       setHomeDelivery(true);
//     else
//       setHomeDelivery(false);
//   }
//
//   setHomeDelivery(bool type) {
//     print("SET HOME DELIVERY");
//     setState(() {
//       if (type) {
//         deliveryTitle = "Home delivery available from";
//         deliveryStartTime =
//             widget.storeDetails.data.shopDetails.deliveryStartTime;
//         deliveryCloseTime =
//             widget.storeDetails.data.shopDetails.deliveryCloseTime;
//       } else {
//         print(
//             "STORE OPEN TIME: ${widget.storeDetails.data.shopDetails.storeOpenTime}");
//         deliveryTitle = "Store pickup is available from";
//         deliveryStartTime = widget.storeDetails.data.shopDetails.storeOpenTime;
//         deliveryCloseTime = widget.storeDetails.data.shopDetails.storeCloseTime;
//       }
//     });
// //    getWD();
//   }
//
//   checkMinOrder() {
//     if (_selectedRadio == 0) {
//       if (widget.cartProduct.total.bill_subtotal <
//           widget.storeDetails.data.shopDetails.minOrder) {
//         showAlert();
//       } else {
//         checkInternet();
//       }
//     } else {
//       checkInternet();
//     }
//   }
//
//   showAlert() {
//     print("ALERT");
//     showDialog(
//         context: context,
//         barrierDismissible: false,
//         builder: (context) {
//           return AlertDialog(
//             title: Text(
//               "Alert",
//               style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
//             ),
//             content: Container(
//               child: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     "Your cart value is less than minimum order amount. To proceed please add more product to your cart.",
//                     style: TextStyle(fontSize: 12),
//                   ),
//                   Text(
//                     "Store's minimum order value is ${widget.storeDetails.data.shopDetails.minOrder}",
//                     style: TextStyle(fontSize: 12),
//                   )
//                 ],
//               ),
//             ),
//             shape:
//                 RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
//             actions: <Widget>[
//               ButtonTheme(
//                 height: 40,
//                 minWidth: 100,
//                 shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(25)),
//                 child: RaisedButton(
//                   onPressed: () {
//                     Navigator.pop(context);
//                   },
//                   child: Text(
//                     "Cancel",
//                     style: TextStyle(color: Colors.white),
//                   ),
//                 ),
//               ),
//             ],
//           );
//         });
//   }
//
//   checkInternet() async {
// //    CheckInternet checkInternet = CheckInternet();
// //    DataConnectionStatus status = await CheckInternet(context).checkInternet();
// //    if (status == DataConnectionStatus.connected) {
// //      print("yes");
//     checkPaymentMode();
// //    } else {
// //      showDialog(
// //          context: context,
// //          builder: (_) {
// //            return isIOS ?
// //            CupertinoAlertDialog(
// //              title: Text("No Internet"),
// //              content:  Text("Check your Internet connection & try again"),
// //              actions: <Widget>[
// //                CupertinoDialogAction(
// //                  onPressed: () {
// //                    Navigator.pop(context);
// //                    checkInternet();
// //                  },
// //                  child: Text("Retry"),
// //                )
// //              ],
// //            ):
// //            AlertDialog(
// //              title: Text("No Internet"),
// //              content: Text("Check your Internet connection & try again"),
// //              actions: <Widget>[
// //                FlatButton(
// //                  onPressed: () {
// //                    Navigator.pop(context);
// //                    checkInternet();
// //                  },
// //                  child: Text("Retry"),
// //                  color:  mColor,
// //                  textColor: Colors.white,
// //                ),
// //              ],
// //            );
// //          });
// //    }
//   }
//
//   void openCheckout() async {
//     var options = {
//       'key': 'rzp_live_5jblswmdl5PMXF',
//       'amount': num.parse("2") * 100,
//       'name': "${widget.storeDetails.data.shopDetails.storeName}",
//       'description': 'Order id: 1231',
// //      'order_id': "order_FGeQJJFFD4pShk",
// //      'external': {
// //        'wallets': ['paytm']
// //      }
//     };
//
//     try {
//       razorpay.open(options);
//     } catch (e) {
//       debugPrint(e);
//     }
//   }
//
//   handlePaymentSuccess(PaymentSuccessResponse response) {
//     Fluttertoast.showToast(
//         msg: "SUCCESS: " + response.paymentId, timeInSecForIosWeb: 4);
//     Navigator.of(context).pushAndRemoveUntil(
//         MaterialPageRoute(
//             builder: (context) => TxnComplete(DateTime.now(),
//                 "Payment Successful", orderId.toString(), paytmWallet)),
//         (Route<dynamic> route) => false);
//   }
//
//   handlePaymentError(PaymentFailureResponse response) {
//     Fluttertoast.showToast(
//         msg: "ERROR: " + response.code.toString() + " - " + response.message,
//         timeInSecForIosWeb: 4);
//     Navigator.of(context).pushAndRemoveUntil(
//         MaterialPageRoute(
//             builder: (context) => TxnFail(
//                 DateTime.now(),
//                 "${response.message}",
//                 widget.storeDetails.data.shopDetails.merchantId.toString(),
//                 orderId.toString(),
//                 gTotal.toString(),
//                 paytmWallet)),
//         (Route<dynamic> route) => false);
//   }
//
// //  handleExternalWallet(ExternalWalletResponse response){
// //    Fluttertoast.showToast(
// //        msg: "WALLET: " + response.walletName, timeInSecForIosWeb: 4);
// //  }
//
//   @override
//   void dispose() {
//     // TODO: implement dispose
//     super.dispose();
//     razorpay.clear();
//   }
//
// //  List<DropdownMenuItem<String>> buildDropDownMenuItem(timeSlots)
//
// //  onChangeDropDown(String value) {
// //    setState(() {
// //      _selectedTime = value;
// //    });
// //  }
//
//   List<String> dList = [];
//   Map<int, DateTime> wDays = {};
//   String offDays = "";
//   int selectedDay = -1;
//
//   getWD() {
//     wDays.clear();
//     dList.clear();
//     offDays =
//         widget.storeDetails.data.shopDetails.storeCloseDays.contains("null")
//             ? ""
//             : widget.storeDetails.data.shopDetails.storeCloseDays
//                 .replaceAll(',', "");
//     var weekday = DateTime.now().weekday - 1;
//     var now = DateTime.now();
//
//     for (int i = 1; i < 7; i++) {
//       wDays[now.weekday] = now;
//       dList.add(DateFormat('d EEE').format(now));
//       now = now.add(Duration(days: 1));
//     }
//
//     wDays[weekday] = now;
//     dList.add(DateFormat('d EEE').format(now));
//     wDays.forEach((key, value) {
//       print("$key |||| ${DateFormat('EEE').format(value)}");
//     });
//     wDays.entries.map((e) {
//       if (!offDays.contains(e.key.toString())) {
//         selectedDay = e.key;
//       }
//     });
//   }
//
//   selectDay(MapEntry<int, DateTime> e) {
//     print("KEY:: ${e.key}");
//     if (!offDays.contains("${e.key}"))
//       setState(() {
//         selectedDay = e.key;
//         _selectedTime = "Tap to select";
//       });
//     else {
//       Fluttertoast.showToast(
//           msg: "Store is closed on ${DateFormat('EEE').format(e.value)}");
//     }
//
//     print(selectedDay);
//   }
//
//   Widget weekDays() {
//     getWD();
//     return Container(
// //      margin: EdgeInsets.only(top: 10),
//         height: 50,
//         child: ListView(
//           itemExtent: 55,
//           scrollDirection: Axis.horizontal,
//           children: wDays.entries.map((e) {
//             return GestureDetector(
//               onTap: () {
//                 selectDay(e);
//               },
//               child: Container(
//                 decoration: selectedDay != e.key
//                     ? BoxDecoration(
//                         color: offDays.contains(e.key.toString())
//                             ? Colors.red
//                             : Colors.green,
//                       )
//                     : BoxDecoration(
//                         border: Border.all(color: Colors.green),
//                         color: Colors.white,
//                       ),
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   mainAxisSize: MainAxisSize.min,
//                   children: <Widget>[
//                     Text(
//                       DateFormat('EEE').format(e.value),
//                       style: TextStyle(
//                           color: selectedDay != e.key
//                               ? Colors.white
//                               : Colors.green,
//                           fontSize: 13,
//                           fontWeight: FontWeight.bold),
//                     ),
//                     SizedBox(
//                       height: 2,
//                     ),
//                     Text(
//                       DateFormat('d').format(e.value),
//                       style: TextStyle(
//                           color: selectedDay != e.key
//                               ? Colors.white
//                               : Colors.green),
//                     ),
//                   ],
//                 ),
//               ),
//             );
//           }).toList(),
//         ));
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     isIOS = Theme.of(context).platform == TargetPlatform.iOS ? true : false;
//     return Stack(
//       children: <Widget>[
//         Scaffold(
//           appBar: AppBar(
//             backgroundColor: mColor,
//             leading: IconButton(
//               onPressed: () {
//                 Navigator.pop(context);
//               },
//               icon: Icon(
//                 Icons.arrow_back_ios,
//                 color: Colors.white,
//               ),
//             ),
//             centerTitle: true,
//             title: Text(
//               "Checkout",
//               style: TextStyle(color: Colors.white),
//             ),
//           ),
//           body: loadScreen
//               ? neoLoader()
//               : SingleChildScrollView(
//                   child: Center(
//                     child: Column(
//                       children: <Widget>[
//                         Card(
//                           margin: EdgeInsets.only(top: 2),
//                           child: Container(
//                             padding: EdgeInsets.all(8.0),
//                             child: Column(
//                               children: <Widget>[
//                                 ListTile(
//                                   dense: true,
//                                   title: Text("${tItems} items",
//                                       style: TextStyle(fontSize: 13)),
//                                   trailing: Text(
//                                     "₹ ${double.tryParse(sTotal.toString()).toStringAsFixed(2)}",
//                                     style: TextStyle(
//                                         fontWeight: FontWeight.bold,
//                                         fontSize: 14),
//                                   ),
//                                 ),
//                                 Divider(),
//                                 ListTile(
//                                     dense: true,
//                                     title: Text("Tax",
//                                         style: TextStyle(fontSize: 13)),
//                                     trailing: Text(
//                                         "₹ ${double.tryParse(widget.cartProduct.total.totaltax.toString()).toStringAsFixed(2)}",
//                                         style: TextStyle(fontSize: 14))),
//                                 Divider(),
//                                 ListTile(
//                                     dense: true,
//                                     title: Text("Delivery Charge",
//                                         style: TextStyle(fontSize: 13)),
//                                     trailing: pickUp
//                                         ? Text("Free")
//                                         : Text(
//                                             "₹ ${double.tryParse(shippingP.toString()).toStringAsFixed(2)}",
//                                             style: TextStyle(fontSize: 14),
//                                           )),
//                                 Divider(),
//                                 ListTile(
//                                   dense: true,
//                                   title: Text("Discount",
//                                       style: TextStyle(fontSize: 13)),
//                                   trailing: Row(
//                                     mainAxisSize: MainAxisSize.min,
//                                     mainAxisAlignment: MainAxisAlignment.end,
//                                     crossAxisAlignment: CrossAxisAlignment.end,
//                                     children: <Widget>[
//                                       Text(
//                                         "You save ",
//                                         style: TextStyle(
//                                             color: Colors.green, fontSize: 13),
//                                       ),
//                                       Text(
//                                         "₹ ${double.tryParse(widget.cartProduct.total.overallDiscount).toStringAsFixed(0)}",
//                                         style: TextStyle(
//                                             color: Colors.green, fontSize: 14),
//                                       ),
//                                     ],
//                                   ),
//                                 ),
//                                 Divider(),
//                                 ListTile(
//                                     dense: true,
//                                     title: OutlineButton(
//                                       borderSide: BorderSide(
//                                           color: Colors.blueAccent, width: 1),
//                                       onPressed: () {
//                                         Navigator.push(
//                                             context,
//                                             MaterialPageRoute(
//                                                 builder: (context) =>
//                                                     ApplyPromo(USER_DETAILES,
//                                                         gTotal)));
//                                       },
//                                       child: Text(
//                                         "Apply Promo",
//                                         style:
//                                             TextStyle(color: Colors.blueAccent),
//                                       ),
//                                     )),
//                                 Divider(
//                                   height: 1,
//                                 ),
//                                 ListTile(
//                                   title: Text(
//                                     "Total",
//                                     style: TextStyle(
//                                         fontWeight: FontWeight.bold,
//                                         fontSize: 13),
//                                   ),
//                                   trailing: pickUp
//                                       ? Text(
//                                           "₹ ${(double.tryParse(sTotal.toString()) + double.tryParse(widget.cartProduct.total.totaltax.toString())).toStringAsFixed(2)}",
//                                           style: TextStyle(
//                                               fontWeight: FontWeight.bold,
//                                               color: mColor,
//                                               fontSize: 14),
//                                         )
//                                       : Text(
//                                           "₹ ${double.tryParse(gTotal.toString()).toStringAsFixed(2)}",
//                                           style: TextStyle(
//                                               fontWeight: FontWeight.bold,
//                                               color: mColor,
//                                               fontSize: 14),
//                                         ),
//                                 )
//                               ],
//                             ),
//                           ),
//                         ),
//                         Card(
//                           margin: EdgeInsets.only(top: 10),
//                           child: Column(
//                             children: <Widget>[
//                               Container(
//                                 padding: EdgeInsets.all(8.0),
//                                 child: Text(
//                                   "Shipping Details",
//                                   textAlign: TextAlign.left,
//                                   style: TextStyle(fontWeight: FontWeight.bold),
//                                 ),
//                               ),
//                               Divider(),
//                               Row(
//                                 children: <Widget>[
//                                   Expanded(
//                                     flex: 1,
//                                     child: RadioListTile(
//                                       value: 0,
//                                       groupValue: _selectedRadio,
//                                       onChanged: deliveryRadius == 0.0 ||
//                                               !isHomeDeliveryAvail
//                                           ? null
//                                           : (val) {
//                                               selectRadio(val);
//                                               setState(() {
//                                                 activeAddress =
//                                                     address.details.isEmpty
//                                                         ? null
//                                                         : addressInRadius[0]
//                                                             .address;
//                                                 pickUp = false;
//                                                 deliveryType = "homedelivery";
//                                                 if (keys.isNotEmpty &&
//                                                     activeAddress != null &&
//                                                     keys[0].contains("true"))
//                                                   deliveryLocation = false;
//                                                 else
//                                                   deliveryLocation = true;
//                                                 _selectedTime = "Tap to select";
//                                               });
//                                               setHomeDelivery(true);
//                                             },
//                                       title: Text("Home Delivery",
//                                           style: TextStyle(fontSize: 14)),
//                                     ),
//                                   ),
//                                   Expanded(
//                                     flex: 1,
//                                     child: RadioListTile(
//                                       value: 1,
//                                       groupValue: _selectedRadio,
//                                       onChanged: (val) {
//                                         selectRadio(val);
//                                         setState(() {
//                                           pickUp = true;
//                                           activeAddress = widget.storeDetails
//                                               .data.shopDetails.location;
//                                           deliveryLocation = false;
//                                           deliveryType = "pickup";
//                                           _selectedTime = "Tap to select";
//                                         });
//                                         setHomeDelivery(false);
//                                         print(pickUp);
//                                       },
//                                       title: Text(
//                                         "Visit Store",
//                                         style: TextStyle(fontSize: 14),
//                                       ),
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                               Divider(),
//                               ListTile(
//                                 leading: Icon(
//                                   Icons.location_city,
//                                   color: Colors.black,
//                                 ),
//                                 title: _selectedRadio == 0
//                                     ? activeAddress == null
//                                         ? Text(
//                                             "You have not setup any address yet.",
//                                             style: TextStyle(
//                                                 fontWeight: FontWeight.bold),
//                                           )
//                                         : Text(
//                                             "${activeAddress}",
//                                             style: TextStyle(fontSize: 13),
// //                            overflow: TextOverflow.ellipsis,
//                                           )
//                                     : Text(
//                                         "${widget.storeDetails.data.shopDetails.location}",
//                                         style: TextStyle(fontSize: 13),
// //                            overflow: TextOverflow.ellipsis,
//                                       ),
//                               ),
//                               deliveryLocation
//                                   ? Container(
//                                       color:  mColor,
//                                       child: ListTile(
//                                         leading: Icon(Icons.info_outline,
//                                             color: Colors.white),
//                                         title: Text(
//                                           "Merchant does not deliver to selected location.",
//                                           style: TextStyle(color: Colors.white),
//                                         ),
//                                       ),
//                                     )
//                                   : SizedBox(),
//                               Visibility(
//                                 visible: _selectedRadio == 0 ? true : false,
//                                 child: Container(
//                                   padding: EdgeInsets.all(8),
//                                   child: Row(
//                                     children: <Widget>[
//                                       Expanded(
//                                         flex: 1,
//                                         child: OutlineButton(
//                                           onPressed: activeAddress == null
//                                               ? null
//                                               : () {
//                                                   showAddList();
// //                                  getAddress();
// //                                  calculateDistance();
//                                                 },
//                                           child: Text("Change"),
//                                         ),
//                                       ),
//                                       Expanded(
//                                         flex: 1,
//                                         child: OutlineButton(
//                                           onPressed: () async {
//                                             // String addressId = null;
//                                             // await Navigator.push(
//                                             //     context,
//                                             //     MaterialPageRoute(
//                                             //         builder: (context) =>
//                                             //             SearchLocationMap(
//                                             //                 USER_DETAILES,
//                                             //                 addressId,
//                                             //                 address.details
//                                             //                         .isEmpty
//                                             //                     ? null
//                                             //                     : address
//                                             //                         .details[0],
//                                             //                 false)));
//                                             // getAddress();
//                                           },
//                                           child: Text("Add new address"),
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                               Divider(),
//                               Container(
//                                 width: double.infinity,
//                                 alignment: Alignment.center,
//                                 color: Colors.teal,
//                                 child: Text(
//                                   "$deliveryTitle ${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(DateTime.parse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} $deliveryStartTime").millisecondsSinceEpoch))} to ${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(DateTime.parse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} $deliveryCloseTime").millisecondsSinceEpoch))}",
//                                   style: TextStyle(color: Colors.white),
//                                 ),
//                               ),
//                               SizedBox(
//                                 height: 20,
//                               ),
//                               weekDays(),
//                               Padding(
//                                 padding: const EdgeInsets.all(8.0),
//                                 child: Container(
//                                   child: Row(
//                                     mainAxisAlignment:
//                                         MainAxisAlignment.spaceEvenly,
//                                     crossAxisAlignment:
//                                         CrossAxisAlignment.center,
// //                              mainAxisSize: MainAxisSize.min,
//                                     children: <Widget>[
//                                       Text("Delivery Time:"),
//                                       Flexible(flex: 1, child: buildTimeSlot())
//                                     ],
//                                   ),
//                                 ),
//                               )
//                             ],
//                           ),
//                         ),
//                         buildPaymentMethod(),
//                         Card(
//                           margin: EdgeInsets.only(top: 4),
//                           child: Column(
//                             children: <Widget>[
//                               ListTile(
//                                 title: Text("Order Instructions"),
//                               ),
//                               Container(
//                                 padding: EdgeInsets.only(
//                                     left: 16, right: 16, bottom: 16),
//                                 child: TextField(
//                                   controller: orderInstruction,
//                                   scrollPadding: EdgeInsets.all(8.0),
//                                   maxLines: 2,
//                                   maxLength: 100,
//                                   inputFormatters: [
//                                     WhitelistingTextInputFormatter(
//                                         RegExp("[a-zA-Z 0-9 ,-/]"))
//                                   ],
//                                   decoration: InputDecoration(
//                                     hintText: "Any Comments (optional)",
//                                   ),
//                                 ),
//                               )
//                             ],
//                           ),
//                         )
//                       ],
//                     ),
//                   ),
//                 ),
//           floatingActionButton: !deliveryLocation
//               ? IgnorePointer(
//                   ignoring: loadScreen || _loading == true ? true : false,
//                   child: FloatingActionButton.extended(
//                       backgroundColor: mColor,
//                       heroTag: 'checkout_button',
//                       elevation: 8.0,
//                       onPressed: () {
//                         print(deliveryType);
//                         print(pickUp);
// //                  generateTxnToken();
// //                   checkInternet();
//                         checkMinOrder();
// //                  openCheckout();
//                       },
//                       label: Text("Place Order")),
//                 )
//               : SizedBox(),
//         ),
//         Visibility(
//           visible: _loading,
//           child: BackdropFilter(
//             filter: ImageFilter.blur(
//               sigmaX: 5.0,
//               sigmaY: 5.0,
//             ),
//             child: neoLoader(),
//           ),
//         )
//       ],
//     );
//   }
//
//   Widget buildTimeSlot() {
//     return GestureDetector(
//       child: Text(
//         "$_selectedTime",
//         style: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
//       ),
//       onTap: () {
//         DatePicker.showTime12hPicker(context, onConfirm: (time) {
//           print("${DateFormat('yyyyMMdd').format(DateTime.now())} date now");
//           if (deliveryDay.contains("Tomorrow")) {
//             if (time.isBefore(DateTime.parse(
//                 "${DateFormat('yyyy-MM-dd').format(DateTime.now())} $deliveryStartTime"))) {
//               Flushbar(
//                 message: "Please select time between business hours",
//                 icon: Icon(Icons.info_outline, color: mColor),
//                 duration: Duration(seconds: 2),
//               )..show(context);
//             } else if (time.isAfter(DateTime.parse(
//                 "${DateFormat('yyyy-MM-dd').format(DateTime.now())} $deliveryCloseTime"))) {
//               Flushbar(
//                 message: "Please select time between business hours",
//                 icon: Icon(Icons.info_outline, color: mColor),
//                 duration: Duration(seconds: 2),
//               )..show(context);
//             } else {
//               setState(() {
//                 print("DELIVERY TIME: $deliveryTime");
//                 _selectedTime =
//                     "${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(time.millisecondsSinceEpoch))}"
//                     " to ${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(time.add(Duration(minutes: deliveryTime)).millisecondsSinceEpoch))}";
// //                        ${DateFormat('hh:mma').format(
// //                        DateTime.fromMillisecondsSinceEpoch(DateTime
// //                            .parse("${DateFormat(
// //                            'yyyy-MM-dd').format(
// //                            DateTime
// //                                .now())} $deliveryCloseTime")
// //                            .millisecondsSinceEpoch))}";
//               });
//             }
//           } else {
//             if (time.isBefore(DateTime.parse(
//                 "${DateFormat('yyyy-MM-dd').format(DateTime.now())} $deliveryStartTime"))) {
//               Flushbar(
//                 message: "Please select time between business hours",
//                 icon: Icon(Icons.info_outline, color: mColor),
//                 duration: Duration(seconds: 2),
//               )..show(context);
//             } else if (time.isAtSameMomentAs(DateTime.now()) ||
//                 time.isBefore(DateTime.fromMillisecondsSinceEpoch(
//                         DateTime.now().millisecondsSinceEpoch - 60000)) &&
//                     selectedDay == time.weekday) {
//               Flushbar(
//                 message: "Delivery time cannot be set",
//                 icon: Icon(Icons.info_outline, color: mColor),
//                 duration: Duration(seconds: 2),
//               )..show(context);
//             } else if (time.isAfter(DateTime.parse(
//                 "${DateFormat('yyyy-MM-dd').format(DateTime.now())} $deliveryCloseTime"))) {
//               Flushbar(
//                 message: "Please select time between business hours",
//                 icon: Icon(Icons.info_outline, color: mColor),
//                 duration: Duration(seconds: 2),
//               )..show(context);
//             } else {
//               setState(() {
//                 print("DELIVERY TIME: $deliveryTime");
//                 _selectedTime =
//                     "${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(time.millisecondsSinceEpoch))}"
//                     " to ${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(time.add(Duration(minutes: deliveryTime)).millisecondsSinceEpoch))}";
// //                        ${DateFormat('hh:mma').format(
// //                        DateTime.fromMillisecondsSinceEpoch(DateTime
// //                            .parse("${DateFormat(
// //                            'yyyy-MM-dd').format(
// //                            DateTime
// //                                .now())} $deliveryCloseTime")
// //                            .millisecondsSinceEpoch))}";
//               });
//             }
//           }
//         });
//       },
//     );
//   }
//
//   Card buildPaymentMethod() {
//     return Card(
//       margin: EdgeInsets.only(top: 4),
//       child: Column(
//         children: <Widget>[
//           ListTile(
//             title: Text("Payment Method"),
//           ),
//           RadioListTile(
//             value: 1,
//             groupValue: _paymentMode,
//             onChanged: (val) {
//               setState(() {
//                 _paymentMode = val;
//                 paytmPaymentMode = 0;
//               });
//             },
//             title: Text("Cash on Delivery"),
//           ),
//           paytmAvail
//               ? RadioListTile(
// //                          subtitle: Text("Coming soon"),
//                   value: 2,
//                   groupValue: _paymentMode,
//                   onChanged: (val) {
//                     setState(() {
//                       _paymentMode = val;
//                       paytmPaymentMode = 1;
//                     });
//                   },
//                   title: Image.asset('drawables/paytm_logo.png',
//                       alignment: Alignment.centerLeft, height: 25),
//                   secondary: _paymentMode == 2
//                       ? Icon(Icons.keyboard_arrow_down)
//                       : Icon(Icons.keyboard_arrow_right),
//                 )
//               : SizedBox(),
//           AnimatedContainer(
//             curve: Curves.decelerate,
//             margin: EdgeInsets.only(left: 25),
//             duration: Duration(milliseconds: 200),
//             height: _paymentMode == 2 ? 240 : 0,
//             child: Wrap(
//               children: <Widget>[
//                 RadioListTile(
//                   onChanged: (val) {
//                     setState(() {
//                       paytmPaymentMode = val;
//                     });
//                   },
//                   value: 1,
//                   groupValue: paytmPaymentMode,
//                   title: Text("Wallet"),
//                 ),
//                 RadioListTile(
//                   onChanged: (val) {
//                     setState(() {
//                       paytmPaymentMode = val;
//                     });
//                   },
//                   value: 2,
//                   groupValue: paytmPaymentMode,
//                   title: Text("UPI"),
//                 ),
//                 RadioListTile(
//                   onChanged: (val) {
//                     setState(() {
//                       paytmPaymentMode = val;
//                     });
//                   },
//                   value: 3,
//                   groupValue: paytmPaymentMode,
//                   title: Text("Credit/Debit Card"),
//                 ),
//                 RadioListTile(
//                   onChanged: (val) {
//                     setState(() {
//                       paytmPaymentMode = val;
//                     });
//                   },
//                   value: 4,
//                   groupValue: paytmPaymentMode,
//                   title: Text("Netbanking"),
//                 ),
//               ],
//             ),
//           ),
//           razorpayAvail
//               ? RadioListTile(
// //                          subtitle: Text("Coming soon"),
//                   value: 3,
//                   groupValue: _paymentMode,
//                   onChanged: (val) {
//                     setState(() {
//                       _paymentMode = val;
//                       paytmPaymentMode = 0;
//                     });
//                   },
//                   title: Image.asset('drawables/razorpay-logo.png',
//                       alignment: Alignment.centerLeft, height: 25),
//                 )
//               : SizedBox()
//         ],
//       ),
//     );
//   }
// }
