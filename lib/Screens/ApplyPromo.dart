import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';

class ApplyPromo extends StatefulWidget {
  User user;
  double gTotal;

  ApplyPromo(this.user, this.gTotal);

  @override
  _ApplyPromoState createState() => _ApplyPromoState();
}

class _ApplyPromoState extends State<ApplyPromo> {
  var promoController = TextEditingController();
  HttpRequests auth = HttpRequests();

  validatePromo() async {
    print('hit..');
    var r = await auth.validatePromo(
        promoController.text,
        widget.gTotal,
        widget.user.data.loginDetails.userId,
        widget.user.data.loginDetails.authKey);

    print(r.toString());
    if (r['info']['message'].toString().contains('Invalid Promocode')) {
      Flushbar(
        message: "Invalid Promocode",
        duration: Duration(seconds: 2),
      ).show(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Apply Promo"),
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: TextField(
                    controller: promoController,
                    scrollPadding: EdgeInsets.all(8.0),
                    decoration: InputDecoration(
                      hintText: "Enter Promocode",
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                    flex: 1,
                    child: FlatButton(
                      textColor: Colors.white,
                      color: Colors.blue,
                      onPressed: () {
                        validatePromo();
                      },
                      child: Text("Apply"),
                    ))
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Text("Choose from the offers below")
          ],
        ),
      ),
    );
  }
}
