import 'dart:async';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:userapp/Screens/LoginRegister/RegisterNewUser.dart';
import 'package:userapp/Screens/LoginRegister/UpdatePassword.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../Components/CommonUtility.dart';
import '../../Getters/GetOtp.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {

  String _usernameError;
  var numberController = TextEditingController(),
      otpController = TextEditingController();
  HttpRequests auth = HttpRequests();
  var otp = 0;
  bool showOtpField = false, disbaleButton = false;
  String phoneNo, smsCode, verificationId;
  OtpSend otpSend;
  FocusNode keyboardFNode;
  bool resendOtp = true;

//  ////////////////////FIREBASE OTP FUNCTION////////////////////
//  Future<void> sendOTP() async {
//    setState(() {
//      phoneNo = "+91${numberController.text}";
//    });
//    print('Code Sent');
//    final PhoneCodeAutoRetrievalTimeout autoRetrievalTimeout = (String verId) {
//      this.verificationId = verId;
//      Navigator.pop(context);
//      autoVerificationFailed();
//    };
//
//    final PhoneCodeSent phoneCodeSent = (String verId, [int forceCodeResend]) {
//      this.verificationId = verId;
//      print("sent ${forceCodeResend}");
//      Fluttertoast.showToast(
//          msg: "OTP Sent Successfully",
//          gravity: ToastGravity.CENTER,
//          toastLength: Toast.LENGTH_SHORT
//      );
//      autoVerification();
//    };
//
//    final PhoneVerificationCompleted verificationCompleted = (
//        AuthCredential user) {
//      print("Successfully verified!");
//      print("$smsCode sms code ${user}");
//      setState(() {
//        showOtpField = true;
//        disbaleButton = false;
//      });
//      Navigator.pop(context);
//      OtpVerified();
//    };
//
//    final PhoneVerificationFailed verificationFailed = (
//        AuthException exception) {
//      print("${exception.message} -- Failed");
//      if (exception.message.contains('Network')) {
//        Flushbar(
//          message: "Network Error",
//          duration: Duration(seconds: 2),
//        )
//          ..show(context);
//      } else if (exception.message.contains('not authorized')) {
//        Navigator.pop(context);
//        Flushbar(
//          message: "Something has gone wrong, please try later",
//          duration: Duration(seconds: 2),
//        )
//          ..show(context);
//      } else {
//        Navigator.pop(context);
//        Flushbar(
//          message: "${exception.message}",
//        )
//          ..show(context);
//      }
//    };
//
//    await FirebaseAuth.instance.verifyPhoneNumber(
//        phoneNumber: phoneNo,
//        timeout: Duration(seconds: 10),
//        verificationCompleted: verificationCompleted,
//        verificationFailed: verificationFailed,
//        codeSent: phoneCodeSent,
//        codeAutoRetrievalTimeout: autoRetrievalTimeout
//    );
//
//    print("$smsCode sms code");
//  }
//
///////////////END////////////////////////////////////////////////
//  verifyOTP() async {
//    var authCred = await PhoneAuthProvider.getCredential(
//        verificationId: verificationId, smsCode: smsCode);
//    print("verification: $authCred");
//    FirebaseAuth.instance.signInWithCredential(authCred).catchError((error) {
//      Flushbar(
//        messageText: Text(
//          "OTP does not match!", style: TextStyle(color: Colors.white),),
//        duration: Duration(seconds: 2),
//      )
//        ..show(context);
//    }).then((AuthResult authResult) {
//      print("${authResult} result");
//      if (authResult != null) {
//        Navigator.pop(context);
//        OtpVerified();
//      }
//    });
//  }

//  testUser() {
//    if (numberController.text.contains('9868563310') ||
//        numberController.text.contains('8810529272') ||
//        numberController.text.contains('9599582029') ||
//        numberController.text.contains('8285311018')) {
//      Fluttertoast.showToast(
//          msg: "TESTING MODE [Sending OTP...]",
//          gravity: ToastGravity.CENTER,
//          toastLength: Toast.LENGTH_SHORT
//      );
////        openBottomSheet();
//      sendOTP();
//    } else
//      checkUser();
//  }

  sendOTP() async {
    var r = await auth.sendOTP("0", numberController.text);
    if (r['status'].toString().toLowerCase().contains("failed")) {
      Fluttertoast.showToast(
        msg: "${r['message'].toString()}",
        gravity: ToastGravity.TOP,
        toastLength: Toast.LENGTH_LONG,
        timeInSecForIosWeb: 4,
      );
    } else if (r['status'].toString().toLowerCase().contains("success")) {
      Fluttertoast.showToast(
          msg: "OTP Sent Successfully",
          gravity: ToastGravity.TOP,
          toastLength: Toast.LENGTH_SHORT);
      setState(() {
        showOtpField = true;
        disbaleButton = false;
      });
    }
  }

  verifyOTP() async {
    var r = await auth.verifyOTP(smsCode, numberController.text);
    if (r['status'].toString().contains("success")) {
      OtpVerified();
    } else {
      Flushbar(
        messageText: Text(
          "OTP does not match!",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(seconds: 2),
      )..show(context);
    }
  }

//  checkUser() a sync {
//    var r = await auth.getOtp(0, 0, numberController.text);
//    print(r.toString());
//    setState(() {
//      otpSend = OtpSend.fromJson(r);
//      if (otpSend.type.toString().contains("failed")) {
//        Flushbar(
//          message: "${otpSend.message.toString()}",
//          flushbarStyle: FlushbarStyle.GROUNDED,
//          flushbarPosition: FlushbarPosition.TOP,
//          icon: Icon(
//            Icons.info,
//            color: mColor,
//          ),
//          duration: Duration(seconds: 4),
//        )
//          ..show(context);
//
////        Navigator.pushReplacement(
////            context, MaterialPageRoute(builder: (context) => LoginPage()));
//      } else if (otpSend.type.toString().contains("success")) {
//        setState(() {
//          disbaleButton = true;
//        });
//        sendOTP();
//      }
//    });
//  }

  OtpVerified() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
        ),
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 30,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "OTP Verification Complete",
                  style: TextStyle(color: Colors.green),
                ),
                SizedBox(
                  height: 25,
                ),
                OutlineButton(
                    color: Colors.blue,
                    borderSide: BorderSide(color: mColor),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  UpdatePassword(numberController.text)));
                    },
                    child: Text(
                      "Proceed",
                      style: TextStyle(color: mColor),
                    ))
              ],
            ),
          );
        });
  }

//  autoVerification() {
//    showModalBottomSheet(
//        context: context,
//        isDismissible: false,
//        isScrollControlled: true,
//        shape: RoundedRectangleBorder(
//          borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
//        ),
//        builder: (context) {
//          return Container(
//            padding: EdgeInsets.all(15),
//            width: MediaQuery
//                .of(context)
//                .size
//                .width,
//            child: Column(
//              mainAxisSize: MainAxisSize.min,
//              children: <Widget>[
//                CircularProgressIndicator(),
//                SizedBox(height: 10,),
//                Text("Auto verifying OTP")
//              ],
//            ),
//          );
//        });
//  }

//  enterManually() {
//    showModalBottomSheet(
//        context: context,
//        isDismissible: false,
//        isScrollControlled: true,
//        shape: RoundedRectangleBorder(
//          borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
//        ),
//        builder: (context) {
//          return Container(
//            padding: EdgeInsets.all(15),
//            width: MediaQuery
//                .of(context)
//                .size
//                .width,
//            child: Column(
//              mainAxisSize: MainAxisSize.min,
//              children: <Widget>[
//                Icon(Icons.sms_failed, color:  mColor, size: 30,),
//                SizedBox(height: 10,),
//                Text("Auto Verification Failed!",
//                  style: TextStyle(color:  mColor),),
//                SizedBox(height: 25,),
//                OutlineButton(
//                    color: Colors.blue,
//                    borderSide: BorderSide(
//                        color: Colors.blue
//                    ),
//                    onPressed: () {
//                      Navigator.pop(context);
//                      setState(() {
//                        showOtpField = true;
//                        disbaleButton = false;
//                      });
//                    },
//                    child: Text("Enter code manually",
//                      style: TextStyle(color: Colors.blue),))
//              ],
//            ),
//          );
//        });
//  }

  getOtp() async {
    var r = await auth.getOTP(numberController.text, otp);
    setState(() {
      if (r['message'] == "User already registered!") {
        Flushbar(
          message: "You are already a member of neomart family!",
          flushbarStyle: FlushbarStyle.GROUNDED,
          flushbarPosition: FlushbarPosition.TOP,
          showProgressIndicator: true,
          icon: Icon(
            Icons.insert_emoticon,
            color: mColor,
          ),
          duration: Duration(seconds: 4),
        )..show(context);
//        Navigator.pushReplacement(
//            context, MaterialPageRoute(builder: (context) => LoginPage()));
      } else if (r['message'] == "User already registered as Merchant!") {
        Flushbar(
          message: "You are already registered as Merchant!",
          flushbarStyle: FlushbarStyle.GROUNDED,
          flushbarPosition: FlushbarPosition.TOP,
          showProgressIndicator: true,
          icon: Icon(
            Icons.info,
            color: mColor,
          ),
          duration: Duration(seconds: 4),
        )..show(context);
      }
    });
  }

  validate() {
    setState(() {
      if (numberController.text.length < 10 || numberController.text == " ") {
        _usernameError = "Check your mobile number";
      } else if (int.parse(numberController.text) < 6000000000 ||
          int.parse(numberController.text) > 9999999999) {
        _usernameError = "Please enter valid mobile number";
      } else {
//        showOtpField = true;
//        Navigator.push(
//            context, MaterialPageRoute(builder: (context) => UpdatePassword()));
        sendOTP();
      }
    });
  }

  //  validatePassword() {
//    setState(() {
//      if (passwordController.text.length < 8 ||
//          passwordController.text == " ") {
//        _passwordError = "Password must be minimum 8 characters!";
//      } else if (passwordController.text != passwordController2.text) {
//        _passwordError2 = "Password does not match!";
//      } else {
//        Flushbar(
//          message: "Password Updated Successfully!",
//          icon: Icon(
//            Icons.check_circle,
//            color: Colors.green,
//          ),
//        )..show(context);
//      }
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Forgot Password",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Center(
          child: Container(
        padding: const EdgeInsets.all(15.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(25.0),
                child: Hero(
                    transitionOnUserGestures: true,
                    tag: 'neomart_logo',
                    child: Image(
                        height: 100,
                        width:100,
                      image: AssetImage('drawables/logo_neo_mart_splash.png'),
                    )),
              ),
              Container(
                padding: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      enabled: showOtpField ? false : true,
                      keyboardType: TextInputType.number,
                      maxLengthEnforced: true,
                      maxLength: 10,
                      onChanged: (value) {
                        if (value.length == 10) {
                          FocusScope.of(context).requestFocus(FocusNode());
                        }
                      },
                      inputFormatters: [
                        WhitelistingTextInputFormatter(RegExp("[0-9]"))
                      ],
                      decoration: InputDecoration(
                          enabled: showOtpField ? false : true,
                          counterText: "",
                          errorText: _usernameError,
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.grey,
                                  width: 1.0,
                                  style: BorderStyle.solid)),
                          hintText: "000 000 0000",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: Colors.grey,
                                  width: 1.0,
                                  style: BorderStyle.solid)),
                          labelText: "Enter Mobile Number",
                          hasFloatingPlaceholder: true,
                          labelStyle: TextStyle(color: Colors.grey)),
                      controller: numberController,
                      focusNode: keyboardFNode,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    Visibility(
                      visible: showOtpField,
                      child: Column(
                        children: <Widget>[
                          GestureDetector(
                            onLongPress: () {
                              showDialog(
                                  context: context,
                                  builder: (_) {
                                    return AlertDialog(
                                      titlePadding: EdgeInsets.all(0.0),
                                      title: ListTile(
                                        onTap: () async {
                                          ClipboardData data =
                                              await Clipboard.getData(
                                                  Clipboard.kTextPlain);
                                          print(data.text);
                                          setState(() {
                                            if (data.text.length > 4 ||
                                                data.text.isEmpty) {
                                              Navigator.pop(context);
                                              Fluttertoast.showToast(
                                                  msg: "Invalid OTP");
                                            } else {
                                              Navigator.pop(context);
                                              otpController.text = data.text;
                                            }
                                          });
                                        },
                                        title: Text("Paste OTP"),
                                      ),
                                    );
                                  });
                            },
                            child: Container(
                              alignment: Alignment.center,
                              child: PinCodeTextField(
                                controller: otpController,
                                onTextChanged: (value) {
                                  smsCode = value;
                                },
                                pinBoxBorderWidth: 0,
                                pinBoxDecoration: ProvidedPinBoxDecoration
                                    .underlinedPinBoxDecoration,
                                pinBoxRadius: 4,
                                wrapAlignment: WrapAlignment.center,
                                keyboardType: TextInputType.numberWithOptions(),
                                pinBoxHeight: 40,
                                maxLength: 4,
                                pinBoxWidth: 40,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Container(
                              padding: EdgeInsets.only(right: 8),
                              alignment: Alignment.centerRight,
                              child: GestureDetector(
                                onTap: resendOtp
                                    ? () {
                                        setState(() {
                                          resendOtp = false;
                                        });
                                        sendOTP();
                                        Timer(Duration(seconds: 15), () {
                                          setState(() {
                                            resendOtp = true;
                                          });
                                        });
                                      }
                                    : null,
                                child: Text(
                                  "Resend",
                                  style: TextStyle(
                                    color: resendOtp ? mColor : Colors.grey,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Hero(
                        tag: 'login_button',
                        child: ButtonTheme(
                          minWidth: double.infinity,
                          height: 50.0,
                          child: RaisedButton(
                            child:
                                showOtpField ? Text("Verify") : Text("Get OTP"),
                            onPressed: disbaleButton
                                ? null
                                : () {
                                    if (showOtpField)
                                      verifyOTP();
                                    else
                                      validate();
                                  },
                            color: mColor,
                            textColor: Colors.white,
                            elevation: 8.0,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(27)),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      )),
    );
  }
}
