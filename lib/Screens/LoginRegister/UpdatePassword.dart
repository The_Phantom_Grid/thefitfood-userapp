import 'dart:ui';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:userapp/Getters/GetUserAuth.dart';
import 'package:userapp/Screens/LoginRegister/LoginPage.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../Components/CommonUtility.dart';
import '../../Components/CommonUtility.dart';

class UpdatePassword extends StatefulWidget {
  String number;

  UpdatePassword(this.number);

  @override
  _UpdatePasswordState createState() => _UpdatePasswordState();
}

class _UpdatePasswordState extends State<UpdatePassword> {

  var passwordController = TextEditingController();
  var passwordController2 = TextEditingController();
  bool _obscureText1 = true;
  bool _obscureText2 = true, _loading = false;
  String _passwordError;
  String _passwordError2;
  HttpRequests auth = HttpRequests();
  VerifyOtp verifyOtp;

  updatePassword() async {
    var r =
        await auth.updatePassword(verifyOtp.authkey, passwordController2.text);
    if (r['message'].toString().contains("Password Set Successfully")) {
      setState(() {
        _loading = false;
      });
      Flushbar(
        message: "Password Updated Successfully!",
        icon: Icon(
          Icons.check_circle,
          color: Colors.green,
        ),
      )..show(context);
      Future.delayed(Duration(seconds: 2)).then((_) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false);
      });
    } else {
      Flushbar(
        message: "${r['message'].toString()}",
        icon: Icon(
          Icons.info_outline,
          color: Colors.green,
        ),
      )..show(context);
      Future.delayed(Duration(seconds: 2)).then((_) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => LoginPage()),
            (Route<dynamic> route) => false);
      });
    }
  }

  validatePassword() {
    setState(() {
      if (passwordController.text.length < 8 ||
          passwordController.text == " ") {
        _passwordError = "Password must be minimum 8 characters!";
      } else if (passwordController.text != passwordController2.text) {
        _passwordError2 = "Password does not match!";
      } else {
        getUser();
      }
    });
  }

////////Getting User Token/Auth Key////////
  getUser() async {
    setState(() {
      _loading = true;
    });
    var r = await auth.verifyOtp(widget.number, 1111);
    setState(() {
      verifyOtp = VerifyOtp.fromJson(r);
      updatePassword();
    });
  }

///////////////End///////////////////////

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.number);
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Scaffold(
//      resizeToAvoidBottomPadding: false,
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.white,
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios, color: Colors.black),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            title: Text(
              "Update Password",
              style: TextStyle(color: Colors.black),
            ),
            centerTitle: true,
          ),
          body: Center(
            child: Container(
              padding: EdgeInsets.all(20),
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: Hero(
                            transitionOnUserGestures: true,
                            tag: 'neomart_logo',
                            child: Image(
                              height: 100,
                              width: 100,
                              image: AssetImage(
                                  'drawables/logo_neo_mart_splash.png'),
                            )),
                      ),
                      TextFormField(
                          controller: passwordController,
                          obscureText: _obscureText1,
                          maxLength: 30,
                          decoration: InputDecoration(
                              counterText: "",
                              suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (_obscureText1)
                                      _obscureText1 = false;
                                    else
                                      _obscureText1 = true;
                                    print(_obscureText1);
                                  });
                                },
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: !_obscureText1 ? mColor : Colors.grey,
                                ),
                              ),
                              errorText: _passwordError,
                              labelStyle: TextStyle(color: Colors.grey),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.grey,
                                      width: 1.0,
                                      style: BorderStyle.solid)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey)),
                              labelText: "New Password")),
                      SizedBox(
                        height: 15,
                      ),
                      TextFormField(
                          controller: passwordController2,
                          obscureText: _obscureText2,
                          maxLength: 30,
                          decoration: InputDecoration(
                              counterText: "",
                              suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    if (_obscureText2)
                                      _obscureText2 = false;
                                    else
                                      _obscureText2 = true;
                                    print(_obscureText2);
                                  });
                                },
                                icon: Icon(
                                  Icons.remove_red_eye,
                                  color: !_obscureText2
                                      ? mColor
                                      : Colors.grey,
                                ),
                              ),
                              errorText: _passwordError2,
                              labelStyle: TextStyle(color: Colors.grey),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: Colors.grey,
                                      width: 1.0,
                                      style: BorderStyle.solid)),
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey)),
                              labelText: "Confirm Password")),
                      SizedBox(
                        height: 15,
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Hero(
                          tag: 'register_button',
                          child: ButtonTheme(
                            minWidth: double.infinity,
                            height: 50.0,
                            child: RaisedButton(
                              child: Text("Update Password"),
                              onPressed: () {
                                validatePassword();
                              },
                              textColor: Colors.white,
                              color: mColor,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: _loading,
          child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 5.0,
              sigmaY: 5.0,
            ),
            child: Center(child: CircularProgressIndicator()),
          ),
        )
      ],
    );
  }
}
