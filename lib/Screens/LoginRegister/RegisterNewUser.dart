import 'dart:async';
import 'dart:ffi';
import 'dart:io';
import 'dart:ui';

import 'package:device_info/device_info.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:pin_code_text_field/pin_code_text_field.dart';
import 'package:userapp/Screens/LoginRegister/CreateAccount.dart';
import 'package:userapp/Getters/GetOtp.dart';
import 'package:userapp/Getters/GetUserAuth.dart';
import 'package:userapp/Screens/WebViewScreen.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../Components/CommonUtility.dart';
import 'LoginPage.dart';

class RegisterNewUser extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<RegisterNewUser> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  PersistentBottomSheetController bottomSheetController;
  bool _tnc = false, resendOtp = true;
  var numberController = TextEditingController(),
      otpController = TextEditingController();
  var _usernameError;
  var auth = new HttpRequests();
  OtpSend otpSend;
  Position _currentPosition;

  var passwordController = TextEditingController();
  var passwordController2 = TextEditingController();
  var referralController = TextEditingController();
  bool _obscureText1 = true, disablebtn = false;
  bool _obscureText2 = true, showReferralBox = false;
  String _passwordError;
  String _passwordError2;
  bool showPassField = false, _loading = false, disbaleButton = true;
  PermissionStatus permissionStatus;
  String userId, deviceUniqueId;
  VerifyOtp verifyOtp;
  int deviceType = 1;

  String phoneNo, smsCode, verificationId, fcmToken;
  DatabaseReference dbRef;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();

  ///////verifying OTP///////
  verifyOTP() async {
    var r = await auth.verifyOTP(smsCode, numberController.text);
    if (r['message'].toString().contains("Welcome to NeoMart")) {
      OtpVerified();
    }
  }

  //////////////Send OTP Functions/////////////
  sendOTP() {
    getReferral();
//    getLocation();
    getUserBypass();
  }

  getReferral() async {
    var r = await auth.getReferralCode(numberController.text);
    setState(() {
      if (r['status'].toString().contains("success")) {
        referralController.text = r['code'];
        showReferralBox = false;
      } else {
        showReferralBox = false;
      }
    });
  }

  validatePassword() {
    setState(() {
      if (passwordController.text.length < 8 ||
          passwordController.text == " ") {
        _passwordError = "Password must be minimum 8 characters!";
      } else if (passwordController.text != passwordController2.text) {
        _passwordError = "";
        _passwordError2 = "Password does not match!";
      } else {
        setState(() {
          _loading = true;
        });
        setPassword();
        print("Validation complete");
//        Navigator.of(context).pushAndRemoveUntil(
//            MaterialPageRoute(builder: (context) =>
//                CreateAccount()), (Route<dynamic> route) => false);

      }
    });
  }

  OtpVerified() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
        ),
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(
                  Icons.check_circle,
                  color: Colors.green,
                  size: 30,
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "OTP Verification Complete",
                  style: TextStyle(color: Colors.green),
                ),
                SizedBox(
                  height: 25,
                ),
                OutlineButton(
                    color: Colors.blue,
                    borderSide: BorderSide(color: mColor),
                    onPressed: () {
                      Navigator.pop(context);
                      setState(() {
                        showPassField = true;
                      });
                    },
                    child: Text(
                      "Proceed",
                      style: TextStyle(color: mColor),
                    ))
              ],
            ),
          );
        });
  }

  autoVerification() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(12.0)),
        ),
        builder: (context) {
          return Container(
            padding: EdgeInsets.all(15),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                CircularProgressIndicator(),
                SizedBox(
                  height: 10,
                ),
                Text("Auto verifying OTP")
              ],
            ),
          );
        });
  }

  openVerifyOTP() async {
    setState(() {
      disablebtn = true;
    });
    bottomSheetController =
        await _scaffoldKey.currentState.showBottomSheet((BuildContext context) {
      return Container(
//            padding: EdgeInsets.only(bottom: MediaQuery
//                .of(context)
//                .viewInsets
//                .bottom),
        child: Container(
          padding: EdgeInsets.all(15),
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text("Enter OTP"),
              SizedBox(
                height: 20,
              ),
              GestureDetector(
                onLongPress: () {
                  showDialog(
                      context: context,
                      builder: (_) {
                        return AlertDialog(
                          titlePadding: EdgeInsets.all(0.0),
                          title: ListTile(
                            onTap: () async {
                              ClipboardData data =
                                  await Clipboard.getData(Clipboard.kTextPlain);
                              print(data.text);
                              setState(() {
                                if (data.text.length > 4 || data.text.isEmpty) {
                                  Navigator.pop(context);
                                  Fluttertoast.showToast(msg: "Invalid OTP");
                                } else {
                                  Navigator.pop(context);
                                  otpController.text = data.text;
                                }
                              });
                            },
                            title: Text("Paste OTP"),
                          ),
                        );
                      });
                },
                child: Container(
                  alignment: Alignment.center,
                  child: PinCodeTextField(
                    controller: otpController,
                    onTextChanged: (value) {
                      smsCode = value;
                      print(smsCode);
                    },
                    pinBoxBorderWidth: 0,
                    pinBoxDecoration:
                        ProvidedPinBoxDecoration.underlinedPinBoxDecoration,
                    pinBoxRadius: 4,
                    wrapAlignment: WrapAlignment.center,
                    keyboardType: TextInputType.numberWithOptions(),
                    pinBoxHeight: 40,
                    maxLength: 4,
                    pinBoxWidth: 40,
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: FlatButton(
                      child: Text("Verify"),
                      onPressed: () {
                        print(smsCode);
                        getVerifyOtp();
//                            verifyOTP();
                      },
                      color: mColor,
                      textColor: Colors.white,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    flex: 1,
                    child: OutlineButton(
                      onPressed: resendOtp
                          ? () {
                              setState(() {
                                resendOtp = false;
                              });
                              Fluttertoast.showToast(
                                  msg: "Sending OTP...",
                                  gravity: ToastGravity.CENTER,
                                  toastLength: Toast.LENGTH_SHORT);
                              Navigator.pop(context);
                              smsCode = "";
                              otpController.text = "";
//                            sendOTP();
                              sendComm();
                              Timer(Duration(seconds: 15), () {
                                int s = 30;
                                bottomSheetController.setState(() {
                                  resendOtp = true;
                                });
                              });
                            }
                          : null,
                      textColor: mColor,
                      child: Text("Resend"),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      );
    });
  }

//   checkUser() async {
//     var r = await auth.getOtp(0, 0, numberController.text);
//     print(r.toString());
//     setState(() {
//       otpSend = OtpSend.fromJson(r);
//       if (otpSend.type.toString().contains("failed")) {
//         Flushbar(
//           message: "${otpSend.message.toString()}",
//           flushbarStyle: FlushbarStyle.GROUNDED,
//           flushbarPosition: FlushbarPosition.TOP,
//           showProgressIndicator: true,
//           icon: Icon(
//             Icons.insert_emoticon,
//             color: mColor,
//           ),
//           duration: Duration(seconds: 4),
//         )
//           ..show(context);
//       }
//       else {
//         setState(() {
//           disbaleButton = false;
//           _loading = true;
//         });
//         getLocation();
// //        getUserBypass();
// //        openBottomSheet();
// //        sendOTP();
// //        autoVerificationFailed();
//       }
//     });
//   }

  checkNumber() {
    if (numberController.text.length < 10 ||
        numberController.text.length > 10 ||
        numberController.text == " ") {
      setState(() {
        _usernameError = "Check your mobile number and try again";
      });
    } else if (int.parse(numberController.text) < 6000000000 ||
        int.parse(numberController.text) > 9999999999) {
      setState(() {
        _usernameError = "Please enter valid mobile number";
      });
    } else if (_tnc == false) {
      Flushbar(
        message: "Please agree to Terms & Conditions",
        duration: Duration(seconds: 2),
        icon: Icon(
          Icons.info_outline,
          color: Colors.white,
        ),
      )..show(context);
    } else {
      setState(() {
        disbaleButton = false;
        _loading = true;
      });
      getLocation();
    }
  }

  Future<String> _getId() async {
    print("getting device Id");
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      deviceType = 2;
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceType = 1;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  setPassword() async {
    print("setting password ");
    var r =
        await auth.updatePassword(verifyOtp.authkey, passwordController2.text);
    if (r['message'].toString().contains("Password Set Successfully")) {
      setState(() {
        _loading = false;
      });
      Flushbar(
        message: "Welcome to Bhadana Mart",
        flushbarStyle: FlushbarStyle.GROUNDED,
        flushbarPosition: FlushbarPosition.TOP,
        showProgressIndicator: true,
        icon: Icon(
          Icons.tag_faces,
          color: mColor,
        ),
        duration: Duration(seconds: 4),
      )..show(context);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => CreateAccount(verifyOtp.authkey.toString(),
                  numberController.text, passwordController2.text)),
          (Route<dynamic> route) => false);
    }
  }

  getVerifyOtp() async {
    print("verifying otp ");
    var r = await auth.verifyOtp(numberController.text, smsCode);
    verifyOtp = VerifyOtp.fromJson(r);
    print("verifyOtp: ${verifyOtp.authkey}");
    if (verifyOtp.status.toString().contains("success")) {
//      setPassword();
      Navigator.pop(context);
      setState(() {
        showPassField = true;
        disablebtn = false;
      });
    } else {
      Flushbar(
        messageText: Text(
          "OTP does not match!",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(seconds: 2),
      )..show(context);
    }
  }

  sendComm() async {
    print("sending comm");
    var r = await auth.sendComm(userId);
    if (r['message'].toString().contains("OTP sent successfully")) {
      setState(() {
        _loading = false;
      });
      openVerifyOTP();
    } else {
      setState(() {
        _loading = false;
      });
      Fluttertoast.showToast(
          msg: "Something went wrong. Please try again.",
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.TOP,
          timeInSecForIosWeb: 4);
    }
  }

  getUserToken() async {
    print("getting user token");
    print("FCMTOKEN --> $fcmToken");
    print("${userId} ||| ${deviceUniqueId}");
    var r = await auth.getToken(userId, deviceUniqueId, fcmToken, deviceType);
    print(r.toString());
    setState(() {
      if (r['message'].toString().contains("Successfully")) {
        print("token get complete");
        sendComm();
      }
    });
  }

  getUserBypass() async {
    print("get user ");
    var r = await auth.registeringUser(_currentPosition.latitude,
        _currentPosition.longitude, numberController.text);
    setState(() {
      otpSend = OtpSend.fromJson(r);
      if (otpSend.type.toString().contains("failed")) {
        Flushbar(
          message: "${otpSend.message.toString()}",
          flushbarStyle: FlushbarStyle.GROUNDED,
          flushbarPosition: FlushbarPosition.TOP,
          showProgressIndicator: true,
          icon: Icon(
            Icons.insert_emoticon,
            color: mColor,
          ),
          duration: Duration(seconds: 4),
        )..show(context);
        _loading = false;
      } else {
        if (r['type'].toString().contains("success")) {
          userId = r['userid'].toString();
          print("USERIDxx:: $userId");
        }
      }
    });
    dbRef.child(userId).child("fcmToken").set(fcmToken);
    dbRef
        .child(userId)
        .child("fcmToken")
        .once()
        .then((DataSnapshot data) => fcmToken = data.value);
    deviceUniqueId = await _getId();
    print("device id $deviceUniqueId");
    print("device type $deviceType");
    getUserToken();
  }

//  getUser() async {
//    print("get user ");
//    var r = await auth.registeringUser(
//        _currentPosition.latitude, _currentPosition.longitude,
//        numberController.text);
//    setState(() {
//      if (r['type'].toString().contains("success")) {
//        userId = r['userid'].toString();
//        print(userId);
//      }
//    });
//    dbRef.child(userId).child("fcmToken").set(fcmToken);
//    dbRef.child(userId).child("fcmToken").once().then((DataSnapshot data) =>
//    fcmToken = data.value);
//    deviceUniqueId = await _getId();
//    print("device id $deviceUniqueId");
//    print("device type $deviceType");
//    getUserToken();
//  }

  getLocation() {
    print("getting location");
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    try {
      geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .then((Position position) {
        setState(() {
          _currentPosition = position;
          print(
              "location ${_currentPosition.latitude}| ${_currentPosition.longitude}");
          getUserBypass();
//          getUser();
//          getFullAddress();
        });
      });
    } catch (e) {
      print("yess.s...");
      if (e.toString().contains("PERMISSION_DENIED")) {
//        showUpdateLocation();
        showLocationAccessModal();
      }
    }
  }

  askPermission() async {
    await Permission.locationWhenInUse.request();
    PermissionStatus status = await Permission.locationWhenInUse.status;
    await askPermissions(status, "location", Permission.locationWhenInUse);
  }

  // void onPermissionAsked(Map<PermissionGroup, PermissionStatus> value) {
  //   final status = value[PermissionGroup.locationWhenInUse];
  //   _updateStatus(status);
  // }

//   void _updateStatus(PermissionStatus status) {
// //    if (status != permissionStatus) {
// //      setState(() {
// //        permissionStatus = status;
// //        askPermission();
// //      });
// //    }
//     print("${status.toString()} permission-----");
//     if (status.toString().contains("PermissionStatus.unknown"))
//       askPermission();
//     else if (status.toString().contains("PermissionStatus.denied"))
//       showLocationAccessModal();
// //    else if (status.toString().contains("PermissionStatus.granted"))
// //      checkInternet();
//   }

  showLocationAccessModal() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        isScrollControlled: true,
        builder: (context) {
          return Container(
              padding: EdgeInsets.all(15),
//              height: MediaQuery
//                  .of(context)
//                  .size
//                  .height * .32,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Cant't Access your location",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                      "In order to proceed further you must provide location permission to application."),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: OutlineButton(
                          onPressed: () {
                            exit(0);
//                          Navigator.pop(context);
                          },
                          child: Text("Exit"),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        flex: 1,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            openAppSettings();
                          },
                          child: Text("Open Settings"),
                          textColor: Colors.white,
                          color: mColor,
                        ),
                      )
                    ],
                  )
                ],
              ));
        });
  }

//  checkPermission() {
//    print("permission check");
//    PermissionHandler().checkPermissionStatus(PermissionGroup.locationAlways)
//        .then(_updateStatus);
//    print(PermissionStatus);
//  }
//
//  askPermission() {
//    PermissionHandler().requestPermissions([PermissionGroup.locationWhenInUse])
//        .then(onPermissionAsked);
//  }
//
//  void onPermissionAsked(Map<PermissionGroup, PermissionStatus> value) {
//    final status = value[PermissionGroup.locationWhenInUse];
//    print(status);
//    if (status == PermissionStatus.denied) {
////    showLocationAccessModal();
//    } else {
//      _updateStatus(status);
//      getLocation();
//    }
//  }
//
//  void _updateStatus(PermissionStatus status) {
//    if (status != permissionStatus)
//      setState(() {
//        permissionStatus = status;
//        askPermission();
//      });
//  }

  Widget buildPasswordField() {
    return Column(
      children: <Widget>[
        TextFormField(
            maxLengthEnforced: true,
            maxLength: 25,
            controller: passwordController,
            obscureText: _obscureText1,
            decoration: InputDecoration(
                counterText: "",
                suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      if (_obscureText1)
                        _obscureText1 = false;
                      else
                        _obscureText1 = true;
                      print(_obscureText1);
                    });
                  },
                  icon: Icon(
                    Icons.remove_red_eye,
                    color: !_obscureText1 ? mColor : Colors.grey,
                  ),
                ),
                errorText: _passwordError,
                labelStyle: TextStyle(color: Colors.grey),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 1.0,
                        style: BorderStyle.solid)),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey)),
                labelText: "New Password")),
        SizedBox(
          height: 15,
        ),
        TextFormField(
            maxLengthEnforced: true,
            maxLength: 25,
            controller: passwordController2,
            obscureText: _obscureText2,
            decoration: InputDecoration(
                counterText: "",
                suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      if (_obscureText2)
                        _obscureText2 = false;
                      else
                        _obscureText2 = true;
                      print(_obscureText2);
                    });
                  },
                  icon: Icon(
                    Icons.remove_red_eye,
                    color: !_obscureText2 ? mColor : Colors.grey,
                  ),
                ),
                errorText: _passwordError2,
                labelStyle: TextStyle(color: Colors.grey),
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.grey,
                        width: 1.0,
                        style: BorderStyle.solid)),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey)),
                labelText: "Confirm Password")),
      ],
    );
  }

  Widget buildOtpContainer() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          PinCodeTextField(
            wrapAlignment: WrapAlignment.center,
            textDirection: TextDirection.ltr,
            pinBoxBorderWidth: 1,
            pinBoxRadius: 6,
            pinBoxWidth: 40,
            maxLength: 6,
            pinBoxHeight: 40,
            keyboardType: TextInputType.numberWithOptions(),
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: FlatButton(
                  child: Text("Verify"),
                  onPressed: () {},
                  color: mColor,
                  textColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 15,
              ),
              Expanded(
                flex: 1,
                child: OutlineButton(
                  onPressed: () {},
                  textColor: mColor,
                  child: Text("Resend"),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    askPermission();
//    getLocation();
    print("InitState");
    dbRef = FirebaseDatabase.instance.reference().child("registeredUser");
    firebaseMessaging.getToken().then((value) {
      fcmToken = value;
      print("fcmToken -> $fcmToken");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white,
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => LoginPage()),
                (Route<dynamic> route) => false);
          },
        ),
        title: Text(
          "Register",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(left: 15, right: 15),
                  child: Column(
                    children: <Widget>[
                      SizedBox(
                        height: 50,
                      ),
                      Hero(
                        tag: 'neomart_logo',
                        child: Image(
                          image:
                              AssetImage('drawables/logo_neo_mart_splash.png'),
                            height: 100,
                            width:100                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),

                     Container(child: Text("Welcome to Bhadana Mart",style: TextStyle(color: mColor,fontSize: 14,fontWeight: FontWeight.bold),)),
                      SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        enabled: disbaleButton,
                        keyboardType: TextInputType.number,
                        maxLength: 10,
                        onChanged: (value) {
                          if (value.length == 10) {
                            FocusScope.of(context).requestFocus(FocusNode());
                          }
                        },
                        maxLengthEnforced: true,
                        inputFormatters: [
                          WhitelistingTextInputFormatter(RegExp("[0-9]"))
                        ],
                        decoration: InputDecoration(
                            enabled: disbaleButton,
                            counterText: "",
                            errorText: _usernameError,
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey,
                                    width: 1.0,
                                    style: BorderStyle.solid)),
                            hintText: "000 000 0000",
                            border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey,
                                    width: 1.0,
                                    style: BorderStyle.solid)),
                            labelText: "Mobile Number",
                            hasFloatingPlaceholder: true,
                            labelStyle: TextStyle(color: Colors.grey)),
                        controller: numberController,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      // showReferralBox
                      //     ? TextFormField(
                      //         enabled: false,
                      //         controller: referralController,
                      //         obscureText: false,
                      //         maxLength: 25,
                      //         maxLengthEnforced: true,
                      //         decoration: InputDecoration(
                      //             labelStyle: TextStyle(color: Colors.grey),
                      //             focusedBorder: OutlineInputBorder(
                      //                 borderSide: BorderSide(
                      //                     color: Colors.grey,
                      //                     width: 1.0,
                      //                     style: BorderStyle.solid)),
                      //             border: OutlineInputBorder(
                      //                 borderSide:
                      //                     BorderSide(color: Colors.grey)),
                      //             labelText: "Referral Code"))
                      //     : SizedBox(),
                      showPassField ? buildPasswordField() : SizedBox(),
                      SizedBox(
                        height: 15,
                      ),
                      Container(
                        child: Text(
                          "* To recieve cashbacks, Register through your paytm mobile number.",
                          textAlign: TextAlign.center,
                        ),
                        padding: EdgeInsets.all(8.0),
                      ),
                      showPassField
                          ? SizedBox()
                          : CheckboxListTile(
                              dense: true,
                              value: _tnc,
                              title: Row(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  TermsAndConditions()));
                                    },
                                    child: Text(
                                      "Terms & Conditions ",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 11),
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PrivacyPolicy()));
                                    },
                                    child: Text(
                                      "& Privacy Policy",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 11),
                                    ),
                                  ),
                                ],
                              ),
                              onChanged: (bool val) {
                                setState(() {
                                  if (_tnc)
                                    _tnc = false;
                                  else
                                    _tnc = true;
                                });
                              },
                              checkColor: Colors.white,
                              activeColor: mColor,
                            ),
                      SizedBox(
                        height: 15,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Visibility(
            visible: _loading,
            child: BackdropFilter(
              filter: ImageFilter.blur(
                sigmaX: 5.0,
                sigmaY: 5.0,
              ),
              child: Center(
                  child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 10,
                    ),
                    showPassField ? SizedBox() : Text("Sending OTP...")
                  ],
                ),
              )),
            ),
          )
        ],
      ),
      bottomNavigationBar: disablebtn
          ? SizedBox()
          : BottomAppBar(
              child: SafeArea(
                child: Hero(
                  tag: 'register_button',
                  child: ButtonTheme(
                    minWidth: double.infinity,
                    height: 50.0,
                    child: IgnorePointer(
                      ignoring: _loading,
                      child: RaisedButton(
                        child:
                            showPassField ? Text("Register") : Text("Get OTP"),
                        onPressed: () {
                          print(showPassField);
//                openBottomSheet();
                          if (!showPassField)
                            checkNumber();
                          else {
                            validatePassword();
                          }
                        },
                        textColor: Colors.white,
                        color: mColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
