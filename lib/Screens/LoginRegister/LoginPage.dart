import 'dart:ui';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Screens/LoginRegister/ForgotPassword.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/MainApp.dart';
import 'package:userapp/Screens/LoginRegister/RegisterNewUser.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../constants.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  DatabaseReference dbRef;

//  var mAccentColor = Color(0xFFf82d70);
  User user = User();
  bool disableEverything = true;

//  bool rememberMe = false;

  var auth = new HttpRequests();
  bool _loading = false;
  String token, _usernameError, _passwordError;
  var data;

  String number, password;
  final numberController = TextEditingController();
  final passwordController = TextEditingController();

//  List permission;
//  PermissionName permissionName;

  bool _obscureText = true;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  String fcmToken, deviceUniqueId;
  int deviceType = 1;
  StreamVars streamVars = Get.put(StreamVars());

//  getPermission() async{
//    await Permission.requestPermissions([PermissionName.Internet, PermissionName.Calendar]);
//  }
//
  @override
  initState() {
    super.initState();
//    getPermission();
    _loading = false;
    dbRef = FirebaseDatabase.instance.reference().child("registeredUser");
    firebaseMessaging.getToken().then((value) {
      fcmToken = value;
      print("fcmToken -> $fcmToken");
    });
  }

  Future<String> _getId() async {
    print("getting device Id");
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      deviceType = 2;
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceType = 1;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  _validate() {
    setState(() {
      _loading = true;
      disableEverything = false;
      if (numberController.text.length < 10 ||
          numberController.text.length > 10 ||
          numberController.text == " ") {
        _usernameError = "Check your mobile number";
        _loading = false;
        disableEverything = true;
      } else if (int.parse(numberController.text) < 6000000000 ||
          int.parse(numberController.text) > 9999999999) {
        _usernameError = "Please enter valid mobile number";
        _loading = false;
        disableEverything = true;
      } else if (passwordController.text.length < 8 ||
          passwordController.text == " ") {
        _passwordError = "Password must be minimum 8 characters!";
        _loading = false;
        disableEverything = true;
      } else {
        getAuth();
      }
    });
  }

  checkInternet() async {
//    CheckInternet checkInternet = CheckInternet();
//    DataConnectionStatus status = await CheckInternet(context).checkInternet();
//    if (status == DataConnectionStatus.connected) {
//      print("yes");
    _validate();
//    } else {
//      Flushbar(
//        title: "No Internet",
//        duration: Duration(seconds: 3),
//        messageText: Text(
//          "Check your Internet connection & try again.",
//          style: TextStyle(color: Colors.white),
//        ),
//        icon: Icon(Icons.signal_wifi_off, color:  mColor),
//        isDismissible: true,
//        leftBarIndicatorColor:  mColor,
//      )
//        ..show(context);
//    }
  }

//  NotificationHandler notificationHandler;

  getAuth() async {
    if (streamVars.noInternet.value) {
      showToast("No Internet",  mColor);
      setState(() {
        _loading = false;
        disableEverything = true;
      });
    } else {
      deviceUniqueId = await _getId();
      deviceType = 3;
      print("device type $deviceType");
      var r =
          await auth.LoginAuth(numberController.text, passwordController.text)
              .timeout(Duration(seconds: 10), onTimeout: () {
        setState(() {
          disableEverything = true;
          _loading = false;
        });
        Flushbar(
          titleText: Text(
            "Error",
            style: TextStyle(color:  mColor),
          ),
          message: "Connection Timeout",
          flushbarPosition: FlushbarPosition.TOP,
          leftBarIndicatorColor:  mColor,
          icon: Icon(
            Icons.not_interested,
            color:  mColor,
          ),
          duration: Duration(seconds: 3),
          flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      });
      SharedPreferences preferences = await SharedPreferences.getInstance();
      user = User.fromJson(r);
      if (user.meta.message.toString().contains("Successfully Login!") ||
          user.meta.status.toString().contains("success")) {
        userDetails = user;
        USER_DETAILES = user;
        USER_ID = USER_DETAILES.data.loginDetails.userId.toString();
        TOKEN = user.data.loginDetails.authKey;
        print(
            "success ${USER_DETAILES.data.loginDetails.lat} | ${USER_DETAILES.data.loginDetails.lng}");
        print("userID ${user.data.loginDetails.userId}");
        preferences?.setBool("isLogin", true);
        preferences?.setString("username", numberController.text);
        preferences?.setString("password", passwordController.text);
        preferences?.setString("showIntro", 'NO');

        dbRef
            .child(user.data.loginDetails.userId.toString())
            .child("fcmToken")
            .set(fcmToken);
        dbRef
            .child(user.data.loginDetails.userId.toString())
            .child("fcmToken")
            .once()
            .then((DataSnapshot data) => fcmToken = data.value);

        print("FCMTOKEN --> $fcmToken");
        await auth
            .getToken(user.data.loginDetails.userId, deviceUniqueId, fcmToken,
                deviceType)
            .then((value) => print(value.toString()));
//      notificationHandler = NotificationHandler(context, user)..setupFirebaseNotification();

        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => MainApp()));
      } else {
        setState(() {
          _usernameError = "";
          _passwordError = "";
          _loading = false;
          disableEverything = true;
        });
        Flushbar(
          titleText: Text(
            "Login Failed",
            style: TextStyle(color:  mColor),
          ),
          message: "Invalid credentials",
          flushbarPosition: FlushbarPosition.TOP,
          leftBarIndicatorColor:  mColor,
          icon: Icon(
            Icons.info_outline,
            color:  mColor,
          ),
          duration: Duration(seconds: 2),
          flushbarStyle: FlushbarStyle.FLOATING,
        )..show(context);
      }
      if (data != null)
        setState(() {
          _loading = false;
        });
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    passwordController.dispose();
    numberController.dispose();
    _loading = false;
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.dark));

    BoxDecoration _boxGradient = BoxDecoration(
        gradient: LinearGradient(
      colors: [mColor, mColor],
      begin: FractionalOffset(0.0, 0.2),
      end: FractionalOffset(0.8, 0.0),
      stops: [0.0, 1.0],
    ));

    return Stack(
      children: <Widget>[
        Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0.0,
            backgroundColor: Colors.transparent,
//        leading: IconButton(
//          icon: Icon(Icons.arrow_back_ios, color: Colors.black),
//          onPressed: () {
//            Navigator.pop(context);
//          },
//        ),
//        title: Text(
//          "Login",
//          style: TextStyle(color: Colors.black),
//        ),
//        centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Hero(
                          transitionOnUserGestures: true,
                          tag: 'neomart_logo',
                          child: Image(
                              height: 100,
                              width:100,
                            image: AssetImage(
                                'drawables/logo_neo_mart_splash.png'),
                          )),
                    ),

                    Container(child: Text("Welcome to Bhadana Mart",style: TextStyle(color: mColor,fontSize: 14,fontWeight: FontWeight.bold),)),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            enabled: disableEverything,
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              WhitelistingTextInputFormatter(RegExp("[0-9]"))
                            ],
                            maxLengthEnforced: true,
                            maxLength: 10,
                            onChanged: (value) {
                              if (value.length == 10) {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                              }
                            },
                            decoration: InputDecoration(
                                counterText: "",
//                              prefixIcon: Icon(Icons.phone_iphone),
                                errorText: _usernameError,
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.grey,
                                        width: 1.0,
                                        style: BorderStyle.solid)),
                                hintText: "000 000 0000",
                                border: OutlineInputBorder(
                                    borderSide: BorderSide(
                                        color: Colors.grey,
                                        width: 1.0,
                                        style: BorderStyle.solid)),
                                labelText: "Enter Mobile Number",
                                hasFloatingPlaceholder: true,
                                labelStyle: TextStyle(color: Colors.grey)),
                            controller: numberController,
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          TextFormField(
                              enabled: disableEverything,
                              controller: passwordController,
                              obscureText: _obscureText,
                              maxLength: 25,
                              maxLengthEnforced: true,
                              decoration: InputDecoration(
                                  counterText: "",
                                  suffixIcon: IconButton(
                                    onPressed: disableEverything == false
                                        ? null
                                        : () {
                                            setState(() {
                                              if (_obscureText)
                                                _obscureText = false;
                                              else
                                                _obscureText = true;
                                              print(_obscureText);
                                            });
                                          },
                                    icon: Icon(
                                      Icons.remove_red_eye,
                                      color:
                                          !_obscureText ? mColor : Colors.grey,
                                    ),
                                  ),
//                                    prefixIcon: Icon(Icons.lock_outline),
                                  errorText: _passwordError,
                                  labelStyle: TextStyle(color: Colors.grey),
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.grey,
                                          width: 1.0,
                                          style: BorderStyle.solid)),
                                  border: OutlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Colors.grey)),
                                  labelText: "Password")),
                          SizedBox(
                            height: 15,
                          ),

//                                Container(
//                                  child: Row(children: <Widget>[
//                                    Checkbox(
//                                      onChanged: (val){rememberMe = val;},
//                                      value: rememberMe,
//                                    ),
//                                    Text("Remember me")
//                                  ],),
//                                ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Expanded(
                                  flex: 1,
                                  child: Container(
                                      alignment: Alignment.centerLeft,
                                      child: GestureDetector(
                                        onTap: disableEverything == false
                                            ? null
                                            : () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            ForgotPassword()));
                                              },
                                        child: Text(
                                          "Forgot Password?",
                                          style: TextStyle(
                                            color: mColor,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ))),
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(16.0),
                            child: Hero(
                              tag: 'login_button',
                              child: ButtonTheme(
                                minWidth: double.infinity,
                                height: 50.0,
                                child: RaisedButton(
                                  child: Text("LOGIN"),
                                  onPressed: disableEverything == false
                                      ? null
                                      : () {
                                          setState(() {
                                            checkInternet();
                                            print(
                                                "${passwordController.text} || ${numberController.text}");
                                          });
                                        },
                                  color: Colors.white,
                                  textColor: mColor,
                                  elevation: 8.0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(27)),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          bottomNavigationBar: BottomAppBar(
            child: Container(
              child: Hero(
                tag: 'register_button',
                child: ButtonTheme(
                  minWidth: double.infinity,
                  height: 50.0,
                  child: RaisedButton(
                    child: Text("Register"),
                    onPressed: disableEverything == false
                        ? null
                        : () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => RegisterNewUser()));
                          },
                    elevation: 8.0,
                    textColor: Colors.white,
                    color: mColor,
                  ),
                ),
              ),
            ),
          ),
        ),
        Visibility(
          visible: _loading,
          child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: 5.0,
              sigmaY: 5.0,
            ),
            child: neoLoader(),
          ),
        )
      ],
    );
  }
}
