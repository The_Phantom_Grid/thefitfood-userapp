import 'dart:io';
import 'dart:convert';

import 'package:device_info/device_info.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetProfile.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Getters/GetUserAuth.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../constants.dart';
import '../MainApp.dart';
import '../SearchLocation.dart';
import 'LoginPage.dart';

enum updateStatus { NOT_UPDATING, UPDATING, DONE }

class CreateAccount extends StatefulWidget {
  String token, mobile, password;

  CreateAccount(this.token, this.mobile, this.password);

  @override
  _CreateAccountState createState() => _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {
  var auth = HttpRequests();
  ProfileData profileData;
  Profile profile;
  File image;
  String profileImage, fullName, eMail, locality;
  var nameController = TextEditingController();
  var emailController = TextEditingController();
  var localityController = TextEditingController();
  String nameError, emailError, addressError;
  var status;
  bool loading = false, disable = false;
  Position _currentPosition;
  List<Address> addresses;
  var first, lat, lng;

//
//  getProfileData() async {
//    var r = await auth.getProfile(widget.user.data.loginDetails.authKey);
//    setState(() {
//      profile = Profile.fromJson(r);
//      profileData = profile.prifiledata[0];
//      initValues();
////      profileData = profile.prifiledata[0];
//    });
//  }
//
//  initValues() {
//    nameController.text = profileData.firstname;
//    emailController.text = profileData.email;
//    localityController.text = profileData.address;
//    profileImage = "$baseUrl${profileData.avatarPath}";
//  }

  String fcmToken, deviceUniqueId;
  int deviceType = 1;
  DatabaseReference dbRef;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  User user;

  Future<String> _getId() async {
    print("getting device Id");
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      deviceType = 2;
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceType = 1;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  getAuth() async {
    deviceUniqueId = await _getId();
    deviceType = 3;
    print("device type $deviceType");
    var r = await auth.LoginAuth(widget.mobile, widget.password)
        .timeout(Duration(seconds: 15), onTimeout: () {
      Flushbar(
        titleText: Text(
          "Error",
          style: TextStyle(color:  mColor),
        ),
        message: "Connection Timeout",
        flushbarPosition: FlushbarPosition.TOP,
        leftBarIndicatorColor:  mColor,
        icon: Icon(
          Icons.not_interested,
          color:  mColor,
        ),
        duration: Duration(seconds: 3),
        flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    });
    SharedPreferences preferences = await SharedPreferences.getInstance();
    user = User.fromJson(r);
    if (user.meta.message.toString().contains("Successfully Login!") ||
        user.meta.status.toString().contains("success")) {
      userDetails = user;
      USER_DETAILES = user;
      USER_ID = USER_DETAILES.data.loginDetails.userId.toString();
      TOKEN = user.data.loginDetails.authKey;
      print("success ${user.meta.message}");
      print("userID ${user.data.loginDetails.userId}");
      preferences?.setBool("isLogin", true);
      preferences?.setString("username", widget.mobile);
      preferences?.setString("password", widget.password);
      preferences?.setString("showIntro", 'NO');

      dbRef
          .child(user.data.loginDetails.userId.toString())
          .child("fcmToken")
          .set(fcmToken);
      dbRef
          .child(user.data.loginDetails.userId.toString())
          .child("fcmToken")
          .once()
          .then((DataSnapshot data) => fcmToken = data.value);

      print("FCMTOKEN --> $fcmToken");
      await auth
          .getToken(user.data.loginDetails.userId, deviceUniqueId, fcmToken,
              deviceType)
          .then((value) => print(value.toString()));
//      notificationHandler = NotificationHandler(context, user)..setupFirebaseNotification();

      Get.offAll(MainApp());
    } else {
      Flushbar(
        titleText: Text(
          "Login Failed",
          style: TextStyle(color:  mColor),
        ),
        message: "Invalid credentials",
        flushbarPosition: FlushbarPosition.TOP,
        leftBarIndicatorColor:  mColor,
        icon: Icon(
          Icons.info_outline,
          color:  mColor,
        ),
        duration: Duration(seconds: 2),
        flushbarStyle: FlushbarStyle.FLOATING,
      )..show(context);
    }
  }

  getImageFromCamera() async {
    var i = await ImagePicker.pickImage(source: ImageSource.camera);
    i = await ImageCropper.cropImage(
        sourcePath: i.path,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 0,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: mColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));
    setState(() {
      if (i != null) image = i;
    });
  }

  getImageFromGallery() async {
    var i = await ImagePicker.pickImage(source: ImageSource.gallery);
    i = await ImageCropper.cropImage(
        sourcePath: i.path,
        compressFormat: ImageCompressFormat.jpg,
        compressQuality: 0,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: mColor,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        ));
    setState(() {
      if (i != null) image = i;
    });
  }

  updateProfile() async {
    setState(() {
      loading = true;
    });
    print("updating...");
    String base64Image;
    if (image != null) {
      setState(() {
        List<int> imageBytes = image.readAsBytesSync();
        base64Image = Base64Encoder().convert(imageBytes);
        status = updateStatus.UPDATING;
        print(base64Image);
      });
    } else {
      base64Image = null;
    }
    setState(() {
      status = updateStatus.UPDATING;
    });
    await auth.updateLocation(lat, lng, widget.token);
    var r = await auth.updateProfile(widget.token, localityController.text,
        emailController.text, base64Image, nameController.text);
    setState(() {
      if (r != null && r['status'].toString().toLowerCase() == "success") {
        status = updateStatus.DONE;
        Flushbar(
          message: "${r['message']}",
          duration: Duration(seconds: 2),
        )..show(context);
        loading = false;
        getAuth();
        // Navigator.of(context).pushAndRemoveUntil(
        //     MaterialPageRoute(builder: (context) => LoginPage()),
        //     (Route<dynamic> route) => false);
      }
    });
  }

  validateInfo() {
    setState(() {
      print("Validating...");
      if (nameController.text.isEmpty)
        nameError = "Name Field cannot be empty!";
      else if (!EmailValidator.validate(emailController.text))
        emailError = "Email address is not valid";
      else if (localityController.text.isEmpty)
        addressError = "Enter your location!";
      else
        updateProfile();
    });
  }

  openBottomSheet() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(10.0)),
        ),
        builder: (context) {
          return Container(
              height: MediaQuery.of(context).size.height * .2,
              width: MediaQuery.of(context).size.width,
              child: Column(
                children: <Widget>[
                  ListTile(
                    onTap: () {
                      getImageFromCamera();
                      Navigator.pop(context);
                    },
                    leading: Icon(Icons.camera_alt),
                    title: Text("Camera"),
                  ),
                  ListTile(
                    onTap: () {
                      getImageFromGallery();
                      Navigator.pop(context);
                    },
                    leading: Icon(Icons.photo_library),
                    title: Text("Gallery"),
                  ),
                ],
              ));
        });
  }

  getFullAddress() async {
    final coordinates = Coordinates(lat, lng);
    addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      first = addresses.first;
      print(addresses.length);
      print("${first.addressLine}");

      print(
          ' ${first.addressLine}, ||||  ${first.adminArea}, |||| ${first.subLocality},  |||| ${first.subAdminArea}, |||| ${first.locality},  |||| ${first.featureName}, |||| ${first.thoroughfare}, ||||  ${first.subThoroughfare} |||| ${first.countryName} |||| ${first.countryCode} |||| ${first.subAdminArea} |||| ${first.postalCode} |||| ');

      localityController.text = "${first.subAdminArea}".replaceAll('null', "");
      disable = false;
    });
  }

  getLocation() {
    setState(() {
      disable = true;
    });
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
        lat = _currentPosition.latitude;
        lng = _currentPosition.longitude;
        getFullAddress();
      });
    }).catchError((e) {
      print(e);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    dbRef = FirebaseDatabase.instance.reference().child("registeredUser");
    firebaseMessaging.getToken().then((value) {
      fcmToken = value;
      print("fcmToken -> $fcmToken");
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {},
      child: Scaffold(
          appBar: AppBar(
            title: Text(
              "Create Account",
              style: TextStyle(color: Colors.white),
            ),
            automaticallyImplyLeading: false,
            backgroundColor: mColor,
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Center(
                child: Container(
              child: Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Column(
                        children: <Widget>[
                          Stack(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(color: mColor)),
                                width: 200,
                                height: 200,
                                child: ClipOval(
                                  child: Hero(
                                      tag: 'neomart_logo',
                                      child: image != null
                                          ? Image.file(
                                              image,
                                              fit: BoxFit.cover,
                                            )
                                          : Icon(
                                              Icons.account_circle,
                                              size: 200,
                                              color: Colors.grey,
                                            )),
                                ),
                              ),
                              Positioned(
                                bottom: 0,
                                right: 15,
                                child: GestureDetector(
                                  onTap: openBottomSheet,
                                  child: CircleAvatar(
                                    radius: 25,
                                    child: Icon(Icons.camera_alt),
                                  ),
                                ),
                              )
                            ],
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          TextFormField(
                            controller: nameController,
//                                    "${profile.prifiledata[0].firstname}",
                            enabled: true,
                            maxLength: 30,
                            decoration: InputDecoration(
                              counterText: "",
                              errorText: nameError,
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                              labelText: "Name",
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          TextFormField(
//                                initialValue: "${profile.prifiledata[0].email}",
                            enabled: true,
                            maxLength: 30,
                            controller: emailController,
                            decoration: InputDecoration(
                              counterText: "",
                              errorText: emailError,
                              isDense: true,
                              hasFloatingPlaceholder: true,
                              border: OutlineInputBorder(
                                  borderSide:
                                      BorderSide(color: mColor, width: 5.0)),
                              labelText: "E-mail address",
                            ),
                          ),
                          SizedBox(
                            height: 25,
                          ),
                          TextFormField(
                            controller: localityController,
                            enabled: true,
                            maxLength: 30,
                            onChanged: (val) {
                              setState(() {
                                localityController.text = locality;
                              });
                            },
                            onTap: () async {
                              var loc = await Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          SearchLocation(widget.token, true)));
                              setState(() {
                                locality = loc[0];
                                localityController.text = locality;
                                lat = loc[2];
                                lng = loc[3];
                                disable = false;
                              });
                            },
                            decoration: InputDecoration(
                                counterText: "",
                                suffixIcon: Icon(
//                                   onPressed: () async {
// //                                  getLocation();
//                                     var loc = await Navigator.push(context,
//                                         MaterialPageRoute(builder: (context) =>
//                                             SearchLocation(
//                                                 widget.token, true)));
//                                     setState(() {
//                                       locality = loc[0];
//                                       localityController.text = locality;
//                                       lat = loc[2];
//                                       lng = loc[3];
//                                       disable = false;
//                                     });
//                                   },
                                  Icons.add_location, color: mColor,
                                  size: 26,
                                ),
                                errorText: addressError,
                                isDense: true,
                                hasFloatingPlaceholder: true,
                                border: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: mColor, width: 5.0)),
                                labelText: "Your Location",
                                hintText: "Tap to get location",
                                enabled: disable ? false : true),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: ButtonTheme(
                          minWidth: double.infinity,
                          height: 50.0,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(27)),
                          child: RaisedButton(
                            child: loading
                                ? Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      Container(
                                        height: 15,
                                        width: 15,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2,
                                          backgroundColor: Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text("Updating Profile..."),
                                    ],
                                  )
                                : Text("Save"),
                            onPressed: loading
                                ? null
                                : () {
                                    validateInfo();
                                  },
                            textColor: Colors.white,
                            color: mColor,
                          )),
                    )
                  ],
                ),
              ),
            )),
          )),
    );
  }
}
