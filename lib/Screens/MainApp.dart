import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:http/http.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Components/ProductSearch.dart';
import 'package:userapp/Components/VariableController.dart';
import 'package:userapp/Network/NotificationHandler.dart';
import 'package:userapp/Screens/AccountInformation.dart';
import 'package:userapp/Components/Chats.dart';
import 'package:userapp/Network/CheckInternetConnection.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/Home/Dashboard.dart';
import 'package:userapp/Screens/Home/HomeScreen.dart';
import 'package:userapp/Screens/Home/SearchPage.dart';
import 'package:userapp/Screens/LoginRegister/LoginPage.dart';
import 'package:userapp/Screens/MyAddress.dart';
import 'package:userapp/Screens/MyCart.dart';
import 'package:userapp/Screens/MyOrders.dart';
import 'package:userapp/Screens/Home/NearMe.dart';
import 'package:userapp/Screens/NotificationCenter.dart';
import 'package:userapp/Screens/Offers/OfferScreen.dart';
import 'package:userapp/Screens/OrderDetail.dart';
import 'package:userapp/Screens/ReferralPage.dart';
import 'package:userapp/Screens/SearchScreen.dart';
import 'package:userapp/Screens/WebViewScreen.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:userapp/checkout.dart';

import '../constants.dart';
import 'Home/Favourites.dart';
import '../Getters/GetNotification.dart';
import '../Getters/GetProfile.dart';
import 'Home/Products.dart';
import 'SearchLocation.dart';

_MainAppState mainAppState;

class MainApp extends StatefulWidget {
  @override
  _MainAppState createState() {
    mainAppState = _MainAppState();
    return mainAppState;
  }
}

class _MainAppState extends State<MainApp> with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  var currentTab = 1;
  bool noInternet = false;
  Position _currentPosition;
  List<Address> addresses, a2;
  double lat = 0.0, lng = 0.0;
  Address first, current;
  String full_address, username, email, packageVersion;
  PermissionStatus permissionStatus;
  SharedPreferences preferences;

  // Timer timer;

//  var user = User();
  int shoppingListCount = 0;
  var auth = HttpRequests();
  TotalNotification totalNotification;
  int notifications;
  int appVersion;
  UserNotification userNotification;
  ProfileData profileData;
  Profile profile;
  String cLocation, profileImage, postalCode;
  bool reLocation = false;
  var pageController = PageController(initialPage: 0);

  DynamicVar v = Get.put(DynamicVar());

//  String deviceId = DeviceInfoPlugin.channel.name;

//  Future<String> _getId(BuildContext context) async {
//    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
//    if (Theme
//        .of(context)
//        .platform == TargetPlatform.iOS) {
//      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
//      print("${iosDeviceInfo.identifierForVendor} ------device ID");
//      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
//    } else {
//      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
//      setState(() {
//        deviceId = androidDeviceInfo.androidId;
//      });
//      print("${androidDeviceInfo.androidId} ------device ID");
//      return androidDeviceInfo.androidId; // unique ID on Android
//    }
//  }

  _getPackageInfo() async {
    int version;
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    packageVersion = packageInfo.version.toString();
    version = int.parse(packageInfo.version.toString().replaceAll(".", ""));

    return version;
  }

  getNotifications() async {
    int currentVersion = await _getPackageInfo();
    print("version : $currentVersion");
    var r = await auth
        .getUnreadNotifications(USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      totalNotification = TotalNotification.fromJson(r);
      print("tNOT: ${totalNotification.message}");
      if (totalNotification.data != null)
        notifications = totalNotification.data[0].totalMessages;
      else
        notifications = 0;

      if (Platform.isAndroid) {
        appVersion = int.tryParse(
            totalNotification.version.toString().replaceAll(".", ""));
        // if (currentVersion < appVersion) {
        //   showUpdateDialogue();
        // }
        print(totalNotification.version.toString());
      } else if (Platform.isIOS) {
        appVersion = int.tryParse(
            totalNotification.iosVersion.toString().replaceAll(".", ""));
        print("APP VER: $appVersion");
        print("CUR VER: $currentVersion");
        // if (currentVersion < appVersion) {
        //   showUpdateDialogue();
        // }
        print("c v: $currentVersion");
        print(totalNotification.iosVersion.toString());
      }
    });
  }

  showUpdateDialogue() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return Platform.isAndroid
              ? AlertDialog(
                  insetPadding: EdgeInsets.all(30),
                  contentPadding:
                      EdgeInsets.only(left: 25, right: 25, bottom: 5, top: 25),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8)),
                  content: Text(
                      "There is a newer version of app available please update it now."),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("Update"),
                      onPressed: () {
                        UrlLauncher.launch(
                            "https://play.google.com/store/apps/details?id=com.nukkadse.userapp");
                        Navigator.pop(context);
                      },
                    ),
//                    FlatButton(
//                      child: Text("Close"),
//                      onPressed: () {
//                        Navigator.pop(context);
//                      },
//                    )
                  ],
                )
              : CupertinoAlertDialog(
                  title: Text("Info"),
                  content: Text(
                      "There is a newer version of app available please update it now."),
                  actions: <Widget>[
                    CupertinoDialogAction(
                      child: Text("Update"),
                      onPressed: () {
                        UrlLauncher.launch(
                            "https://apps.apple.com/us/app/id1519575784");
                        Navigator.pop(context);
                      },
                    ),
//                    CupertinoDialogAction(
////              color: Colors.blue,
//                      child: Text("Close"),
//                      onPressed: () {
//                        Navigator.pop(context);
//                      },
//                    )
                  ],
                );
        });
  }

  getProfileData() async {
    var r = await auth.getProfile(USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      profile = Profile.fromJson(r);
      profileData = profile.prifiledata[0];
      print(profile.prifiledata[0].id);
      print(profile.status);
      print(profileData.username);
      print(profileData.email);
      print(profileData.address);
      print(profileData.avatarPath);
      if (profileData.email != null &&
          profileData.address != "" &&
          profileData.avatarPath != null) {
        profileData = profile.prifiledata[0];
        username = profileData.firstname;
        email = profileData.email;
        if (profileData.avatarPath.contains('jpg') ||
            profileData.avatarPath.contains('jpeg') ||
            profileData.avatarPath.contains('png'))
          profileImage =
              "${profileData.avatarBaseUrl}${profileData.avatarPath}";
        else
          profileImage = null;
        print("hitt....22. $profileImage");
        print("token ======== ${USER_DETAILES.data.loginDetails.authKey}");
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    AccountInformation(USER_DETAILES, "NewUser")));
      }
    });
  }

  showUpdateLocation() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        isScrollControlled: true,
        builder: (context) {
          return Container(
              padding: EdgeInsets.all(15),
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Change Location",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 15,
                  ),
                  RichText(
                      text: TextSpan(children: <TextSpan>[
                    TextSpan(
                      text: "We have detected that you are in ",
                      style: TextStyle(color: Colors.black),
                    ),
                    TextSpan(
                      text: "${first.subLocality} - ${first.postalCode}.",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                    TextSpan(
                      text: " Do you wish to change your selected location?",
                      style: TextStyle(color: Colors.black),
                    ),
                  ])),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: OutlineButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Cancel"),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        flex: 1,
                        child: FlatButton(
                          onPressed: () {
                            Navigator.pop(context);
                            setState(() {
                              cLocation = first.subLocality;
                              postalCode = first.postalCode;
                            });
                            setLocation(_currentPosition.latitude.toString(),
                                _currentPosition.longitude.toString());
                          },
                          child: Text("Update Location"),
                          textColor: Colors.white,
                          color: mColor,
                        ),
                      )
                    ],
                  )
                ],
              ));
        });
  }

  // Future<String> _updateLocation(double lat, double lng) async {
  //   print("INPUT:: $lat|| $lng");
  //   var r = await auth.updateLocation(
  //       lat, lng, USER_DETAILES.data.loginDetails.authKey);
  //   return r['message'].toString();
  // }

//   setLocation(double lat, double lng) async {
//     var r = await auth.updateLocation(
//         lat, lng, USER_DETAILES.data.loginDetails.authKey);
//
//     setState(() {
//       preferences?.setString('lastLocation', cLocation);
//       preferences?.setString('postalCode', postalCode);
//       preferences?.setDouble('lat', lat);
//       preferences?.setDouble('lng', lng);
//       nearmeState.getData();
//     });
//     if (r['message'].toString().contains("Location updated successfully")) {
// //      getNotifications();
//       Flushbar(
//         duration: Duration(seconds: 2),
//         flushbarStyle: FlushbarStyle.GROUNDED,
//         messageText: Text(
//           "Location Updated",
//           style: TextStyle(color: Colors.white),
//         ),
//         icon: Icon(Icons.location_on, color: Colors.blue),
//         isDismissible: true,
//       )
//         ..show(context);
//     } else {
//       Fluttertoast.showToast(
//           msg: "${r['message'].toString()}", toastLength: Toast.LENGTH_LONG);
//     }
//   }

//   relocate() async {
//     var r = await auth.getProfile(USER_DETAILES.data.loginDetails.authKey);
//     var LAT = double.tryParse(r['prifiledata'][0]['lat'].toString());
//     var LONG = double.tryParse(r['prifiledata'][0]['lng'].toString());
//     print("LAT:::: $LAT || LONG::: $LONG");
//
//
//     var c = Coordinates(LAT, LONG);
//
//     addresses = await Geocoder.local.findAddressesFromCoordinates(c);
//     setState(() {
//       current = addresses.first;
//       cLocation = current.subLocality;
//       postalCode = current.postalCode;
//     });
//     print("CURRENT POSITION: $cLocation | $postalCode");
//     print("CURRENT POSITION 2: ${_currentPosition.latitude} | ${_currentPosition
//         .longitude}");
//     setLocation(LAT, LONG);
//     if (first.subLocality != cLocation) {
//       showUpdateLocation();
//     }
//   }
//
//   getFullAddress() async {
//     preferences = await SharedPreferences.getInstance();
//     var coordinates = Coordinates(
//         _currentPosition.latitude, _currentPosition.longitude);
//     addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
//     first = addresses.first;
//     relocate();
//     full_address = "${first.addressLine}";
//     print("FULL ADDRESS => $full_address");
//   }
//
//   getLocation() {
//     // print("timer=== ${timer.isActive} ----- $cLocation");
//     final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
//     try {
//       geolocator
//           .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
//           .then((Position position) {
//         setState(() {
//           _currentPosition = position;
//           getFullAddress();
//         });
//       });
//     } catch (e) {
// //      if (e.toString().contains("PERMISSION_DENIED")) {
// //        showUpdateLocation();
// //      }
//       print("========== $e ================");
//     }
//   }

  NotificationHandler notificationHandler;

  @override
  void initState() {
    super.initState();
    notificationHandler = NotificationHandler()..setupFirebaseNotification();
    username = "";
    email = "";
    // timer = Timer.periodic(Duration(seconds: 5), (timer) {
    //   if (cLocation == null) {
    //     relocate();
    //   } else {
    //     timer.cancel();
    //   }
    // });
    tabController =
        new TabController(vsync: this, length: _tabs.length, initialIndex: 1);
    initPermission();
//    PermissionHandler().checkPermissionStatus(PermissionGroup.locationAlways)
//        .then(_updateStatus);
//    print(PermissionStatus);
//    _getId(context);
  }

  @override
  void dispose() {
    // timer?.cancel();
    super.dispose();
  }

  initPermission() async {
    await Permission.locationWhenInUse.request();
    PermissionStatus status = await Permission.locationWhenInUse.status;
    await askPermissions(status, "location", Permission.locationWhenInUse);
    checkInternet();
  }

//   askPermission() {
//     PermissionHandler().requestPermissions(
//         [PermissionGroup.locationWhenInUse]).then(onPermissionAsked);
//   }
//
//   void onPermissionAsked(Map<PermissionGroup, PermissionStatus> value) {
//     final status = value[PermissionGroup.locationWhenInUse];
//     _updateStatus(status);
//   }
//
//   void _updateStatus(PermissionStatus status) {
// //    if (status != permissionStatus) {
// //      setState(() {
// //        permissionStatus = status;
// //        askPermission();
// //      });
// //    }
//     print("${status.toString()} permission-----");
//     if (status.toString().contains("PermissionStatus.unknown"))
//       askPermission();
//     else if (status.toString().contains("PermissionStatus.denied"))
//       showLocationAccessModal();
//     else if (status.toString().contains("PermissionStatus.granted"))
//       checkInternet();
//   }
//
//   showLocationAccessModal() {
//     showModalBottomSheet(
//         context: context,
//         isDismissible: false,
//         isScrollControlled: true,
//         builder: (context) {
//           return Container(
//               padding: EdgeInsets.all(15),
// //              height: MediaQuery
// //                  .of(context)
// //                  .size
// //                  .height * .32,
//               width: MediaQuery.of(context).size.width,
//               child: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 children: <Widget>[
//                   Text("Cant't Access your location",
//                       style:
//                           TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
//                   SizedBox(
//                     height: 15,
//                   ),
//                   Text(
//                       "In order to access your location you must provide location permission to application."),
//                   SizedBox(
//                     height: 15,
//                   ),
//                   Row(
//                     children: <Widget>[
//                       Expanded(
//                         flex: 1,
//                         child: OutlineButton(
//                           onPressed: () {
//                             exit(0);
// //                          Navigator.pop(context);
//                           },
//                           child: Text("Exit"),
//                         ),
//                       ),
//                       SizedBox(
//                         width: 15,
//                       ),
//                       Expanded(
//                         flex: 1,
//                         child: FlatButton(
//                           onPressed: () {
//                             Navigator.pop(context);
//                             PermissionHandler().openAppSettings();
//                             setState(() {
//                               reLocation = true;
//                             });
//                           },
//                           child: Text("Open Settings"),
//                           textColor: Colors.white,
//                           color: mColor,
//                         ),
//                       )
//                     ],
//                   )
//                 ],
//               ));
//         });
//   }

  checkInternet() async {
    // getMyLocation();
//    CheckInternet checkInternet = CheckInternet();
//    DataConnectionStatus status = await CheckInternet(context).checkInternet();
//    if (status == DataConnectionStatus.connected) {
//      print("yes");
    // getLocation();
    // getMyLocation();
    getNotifications();
    getProfileData();
//    } else {
//      noInternet = true;
//      Flushbar(
//        title: "No Internet",
//        messageText: Text(
//          "Check your Internet connection & try again.",
//          style: TextStyle(color: Colors.white),
//        ),
//        icon: Icon(Icons.signal_wifi_off, color: mColor),
//        isDismissible: true,
//        leftBarIndicatorColor: mColor,
//      )..show(context);
//    }
  }

  setIcon() {
    print("Current tab value:  $currentTab");
    setState(() {
      mainAppState.currentTab = 1;
    });
    print("Current tab value:  $currentTab");
  }

  Logout() async {
//    print("LATLONG =>>>>> $lat || $lng");
//    print("cLOCATION & POSTCODe =>>>>> $cLocation || $postalCode");
    preferences = await SharedPreferences.getInstance();
//    await auth.updateLocation(lat, lng, USER_DETAILES.data.loginDetails.authKey);
//    lat = double.tryParse(USER_DETAILES.data.loginDetails.lat);
//    lng = double.tryParse(USER_DETAILES.data.loginDetails.lng);
    setState(() {
      preferences?.setBool('isLogin', null);
      preferences?.setString('username', null);
      preferences?.setString('password', null);
      preferences?.setString('showIntro', 'NO');
      preferences?.setDouble('lat', null);
      preferences?.setDouble('lng', null);
      preferences?.setString('lastLocation', null);
      preferences?.setString('postalCode', null);
//      Navigator.pushReplacement(
//          context, MaterialPageRoute(builder: (context) => LoginPage()));
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => LoginPage()),
          (Route<dynamic> route) => false);
    });
  }

//  getCart() async {
//    var r = await auth.getMyCart(USER_DETAILES.data.loginDetails.userId,
//        USER_DETAILES.data.loginDetails.authKey);
//    setState(() {
//      myCartItems = MyCartItems.fromJson(r);
//    });
//  }
  TabController tabController;
  static List<Tab> _tabs = <Tab>[
//    Tab(
//      child: Container(
//        child: Center(
//          child: Text(
//            "Favourite",
//          ),
//        ),
//      ),
//    ),
    Tab(
      child: Container(
        child: Center(
          child: Text(
            "Near me",
          ),
        ),
      ),
    ),
    Tab(
      child: Container(
        child: Center(
          child: Text(
            "Products",
          ),
        ),
      ),
    ),
  ];

//  static List<Widget> _screens = [HomeScreen(), SearchScreen(), OfferScreen()];

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarBrightness: Brightness.dark));

    bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;

    Widget appBarAction = Center(
        child: Padding(
      padding: EdgeInsets.only(right: 12.0),
      child: Container(
        height: 40,
        width: 40,
        padding: EdgeInsets.all(1.0),
        decoration: BoxDecoration(shape: BoxShape.circle),
            child: Hero(
                tag: 'neomart_logo',
                child: GestureDetector(
                  onTap: () async {
                    await Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                AccountInformation(USER_DETAILES, "")));
                    setState(() {
                      profileImage = null;
                    });
                    getProfileData();
                  },
                  child: ClipOval(
                    child: Container(
                      height: 40,
                      width: 40,
                      child: profileImage == null
                          ? Icon(
                        Icons.account_circle,
                        color: Colors.white,
                        size: 40,
                      )
                          : Image.network(
                        profileImage,
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                )),
          ),
        ));

    Widget _leading = GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => NotificationCenter()));
        },
        child: Center(
            child: Stack(
              children: <Widget>[
                Icon(
                  Icons.notifications,
                  color: Colors.white,
                  size: 27,
                ),
                notifications == null
                    ? SizedBox()
                    : Positioned(
                    right: 0,
                    top: 0,
                    child: notifications == 0
                        ? SizedBox()
                        : Container(
                      padding: EdgeInsets.all(2),
                      decoration: new BoxDecoration(
                        color: Colors.white,
                        border: Border.all(color: mColor),
                        borderRadius: BorderRadius.circular(6),
                      ),
                      constraints: BoxConstraints(
                        minWidth: 10,
                        minHeight: 10,
                      ),
                      child: Text(
                        '$notifications',
                        style: TextStyle(
                          color: mColor,
                          fontSize: 8,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    )),
              ],
            )));

    return Scaffold(
      key: _scaffoldKey,
      extendBody: true,
      endDrawer: SafeArea(
        child: Drawer(
            child: Container(

              child: Column(
                children: <Widget>[
                  Expanded(
                      flex: 1,
                      child:Container(
                        color: mColor,
                        child: DrawerHeader(
                            padding: EdgeInsets.all(0),
                            margin: EdgeInsets.all(0),
                            child: UserAccountsDrawerHeader(

                              margin: EdgeInsets.only(bottom: 0),
                              accountName:
                              username.isEmpty ? SizedBox() : Text("${username}"),
                              accountEmail:
                              email.isEmpty ? SizedBox() : Text("${email}"),
                              currentAccountPicture: GestureDetector(
                                onTap: () {
                                  Navigator.pop(context);
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              AccountInformation(USER_DETAILES, "")));
                                },
                                child: ClipOval(
                                  child: Container(
                                    height: 100,
                                    width: 100,
                                    child: profileImage == null
                                        ? Icon(
                                      Icons.account_circle,
                                      color: Colors.white,
                                      size: 70,
                                    )
                                        : Image.network(
                                      profileImage,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ),
                            )),
                      )),
                  Expanded(
                    flex: 3,
                    child: ListView(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => NotificationCenter()));
                          },
                          child: ListTile(
                            leading: Icon(Icons.notifications_active),
                            title: Text("Notifications"),
                            trailing: Container(
                                alignment: Alignment.center,
                                height: 20,
                                width: 20,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: mColor
                                ),
                                child: Text("$notifications", style: TextStyle(
                                    fontSize: 12, color: Colors.white),)),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Chats(USER_DETAILES)));
                          },
                          child: ListTile(
                            leading: Icon(Icons.chat),
                            title: Text("Chat"),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => MyOrders()));
                          },
                          child: ListTile(
                            leading: Icon(Icons.shop),
                            title: Text("My Orders"),
                          ),
                        ),
                        InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                        await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MyAddress()));

                        dashboardState.getStoreInfo();
                      },
                          child: ListTile(
                            leading: Icon(Icons.location_city),
                            title: Text("My Address"),
                          ),
                        ),
                        Divider(
                          height: 0,
                        ),
                        // InkWell(
                        //   onTap: () {
                        //     Navigator.pop(context);
                        //     Navigator.push(
                        //         context,
                        //         MaterialPageRoute(
                        //             builder: (context) =>
                        //                 ReferralPage(USER_DETAILES)));
                        //   },
                        //   child: ListTile(
                        //     leading: Icon(Icons.share),
                        //     title: Text("Refer & Earn"),
                        //   ),
                        // ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CancelAndRefund()));
                          },
                          child: ListTile(
                            leading: Icon(Icons.attach_money),
                            title: Text("Cancellation & Refund"),
                          ),
                        ),
//                        Divider(),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => TermsAndConditions()));
                          },
                          child: ListTile(
                            leading: Icon(Icons.content_paste),
                            title: Text("Terms & Conditions"),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => PrivacyPolicy()));
                          },
                          child: ListTile(
                            leading: Icon(Icons.lock),
                            title: Text("Privacy Policy"),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => FAQ()));
                          },
                          child: ListTile(
                            leading: Icon(Icons.question_answer),
                            title: Text("FAQs"),
                          ),
                        ),
                        Divider(
                          height: 0,
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => ContactUs()));
                          },
                          child: ListTile(
                            leading: Icon(Icons.phone),
                            title: Text("Contact Us"),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                            showDialog(
                                context: context,
                                builder: (_) => isIOS == false
                                    ? AlertDialog(
                                  title: Text("Logout"),
                                  elevation: 8.0,
                                  content: Text("Do you want to logout?"),
                                  actions: <Widget>[
                                    RaisedButton(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("Cancel"),
                                      color: Colors.white,
                                      textColor: mColor,
                                    ),
                                    RaisedButton(
                                      onPressed: () {
                                        Logout();
                                      },
                                      child: Text("Logout"),
                                      color: mColor,
                                    )
                                  ],
                                )
                                    : CupertinoAlertDialog(
                                  title: Text("Logout"),
                                  content: Text("Do you want to logout?"),
                                  actions: <Widget>[
                                    CupertinoDialogAction(
                                      onPressed: () {
                                        Navigator.pop(context);
                                      },
                                      child: Text("Cancel"),
                                    ),
                                    CupertinoDialogAction(
                                      onPressed: () {
                                        Logout();
                                      },
                                      child: Text(
                                        "Logout",
                                        style: TextStyle(color: mColor),
                                      ),
                                    )
                                  ],
                                ));
                          },
                          child: ListTile(
                            leading: Icon(Icons.exit_to_app),
                            title: Text("Logout"),
                            trailing: Text(
                              "Ver ${packageVersion == null ? "--" : "$packageVersion"}",
                              style: TextStyle(fontSize: 12),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )),
      ),

      ///////////////////////////////////body////////////////////
//       appBar: AppBar(
//         centerTitle: true,
//         leading: _leading,
//         elevation: 0.0,
//         title: GestureDetector(
//             onTap: () async {
//               first = null;
// //            getLocation();
//               var loc = await Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                       builder: (context) =>
//                           SearchLocation(USER_DETAILES.data.loginDetails.authKey
//                               .toString(), false)));
//               if (loc != null) {
//                 setState(() {
//                   print("loca==== ${loc[0]} ------ ${loc[1]}");
//                   if (loc[0] != null) {
//                     v.current_location.value = loc[0].toString();
//                     v.current_postalCode.value = loc[1].toString();
//                     lat = loc[2];
//                     lng = loc[3];
//                     preferences?.setString(
//                         'lastLocation', v.current_location.value);
//                     preferences?.setString(
//                         'postalCode', v.current_location.value);
//                     preferences?.setDouble('lat', lat);
//                     preferences?.setDouble('lng', lng);
//                     print("ON PRESS BACK:: LAT $lat || LONG $lng");
//                   }
//                 });
//               }
//               nearmeState.getData();
//             },
//             child: Obx(() =>
//             v.current_location.value == null || v.current_location.value == ""
//                 ? Text("Locating...", style: TextStyle(color: Colors.white))
//                 : v.current_postalCode.value.contains("null") ||
//                 v.current_location.value == "" ||
//                 v.current_postalCode.value == null ||
//                 v.current_postalCode.value == ""
//                 ? Text("${v.current_location.value}",
//               style: TextStyle(color: Colors.white),)
//                 : Text(
//               "${v.current_location.value} - ${v.current_postalCode.value}",
//               style: TextStyle(
//                   fontFamily: 'Poppins-Regular',
//                   color: Colors.white),
//             ))),
//         actions: <Widget>[appBarAction],
// //        bottom: PreferredSize(
// //          preferredSize: Size(double.infinity, 60),
// //          child: Container(
// //            width: double.infinity,
// //            color: Colors.green,
// //            child: ListTile(
// //              dense: true,
// //              title: Text("There is a new version of this app is available.", style: TextStyle(color: Colors.white),),
// //              trailing: Icon(Icons.close),
// //            )
// //          ),
// //        ),
//       ),

      drawerEdgeDragWidth: 0.0,
      body: DoubleBack(
        waitForSecondBackPress: 1,
        onFirstBackPress: (context) {
          if (_scaffoldKey.currentState.isEndDrawerOpen)
            Navigator.pop(context);
          else {
            Fluttertoast.showToast(
                msg: "Press back again to exit",
                gravity: ToastGravity.BOTTOM,
                toastLength: Toast.LENGTH_LONG);
          }
        },
        child: shoppingListCount == null
            ?  neoLoader()
            : Container(
            child: PageView(
              physics: NeverScrollableScrollPhysics(),
              controller: pageController,
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                // HomeScreen(lat, lng),
                Dashboard(),
                SearchPage(),
                OfferScreen(),
              ],
            )),
      ),

      floatingActionButton: Stack(
        fit: StackFit.loose,
        children: <Widget>[
          FloatingActionButton(
            elevation: 8.0,
            heroTag: 'cart',
            tooltip: 'Cart',
            backgroundColor: mColor,
            onPressed: () async {
              await Navigator.push(
                  context, MaterialPageRoute(builder: (context) => checkout()));
              // dashboardState.getProducts();
              getShoppingListCount();
              getNotifications();
              getProfileData();
              dashboardState.getProducts();
            },
            child: Icon(
              Icons.shopping_cart,
              color: Colors.white,
            ),
          ),
          Positioned(
              right: 0,
              top: 0,
              child: Obx(() => AnimatedOpacity(
                opacity: v.cartCount.value > 0 ? 1.0 : 0.0,
                duration: Duration(milliseconds: 0),
                child: Container(
                  padding: EdgeInsets.all(2),
                  decoration: new BoxDecoration(
                    color: Colors.white,
                    border: Border.all(color: mColor),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  constraints: BoxConstraints(
                    minWidth: 20,
                    minHeight: 20,
                  ),
                  child: Text(
                    '${v.cartCount.value}',
                    style: TextStyle(
                      color: mColor,
                      fontSize: 11,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ))),
        ],
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        notchMargin: 6,
        elevation: 2.0,
        shape: CircularNotchedRectangle(),
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Row(
                children: <Widget>[
                  MaterialButton(
                      onPressed: () {
                        if (currentTab == 1) {
                          // nearmeState.listViewController.animateTo(0,
                          //     duration: Duration(milliseconds: 300),
                          //     curve: Curves.decelerate);
                        } else {
                          setState(() {
                            currentTab = 1;
                            pageController.jumpToPage(0);
                          });
                        }
                      },
                      minWidth: 40,
                      child: Icon(
                        Icons.home,
                        color: currentTab == 1 ? mColor : Colors.grey,
                      )),
                  MaterialButton(
                      onPressed: () {
                        setState(() {
                          currentTab = 2;
                          pageController.jumpToPage(1);
                        });
                      },
                      minWidth: 40,
                      child: Icon(
                        currentTab == 2
                            ? Icons.search
                            : Icons.search,
                        color: currentTab == 2 ? mColor : Colors.grey,
                      )),
                ],
              ),
              SizedBox(
                width: 50,
              ),
              Row(
                children: <Widget>[
                  MaterialButton(
                      onPressed: () {
                        setState(() {
                          currentTab = 3;
                          print(pageController.toString());
                          pageController.jumpToPage(2);
                          print("offer");
                        });
                      },
                      minWidth: 40,
                      child: Icon(
                        Icons.local_offer,
                        color: currentTab == 3 ? mColor : Colors.grey,
                      )),
                  MaterialButton(
                      onPressed: () {
                        _scaffoldKey.currentState.openEndDrawer();
                      },
                      minWidth: 40,
                      child: Icon(
                        Icons.menu,
                        color: Colors.grey,
                      )),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
