import 'dart:async';
import 'dart:io';

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Screens/IntroductionScreen.dart';
import 'package:userapp/Screens/LoginRegister/LoginPage.dart';
import 'package:userapp/Screens/MainApp.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../Network/CheckInternetConnection.dart';
import '../Getters/GetUser.dart';
import '../constants.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  var status, username, password;
  var auth = HttpRequests();
  User user;
  FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  DatabaseReference dbRef;
  String fcmToken, deviceUniqueId;
  int deviceType = 1;

  isLoggedin() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    status = preferences?.get("isLogin");
    username = preferences?.get("username");
    password = preferences?.get("password");
    print(status);
    print(username);
    print(password);
    print(preferences?.get("showIntro"));
    if (preferences?.get("showIntro") == null)
      Timer(
          Duration(seconds: 4),
          () => Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => IntroSreen())));
    else if (status == null)
      Timer(
          Duration(seconds: 4),
          () => Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => LoginPage())));
    else {
      checkInternet();
    }
  }

  checkInternet() async {
//    CheckInternet checkInternet = CheckInternet();
    DataConnectionStatus status = await CheckInternet(context).checkInternet();
    if (status == DataConnectionStatus.connected) {
      print("yes");
      getUser();
    } else {
      showDialog(
          context: context,
          builder: (_) {
            return AlertDialog(
              title: Text("No Internet"),
              content: Text("Check your Internet connection & try again"),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    exit(0);
                  },
                  child: Text("Cancel"),
                  color: Colors.white,
                  textColor:  mColor,
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    checkInternet();
                  },
                  child: Text("Retry"),
                  color:  mColor,
                  textColor: Colors.white,
                ),
              ],
            );
          });
    }
  }

  Future<String> _getId() async {
    print("getting device Id");
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Theme.of(context).platform == TargetPlatform.iOS) {
      deviceType = 2;
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      return iosDeviceInfo.identifierForVendor; // unique ID on iOS
    } else {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      deviceType = 1;
      return androidDeviceInfo.androidId; // unique ID on Android
    }
  }

  getUser() async {
    deviceUniqueId = await _getId();
    deviceType = 3;
    SharedPreferences preferences = await SharedPreferences.getInstance();
    var r = await auth.LoginAuth(username, password);
    print(status);
    print(username);
    print(password);
    user = User.fromJson(r);
    if (user.meta.message.toString().contains("Successfully Login!") ||
        user.meta.status.toString().contains("success")) {
      userDetails = user;
      USER_DETAILES = user;
      USER_ID = USER_DETAILES.data.loginDetails.userId.toString();
      TOKEN = user.data.loginDetails.authKey;
      print("success ${user.meta.message}");
      print("userID ${user.data.loginDetails.userId}");
      preferences?.setBool("isLogin", true);
      preferences?.setString("username", username);
      preferences?.setString("password", password);
      preferences?.setString("showIntro", 'NO');

      dbRef
          .child(user.data.loginDetails.userId.toString())
          .child("fcmToken")
          .set(fcmToken);
      dbRef
          .child(user.data.loginDetails.userId.toString())
          .child("fcmToken")
          .once()
          .then((DataSnapshot data) => fcmToken = data.value);

      print("FCMTOKEN --> $fcmToken");
      await auth
          .getToken(user.data.loginDetails.userId, deviceUniqueId, fcmToken,
              deviceType)
          .then((value) => print(value.toString()));
//      notificationHandler = NotificationHandler(context, user)..setupFirebaseNotification();

      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => MainApp()));
    } else {
      Get.offAll(LoginPage());
    }
  }

//  getDeviceInfo() async {
//    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
//    IosDeviceInfo iosDeviceInfo = await deviceInfoPlugin.iosInfo;
//    print(iosDeviceInfo.model);
//  }

  initPermission() async {
    await Permission.locationWhenInUse.request();
    PermissionStatus status = await Permission.locationWhenInUse.status;
    await askPermissions(status, "location", Permission.locationWhenInUse);
  }

  void initState() {
    // TODO: implement initState
    dbRef = FirebaseDatabase.instance.reference().child("registeredUser");
    firebaseMessaging.getToken().then((value) {
      fcmToken = value;
      print("fcmToken -> $fcmToken");
    });
    initPermission();
    isLoggedin();
    super.initState();
//    getDeviceInfo();
//    getUser();
//    Timer(
//        Duration(seconds: 4),
//        () => Navigator.pushReplacement(
//            context, MaterialPageRoute(builder: (context) => LoginPage())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
            padding: EdgeInsets.all(20),
            child: Hero(
                tag: 'neomart_logo',
                child: Image(
                  image: AssetImage('drawables/logo_neo_mart_splash.png'),
                  height: 150,
                  width: 150,
                ))),
      ),
      bottomNavigationBar: Container(
        child: SafeArea(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "POWERED BY ",
                style: TextStyle(fontSize: 10),
              ),
              Image.asset(
                'assets/retail-magik-logo.png',
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }
}
