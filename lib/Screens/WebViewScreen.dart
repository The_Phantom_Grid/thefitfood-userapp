import 'dart:async';

import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:dio/dio.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import 'package:http/http.dart' as http;

import '../Components/CommonUtility.dart';
import '../constants.dart';

class CancelAndRefund extends StatefulWidget {
  @override
  _CancelAndRefundState createState() => _CancelAndRefundState();
}

class _CancelAndRefundState extends State<CancelAndRefund> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) loading = false;
            });
          }
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: mColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: loading
              ? Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Loading..."),
            ],
          )
              : Text("Cancellation & Refund"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/site/returns_user",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class TermsAndConditions extends StatefulWidget {
  @override
  _TermsAndConditionsState createState() => _TermsAndConditionsState();
}

class _TermsAndConditionsState extends State<TermsAndConditions> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();

  StreamSubscription<double> _onProgressChanged;

  final _history = [];

  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) loading = false;
            });
          }
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: mColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: loading
              ? Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Loading..."),
            ],
          )
              : Text("Terms & Conditions"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/site/terms_conditions",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class PrivacyPolicy extends StatefulWidget {
  @override
  _PrivacyPolicyState createState() => _PrivacyPolicyState();
}

class _PrivacyPolicyState extends State<PrivacyPolicy> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) loading = false;
            });
          }
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: mColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: loading
              ? Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Loading..."),
            ],
          )
              : Text("Privacy Policy"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/site/privacy_policy",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) loading = false;
            });
          }
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: mColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: loading
              ? Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Loading..."),
            ],
          )
              : Text("Contact Us"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/site/contactusdata",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class Help extends StatefulWidget {
  @override
  _HelpState createState() => _HelpState();
}

class _HelpState extends State<Help> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) loading = false;
            });
          }
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: mColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: loading
              ? Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Loading..."),
            ],
          )
              : Text("Help"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/site/help",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class FAQ extends StatefulWidget {
  @override
  _FAQState createState() => _FAQState();
}

class _FAQState extends State<FAQ> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  final _history = [];
  bool loading = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) loading = false;
            });
          }
        });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: mColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title: loading
              ? Row(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 15,
                width: 15,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  backgroundColor: Colors.white,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Loading..."),
            ],
          )
              : Text("FAQ"),
        ),
        body: WebviewScaffold(
          url: "https://www.neomart.com/frontend/web/site/data",
          withJavascript: true,
          withZoom: true,
        ));
  }
}

class ShowInvoice extends StatefulWidget {
  String pdfUrl;
  String orderId;

  ShowInvoice(this.pdfUrl, this.orderId);

  @override
  _ShowInvoiceState createState() => _ShowInvoiceState();
}

class _ShowInvoiceState extends State<ShowInvoice> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  StreamSubscription<double> _onProgressChanged;
  Dio dio = Dio();
  final _history = [];
  bool loading = true,
      downloading = false,
      isIOSDevice = false,
      isExist = false;
  PermissionStatus permissionStatus;

  // PermissionHandler _permissionHandler;
  String downloadMessage;
  String path;
  var dir;
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    loadDocument();
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var android = AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: true,
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initSettings = InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: onSelectNotification);
//    if(Theme.of(context).platform == TargetPlatform.iOS)
//      isIOSDevice = true;
    _onProgressChanged =
        flutterWebViewPlugin.onProgressChanged.listen((double progress) {
          if (mounted) {
            setState(() {
              print("Progress: $progress");
              _history.add('onProgressChanged: $progress');
              if (progress == 1.0) {
                loading = false;
              }
            });
          }
        });
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
              })
        ],
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    print('Working on select');
    print('PAYLOAD: $payload');
    openPdf();
  }

  showNotification(String name) async {
    var android = AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION');

    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(android, iOS);
    print("${platform.iOS.toString()} platform");

    await flutterLocalNotificationsPlugin.show(0, "Invoice", "$name", platform);
  }

  // checkPermissionStatus() {
  //   PermissionHandler()
  //       .checkPermissionStatus(PermissionGroup.storage)
  //       .then(_updateStatus);
  // }

//  checkStoragePermission() {
//    PermissionHandler().requestPermissions([PermissionGroup.storage]);
////    PermissionHandler().checkPermissionStatus(PermissionGroup.storage)
////        .then(_updateStatus);
//  }

  void _updateStatus(PermissionStatus status) {
    print(status);

    if (status.toString().contains("PermissionStatus.denied")) {
      Flushbar(
        messageText: Text(
          "Storage Permission is must.",
          style: TextStyle(color: Colors.white),
        ),
        flushbarStyle: FlushbarStyle.GROUNDED,
        flushbarPosition: FlushbarPosition.TOP,
        dismissDirection: FlushbarDismissDirection.HORIZONTAL,
        isDismissible: true,
        mainButton: FlatButton(
          onPressed: () {
            openAppSettings();
          },
          child: Text("Open Settings"),
          color: Colors.transparent,
          textColor: Colors.blue,
        ),
      )..show(context);
    }

    if (status.toString().contains("PermissionStatus.unknown")) {
      askPermission();
    }

    if (status.toString().contains("PermissionStatus.granted")) {
      print('download');
      getAppDirectory();
    }
  }

  // PDFDocument document;
  getAppDirectory() async {
    Directory dir;
    if (Platform.isIOS)
      dir = await getApplicationDocumentsDirectory();
    else if (Platform.isAndroid)
      dir = await getExternalStorageDirectory();
    String filePath = dir.path +
        "/invoice-${widget.orderId.toString()}.pdf";
    print("FILE PATH: $filePath");
    download2(widget.pdfUrl, filePath);
  }

  Future download2(String url, String savePath) async {
    try {
      Response response = await dio.get(
        url,
        onReceiveProgress: showDownloadProgress,
        //Received data with List<int>
        options: Options(
            responseType: ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              return status < 500;
            }),
      );
      print(response.headers);
      File file = File(savePath);
      var raf = file.openSync(mode: FileMode.write);
      // response.data is List<int> type
      raf.writeFromSync(response.data);
      await raf.close();
      showNotification("invoice-${widget.orderId.toString()}.pdf");
      showToast("Invoice downloaded & saved in phone storage.", Colors.green);
    } catch (e) {
      print("EXCEPTION: $e");
      showToast("Something went wrong. Cannot download Invoice", Colors.red);
    }
  }

  void showDownloadProgress(received, total) {
    print("DOWNLOADIN>> $received || $total");
    print("DOWNLOAD PROGRESS: " + (received / total * 100).toStringAsFixed(0) +
        "%");
    if (total != -1) {
      print(
          "DOWNLOAD PROGRESS: " + (received / total * 100).toStringAsFixed(0) +
              "%");
    }
//    Get.snackbar("Invoice", "Downloading", progressIndicatorController: )
  }

  askPermission() async {
    print("req");
    PermissionStatus status = await Permission.storage.status;
    await askPermissions(status, "storage", Permission.storage);
    // downloadInvoice();
    dir = await getApplicationDocumentsDirectory();
    print(dir);
    getAppDirectory();
  }

  // void _onStatusRequested(Map<PermissionGroup, PermissionStatus> statuses) {
  //   final status = statuses[PermissionGroup.storage];
  //   _updateStatus(status);
  // }

//  void onPermissionAsked(Map<PermissionGroup, PermissionStatus> value) {
//    final status = value[PermissionGroup.locationWhenInUse];
//    if (status == PermissionStatus.denied || status == null) {
//      PermissionHandler().requestPermissions([PermissionGroup.storage]);
//    } else {
//      _updateStatus(status);
//    }
//  }

  // checkFile() async {
  //   print("hit...");
  //   try {
  //     if (Theme.of(context).platform == TargetPlatform.iOS) {
  //       dir = await getApplicationDocumentsDirectory();
  //     } else
  //       dir = await getExternalStorageDirectory();
  //     String path = "${dir.path}/neomart-invoice-${widget.orderId}.pdf";
  //
  //     if (await File(path).exists() &&
  //         Theme.of(context).platform != TargetPlatform.iOS) {
  //       setState(() {
  //         isExist = true;
  //       });
  //     } else
  //       print("not exist");
  //   } catch (e) {
  //     print("Download file Exception: $e");
  //   }
  // }

//  checkInvoiceExist() async {
//    try {
//      if (Theme
//          .of(context)
//          .platform == TargetPlatform.iOS) {
//        dir = await getApplicationDocumentsDirectory();
//      } else
//        dir = await getExternalStorageDirectory();
//      String path = "${dir.path}/neomart-invoice-${widget.orderId}.pdf";
//
//      if (await File(path).exists() && Theme
//          .of(context)
//          .platform != TargetPlatform.iOS) {
////        openPdf();
//        setState(() {
//          isExist = true;
//        });
//      }
//      else
//        downloadInvoice();
//    } catch (e) {
//      print("Download file Exception: $e");
//    }
//  }

//   Future<void> downloadInvoice() async {
//     setState(() {
//       downloading = true;
//     });
// //    Flushbar(
// //      flushbarPosition: FlushbarPosition.TOP,
// //      flushbarStyle: FlushbarStyle.GROUNDED,
// //      icon: Icon(Icons.file_download, color: Colors.green),
// //      showProgressIndicator: true,
// //      duration: Duration(seconds: 2),
// //      messageText: Text("Your invoice is being downloaded",
// //          style: TextStyle(color: Colors.white)),
// //    )
// //      ..show(context);
//     Dio dio = Dio();
//     try {
//       if (Theme.of(context).platform == TargetPlatform.iOS)
//         dir = await getApplicationSupportDirectory();
//       else
//         dir = await getApplicationSupportDirectory();
//       ////downloading invoice pdf//////////
//       await dio.download(
//           widget.pdfUrl, "${dir.path}/neomart-invoice-${widget.orderId}.pdf",
//           onReceiveProgress: (recieved, total) {
//         setState(() {
//           downloadMessage = "Downloading Invoice...";
//           path = "${dir.path}/neomart-invoice-${widget.orderId}.pdf";
//         });
//         print(downloadMessage);
//         print(
//             "Rec: $recieved | Total: $total | Percent: ${(recieved / (total * -1)) * 100}%");
//       });
//       setState(() {
//         print(
//             "Download Complete \n ${dir.path}/neomart-invoice-${widget.orderId}.pdf");
//         downloading = false;
//         Fluttertoast.showToast(
//             msg: "Invoice Downloaded",
//             toastLength: Toast.LENGTH_LONG,
//             gravity: ToastGravity.CENTER);
//         showNotification();
//       });
//     } catch (e) {
//       print("Download file Exception: $e");
//       if (e.toString().contains("Failed host lookup: 'neomart.com'"))
//         Fluttertoast.showToast(
//             msg: "Download error: No Internet Connection Available",
//             toastLength: Toast.LENGTH_LONG,
//             gravity: ToastGravity.CENTER);
//     }
//     /////////end//////////
//   }

  openPdf() {
    print("Opening file");
    OpenFile.open(path);
//    UrlLauncher.launch(
//      path,
//    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _onProgressChanged.cancel();
  }
  PDFDocument document;
  bool _isLoading=true;
  loadDocument() async {
    document = await PDFDocument.fromURL(widget.pdfUrl);

    setState(() => _isLoading = false);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: mColor,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          title:
//        loading || downloading ? Row(
//          mainAxisSize: MainAxisSize.min,
//          crossAxisAlignment: CrossAxisAlignment.center,
//          children: <Widget>[
//            Container(
//              height: 15,
//              width: 15,
//              child: CircularProgressIndicator(
//                strokeWidth: 2,
//                backgroundColor: Colors.white,
//              ),
//            ),
//            SizedBox(width: 10,),
//            downloading ? Text("Downloading...") : Text("Loading..."),
//          ],
//        ) :
          Text("Invoice"),
          actions: <Widget>[
            IconButton(
              onPressed: downloading
                  ? null
                  : () {
                askPermission();
              },
              tooltip: "Save",
              icon: Icon(
                Icons.save,
                color: Colors.white,
              ),
            )
          ],
        ),
        body:  Center(
          child: _isLoading
              ? Center(child: CircularProgressIndicator())
              :
          PDFViewer(
            document: document,
            zoomSteps: 1,
            //uncomment below line to preload all pages
            // lazyLoad: false,
            // uncomment below line to scroll vertically
            // scrollDirection: Axis.vertical,

            //uncomment below code to replace bottom navigation with your own
            /* navigationBuilder:
                      (context, page, totalPages, jumpToPage, animateToPage) {
                    return ButtonBar(
                      alignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.first_page),
                          onPressed: () {
                            jumpToPage()(page: 0);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_back),
                          onPressed: () {
                            animateToPage(page: page - 2);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.arrow_forward),
                          onPressed: () {
                            animateToPage(page: page);
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.last_page),
                          onPressed: () {
                            jumpToPage(page: totalPages - 1);
                          },
                        ),
                      ],
                    );
                  }, */
          ),
        )
//      WebviewScaffold(
//        resizeToAvoidBottomInset: false,
//        url:
//            "https://drive.google.com/viewerng/viewer?embedded=true&url=${widget.pdfUrl}",
//        withJavascript: false,
//        withZoom: true,
//      ),
    );
  }
}
