import 'dart:math';

import 'package:flutter/material.dart';
import 'package:userapp/Getters/GetNotification.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/OrderDetail.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../Components/CommonUtility.dart';
import '../Getters/GetOrders.dart';
import '../constants.dart';

class NotificationCenter extends StatefulWidget {

  @override
  _NotificationCenterState createState() => _NotificationCenterState();
}

class _NotificationCenterState extends State<NotificationCenter> {
  HttpRequests auth = HttpRequests();
  List<NotificationData> data;
  String orderId = "", letter;
  Order order;
  bool loading = true;
  int randomColorIndex = 1;
  List colors = [
     mColor,
    Colors.green,
    Colors.blue,
    Colors.black45,
    Colors.amber,
    Colors.brown,
    Colors.cyan,
    Colors.deepPurpleAccent,
    Colors.indigoAccent
  ];
  Random random = new Random();

  getNotifications() async {
    var r =
        await auth.getNotifications(USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      var n = UserNotification.fromJson(r);
      data = n.data;
      loading = false;
    });
  }

//  getRecentOrders() async {
//    try {
//      var r = await auth.getOrders(widget.user.data.loginDetails.authKey);
//      setState(() {
//        order = Order.fromJson(r);
//        loading = false;
//      });
//    } catch (e) {
//      if (e.toString().contains(
//          "'String' is not a subtype of type 'Map<String, dynamic>'")) {
//        setState(() {
//          order = null;
//          loading = false;
//        });
//      }
//    }
//  }

//  getOrderId(String message) {
//    setState(() {
//      orderId = message.replaceAll("New order is ", "");
//      orderId = orderId.substring(0, 4);
//      print(orderId);
//      print(order.myorder.recent.length);
//      for (int x = 0; x < order.myorder.recent.length; x++) {
//        if (order.myorder.recent[x].id.toString() == orderId) {
//          print("${order.myorder.recent[x].storeName}");
//          Navigator.push(
//              context,
//              MaterialPageRoute(
//                  builder: (context) =>
//                      OrderDetail(order.myorder.recent[x].id, widget.user)));
//        }
//      }
//    });
//  }

  String getLetter(int index) {
    letter = data[index]
        .sender
        .replaceRange(1, data[index].sender.length, "")
        .toUpperCase();
    randomColorIndex = random.nextInt(colors.length);
    return letter;
  }

  String getTimeDuration(int index) {
    var sentAt = data[index].sentAt.toString();
    var now = DateTime.now();
    int diff = now.difference(DateTime.parse(sentAt)).inSeconds;
    if (diff <= 59) {
      return "$diff seconds ago";
    }
    diff = now.difference(DateTime.parse(sentAt)).inMinutes;
    if (diff <= 60) {
      return "$diff minutes ago";
    }
    diff = now.difference(DateTime.parse(sentAt)).inHours;
    if (diff <= 24) {
      return "$diff hours ago";
    }
    diff = now.difference(DateTime.parse(sentAt)).inDays;
    return "$diff days ago";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getNotifications();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notifications"),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
      ),
      body: loading
          ? neoLoader()
          : data.isEmpty
              ? Center(
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: MediaQuery.of(context).size.height * .3,
                        ),
                        Icon(
                          Icons.notifications_none,
                          size: 60,
                          color: mColor,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Text("No Recent Notification"),
                      ],
                    ),
                  ),
                )
              : Container(
                  color: Colors.white,
                  child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return Divider(
                        indent: 15,
                        endIndent: 15,
                        height: 0,
                      );
                    },
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      return InkWell(
                        onTap: data[index].orderId != null
                            ? () {
                          print(USER_DETAILES.data.loginDetails.authKey);
                          print("Order Id: ${data[index].orderId}");
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      OrderDetail(
                                          data[index].orderId.toString())));
                              }
                            : null,
                        child: Stack(
                          children: <Widget>[
                            Card(
                                elevation: 0.0,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Container(
                                          color: colors[randomColorIndex],
                                          height: 60,
                                          width: 60,
                                          child: Center(
                                              child: Text(
                                            getLetter(index),
                                            style: TextStyle(
                                                fontSize: 26,
                                                color: Colors.white),
                                          )),
                                        ),
                                        Expanded(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                left: 8.0, right: 8.0),
                                            child: Container(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  Text(
                                                    "${data[index].sender}",
                                                    style: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                  Text(
                                                    "${data[index].messageContent}",
                                                    style:
                                                        TextStyle(fontSize: 11),
                                                  ),
                                                  Text(
                                                    getTimeDuration(index),
                                                    style: TextStyle(
                                                        fontSize: 11,
                                                        color: mColor),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )),
                            Visibility(
                              visible: getTimeDuration(index)
                                          .toString()
                                          .contains("seconds") ||
                                      getTimeDuration(index)
                                          .toString()
                                          .contains("minutes")
                                  ? true
                                  : false,
                              child: Positioned(
                                  top: 9,
                                  right: 9,
                                  child: Container(
                                    padding: EdgeInsets.fromLTRB(5, 2, 5, 2),
                                    color: mColor,
                                    child: Text(
                                      "NEW",
                                      style: TextStyle(
                                          fontSize: 9, color: Colors.white),
                                    ),
                                  )),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                ),
    );
  }
}
