// import 'dart:convert';
//
// import 'package:auto_size_text/auto_size_text.dart';
// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/foundation.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_slidable/flutter_slidable.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:get/get.dart';
// import 'package:userapp/Components/CommonUtility.dart';
// import 'package:userapp/Getters/GetCartProducts.dart';
// import 'package:userapp/Getters/GetMyCart.dart';
// import 'package:userapp/Getters/GetStoreDetails.dart';
// import 'package:userapp/Getters/GetUser.dart';
// import 'package:userapp/Screens/MyCart.dart';
// import 'package:userapp/Screens/PlaceOrder.dart';
// import 'package:userapp/Screens/StoreInventory.dart';
// import 'package:userapp/Network/httpRequests.dart';
//
// import '../Getters/GetComboOffer.dart';
// import '../constants.dart';
// import 'StoreDetail.dart';
//
// _CheckoutState checkOutState;
//
// class Checkout extends StatefulWidget {
//   StoreItemProduct storeItemProduct;
//
//   Checkout(this.storeItemProduct);
//
//   @override
//   _CheckoutState createState() {
//     checkOutState = _CheckoutState();
//     return checkOutState;
//   }
// }
//
// class _CheckoutState extends State<Checkout> {
// //  var mAccentColor = Color(0xFFf82d70);
//   var auth = new HttpRequests();
//   StoreDetails storeDetails;
//   CartProduct cartProduct;
//   var offer_id = "", offer_price = "";
//
//   // double subtotal = 0.0;
//   bool loading = true, placeOrder = true, firstTime = true;
//   int totalItems;
//   String src = "Checkout";
//
// //  var scrollController = ScrollController();
//
// //  AppState appState;
//
//   MyCartItems myCartItems;
//
//   getStoreDetails() async {
//     setState(() {
//       loading = true;
//     });
//     var r = await auth.getStoreDetail(
//       widget.storeItemProduct.storeid,
// //      USER_DETAILESData['data']['login_details']['user_id'],
// //      widget.distance,
// //      min,
// //      max,
//       USER_DETAILES.data.loginDetails.authKey,
//     );
//     setState(() {
//       var jData = json.decode(r.body);
//       storeDetails = StoreDetails.fromJson(jData);
//       getProducts();
//     });
//   }
//
//   getProducts() async {
//     var r = await auth.getCartProducts(
//         widget.storeItemProduct.merchantid,
//         widget.storeItemProduct.storeid,
//         USER_DETAILES.data.loginDetails.userId,
//         USER_DETAILES.data.loginDetails.authKey);
//     setState(() {
//       cartProduct = CartProduct.fromJson(r);
//       print("BILLING SUB: ${r['total']}");
//       if (cartProduct != null && cartProduct.product.isNotEmpty) {
//         print("OFFER EXPIRED: ${cartProduct.product[0].offerExpired}");
//         if (cartProduct.product[0].offerExpired == 1) {
//           showOfferRemoved();
//         } else {
//           print("CALLLL");
//           getComboOffers();
//         }
//       } else if (cartProduct == null || cartProduct.product.isEmpty) {
//         print("empty...... ${cartProduct.product.length}");
//         loading = false;
//         placeOrder = false;
//         Get.back();
//       }
//     });
//   }
//
// //   calSubtotal() {
// //     setState(() {
// //       totalItems = 0;
// //       subtotal = 0;
// //     });
// //     double price, comboPrice;
// // //    for (int x = 0; x < cartProduct.product.length; x++) {
// // //      if (cartProduct.product[x].offerId != null &&
// // //          cartProduct.product[x].masterproductid != 0)
// // //        setState(() {
// // //          cartProduct.product.removeAt(x);
// // //          x--;
// // //        });
// // //    }
// //     for (int p = 0; p < cartProduct.product.length; p++) {
// //       setState(() {
// // //        if(cartProduct.product[p].pricewithtax)
// // //          price = 0;
// //         price = cartProduct.product[p].pricewithtax == null? 0.0 : cartProduct.product[p].pricewithtax;
// // //        if(cartProduct.product[p].comboPrice.toString().isEmpty)
// // //          comboPrice = 0;
// //         comboPrice = cartProduct.product[p].comboPrice;
// //
// //         if (cartProduct.product[p].masterproductid == 0)
// //           subtotal = subtotal + cartProduct.product[p].quantity * comboPrice;
// //         else
// //           subtotal = subtotal + cartProduct.product[p].quantity * price;
// //         print("$subtotal ------------------------------------");
// // //        subTotal += (cartProduct.product[p].quantity *
// // //            cartProduct.product[p].pricewithtax.toInt());
// //       });
// //     }
// //     setState(() {
// //       loading = false;
// //       placeOrder = true;
// //       totalItems = cartProduct.product.length;
// //     });
// //     print("${cartProduct.product.length} lengthxxxxxxxlast");
// // //    if (!firstTime) {
// // //      Fluttertoast.showToast(
// // //          msg: "Cart Updated",
// // //          toastLength: Toast.LENGTH_SHORT,
// // //          gravity: ToastGravity.CENTER,
// // //          timeInSecForIosWeb: 1,
// // //          backgroundColor: mColor,
// // //          textColor: Colors.white,
// // //          fontSize: 16.0
// // //      );
// // //    } else
// // //      firstTime = false;
// //   }
//
//   /////enf of removeOffer///////
//
//   removeItem(int index) async {
//     var r = await auth.deleteItemFromCart(
//         cartProduct.product[index].shoppingListItemId,
//         storeDetails.data.shopDetails.merchantId,
//         storeDetails.data.shopDetails.id,
//         USER_DETAILES.data.loginDetails.authKey);
//     print(r['message'].toString());
//
//     if (r['status'].toString().toLowerCase() == "success") {
//       getStoreDetails();
//       print("item removed");
//     } else {
//       getStoreDetails();
//       print(r['message'].toString());
//       Fluttertoast.showToast(
//           msg: "${r['message'].toString()}",
//           toastLength: Toast.LENGTH_LONG,
//           gravity: ToastGravity.CENTER,
//           timeInSecForIosWeb: 1,
//           textColor: Colors.white,
//           fontSize: 16.0);
//     }
//   }
//
//   int distance = 5;
//   ComboOffer comboOffer;
//   int comboIndex;
//
//   getComboOffers() async {
//     print("LOADING: $loading");
//     var r = await auth.getComboOffer(USER_DETAILES.data.loginDetails.authKey,
//         distance, widget.storeItemProduct.storeid);
//     setState(() {
//       comboOffer = ComboOffer.fromJson(r);
// //      checkOfferExist();
//       if (comboOffer.info.isNotEmpty) {
//         print("${comboOffer.info[0].collectionProducts.length} length");
//         createOfferProductList();
//       } else
//         createOfferProductList();
//       loading = false;
//     });
//   }
//
// //  bool offerRemoved = false;
// //  checkOfferExist() async {
// //    for (int x = 0; x < cartProduct.product.length; x++) {
// //      if (cartProduct.product[x].offerId != 0){
// //        print("FOUND");
// //        print("Length: ${comboOffer.info.length}");
// //        print("CID: ${cartProduct.product[x].offerId}");
// //        if(comboOffer.info.isNotEmpty) {
// //
// //          comboOffer.info.forEach((e) async {
// //
// //            print("CID2: ${e.id}");
// //            if (!e.id.toString().contains(
// //                cartProduct.product[x].offerId.toString())) {
// //              print("REMOVING: ${e.id}");
// //              await removeOffer(cartProduct.product[x]);
// //            }
// //          });
// //        }else {
// //          print("REMOVING 2");
// //          await removeOffer(cartProduct.product[x]);
// //        }
// //      }
// //    }
// //    if(offerRemoved) {
// //      print("OFFER REMOVED1: $offerRemoved");
// //      print("LOADING: $loading");
// //      showOfferRemoved();
// //      getProducts();
// //    } else {
// //      createOfferProductList();
// //    }
// //  }
// //
// //  removeOffer(Product product) async {
// //    print("REMOVING product");
// //    await auth.deleteItemFromCart(
// //        product.shoppingListItemId,
// //        storeDetails.data.shopDetails.merchantId,
// //        storeDetails.data.shopDetails.id,
// //        USER_DETAILES.data.loginDetails.authKey
// //    );
// //    offerRemoved = true;
// ////    cartProduct.product.removeWhere((element) => element.offerId == product.offerId);
// //  }
// //
//   showOfferRemoved() {
//     showDialog(
//         context: context,
//         builder: (_) => AlertDialog(
//               content: Text(
//                   "The offer(s) you added earlier has been expired or removed by the merchant."),
//               actions: <Widget>[
//                 FlatButton(
//                   onPressed: () {
//                     Navigator.pop(context);
//                     Get.back();
//                   },
//                   child: Text("Okay"),
//                 )
//               ],
//             ));
//   }
//
//   List<String> offerProduct = [];
//
//   createOfferProductList() {
//     offerProduct.clear();
//
//     for (int x = 0; x < cartProduct.product.length; x++) {
//       print("OFFER ID: ${cartProduct.product[x].offerId}");
//       print("OFFER ID: ${cartProduct.product[x].productname}");
//       if (cartProduct.product[x].offerId == 0)
//         offerProduct.add("product");
//       else
//         offerProduct.add("offer");
//     }
//     print(offerProduct);
//     print("${cartProduct.product.length} lengthxxxxxx22x");
//     // calSubtotal();
//   }
//
//   Widget buildComboListFunction(int index) {
//     print("$index indexxxxxxx");
//
//     for (int x = 0; x < comboOffer.info.length; x++) {
//       if (comboOffer.info[x].id == cartProduct.product[index].offerId) {
//         comboIndex = x;
//         break;
//       }
//     }
// //    tIndex = tIndex + comboOffer.info[comboIndex].collectionProducts.length;
// //    print("$tIndex indexxxxxxx");
//     return buildComboList(comboOffer.info[comboIndex], USER_DETAILES,
//         comboOffer, comboIndex, src);
//   }
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     getStoreDetails();
//   }
//
//   int tIndex = -1;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         elevation: 8.0,
//         centerTitle: true,
//         leading: IconButton(
//           onPressed: () {
//             Navigator.pop(context);
//           },
//           icon: Icon(
//             Icons.arrow_back_ios,
//             color: Colors.white,
//           ),
//         ),
//         title: Text(
//           "Cart",
//           style: TextStyle(color: Colors.white),
//         ),
//       ),
//       body: loading
//           ? neoLoader()
//           : SingleChildScrollView(
//               padding: EdgeInsets.only(bottom: 60),
//               child: Center(
//                 child: Container(
//                   child: Column(
//                     children: <Widget>[
//                       GestureDetector(
//                         onTap: () {
//                           Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                   builder: (context) => StoreDetail(storeDetails
//                                       .data.shopDetails.id
//                                       .toString())));
//                         },
//                         child: Card(
//                           color: Colors.pink[50],
//                           elevation: 4.0,
//                           child: ListTile(
//                             leading: ClipRRect(
//                               borderRadius: BorderRadius.circular(30),
//                               child: storeDetails.data.shopDetails.logo
//                                       //       .toString()
//                                       .toString()
//                                       .isEmpty
//                                   ? Image(
//                                       image: AssetImage(
//                                           'drawables/logo_neo_mart_splash.png'),
//                                       height: 100,
//                               width:100)
//                                   : Image.network(
//                                       storeDetails.data.shopDetails.logo
//                                           .toString()
//                                           .toString(),
//                                       fit: BoxFit.cover,
//                                     ),
//                             ),
//                             // leading: ClipOval(
//                             //   child: storeDetails.data.shopDetails.logo
//                             //       .toString()
//                             //       .isEmpty ?
//                             //   Image.asset(
//                             //     'drawables/logo_neo_mart_splash.png', height: 55, width: 55,)
//                             //       : Image.network(storeDetails.data.shopDetails.logo, height: 55, width: 55,),
//                             // ),
//                             title: Text(
//                               "${storeDetails.data.shopDetails.storeName}",
//                               maxLines: 2,
//                               overflow: TextOverflow.ellipsis,
//                             ),
//                             subtitle: Text(
//                               "${cartProduct.product.length} items",
//                               maxLines: 2,
//                               overflow: TextOverflow.ellipsis,
//                             ),
//                           ),
//                         ),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(4.0),
//                         child: Text("Swipe to remove Item"),
//                       ),
//                       Card(
//                         elevation: 0.0,
//                         child: Container(
//                           child: ListView.separated(
//                             separatorBuilder: (context, index) {
//                               return Divider(height: 0);
//                             },
// //                                controller: scrollController,
//                             shrinkWrap: true,
//                             physics: NeverScrollableScrollPhysics(),
//                             itemCount: cartProduct.product.length,
//                             itemBuilder: (context, index) {
// //                        tIndex = index;
//                               tIndex++;
//                               return Slidable(
//                                   actionPane: SlidableDrawerActionPane(),
//                                   actionExtentRatio: 0.25,
//                                   movementDuration: (Duration(seconds: 3)),
//                                   secondaryActions: <Widget>[
//                                     IconSlideAction(
//                                       caption: 'Remove',
//                                       color: mColor,
//                                       icon: Icons.delete,
//                                       onTap: () => removeItem(index),
//                                     ),
//                                   ],
//                                   key: UniqueKey(),
//                                   child: cartProduct
//                                               .product[index].masterproductid ==
//                                           0
//                                       ? buildComboListFunction(index)
//                                       : cartProduct.product[index]
//                                                           .masterproductid !=
//                                                       null &&
//                                                   cartProduct.product[index]
//                                                           .offerId !=
//                                                       0 ||
//                                               cartProduct
//                                                       .product[index].offerId ==
//                                                   null
//                                           ? SizedBox()
//                                           : buildItems(
//                                               USER_DETAILES,
//                                               cartProduct.product[index],
//                                               widget.storeItemProduct,
//                                               cartProduct
//                                                   .product[index].quantity,
//                                               index,
//                                             ));
//                             },
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ),
//       floatingActionButton: loading
//           ? SizedBox()
//           : IgnorePointer(
//               ignoring: double.tryParse(cartProduct.total.cartSubtotal) == 0
//                   ? true
//                   : false,
//               child: FloatingActionButton.extended(
//                 backgroundColor: mColor,
//                 heroTag: 'checkout_button',
//                 onPressed: () async {
//                   await Navigator.push(
//                       context,
//                       MaterialPageRoute(
//                           builder: (context) => PlaceOrder(
//                               cartProduct,
//                               storeDetails,
//                               widget.storeItemProduct,
//                               double.tryParse(cartProduct.total.cartSubtotal),
//                               cartProduct.product.length)));
//                   getStoreDetails();
//                   firstTime = true;
//                 },
//                 elevation: 8.0,
//                 label: Row(children: <Widget>[
//                   Text("Proceed to checkout"),
//                   SizedBox(
//                     width: 20,
//                   ),
//                   Icon(Icons.arrow_forward)
//                 ]),
//               ),
//             ),
//     );
//   }
// }
//
// _buildItemsState buildItemState;
//
// class buildItems extends StatefulWidget {
//   User user;
//   Product product;
//   int quantity;
//   StoreItemProduct storeItemProduct;
//   int index;
//
//   buildItems(
//       this.user, this.product, this.storeItemProduct, this.quantity, this.index,
//       {Key key})
//       : super(key: key);
//
//   @override
//   _buildItemsState createState() {
//     buildItemState = _buildItemsState();
//     return buildItemState;
//   }
// }
//
// class _buildItemsState extends State<buildItems> {
//   var mColor = Color(0xFFe9991e);
//   var auth = new HttpRequests();
//   var offer_id = "", offer_price = "";
//   bool disableButton = false,
//       isOffer = false,
//       isOfferProduct = false,
//       isProduct = false;
//   double comboPrice, price;
//
//   addToCart(itemCount) async {
//     print("${widget.product.storeId} storeid");
//     print("${widget.product.merchantid} merchant");
//     await auth.addItemToCart(
//         widget.product.masterproductid,
//         widget.product.merchantlistid,
//         offer_id,
//         offer_price,
//         widget.quantity,
//         widget.product.merchantid,
//         widget.product.storeId,
//         USER_DETAILES.data.loginDetails.userId,
//         USER_DETAILES.data.loginDetails.authKey);
//
//     checkOutState.getProducts();
//   }
//
//   addItem() {
//     setState(() {
//       checkOutState.placeOrder = false;
//     });
//     print("${widget.product.totalLeft} total left");
//     if (widget.quantity >= widget.product.totalLeft) {
//       Flushbar(
//         icon: Icon(
//           Icons.cancel,
//           color: mColor,
//         ),
//         duration: Duration(seconds: 2),
//         messageText: Text(
//           "Out of stock",
//           style: TextStyle(color: mColor),
//         ),
//       )..show(context);
//     } else {
//       widget.quantity++;
//       addToCart(widget.quantity);
//     }
//   }
//
//   minusItem() {
//     print("Item ID: ${widget.product.shoppingListItemId}");
//     if (widget.quantity != 1) {
//       widget.quantity--;
//       addToCart(widget.quantity);
//     } else {
//       showDialog(
//           context: context,
//           builder: (_) => AlertDialog(
//                 content: Text("Do you want to remove this item?"),
//                 actions: <Widget>[
//                   FlatButton(
//                     onPressed: () {
//                       Get.back();
//                       checkOutState.removeItem(widget.index);
//                     },
//                     child: Text("Remove"),
//                   ),
//                   FlatButton(
//                     onPressed: () {
//                       Navigator.pop(context);
//                     },
//                     child: Text("Cancel"),
//                   )
//                 ],
//               ));
//     }
//   }
//
// //  checkOffer() {
// //    setState(() {
// //      if (widget.product.offerId != null && widget.product.masterproductid != 0)
// //        isOfferProduct = true;
// //      else if (widget.product.masterproductid == 0)
// //        isOffer = true;
// ////     else if(isOffer == false && isOfferProduct == false)
// ////       isProduct = true;
// //    });
// //  }
//
//   @override
//   void initState() {
//     print("${widget.product.totalLeft} total left");
//     // TODO: implement initState
//     super.initState();
// //    checkOffer();
//     comboPrice = widget.product.comboPrice;
//     price = widget.product.sellingPrice.toString().isEmpty
//         ? 0
//         : widget.product.sellingPrice;
//     // mrp = widget.product.pricewithtax
//     // .toString()
//     // .isEmpty ? 0 : widget.product.pricewithtax;
//     // if (mrp == price)
//     //   mrp = 0;
//     if (widget.quantity == 1)
//       setState(() {
//         disableButton = true;
//       });
//   }
//
//   Widget newProductCard() {
//     return Card(
//       elevation: 0.0,
//       child: InkWell(
//         onTap: () {},
//         child: Container(
//           padding: EdgeInsets.all(4),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               Container(
//                 height: 75,
//                 width: 75,
//                 child: Image.network(
//                   widget.product.icon,
//                   height: 75,
//                   width: 75,
//                   fit: BoxFit.contain,
//                 ),
//               ),
//               Expanded(
//                 child: Container(
//                   padding: EdgeInsets.only(left: 8, right: 8),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     children: <Widget>[
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           Expanded(
//                             child: AutoSizeText(
//                                 "₹ ${double.tryParse((price * widget.quantity).toStringAsFixed(2)).toStringAsFixed(2)}",
//                                 maxLines: 1,
//                                 minFontSize: 9,
//                                 maxFontSize: 13,
//                                 overflow: TextOverflow.ellipsis,
//                                 textAlign: TextAlign.end,
//                                 style: TextStyle(
//                                     color: mColor,
//                                     fontWeight: FontWeight.bold)),
//                           ),
//                         ],
//                       ),
//                       Text(
//                         "${widget.product.productname}",
//                         maxLines: 2,
//                         overflow: TextOverflow.ellipsis,
//                       ),
//                       Row(
//                         children: <Widget>[
//                           isOfferProduct
//                               ? SizedBox()
//                               : Text(
//                                   "${widget.quantity} X ",
//                                   maxLines: 2,
//                                   style: TextStyle(
//                                       fontSize: 12, color: Colors.black45),
//                                   overflow: TextOverflow.ellipsis,
//                                 ),
//                           isOfferProduct
//                               ? SizedBox()
//                               : AutoSizeText(
//                                   "${double.tryParse(price.toString()).toStringAsFixed(2)}",
//                                   maxLines: 1,
//                                   maxFontSize: 13,
//                                   minFontSize: 9,
//                                   style: TextStyle(color: Colors.black45),
//                                   overflow: TextOverflow.ellipsis,
//                                 )
//                         ],
//                       ),
//                       Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           SizedBox(),
//                           Container(
//                             child: Stack(
//                               children: <Widget>[
//                                 Container(
//                                   width: 110,
//                                   height: 35,
//                                   decoration: BoxDecoration(
//                                       borderRadius:
//                                           BorderRadius.all(Radius.circular(25)),
//                                       border: Border.all(color: mColor)),
//                                   child: Visibility(
//                                     visible: false,
//                                     child: OutlineButton(
//                                       textColor: mColor,
//                                       onPressed: () {},
//                                       borderSide:
//                                           BorderSide(color: Colors.transparent),
//                                       child: Text("Add"),
//                                     ),
//                                   ),
//                                 ),
//                                 Visibility(
//                                   visible: true,
//                                   child: Container(
//                                     height: 35,
//                                     width: 110,
//                                     child: Row(
//                                       mainAxisSize: MainAxisSize.min,
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: <Widget>[
//                                         Expanded(
//                                           child: Container(
//                                             height: 40,
//                                             width: 30,
//                                             child: IconButton(
// //                                            textColor: mColor,
//                                               onPressed: () {
//                                                 minusItem();
//                                               },
// //                                            borderSide:
// //                                            BorderSide(color: Colors.transparent),
//                                               icon: Icon(
//                                                 Icons.remove,
//                                                 size: 16,
//                                                 color: mColor,
//                                               ),
//                                             ),
//                                           ),
//                                         ),
//                                         Expanded(
//                                           child: Container(
//                                               width: 20,
//                                               child: Center(
//                                                   child: Text(
//                                                       "${widget.quantity}"))),
//                                         ),
//                                         Expanded(
//                                           child: Container(
//                                             height: 40,
//                                             width: 30,
//                                             child: IconButton(
// //                                            textColor: mColor,
//                                               onPressed: () {
//                                                 addItem();
//                                               },
// //                                            borderSide:
// //                                            BorderSide(color: Colors.transparent),
//                                               icon: Icon(
//                                                 Icons.add,
//                                                 size: 16,
//                                                 color: mColor,
//                                               ),
//                                             ),
//                                           ),
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 )
//                               ],
//                             ),
//                           ),
//                         ],
//                       ),
//                     ],
//                   ),
//                 ),
//               )
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget oldPCard() {
//     return Card(
//         elevation: 3.0,
//         margin: EdgeInsets.only(top: 8.0),
//         child: Container(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceBetween,
//             children: <Widget>[
//               InkWell(
//                 onTap: () {},
//                 child: Container(
//                   child: Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: <Widget>[
//                       widget.product.icon == null
//                           ? Image(
//                               image: AssetImage(
//                                   'drawables/logo_neo_mart_splash.png'),
//                               height: 100,
//                               width: 100,
//                             )
//                           : isOffer
//                               ? Image.network(
//                                   widget.product.offerImage.toString(),
//                                   height: 100,
//                                   width: 100,
//                                 )
//                               : Image.network(
//                                   widget.product.icon.toString(),
//                                   height: 100,
//                                   width: 100,
//                                 ),
//                       Container(
//                         alignment: Alignment.centerLeft,
//                         width: 250,
//                         child: ListTile(
//                             title:
// //                            isOffer ? Text(
// //                              "${widget.product.offerName}",
// //                              maxLines: 2,
// //                              overflow: TextOverflow.ellipsis,
// //                            ) :
//                                 Text(
//                               "${widget.product.productname}",
//                               maxLines: 2,
//                               overflow: TextOverflow.ellipsis,
//                             ),
//                             subtitle:
// //                            isOffer ? Text("${widget.product
// //                                .offerDescr}",
// //                              maxLines: 2,
// //                              overflow: TextOverflow.ellipsis,) :
//                                 Row(
//                               children: <Widget>[
//                                 isOfferProduct
//                                     ? SizedBox()
//                                     : Text(
//                                         "${widget.quantity} X ",
//                                         maxLines: 2,
//                                         overflow: TextOverflow.ellipsis,
//                                       ),
//                                 isOfferProduct
//                                     ? SizedBox()
//                                     : Text(
//                                         "${double.tryParse(price.toString()).toStringAsFixed(2)}",
//                                         maxLines: 1,
//                                         overflow: TextOverflow.ellipsis,
//                                       )
//                               ],
//                             ),
//                             trailing:
// //                            isOfferProduct ? SizedBox() : isOffer
// //                                ? Text(
// //                              "₹ ${comboPrice * widget
// //                                  .quantity} ",
// //                              maxLines: 2,
// //                              overflow: TextOverflow.ellipsis,
// //                              style: TextStyle(color: mColor),
// //                            )
// //                                :
//                                 Column(
//                               children: <Widget>[
//                                 Text(
//                                   "₹ ${(price * widget.quantity).toStringAsFixed(2)}",
//                                   maxLines: 1,
//                                   overflow: TextOverflow.ellipsis,
//                                   style: TextStyle(color: mColor),
//                                 ),
//                                 // mrp != 0? Text(
//                                 //   "${(mrp * widget
//                                 //       .quantity).toStringAsFixed(2)}",
//                                 //   style: TextStyle(decoration: TextDecoration.lineThrough),
//                                 //   maxLines: 1,
//                                 //   overflow: TextOverflow.ellipsis,): SizedBox()
//                               ],
//                             )),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
// //              isOfferProduct ? SizedBox() :
//               Container(
//                 padding: EdgeInsets.only(left: 4.0, right: 4.0),
//                 alignment: Alignment.bottomRight,
//                 width: MediaQuery.of(context).size.width,
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     SizedBox(),
//                     Container(
//                       margin: EdgeInsets.only(bottom: 6),
//                       decoration: BoxDecoration(
//                           borderRadius: BorderRadius.circular(3),
//                           border: Border.all(color: mColor)),
//                       height: 40,
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: <Widget>[
//                           IconButton(
// //                            disabledBorderColor: Colors.transparent,
//                             color: mColor,
//                             onPressed: () {
//                               minusItem();
//                             },
// //                            borderSide:
// //                            BorderSide(color: Colors.transparent),
//                             icon: Icon(Icons.remove),
//                           ),
//                           Container(
//                               width: 25,
//                               child: Center(
//                                   child: Text(
//                                 "${widget.quantity}",
//                                 style: TextStyle(fontSize: 13),
//                               ))),
//                           IconButton(
//                             color: mColor,
//                             onPressed: () {
//                               setState(() {
//                                 addItem();
//                               });
//                             },
// //                            borderSide:
// //                            BorderSide(color: Colors.transparent),
//                             icon: Icon(Icons.add),
//                           ),
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ],
//           ),
//         ));
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return newProductCard();
//   }
// }
