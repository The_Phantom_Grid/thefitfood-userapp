import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/MainApp.dart';
import 'package:userapp/Screens/MyOrders.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:userapp/Components/rating_icon_icons.dart';

import '../Getters/GetStoreDetails.dart';

class OrderPlaced extends StatefulWidget {
  int orderId;
  StoreDetails storeDetails;
  String timeSlot, activeAddress;
  bool pickUp;

  OrderPlaced(this.orderId, this.activeAddress, this.timeSlot,
      this.storeDetails, this.pickUp);

  @override
  _OrderPlacedState createState() => _OrderPlacedState();
}

class _OrderPlacedState extends State<OrderPlaced> {

  double rating = 3;
  Widget rateIcon = Image.asset(
    'drawables/good.png',
    height: 80,
    width: 80,
  );
  String dimension;
  HttpRequests auth = HttpRequests();

  setRating(int val) {
    setState(() {
      if (val == 0) {
        rateIcon = Image.asset(
          'drawables/miserable.png',
          height: 80,
          width: 80,
        );
        dimension = "miserable";
      } else if (val < 2) {
        rateIcon = Image.asset(
          'drawables/sad.png',
          height: 80,
          width: 80,
        );
        dimension = "sad";
      } else if (val < 3) {
        rateIcon = Image.asset(
          'drawables/good.png',
          height: 80,
          width: 80,
        );
        dimension = "good";
      } else if (val < 4) {
        rateIcon = Image.asset(
          'drawables/awesome.png',
          height: 80,
          width: 80,
        );
        dimension = "awesome";
      } else if (val < 5) {
        rateIcon = Image.asset(
          'drawables/marvellous.png',
          height: 80,
          width: 80,
        );
        dimension = "marvellous";
      }
    });
  }

  showRateAlert() {
    showDialog(
        context: context,
        builder: (_) => StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return AlertDialog(
                  title: Text(
                    "Rate Your Experience",
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center,
                  ),
                  content: Container(
                      height: 150,
                      width: MediaQuery.of(context).size.width - 100,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          rateIcon,
                          Slider(
                            min: 0,
                            max: 4,
                            value: rating,
                            onChanged: (val) {
                              print(val.toInt());
                              setState(() {
                                rating = val;
                              });
                              setRating(val.toInt());
                            },
                          ),
                        ],
                      )),
                  actions: <Widget>[
                    ButtonTheme(
                      height: 40,
                      minWidth: MediaQuery.of(context).size.width,
                      child: OutlineButton(
                        onPressed: () {
                          Navigator.pop(context);
                          giveFeedback();
                        },
                        child: Text("Submit"),
                        borderSide: BorderSide(color: mColor),
                        textColor: mColor,
                      ),
                    )
                  ],
                );
              },
            ));
  }

  giveFeedback() async {
    var r = await auth.giveFeedback("", dimension, widget.orderId, rating,
        USER_DETAILES.data.loginDetails.authKey);

    if (r['message'].toString().contains("Thank you for your feedback.")) {
      Flushbar(
        icon: Icon(Icons.insert_emoticon, color: mColor),
        messageText: Text(
          "Thank you for your feedback.",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(seconds: 2),
      )..show(context);
    }
  }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState() {
    super.initState();
    print("PICKUP");
    print(widget.pickUp);
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var android = AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: true,
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initSettings = InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: onSelectNotification);
//    if(Theme.of(context).platform == TargetPlatform.iOS)
//      isIOSDevice = true;

    print("kghjgkhgkhgkjhgkjhgkj");
//    showRateAlert();
    setRating(rating.toInt());
    Future.delayed(Duration(seconds: 2)).then((_) {
      showRateAlert();
      showNotification();
    });
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
              })
        ],
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    print('Working on select');
    print('PAYLOAD: $payload');
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyOrders()));
  }

  showNotification() async {
    var android = AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION');

    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(android, iOS);
    print("${platform.iOS.toString()} platform");

    await flutterLocalNotificationsPlugin.show(
        0,
        "Order Placed",
        "Thanks for shopping! You order has been placed on ${widget.storeDetails.data.shopDetails.storeName}.\n Your order id is ${widget.orderId}.",
        platform);
  }

  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => MainApp()),
            (Route<dynamic> route) => false);
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Text("Order Placed"),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.close,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(builder: (context) => MainApp()),
                  (Route<dynamic> route) => false);
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Your order has been placed.",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                  textAlign: TextAlign.left,
                ),
                Padding(
                  padding: EdgeInsets.all(15),
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Center(
                            child: Container(
                          height: 70,
                          width: 70,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(color: Colors.green, width: 2),
                              shape: BoxShape.circle),
                          child: Icon(
                            Icons.check,
                            color: Colors.green,
                            size: 50,
                          ),
                        )),
                        SizedBox(
                          height: 15,
                        ),
                        Text(
                          "Order id: #${widget.orderId}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        Container(
                          width: double.infinity,
                          child: Card(
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 5,
                                ),
                                Text("Shipping Detail"),
                                Divider(),
                                Text(
                                  "${widget.storeDetails.data.shopDetails.storeName}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 16),
                                  textAlign: TextAlign.left,
                                ),
                                Divider(
                                  indent: 30,
                                  endIndent: 30,
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                widget.pickUp
                                    ? Text("Pick up")
                                    : Text("Home Delivery"),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 4, right: 4),
                                  child: Text(
                                    "${widget.activeAddress}",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                ),
                                Divider(
                                  indent: 30,
                                  endIndent: 30,
                                ),
                                Text("Date & Time"),
                                SizedBox(
                                  height: 15,
                                ),
                                Text(
                                  "${widget.timeSlot}",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        extendBody: true,
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          color: Colors.white30,
          child: Container(
            height: 60,
            padding: EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: ButtonTheme(
                    height: 100,
                    child: RaisedButton(
                      child: Text("Continue Shopping"),
                      onPressed: () {
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(builder: (context) => MainApp()),
                            (Route<dynamic> route) => false);
                      },
                      color: Colors.green,
                      textColor: Colors.white,
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(27)),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 1,
                  child: ButtonTheme(
                    height: 100,
                    child: RaisedButton(
                      child: Text("My Orders"),
                      onPressed: () {
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(builder: (context) => MyOrders()),
                            (Route<dynamic> route) => false);
                      },
                      color: Colors.white,
                      textColor: Colors.green,
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(27)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
