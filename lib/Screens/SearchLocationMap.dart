import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:search_map_place/search_map_place.dart';
import 'package:userapp/Getters/GetAddress.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../Components/CommonUtility.dart';
import '../Components/CommonUtility.dart';
import '../Components/CommonUtility.dart';
import '../constants.dart';

class SearchLocationMap extends StatefulWidget {
  String addressId;
  Details details;
  bool edit;

  SearchLocationMap(this.addressId, this.details, this.edit);

  @override
  _SearchLocationMapState createState() => _SearchLocationMapState();
}

class _SearchLocationMapState extends State<SearchLocationMap> {

//  Completer<GoogleMapController> gController = Completer();
  LatLng cameraTarget, initialPosition;
  bool loading = true,
      _loading = false,
      ignoreButton = false,
      editAddress = false,
      expandSheet = false;
  GoogleMapController gController;
  String placeInfo = "Your location";
  var house = TextEditingController(),
      streetName = TextEditingController(),
      fullNAme = TextEditingController(),
      pincode = TextEditingController(),
      city = TextEditingController(),
      state = TextEditingController(),
      landmark = TextEditingController();
  List<Address> addresses;
  var first;
  String full_address = "";
  HttpRequests auth = HttpRequests();
  var marker = [].toSet();
  String androidApiKey = "AIzaSyAa2HHkeEv6nUPQ8REDxnbBtiY1FjrRorA",
      iosApiKey = "AIzaSyCflFUrYBUOu-X-Izx0voYqtdaXsuq4SVQ";

  onCameraMove(CameraPosition position) {
    cameraTarget = position.target;
//    markerPosition = cameraTarget;
  }

  getLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        initialPosition = LatLng(position.latitude, position.longitude);
        cameraTarget = initialPosition;
//        marker = [
//          Marker(
//              markerId: MarkerId('cur_loc'),
//              infoWindow: InfoWindow(title: placeInfo),
//              position: cameraTarget)
//        ].toSet();
//        markerPosition = cameraTarget;
//        loading = false;
        getFullAddress();
      });
    }).catchError((e) {
      if (e.toString().contains("PERMISSION_DENIED")) {
//        showUpdateLocation();
      }
    });
  }

  getCurrentLocation() {
    setState(() {
      _loading = true;
    });
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        cameraTarget = LatLng(position.latitude, position.longitude);
//        markerPosition = cameraTarget;
        print(
            "Current location CAMERA TARGET: ${cameraTarget.latitude} || ${cameraTarget.longitude}");
      });
      getFullAddress();
    }).catchError((e) {
      if (e.toString().contains("PERMISSION_DENIED")) {
//        showUpdateLocation();
      }
    });
  }

  getFullAddress() async {
    print(
        "FULL ADDRESS CAMERA TARGET: ${cameraTarget.latitude} || ${cameraTarget.longitude}");
    final coordinates =
        Coordinates(cameraTarget.latitude, cameraTarget.longitude);
    addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    setState(() {
      first = addresses.first;
      placeInfo = first.featureName;
      marker = [
        Marker(
            markerId: MarkerId('cur_loc'),
            infoWindow: InfoWindow(title: placeInfo),
            position: cameraTarget)
      ].toSet();
      _loading = false;
      initTextControllers();
    });
    if (gController != null)
      gController.animateCamera(CameraUpdate.newLatLng(cameraTarget));
  }

  initTextControllers() {
    setState(() {
//      expandSheet = true;
//      house.text =
//          "${first.featureName} , ${first.subLocality}".replaceAll('null', "");
      streetName.text =
          "${first.featureName} , ${first.locality}".replaceAll('null', "");
//      streetName.text = "${first.thoroughfare} , ${first.locality}".replaceAll('null', "");
      pincode.text = "${first.postalCode}".replaceAll('null', "");
      city.text = "${first.subAdminArea}".replaceAll('null', "");
      state.text = "${first.adminArea}".replaceAll('null', "");
//      print("Address Line" + first.addressLine);
      loading = false;
    });
  }

  validateFields() {
    if (house.text.isEmpty ||
        streetName.text.isEmpty ||
        pincode.text.isEmpty ||
        city.text.isEmpty ||
        fullNAme.text.isEmpty ||
        state.text.isEmpty) {
      setState(() {
//        ignoreButton = false;
        showDialog(
            context: context,
            builder: (_) => AlertDialog(
                  content: Text(
                      "Mandatory fields incomplete. Please review and resubmit"),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: Text("OK"),
                    )
                  ],
                ));
      });
    } else {
      if (editAddress)
        updateAddress();
      else
        addNewAddress();
    }
  }

  addNewAddress() async {
    print("ADDINGGG");
//    if(lat == 0.0 && lng == 0.0) {
//      addingAddress = true;
//      await getLocation();
//    }
    full_address =
        "${house.text}, ${streetName.text}, ${city.text} - ${pincode.text}, ${state.text}";
    var r = await auth.addNewAddress(
        full_address,
        city.text,
        streetName.text,
        house.text,
        fullNAme.text,
        landmark.text,
        cameraTarget.latitude,
        cameraTarget.longitude,
        pincode.text,
        state.text,
        TOKEN,
        USER_ID);
    if (r['message'] == "Address already saved") {
      Flushbar(
        duration: Duration(seconds: 2),
        message: "Address already saved",
      )..show(context);
      setState(() {
        ignoreButton = false;
      });
    } else {
      Navigator.pop(context);
      Flushbar(
        duration: Duration(seconds: 3),
        flushbarPosition: FlushbarPosition.BOTTOM,
        message: "Address Saved",
      )..show(context);
//      Navigator.pop(context);
    }
//    setState(() {
//      showDialog(
//          context: context,
//          builder: (_) => AlertDialog(
//            content: Text("${r['Address already saved']}"),
//          ));
//    });
  }

  updateAddress() async {
    print("UPDATING");
    print("LAT:: ${cameraTarget.latitude} || LONg:: ${cameraTarget.longitude}");
    full_address =
        "${house.text}, ${streetName.text}, ${city.text} - ${pincode.text}, ${state.text}";
    print(widget.addressId);
    var r = await auth.updateAddress(
        full_address,
        widget.addressId,
        city.text,
        streetName.text,
        house.text,
        fullNAme.text,
        landmark.text,
        cameraTarget.latitude,
        cameraTarget.longitude,
        pincode.text,
        state.text,
        TOKEN,
        USER_ID);
    print("MESSAGE: ${r['message']}");

    if (r['message'].toString().contains(widget.addressId)) {
      Navigator.pop(context);
      Flushbar(
        duration: Duration(seconds: 2),
        flushbarPosition: FlushbarPosition.TOP,
        message: "Address Updated",
      )
        ..show(context);
    }
  }

  fillAddress() {
    print("LAT:  ${widget.details.lat} | LONg:: ${widget.details.lng}");
    setState(() {
      if (widget.edit) {
        var lat = double.tryParse(widget.details.lat.toString());
        var lng = double.tryParse(widget.details.lng.toString());
        initialPosition = LatLng(lat, lng);
        cameraTarget = initialPosition;
        marker = [
          Marker(
              markerId: MarkerId('cur_loc'),
              infoWindow: InfoWindow(title: placeInfo),
              position: cameraTarget)
        ].toSet();
        house.text = widget.details.flatHouseno;
        streetName.text = widget.details.colonyStreet;
        state.text = widget.details.state;
        pincode.text = widget.details.pincode.toString();
        fullNAme.text = widget.details.fullName;
        city.text = widget.details.city;
        loading = false;
//        pincode.text = cAddress.;
//        house.text = cAddress.split(',') as String;
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.addressId != null) {
      editAddress = true;
      fillAddress();
    } else
      getLocation();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    gController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: _loading,
      child: Stack(
        children: <Widget>[
          Scaffold(
              appBar: AppBar(
                backgroundColor: mColor,
                centerTitle: true,
                title: editAddress
                    ? Text(
                  "Edit Address",
                  style: TextStyle(color: Colors.white),
                )
                    : Text(
                  "Create New Address",
                  style: TextStyle(color: Colors.white),
                ),
                leading: IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_ios),
                  color: Colors.white,
                ),
              ),
              body: loading
                  ?  neoLoader()
                  : SizedBox.expand(
                      child: Stack(
                        children: <Widget>[
                          GoogleMap(
                            rotateGesturesEnabled: true,
                            zoomGesturesEnabled: true,
                            scrollGesturesEnabled: true,
                            initialCameraPosition: CameraPosition(
                                target: initialPosition,
                                zoom: 14.4746,
                                bearing: 30,
                                tilt: 40),
                            onTap: (LatLng value) {
                              setState(() {
                                cameraTarget = value;
                              });
                              getFullAddress();
                              //                  gController.animateCamera(CameraUpdate.newLatLng(value));
                            },
                            markers: marker,
                            onCameraMove: onCameraMove,
                            mapToolbarEnabled: true,
                            myLocationEnabled: true,
                            myLocationButtonEnabled: true,
                            mapType: MapType.normal,
                            onMapCreated: (GoogleMapController controller) {
                              gController = controller;
                            },
                          ),
                          Positioned(
                            top: 15,
                            right: 15,
                            left: 15,
                            child: SearchMapPlaceWidget(
                              hasClearButton: true,
//                  location: LatLng(cameraTarget.latitude, cameraTarget.longitude),
//                  radius: 30000,
                              placeType: PlaceType.address,
                              apiKey:
                                  Platform.isIOS ? iosApiKey : androidApiKey,
                              onSelected: (Place place) async {
                                final geolocation = await place.geolocation;
                                setState(() {
                                  cameraTarget = geolocation.coordinates;
                                });
//                          cameraTarget = geolocation.coordinates;
//                            cameraTarget = geolocation.coordinates;
//                            markerPosition = cameraTarget;
//                            gController.animateCamera(
//                                CameraUpdate.newLatLng(geolocation.coordinates));
//                            gController.animateCamera(
//                                CameraUpdate.newLatLngBounds(geolocation.bounds, 0));

//                            placeInfo = place.description.toString();
                                getFullAddress();
                              },
                            ),
                          ),
                          SizedBox.expand(
                            child: DraggableScrollableSheet(
                              maxChildSize: 1,
                              minChildSize: .1,
                              initialChildSize: .25,
                              builder: (context, scrollController) {
                                return Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 8),
                                  decoration: BoxDecoration(
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.blueGrey,
                                            blurRadius: 10.0)
                                      ],
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(18),
                                          topRight: Radius.circular(18))),
                                  child: SingleChildScrollView(
                                    controller: scrollController,
                                    child: Column(
                                      children: <Widget>[
                                        Column(
                                          children: <Widget>[
                                            Center(
                                              child: Column(
                                                children: <Widget>[
                                                  Align(
                                                    alignment:
                                                        Alignment.topCenter,
                                                    child: FlatButton(
                                                      color: Colors.blue,
                                                      textColor: Colors.white,
                                                      onPressed: () {
                                                        getCurrentLocation();
                                                      },
                                                      child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: <Widget>[
                                                            Icon(
                                                              Icons.my_location,
                                                              color:
                                                                  Colors.white,
                                                            ),
                                                            SizedBox(
                                                              width: 10,
                                                            ),
                                                            Flexible(
                                                              child: Text(
                                                                "Get Current Location",
                                                              ),
                                                            )
                                                          ]),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(0)),
                                              child: Container(
                                                padding: EdgeInsets.all(15),
                                                child: Column(
                                                  children: <Widget>[
                                                    Align(
                                                      alignment:
                                                          Alignment.topRight,
                                                      child: RichText(
                                                        text: TextSpan(
                                                            children: <
                                                                TextSpan>[
                                                              TextSpan(
                                                                  text: "*",
                                                                  style: TextStyle(
                                                                      color:
                                                                      Colors
                                                                          .red)),
                                                              TextSpan(
                                                                  text:
                                                                  " Mandatory fields",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .black)),
                                                            ]),
                                                      ),
                                                    ),
                                                    TextField(
                                                      controller: house,
                                                      maxLength: 50,
                                                      scrollPadding:
                                                          EdgeInsets.all(8.0),
                                                      inputFormatters: [
                                                        WhitelistingTextInputFormatter(
                                                            RegExp(
                                                                "[a-zA-Z 0-9 ,-/]"))
                                                      ],
                                                      decoration:
                                                          InputDecoration(
                                                        counterText: "",
//                                            labelText: "House No. / Flat No./ Building Name*",
                                                        hintText:
                                                            "House No. / Flat No./ Building Name*",
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                    TextField(
                                                      maxLength: 40,
                                                      controller: streetName,
                                                      scrollPadding:
                                                          EdgeInsets.all(8.0),
                                                      inputFormatters: [
                                                        WhitelistingTextInputFormatter(
                                                            RegExp(
                                                                "[a-zA-Z 0-9 ,-/]"))
                                                      ],
                                                      decoration:
                                                          InputDecoration(
                                                        counterText: "",
                                                        hintText:
                                                            "Colony/ Street Name*",
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                    TextField(
                                                      enabled: false,
                                                      controller: pincode,
                                                      maxLength: 6,
                                                      onChanged: (value) {
                                                        if (value.length == 6) {
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  FocusNode());
                                                        }
                                                      },
                                                      maxLengthEnforced: true,
                                                      inputFormatters: [
                                                        WhitelistingTextInputFormatter(
                                                            RegExp("[0-9]"))
                                                      ],
                                                      keyboardType: TextInputType
                                                          .numberWithOptions(),
                                                      scrollPadding:
                                                          EdgeInsets.all(8.0),
                                                      decoration:
                                                          InputDecoration(
                                                        counterText: "",
                                                        hintText: "Pincode*",
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                    Row(
                                                      children: <Widget>[
                                                        Expanded(
                                                          flex: 1,
                                                          child: TextField(
                                                            enabled: false,
                                                            controller: city,
                                                            maxLength: 30,
                                                            scrollPadding:
                                                                EdgeInsets.all(
                                                                    8.0),
                                                            inputFormatters: [
                                                              WhitelistingTextInputFormatter(
                                                                  RegExp(
                                                                      "[a-zA-Z 0-9 - ,]"))
                                                            ],
                                                            decoration:
                                                                InputDecoration(
                                                              counterText: "",
                                                              hintText: "City*",
                                                            ),
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: 15,
                                                        ),
                                                        Expanded(
                                                          flex: 1,
                                                          child: TextField(
                                                            enabled: false,
                                                            maxLength: 30,
                                                            controller: state,
                                                            scrollPadding:
                                                                EdgeInsets.all(
                                                                    8.0),
                                                            inputFormatters: [
                                                              WhitelistingTextInputFormatter(
                                                                  RegExp(
                                                                      "[a-zA-Z 0-9 - ,]"))
                                                            ],
                                                            decoration:
                                                                InputDecoration(
                                                              counterText: "",
                                                              hintText:
                                                                  "State*",
                                                            ),
                                                          ),
                                                        )
                                                      ],
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                    TextField(
                                                      maxLength: 30,
                                                      controller: landmark,
                                                      scrollPadding:
                                                          EdgeInsets.all(8.0),
                                                      decoration:
                                                          InputDecoration(
                                                        counterText: "",
                                                        hintText:
                                                            "Landmark (optional)",
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Card(
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(0)),
                                              child: Container(
                                                padding: EdgeInsets.all(15),
                                                child: Column(
                                                  children: <Widget>[
                                                    TextField(
                                                      maxLength: 30,
                                                      controller: fullNAme,
                                                      scrollPadding:
                                                          EdgeInsets.all(8.0),
                                                      inputFormatters: [
                                                        WhitelistingTextInputFormatter(
                                                            RegExp(
                                                                "[a-zA-Z 0-9 - ,]"))
                                                      ],
                                                      decoration:
                                                          InputDecoration(
                                                        counterText: "",
                                                        hintText: "Full Name*",
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 15,
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        ButtonTheme(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(25)),
                                            minWidth: 300,
                                            height: 45.0,
                                            child: IgnorePointer(
                                              ignoring: false,
                                              child: RaisedButton(
                                                child: Text("Submit"),
                                                onPressed: () {
//                                      Navigator.pop(context);
                                                  setState(() {
                                                    ignoreButton = true;
                                                  });
                                                  print(house.text);
                                                  validateFields();
                                                },
                                                textColor: Colors.white,
                                                color: mColor,
                                              ),
                                            )),
                                        SizedBox(
                                          height: 30,
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ),
                          )
                        ],
                      ),
                    )),
          Visibility(
            visible: _loading,
            child: neoLoader(),
          )
        ],
      ),
    );
  }
}
