import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:userapp/Getters/GetReferralCode.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:wc_flutter_share/wc_flutter_share.dart';

import '../Components/CommonUtility.dart';
import '../constants.dart';

class ReferralPage extends StatefulWidget {
  User user;

  ReferralPage(this.user);

  @override
  _ReferralPageState createState() => _ReferralPageState();
}

class _ReferralPageState extends State<ReferralPage> {
  HttpRequests auth = HttpRequests();
  String rCode;
  String paytmNo = "";
  ReferralCode referral;
  bool _loading = true;
  String referUrl = "https://www.neomart.com/frontend/web/put?c=";

  getreferralCode() async {
    var r = await auth.getReferral(widget.user.data.loginDetails.authKey);
    setState(() {
      referral = ReferralCode.fromJson(r);
      if (r['status'] == "success") {
        _loading = false;
        rCode = referral.data[0].referalCode;
        paytmNo = referral.data[0].paytmNo;
        print(rCode);
      }
    });
  }

  Copy2Clipboard() {
    Clipboard.setData(ClipboardData(
        text: "https://www.neomart.com/frontend/web/put?c=${rCode}"));
    Flushbar(
      message: "Referral Link Copied",
      duration: Duration(seconds: 1),
    )..show(context);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getreferralCode();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: _loading
            ? Text(
                "Loading...",
                style: TextStyle(color: Colors.white),
              )
            : Text(
                "${referral.title}",
                style: TextStyle(color: Colors.white),
              ),
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: _loading == true
          ?  neoLoader()
          : Center(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 15,
                  ),
                  Image.asset(
                    'drawables/piggy_icon.png',
                    height: 100,
                    width: 100,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                      child: Container(
                    padding: EdgeInsets.all(16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          width: 50,
                        ),
                        GestureDetector(
                          onTap: Copy2Clipboard,
                          child: Column(
                            children: <Widget>[
                              Text(
                                "${rCode}",
                                style: TextStyle(color: mColor, fontSize: 22),
                              ),
                              Text("Tap to copy")
                            ],
                          ),
                        ),
                        IconButton(
                          icon: Icon(Icons.share),
                          onPressed: () async {
                            ByteData bytes = await rootBundle
                                .load('assets/default-image.jpg');
                            await WcFlutterShare.share(
                                sharePopupTitle: 'Share',
                                subject: 'Refer & Earn',
                                text:
                                    "Click on the url and download the app to enjoy our services.\n"
                                    "$referUrl$rCode",
                                fileName: 'thefitfood.jpg',
                                mimeType: 'image/png',
                                bytesOfFile: bytes.buffer.asUint8List());
                            // WcFlutterShare.share(
                            //     sharePopupTitle: 'Share',
                            //     mimeType: 'text/plain',
                            //     text:
                            //         "Click on the url and download the app to enjoy our services.\n"
                            //         "$referUrl$rCode",
                            //     subject: "");
                            print("hit..");
                          },
                        )
                      ],
                    ),
                  )),
                  Card(
                    child: Padding(
                        padding: const EdgeInsets.only(top: 8, bottom: 8),
                        child: ListView.separated(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return ListTile(
                                leading: Icon(
                                  Icons.arrow_right,
                                  color: mColor,
                                ),
                                title: Text("${referral.content[index]}"),
                              );
                            },
                            separatorBuilder: (context, index) {
                              return SizedBox(
                                height: 4,
                              );
                            },
                            itemCount: referral.content.length)),
                  ),
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          SizedBox(
                            width: 50,
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "${paytmNo}",
                                style: TextStyle(color: mColor, fontSize: 22),
                              ),
                              Text("Your Paytm Number")
                            ],
                          ),
                          SizedBox(
                            width: 50,
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
    );
  }
}
