import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Screens/Checkout.dart';
import 'package:userapp/Getters/GetMyCart.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../constants.dart';

class MyCart extends StatefulWidget {
  @override
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
//  var mColor = Color(0xFFe9991e);

//  var mColor = Color(0xFFe9991e);
//  var mColor = Color(0xFFac1647);
  var auth = HttpRequests();
  MyCartItems myCartItems;
  bool loading = true;

  getCart() async {
    var r = await auth.getMyCart(USER_DETAILES.data.loginDetails.userId,
        USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      myCartItems = MyCartItems.fromJson(r);
      myCartItems.product.forEach((element) {
        print("${element.minOrder} || ${element.storeName}}");
      });
      loading = false;
    });
  }

  removeFromCart(var storeId, var index) async {
    var r = await auth.removeStore(
        storeId, USER_DETAILES.data.loginDetails.authKey);

    print(storeId);

    setState(() {
      if (r['message'] == "success") {
        myCartItems.product.removeAt(index);
        Flushbar(
          message: "Store Removed from the Cart.",
          duration: Duration(seconds: 2),
        )..show(context);
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCart();
  }

  showAlertDialogue(int index) {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              content: Text("Do you want to remove this item?"),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    removeFromCart(myCartItems.product[index].storeid, index);
                  },
                  child: Text("Remove"),
                ),
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Cancel"),
                )
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    BoxDecoration _boxGradient = BoxDecoration(
        gradient: LinearGradient(
    //  colors: [mColor, mColor],
      begin: FractionalOffset(0.0, 0.2),
      end: FractionalOffset(0.8, 0.0),
    //  stops: [0.0, 1.0],
    ));

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: mColor,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
        ),
        title: Text(
          "My Cart",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: loading
          ? neoLoader()
          : myCartItems.product.length == 0
              ? Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'drawables/start_shopping.png',
                        height: 150,
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text("Your Cart looks empty."),
                      SizedBox(
                        height: 10,
                      ),
                      ButtonTheme(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25)),
                        height: 40,
                        minWidth: 200,
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Start Shopping"),
                          color: mColor,
                          textColor: Colors.white,
                        ),
                      )
                    ],
                  ),
                )
              : Container(
                  child: ListView.builder(
                    padding: EdgeInsets.all(4),
                    itemExtent: 120,
                    itemCount: myCartItems.product.length,
                    itemBuilder: (context, index) {
                      return Slidable(
                        actionPane: SlidableDrawerActionPane(),
                        actionExtentRatio: 0.25,
                        movementDuration: (Duration(seconds: 3)),
                        secondaryActions: <Widget>[
                          IconSlideAction(
                              caption: 'Remove',
                              color:  mColor,
                              icon: Icons.delete,
                              onTap: () {
                                showAlertDialogue(index);
                              })
                        ],
                        key: UniqueKey(),
                        child: GestureDetector(
                          onTap: () async {
                            // await Navigator.push(
                            //     context,
                            //     MaterialPageRoute(
                            //         builder: (context) =>
                            //             Checkout(myCartItems.product[index])));
                            getCart();
                          },
                          child: Card(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                                side: BorderSide(color: mColor)),
                            key: UniqueKey(),
                            elevation: 12.0,
                            child: Center(
                              child: ListTile(
                                contentPadding:
                                    EdgeInsets.only(left: 12, right: 10),
                                leading: Container(
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(30),
                                      border: Border.all(color: mColor)),
                                  height: 55,
                                  width: 55,
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(30),
                                    child: myCartItems.product[index].storeIcon
                                            .toString()
                                            .isEmpty
                                        ? Image(
                                            image: AssetImage(
                                                'drawables/logo_neo_mart_splash.png'),

                                        height: 100,
                                        width:100)
                                        : Image.network(
                                            myCartItems.product[index].storeIcon
                                                .toString(),
                                            fit: BoxFit.cover,
                                          ),
                                  ),
                                ),
                                title: Text(
                                  "${myCartItems.product[index].storeName}",
                                  maxLines: 3,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                subtitle: Text(
                                    "${myCartItems.product[index].totalItems} items"),
                                trailing: AutoSizeText(
                                  "₹${double.tryParse(myCartItems.product[index].price.toString()).toStringAsFixed(2)}",
                                  style: TextStyle(color: mColor),
                                  maxFontSize: 18,
                                  minFontSize: 12,
                                ),
                              ),
                            ),
                          ),
                        ),

//              buildCartItems(
//                USER_DETAILES,
//                myCartItems.product[index].storeid,
//                myCartItems.product[index].storeIcon,
//                myCartItems.product[index].storeName,
//                myCartItems.product[index].totalItems,
//                myCartItems.product[index].price,
//                myCartItems.product[index],
//              ),
                      );
                    },
                  ),
                ),
    );
  }
}

//class buildCartItems extends StatefulWidget {
//  User user;
//  final storeid;
//  final storeIcon;
//  final storeName;
//  int totalItems;
//  final price;
//  StoreItemProduct product;
//
//  buildCartItems(this.user, this.storeid, this.storeIcon, this.storeName,
//      this.totalItems, this.price, this.product);
//
//  @override
//  _buildCartItemsState createState() => _buildCartItemsState();
//}
//
//class _buildCartItemsState extends State<buildCartItems> {
//  var mColor = Color(0xFFe9991e);
//
////  var mColor = Color(0xFFe9991e);
////  var mColor = Color(0xFFac1647);
//
//  @override
//  Widget build(BuildContext context) {
//    return InkWell(
//      onTap: () async {
//        await Navigator.push(
//            context,
//            MaterialPageRoute(
//                builder: (context) =>
//                    Checkout(widget.product, USER_DETAILES)));
//      },
//      child: Card(
//        key: UniqueKey(),
//        elevation: 12.0,
//        child: Center(
//          child: ListTile(
//            contentPadding: EdgeInsets.only(left: 12, right: 10),
//            leading: ClipOval(
//              child: Container(
//                height: 60,
//                width: 60,
//                child: widget.storeIcon
//                    .toString()
//                    .isEmpty
//                    ? Image(
//                  image: AssetImage('drawables/logo_neo_mart_splash.png'),
//                  fit: BoxFit.fitWidth,
//                )
//                    : Image.network(
//                  widget.storeIcon.toString(),
//                  fit: BoxFit.fitWidth,
//                ),
//              ),
//            ),
//            title: Text("${widget.storeName}", maxLines: 3,
//              overflow: TextOverflow.ellipsis,),
//            subtitle: Text("${widget.totalItems} items"),
//            trailing: Text(
//              "₹${widget.price}",
//              style: TextStyle(color: mColor, fontSize: 18),
//            ),
//          ),
//        ),
//      ),
//    );
//  }
//}
