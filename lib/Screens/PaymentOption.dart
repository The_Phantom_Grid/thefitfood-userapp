import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:paytm/paytm.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetPaytm.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:http/http.dart' as http;

import '../constants.dart';
import 'OrderPlacedSuccessfully.dart';
import 'TxnComplete.dart';
import 'TxnFail.dart';

class PaymentOption extends StatefulWidget {
  String grandTotal;
  List<String> orderInfo;

  PaymentOption(this.grandTotal, this.orderInfo);

  @override
  _PaymentOptionState createState() => _PaymentOptionState();
}

class _PaymentOptionState extends State<PaymentOption> {
  HttpRequests requests = HttpRequests();
  bool load = true,
      paytmAvail = false,
      // razorPayAvail = false,
      placingOrder = false,
      ignore = false;
  int paytmPaymentMode = 2, _paymentMode = 1;
  PaytmWallet paytmInfo;
  String ORDER_ID, deliveryAddress, tSlot, paymentType;
  int deliveryTypeOption;

  getPaymentOptions() async {
    var response = await requests.getPaymentMethod();
    print("RESPOSNSE >>>> $response");
    if (response != null) {
      setState(() {
        // if (response['total']['razorpay_avail'] == 1) razorPayAvail = true;
        if (response['total']['paytm_avail'] == 1) paytmAvail = true;
        load = false;
      });
    } else {
      setState(() {
        load = false;
      });
    }
  }

  placeOrder() async {
    setState(() {
      placingOrder = true;
      ignore = true;
    });

    var response = await requests.placeCustomerOrder(
      widget.orderInfo[0],
      widget.orderInfo[1],
      widget.orderInfo[2],
      widget.orderInfo[3],
      widget.orderInfo[4],
      widget.orderInfo[5],
      _paymentMode == 1 ? "CASH" : "Paytm",
      widget.orderInfo[7],
      widget.orderInfo[8],
      widget.orderInfo[9],
      widget.orderInfo[10],
      widget.orderInfo[11],
      widget.orderInfo[12],
    );
    print("RESPONSE>>>> $response");

    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      paytmInfo = PaytmWallet.fromJson(response);
      ORDER_ID = response['orderid'].toString();
      if (_paymentMode == 1) {
        print("TSLOT>>> $tSlot");
        Get.offAll(OrderPlacedSuccessfully(
            ORDER_ID, deliveryAddress, deliveryTypeOption, tSlot.toString()));
      } else {
        generateTxnToken();
      }
    } else {
      setState(() {
        placingOrder = false;
      });
      showToast("Something went wrong. Please try again", Colors.black);
    }
  }

  void generateTxnToken() async {
    String mid = paytmInfo.paytm.mID,
        channelId = paytmInfo.paytm.cHANNELID,
        industryTypeId = paytmInfo.paytm.iNDUSTRYTYPEID,
        website = paytmInfo.paytm.wEBSITE,
        merchantKey = paytmInfo.paytm.MERCHANT_KEY.toString(),
        amount = paytmInfo.paytm.tXNAMOUNT,
        orderId = paytmInfo.paytm.oRDERID,
        custId = paytmInfo.paytm.cUSTID,
        callbackUrl = paytmInfo.paytm.cALLBACKURL,
        checkSum = paytmInfo.paytm.checksum;
//
//    print(callBackUrl);
    merchantKey = Uri.encodeComponent(merchantKey);
    print("merchant KEy = $merchantKey");
    String pMode = "0";
    switch (paytmPaymentMode) {
      case 1:
        pMode = "0";
        break;
      case 2:
        pMode = "2";
        break;
      case 3:
        pMode = "3";
        break;
      case 4:
        pMode = "1";
        break;
      default:
        pMode = "0";
    }
    var url = 'https://sleepy-dusk-58079.herokuapp.com/generateToken' +
        "?mid=" +
        mid +
        "&key_secret=" +
        merchantKey +
        "&website=" +
        website +
        "&orderId=" +
        orderId +
        "&amount=" +
        widget.grandTotal +
        "&callbackUrl=" +
        callbackUrl +
        "&custId=" +
        custId +
        "&mode=" +
        pMode;
    print("url::: $url");
    print("orderId::: $orderId");
    final response = await http.get(url);
    String txnToken = response.body;
    print("token::: $txnToken");
    print("RESPONSE CODE::: ${response.statusCode}");

    if (response.statusCode != 200) {
      print("payment_resp: you are here");
      await requests.setOrderPaymentStatus(
          "Unpaid",
          orderId
      );
      String resMsg = "Something went wrong.";
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(
              builder: (context) => TxnFail(DateTime.now(), resMsg, MERCHANT_ID,
                  orderId, widget.grandTotal, paytmInfo)),
          (Route<dynamic> route) => false);
    } else {


      var paytmResponse = await Paytm.payWithPaytm(
          mid, orderId, txnToken, widget.grandTotal, callbackUrl);

      print("payment_resp:  $paytmResponse");
      if (paytmResponse['error'].toString().contains("false") ||
          paytmResponse.toString().contains("TXN_SUCCESS")) {
        await requests.setOrderPaymentStatus("Paid", orderId);
        String resMsg = "Transaction Successful";
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) =>
                    TxnComplete(DateTime.now(), resMsg, orderId, paytmInfo)),
            (Route<dynamic> route) => false);
      } else if (paytmResponse
          .toString()
          .contains('onBackPressedCancelTransaction')) {
        print("payment_resp 2:  $paytmResponse.toString()");

        await requests.setOrderPaymentStatus(
            "Unpaid",
            orderId
        );
        String resMsg = "User cancelled the transaction";
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) =>
                    TxnFail(DateTime.now(), resMsg,
                        MERCHANT_ID, orderId, widget.grandTotal, paytmInfo)),
                (Route<dynamic> route) => false);
      } else {
        print("payment_resp 2:  $paytmResponse.toString()");

        print(paytmResponse['response']['RESPMSG'].toString());
        String resMsg = paytmResponse['response']['RESPMSG'].toString();
        await requests.setOrderPaymentStatus(
            "Unpaid",
            orderId
        );
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(
                builder: (context) =>
                    TxnFail(DateTime.now(), resMsg,
                        MERCHANT_ID, orderId, widget.grandTotal, paytmInfo)),
                (Route<dynamic> route) => false);
      }
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    deliveryAddress = widget.orderInfo[1];
    if (widget.orderInfo[3] == "pickup")
      deliveryTypeOption = 1;
    else
      deliveryTypeOption = 0;
    tSlot = widget.orderInfo[11];
    getPaymentOptions();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      ignoring: ignore,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: false,
          title: Text(
            "BILL AMOUNT: ₹${widget.grandTotal}", style: largeTextStyleBlack,),
          backgroundColor: Colors.white,
          leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
          ),
        ),
        body: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 120),
          child: Container(
            child: buildPaymentMethod(),
          ),
        ),

        bottomNavigationBar: SafeArea(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 16),
            child: GestureDetector(
              onTap: () {
                print(deliveryAddress);
                if (!placingOrder) {
                  // getShoppingCartId();
                  // Get.to(PaymentOption(grandTotal));
                  placeOrder();
                }
              },
              child: Container(
                //width: 100.0,
                height: 55.0,
                decoration: BoxDecoration(
                  color: mColor,
                  borderRadius: BorderRadius.circular(6.0),
                ),
                child: Center(
                  child: placingOrder ?
                  SizedBox(
                      height: 25,
                      width: 25,

                      child: SpinKitCircle(
                        color: Colors.white,
                        size: 25,
                      )
                  ) :
                  Text(
                    'PLACE ORDER',
                    style: TextStyle(fontSize: 16.0, color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildPaymentMethod() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("PAY ON DELIVERY", style: mediumTextStyleBlack,),
                  Divider(),
                  Container(
                    color: Colors.white,
                    child: RadioListTile(
                      value: 1,
                      groupValue: _paymentMode,
                      onChanged: (val) {
                        setState(() {
                          _paymentMode = val;
                          paytmPaymentMode = 0;
                        });
                      },
                      title: Text(
                        "Cash on Delivery", style: largeTextStyleBlack,),
                    ),
                  ),
                  SizedBox(height: 16,),
                  !paytmAvail ? SizedBox() : Text(
                    "OTHER PAYMENT OPTION", style: mediumTextStyleBlack,),
                  !paytmAvail ? SizedBox() : Divider(),
                  paytmAvail ?
                  RadioListTile(
                    value: 2,
                    groupValue: _paymentMode,
                    onChanged: (val) {
                      setState(() {
                        _paymentMode = val;
                        paytmPaymentMode = 1;
                      });
                    },
                    title: Image.asset('drawables/paytm_logo.png',
                        alignment: Alignment.centerLeft, height: 25),
                    secondary: _paymentMode == 2
                        ? Icon(Icons.keyboard_arrow_down)
                        : Icon(Icons.keyboard_arrow_right),
                  ) : SizedBox(),

                  AnimatedContainer(
                    curve: Curves.decelerate,
                    margin: EdgeInsets.only(left: 25),
                    duration: Duration(milliseconds: 200),
                    height: _paymentMode == 2 ? 240 : 0,
                    child: Wrap(
                      children: <Widget>[
                        RadioListTile(
                          onChanged: (val) {
                            setState(() {
                              paytmPaymentMode = val;
                            });
                          },
                          value: 1,
                          groupValue: paytmPaymentMode,
                          title: Text("Wallet", style: largeTextStyleBlack,),
                        ),
                        RadioListTile(
                          onChanged: (val) {
                            setState(() {
                              paytmPaymentMode = val;
                            });
                          },
                          value: 2,
                          groupValue: paytmPaymentMode,
                          title: Text("UPI", style: largeTextStyleBlack,),
                        ),
                        RadioListTile(
                          onChanged: (val) {
                            setState(() {
                              paytmPaymentMode = val;
                            });
                          },
                          value: 3,
                          groupValue: paytmPaymentMode,
                          title: Text(
                            "Credit/Debit Card", style: largeTextStyleBlack,),
                        ),
                        RadioListTile(
                          onChanged: (val) {
                            setState(() {
                              paytmPaymentMode = val;
                            });
                          },
                          value: 4,
                          groupValue: paytmPaymentMode,
                          title: Text(
                            "Netbanking",
                            style: largeTextStyleBlack,
                          ),
                        ),
                      ],
                    ),
                  ),
//                   razorPayAvail
//                       ? RadioListTile(
// //                          subtitle: Text("Coming soon"),
//                     value: 3,
//                     groupValue: _paymentMode,
//                     onChanged: (val) {
//                       setState(() {
//                         _paymentMode = val;
//                         paytmPaymentMode = 0;
//                       });
//                     },
//                     title: Image.asset('drawables/razorpay-logo.png',
//                         alignment: Alignment.centerLeft, height: 25),
//                   )
//                       : SizedBox(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
