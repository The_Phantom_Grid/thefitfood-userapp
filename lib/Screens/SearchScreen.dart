//import 'dart:convert';
//
//import 'package:chips_choice/chips_choice.dart';
//import 'package:flushbar/flushbar.dart';
//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter/services.dart';
//import 'package:flutter_rating_bar/flutter_rating_bar.dart';
//import 'package:fluttertoast/fluttertoast.dart';
//import 'package:maps_launcher/maps_launcher.dart';
//import 'package:userapp/Getters/GetNearbyStores.dart';
//import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
//import 'package:userapp/Getters/GetUser.dart';
//import 'package:userapp/StoreInventory.dart';
//import 'package:userapp/httpRequests.dart';
//import 'package:userapp/rating_icon_icons.dart';
//
//import 'MainApp.dart';
//
//class SearchScreen extends StatefulWidget {
//  User user;
//
//  SearchScreen(this.user);
//
//  @override
//  _SearchScreenState createState() => _SearchScreenState();
//}
//
//class _SearchScreenState extends State<SearchScreen> {
//  HttpRequests auth = HttpRequests();
//  NearbyStores nearbyStores;
//  List<Info> temp = List<Info>();
//  List<Info> filterSearch = List<Info>();
//  var searchController = TextEditingController();
//  List<String> tag = [];
//  bool loading = true;
//  List<String> searchBy = ["Search by Store Name", "Search by Store Address", "Search by Area Pincode"];
//  String searchByValue = "Search by Store Name";
//  List<String> options = [
//    'Grocery',
//    'Pet Shop',
//    'Footwear',
//    'Electronics',
//    'Beauty & Wellness',
//    'Restaurants',
//    'Stationary',
//    'Kitchenware',
//    'Dairy',
//    'Bags & Luggages',
//    'Beverages',
//    'Bakery',
//    'Electricals',
//    'Pharmacy',
//    'Gifts & Toys',
//    'Clothing',
//    'Dry Fruits',
//    'Fruits & Vegetables',
//    'Screens.Home Needs & Decor',
//    'Jewellery',
//    'Meat Shop',
//    'Medical Services',
//    'Namkeen Shop',
//    'Organic Store',
//    'Photography',
//    'Real Estate',
//    'Spa & Salon',
//    'Hardware & Sanitary',
//    'Industrial',
//    'Aata Chakki',
//    'Wine & Liquor',
//    'Automobile',
//    'Cigaratte & Paan Shop',
//    'Fitness',
//    'Sports',
//    'Chai & Cafe',
//    'Flowers',
//  ];
//  int distance = 25,
//      _selectedRadio,
//      rating;
//  RangeValues _values = RangeValues(1, 25);
//  bool filter = false,
//      notFound = false,
//      enableApplyButton = true;
//
//  getData() async {
//    var storeData = await auth.getNearbyStores(
//        10,
//        widget.user.data.loginDetails.userId,
//        widget.user.data.loginDetails.authKey);
//    setState(() {
//      var jData = json.decode(storeData.body);
//      nearbyStores = NearbyStores.fromJson(jData);
//      rmStore();
//    });
//  }
//
//  rmStore() {
//    int x = 0;
//    while (nearbyStores.info[x].distance == null) {
//      setState(() {
//        nearbyStores.info.removeAt(x);
//      });
//    }
//
//    print("${nearbyStores.info.length} length");
//    setState(() {
//      temp.addAll(nearbyStores.info);
//      loading = false;
//    });
//  }
//
//
//  searchStores() {
////    if (filter == false) {
//        if(searchBy[0].contains(searchByValue)){
//          notFound = false;
//          temp.clear();
//          for (int x = 0; x < nearbyStores.info.length; x++) {
//            if (nearbyStores.info[x].storeName
//                .toString()
//                .toLowerCase()
//                .contains(searchController.text.toLowerCase())) {
//              temp.add(nearbyStores.info[x]);
//            }
//          }
//          setState(() {});
//        }else if(searchBy[1].contains(searchByValue)){
//          notFound = false;
//          temp.clear();
//          for (int x = 0; x < nearbyStores.info.length; x++) {
//            if (nearbyStores.info[x].location
//                .toString()
//                .toLowerCase()
//                .contains(searchController.text.toLowerCase())) {
//              temp.add(nearbyStores.info[x]);
//            }
//          }
//          setState(() {});
//        }else if(searchBy[2].contains(searchByValue)){
//          notFound = false;
//          temp.clear();
//          for (int x = 0; x < nearbyStores.info.length; x++) {
//            if (nearbyStores.info[x].location
//                .toString()
//                .toLowerCase()
//                .contains(searchController.text.toLowerCase())) {
//              temp.add(nearbyStores.info[x]);
//            }
//          }
//          setState(() {});
//        }
////    } else {
////      setState(() {
////        temp.clear();
////        for (int x = 0; x < filterSearch.length; x++) {
////          if (filterSearch[x].storeName
////              .toString()
////              .toLowerCase()
////              .contains(searchController.text.toLowerCase())) {
////            temp.add(filterSearch[x]);
////          }
////        }
////      });
////    }
//    if (temp.isEmpty)
//      setState(() {
//        notFound = true;
//      });
//  }
//
//  filterStore() {
//    print("=====filter=====");
//    setState(() {
//      notFound = false;
//      temp.clear();
//      if (tag.isNotEmpty) {
////        for (int x = 0; x < nearbyStores.info.length; x++) {
////          print(x);
////          for (int i = 0; i < tag.length; i++) {
////            print("inn $i");
////            for (int k = 0; k <
////                nearbyStores.info[x].categoryidArray.length; k++) {
////              if (nearbyStores.info[x].categoryidArray[k].categoryName
////                  .toString().contains(tag[i])) {
////                temp.add(nearbyStores.info[x]);
////              }
////              break;
////            }
//////            if(nearbyStores.info[x].categoryidArray.contains(tag[i])){
//////              print(nearbyStores.info[x].storeName);
//////              temp.add(nearbyStores.info[x]);
//////              print(temp.length);
//////            }
////          }
////        }
//        for(int x = 0; x < nearbyStores.info.length; x++){
//          for(int i = 0; i < tag.length; i++){
//            if(nearbyStores.info[x].storeType.toLowerCase().contains(tag[i].toLowerCase()))
//              temp.add(nearbyStores.info[x]);
//          }
//        }
//      } else {
//        for (int x = 0; x < nearbyStores.info.length; x++) {
//          temp.add(nearbyStores.info[x]);
//        }
//      }
//    });
//    filterByDistance();
//  }
//
//  filterByDistance() {
//    print("=====distance=====");
//    double d;
//    setState(() {
//      print("${temp.length} before");
//      print("distance : $distance");
//      for (int x = 0; x < temp.length; x++) {
//        d = temp[x].distance;
//        print("distance : ${temp[x].distance}");
//        print("distance km : $d");
//        if (d > (distance / 1.60934)) {
//          temp.removeAt(x);
//          x--;
//        } else {
//          print("else----");
//          print(temp[x].storeName);
//          print(temp[x].distance);
//          print("---------");
//        }
//      }
//      print("${temp.length} after");
//    });
//    filterByRating();
//  }
//
//  filterByRating() {
//    print("=====rating=====");
//    print(_selectedRadio);
//    switch (_selectedRadio) {
//      case 1:
//        setState(() {
//          print("5 STAR");
//          for (int x = 0; x < temp.length; x++) {
//            if (temp[x].rating < 4 || temp[x].rating > 4) {
//              print(temp[x].storeName);
//              print(temp[x].rating);
//              temp.removeAt(x);
//              print(temp.length);
//              x--;
//            }
//          }
//        });
//        break;
//
//      case 2:
//        setState(() {
//          print("4 STAR");
//          for (int x = 0; x < temp.length; x++) {
//            if (temp[x].rating < 3 || temp[x].rating > 3) {
//              temp.removeAt(x);
//              x--;
//            }
//          }
//        });
//        break;
//
//      case 3:
//        setState(() {
//          print("3 STAR");
//          for (int x = 0; x < temp.length; x++) {
//            if (temp[x].rating < 2 || temp[x].rating > 2) {
//              temp.removeAt(x);
//              x--;
//            }
//          }
//        });
//        break;
//
//      case 4:
//        setState(() {
//          print("2 STAR");
//          for (int x = 0; x < temp.length; x++) {
//            if (temp[x].rating < 1 || temp[x].rating > 1) {
//              temp.removeAt(x);
//              x--;
//            }
//          }
//        });
//        break;
//    }
//    filterSearch.addAll(temp);
//
//    if (temp.isEmpty)
//      setState(() {
//        notFound = true;
//      });
//  }
//
//  Widget _bottomAppbar(BuildContext context, StateSetter setState) {
//    return Card(
//      elevation: 8.0,
//      child: Container(
//        padding: EdgeInsets.all(8.0),
//        width: MediaQuery
//            .of(context)
//            .size
//            .width,
//        child: Column(
//          children: <Widget>[
//            Container(
//              width: double.infinity,
//              child: Text(
//                "Store Type:",
//                style: TextStyle(
//                    fontFamily: 'MBK-medium'),
//                textAlign: TextAlign.start,
//              ),
//            ),
//            Divider(),
//            ChipsChoice<String>.multiple(
//              isWrapped: true,
//              value: tag,
//              options: ChipsChoiceOption.listFrom<String, String>(
//                source: options,
//                value: (i, v) => v,
//                label: (i, v) => v,
//              ),
//              onChanged: (val) =>
//                  setState(() {
//                    tag = val;
//                    print(tag);
//                  }),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  Widget _searchBy(BuildContext context, StateSetter setState){
//    return Card(
//      elevation: 8.0,
//      child: Container(
//        padding: EdgeInsets.all(8.0),
//        width: MediaQuery.of(context).size.width,
//        child: Column(
//          children: <Widget>[
//            Container(
//              width: double.infinity,
//              child: Text("Search By:",style: TextStyle(
//                  fontFamily: 'MBK-medium'),
//                textAlign: TextAlign.start,
//              ),
//            ),
//            Divider(),
//            DropdownButtonHideUnderline(
//                child: DropdownButton(
//                  isExpanded: false,
//                  hint: Text(searchByValue),
//                  onChanged: (value){
//                    print(value);
//                    setState(() {
//                      searchByValue = value;
//                    });
//                  },
//                  items: searchBy.map(
//                          (String value) => DropdownMenuItem(
//                          value: value,
//                          child: Text(value)
//                      )).toList(),
//                )
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  Widget _distance(BuildContext context, StateSetter setState) {
//    return Card(
//      elevation: 8.0,
//      child: Container(
//        padding: EdgeInsets.all(8.0),
//        width: MediaQuery
//            .of(context)
//            .size
//            .width,
//        child: Column(
//          children: <Widget>[
//            Container(
//              width: double.infinity,
//              child: Text(
//                "Distance:",
//                style: TextStyle(
//                    fontFamily: 'MBK-medium'),
//                textAlign: TextAlign.start,
//              ),
//            ),
//            Divider(),
//            RangeSlider(
//              values: _values,
//              min: 1,
//              max: 26,
//              activeColor: mColor,
//              onChanged: (value) {
//                setState(() {
//                  _values = value;
//                  print("start: ${value.start}");
//                  print("end: ${value.end}");
//                  print("dis: ${value.end - value.start}");
//                  distance =
//                      int.parse((value.end - value.start).toStringAsFixed(0));
//                  print(distance);
//                });
//              },
//            ),
//            Container(
//              child: Text("$distance Km"),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  Widget _byRating(BuildContext context, StateSetter setState) {
//    return Card(
//      elevation: 8.0,
//      child: Container(
//        padding: EdgeInsets.all(8.0),
//        width: MediaQuery
//            .of(context)
//            .size
//            .width,
//        child: Column(
//          children: <Widget>[
//            Container(
//              width: double.infinity,
//              child: Text(
//                "By Rating:",
//                style: TextStyle(
//                    fontFamily: 'MBK-medium'),
//                textAlign: TextAlign.start,
//              ),
//            ),
//            Divider(),
//            Container(
//              child: Column(
//                children: <Widget>[
//                  RadioListTile(
//                    activeColor: mColor,
//                    value: 1,
//                    groupValue: _selectedRadio,
//                    onChanged: (val) {
//                      setState(() {
//                        selectedRadio(val);
//                      });
//                    },
//                    title: RatingBar(
//                      onRatingUpdate: null,
//                      itemSize: 20,
//                      initialRating: 4,
//                      minRating: 1,
//                      direction: Axis.horizontal,
//                      allowHalfRating: true,
//                      glow: false,
//                      itemCount: 5,
//                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                      itemBuilder: (context, _) =>
//                          Icon(
//                            Icons.star,
//                            color: mColor,
//                          ),
//                    ),
//                  ),
//                  RadioListTile(
//                    activeColor: mColor,
//                    value: 2,
//                    groupValue: _selectedRadio,
//                    onChanged: (val) {
//                      setState(() {
//                        selectedRadio(val);
//                      });
//                    },
//                    title: RatingBar(
//                      onRatingUpdate: null,
//                      itemSize: 20,
//                      initialRating: 3,
//                      minRating: 1,
//                      direction: Axis.horizontal,
//                      allowHalfRating: true,
//                      glow: false,
//                      itemCount: 5,
//                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                      itemBuilder: (context, _) =>
//                          Icon(
//                            Icons.star,
//                            color: mColor,
//                          ),
//                    ),
//                  ),
//                  RadioListTile(
//                    activeColor: mColor,
//                    value: 3,
//                    groupValue: _selectedRadio,
//                    onChanged: (val) {
//                      setState(() {
//                        selectedRadio(val);
//                      });
//                    },
//                    title: RatingBar(
//                      onRatingUpdate: null,
//                      itemSize: 20,
//                      initialRating: 2,
//                      minRating: 1,
//                      direction: Axis.horizontal,
//                      allowHalfRating: true,
//                      glow: false,
//                      itemCount: 5,
//                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                      itemBuilder: (context, _) =>
//                          Icon(
//                            Icons.star,
//                            color: mColor,
//                          ),
//                    ),
//                  ),
//                  RadioListTile(
//                    activeColor: mColor,
//                    value: 4,
//                    groupValue: _selectedRadio,
//                    onChanged: (val) {
//                      setState(() {
//                        selectedRadio(val);
//                      });
//                    },
//                    title: RatingBar(
//                      onRatingUpdate: null,
//                      itemSize: 20,
//                      initialRating: 1,
//                      minRating: 1,
//                      direction: Axis.horizontal,
//                      allowHalfRating: true,
//                      glow: false,
//                      itemCount: 5,
//                      itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                      itemBuilder: (context, _) =>
//                          Icon(
//                            Icons.star,
//                            color: mColor,
//                          ),
//                    ),
//                  )
//                ],
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  selectedRadio(int val) {
//    setState(() {
//      _selectedRadio = val;
//    });
//  }
//
//  showFilter() async {
//    showModalBottomSheet(
//        context: context,
//        isDismissible: true,
//        isScrollControlled: true,
//        builder: (context) {
//          return StatefulBuilder(
//            builder: (BuildContext context, StateSetter ModalsetState) {
//              return Container(
//                padding: EdgeInsets.all(15),
//                height: MediaQuery
//                    .of(context)
//                    .size
//                    .height - 25,
//                width: MediaQuery
//                    .of(context)
//                    .size
//                    .width,
//                child: Center(
//                  child: Column(
//                    children: <Widget>[
//                      Expanded(
//                        flex: 1,
//                        child: Container(
//                          child: ListTile(
//                            leading: IconButton(
//                              onPressed: () {
//                                Navigator.pop(context);
//                              },
//                              icon: Icon(Icons.close, color: Colors.grey,),
//                            ),
//                            title: Text("Filter"),
//                            trailing: GestureDetector(
//                                onTap: () {},
//                                child: Row(
//                                  mainAxisSize: MainAxisSize.min,
//                                  children: <Widget>[
//                                    GestureDetector(
//                                      onTap: !enableApplyButton ? null : () {
//                                        if (enableApplyButton) {
//                                          ModalsetState(() {
//                                            Navigator.pop(context);
//                                            print("APPLY");
//                                            filter = true;
//                                            filterStore();
//                                            enableApplyButton = false;
//                                          });
//                                        }
//                                      },
//                                      child: Text(
//                                        "APPLY",
//                                        style: TextStyle(
//                                            color: enableApplyButton ? Colors
//                                                .blue : Colors.grey),),
//                                    ),
//                                    SizedBox(width: 5,),
//                                    Text("|"),
//                                    SizedBox(width: 5,),
//                                    GestureDetector(
//                                      onTap: () {
//                                        ModalsetState(() {
//                                          enableApplyButton = true;
//                                          _values = RangeValues(1, 25);
//                                          print("RESET");
//                                          filter = false;
//                                          tag = [];
//                                          distance = 25;
//                                          _selectedRadio = null;
//                                          searchStores();
//                                        });
//                                      },
//                                      child: Text(
//                                        "RESET",
//                                        style: TextStyle(color: Colors.blue),),
//                                    ),
//                                  ],
//                                )),
//                          ),
//                        ),
//                      ),
//                      Expanded(
//                        flex: 7,
//                        child: SingleChildScrollView(
//                          child: Column(
//                            children: <Widget>[
////                              _searchBy(context, ModalsetState),
//                              _bottomAppbar(context, ModalsetState),
//                              _distance(context, ModalsetState),
//                              _byRating(context, ModalsetState),
//                            ],
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
//              );
//            },
//          );
//        });
//  }
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    getData();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        backgroundColor: Colors.white,
////        leading: Icon(
////          Icons.search,
////          color: Colors.black,
////        ),
//        title: Container(
//          child: Row(
//            children: <Widget>[
//              Expanded(
//                child: TextField(
//                  controller: searchController,
//                  inputFormatters: searchByValue.contains("Pincode")
//                      ? [WhitelistingTextInputFormatter(RegExp("[0-9]"))]
//                      : [WhitelistingTextInputFormatter(RegExp("[a-zA-Z 0-9 - ,]"))],
//                  autofocus: false,
//                  onChanged: (text) {
//                    print(text);
//                    searchStores();
//                  },
//                  decoration: InputDecoration(
////            prefixIcon: Icon(Icons.search, color: Colors.black),
//                    enabled: enableApplyButton,
//                    hintText: enableApplyButton
//                        ? "$searchByValue..."
//                        : "You need to RESET the filter",
//                  ),
//                  cursorColor: Colors.black,
//                  showCursor: true,
//                ),
//              ),
//              PopupMenuButton<String>(
//                itemBuilder: (context) {
//                  return searchBy.map((str) {
//                    return PopupMenuItem(
//                      value: str,
//                      child: Text(str),
//                    );
//                  }).toList();
//                },
//                child: Icon(Icons.arrow_drop_down, color: Colors.black),
//                onSelected: (v) {
//                  setState(() {
//                    searchByValue = v;
//                  });
//                },
//              ),
//            ],
//          ),
//        ),
//        actions: <Widget>[
//          GestureDetector(
//            onTap: () {
//              setState(() {
//                filter = true;
//              });
//              showFilter();
//            },
//            child: Row(
//              mainAxisSize: MainAxisSize.min,
//              children: <Widget>[
//                Text("Filter",
//                    style: TextStyle(color: Colors.black, fontSize: 15)),
//                SizedBox(width: 5,),
//                Icon(
//                  RatingIcon.filter,
//                  color: Colors.black,
//                  size: 17,
//                ),
//                SizedBox(width: 5,),
////              IconButton(
////                icon:
////                onPressed: () {
////                  setState(() {
////                    filter = true;
////                  });
////                  showFilter();
//////              Navigator.push(context,
//////                  MaterialPageRoute(builder: (context) => FilterStore()));
////                },
////              ),
//              ],
//            ),
//          )
//        ],
//      ),
//      body: loading ? Center(
//        child: CircularProgressIndicator(),
//      )
//          : Center(
//          child: notFound ? Text("Not Found") : Container(
//              width: double.infinity,
//              child: ListView.builder(
//                  itemCount: temp.length,
//                  itemBuilder: (context, index) {
//                    return buildStoreList(
//                      widget.user,
//                      temp[index].categoryidArray,
//                      temp[index],
//                      temp[index].favouriteShop,
//                    );
//                  }))),
//    );
//  }
//}
//
//class buildStoreList extends StatefulWidget {
//  User user;
//
//  Info info;
//  List<CategoryidArray> categoryidArray;
//  int fav;
//
//  buildStoreList(this.user, this.categoryidArray, this.info, this.fav);
//
//  @override
//  _buildStoreListState createState() => _buildStoreListState();
//}
//
//class _buildStoreListState extends State<buildStoreList> {
//  var auth = new HttpRequests();
//  var storeDetail;
//  var mColor = Color(0xFFe9991e);
//  var mAccentColor = Color(0xFFf82d70);
//  NearbyStores nearbyStores;
//  var distance = 10;
//  bool f;
//  String imageUrl, storeType;
//  var bColor = Color(0xFFe9991e);
//
//  var storeTypeList =
//  ["Grocery",
//    "Pets",
//    "Toys, Baby Products",
//    "Car Accessories",
//    "Footwear",
//    "Electronics",
//    "Beauty & Grooming",
//    "Restaurants",
//    "Stationery",
//    "Kitchen Appliances",
//    "Kitchen Ware",
//    "Dairy Products",
//    "Bags & Luggage",
//    "Sports",
//    "Fitness & Outdoor",
//    "Beverages",
//    "Fashion Accessories",
//    "Health & Hygiene",
//    "Desserts",
//    "Bakery",
//    "Florist & Garden Tools",
//    "Electricals",
//    "Super Toys",
//    "Departmental Store",
//    "Pharmacy"];
//
//  var borderColor = [
//    Color(0xFF6D4C41),
//    Color(0xFF388E3C),
//    Color(0xFF1565C0),
//    Color(0xFFB3E5FC),
//    Color(0xFFA1887F),
//    Color(0xFFCDDC39),
//    Color(0xFFFF5252),
//    Color(0xFF880E4F),
//    Color(0xFFAA00FF),
//    Color(0xFF448AFF),
//    Color(0xFF4DB6AC),
//    Color(0xFFFFECB3),
//    Color(0xFFB0BEC5),
//    Color(0xFFF57C00),
//    Color(0xFF76FF03),
//    Color(0xFFCE93D8),
//    Color(0xFFe9991e),
//    Color(0xFFEEFF41),
//    Color(0xFFF48FB1),
//    Color(0xFFAD1457),
//    Color(0xFF43A047),
//    Color(0xFFA7FFEB),
//    Color(0xFF01579B),
//    Color(0xFFAFB42B),
//    Color(0xFFE65100),
//  ];
//
//  addToFav() async {
//    print(widget.user.data.loginDetails.userId);
//    print(widget.info.storeId);
//    print(widget.user.data.loginDetails.authKey);
//    var r = await auth.addToFavourite(widget.user.data.loginDetails.userId,
//        widget.info.storeId, distance, widget.user.data.loginDetails.authKey);
//    setState(() {
//      if (r == "success") {
//        Flushbar(
//          message: "Shop Added to your Favourite List",
//          icon: Icon(
//            Icons.favorite,
//            color: mColor,
//          ),
//          duration: Duration(seconds: 2),
//          flushbarPosition: FlushbarPosition.BOTTOM,
//        )
//          ..show(context);
//      } else
//        print("not done  $r");
//    });
//  }
//
//  removeFav() async {
//    var r = await auth.removeFavStore(widget.user.data.loginDetails.userId,
//        widget.info.storeId, distance, widget.user.data.loginDetails.authKey);
//
//    setState(() {
//      if (r == "success") {
//        Flushbar(
//          message: "Shop Removed from Favourites",
//          duration: Duration(seconds: 2),
//          icon: Icon(
//            Icons.remove_circle,
//            color: mColor,
//          ),
//        )
//          ..show(context);
//      }
//    });
//  }
//
//  setStoreImage() {
//    setState(() {
//      if (widget.info.logo
//          .toString()
//          .isNotEmpty)
//        imageUrl = widget.info.logo.toString();
//      else if (widget.info.imageUrl
//          .toString()
//          .isNotEmpty)
//        imageUrl = widget.info.imageUrl.toString();
//      else
//        imageUrl = null;
////      if(widget.info.logo.toString().isEmpty)
////        imageUrl = widget.info.imageUrl.toString();
////      else imageUrl = null;
//    });
//    print("${imageUrl} image");
//  }
//
//  setBorderColor() {
//    print("${storeTypeList.length} ||||||||| ${borderColor.length}");
//    setState(() {
//      for (int x = 0; x < storeTypeList.length; x++) {
//        if (storeTypeList[x].contains(storeType)) {
//          bColor = borderColor[x];
//        }
//      }
//    });
//  }
//
//  @override
//  void initState() {
//    // TODO: implement initState
//    super.initState();
//    setStoreImage();
//    storeType = widget.info.storeType;
//    setBorderColor();
//  }
//
//  Widget storeCard(){
//    return Container(
//      child: Stack(
//        children: <Widget>[
//          Container(
//            padding: EdgeInsets.all(2),
////              height: 250,
//            margin: EdgeInsets.all(0),
//            child: Column(
//              mainAxisSize: MainAxisSize.max,
//              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//              children: <Widget>[
//                InkWell(
//                  onTap: () async {
//                    if(widget.info.order_enabled == 1){
//                      await Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                              builder: (context) =>
//                                  StoreInventory(
//                                    nearbyStores,
//                                    widget.user,
//                                    widget.info,
//                                    widget.categoryidArray,
//                                  )));
//                      mainAppState.getShoppingListCount();
//                    }else{
//                      Fluttertoast.showToast(
//                          textColor: Colors.white,
//                          toastLength: Toast.LENGTH_LONG,
//                          msg: "Please contact merchant via call & message...",
//                          backgroundColor: Colors.blueAccent
//                      );
//                    }
//
//                  },
//                  child: Container(
//                    padding: EdgeInsets.all(4),
//                    child: Row(
//                      mainAxisSize: MainAxisSize.min,
////                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                      children: <Widget>[
//                        ClipRRect(
//                          borderRadius: BorderRadius.circular(12),
//                          child: imageUrl == null ? Image(
//                            image: AssetImage(
//                                'drawables/logo_neo_mart_splash.png'),
//                            height: 100,
//                            width: 100,
//                          )
//                              :
//                          Image.network(
//                            imageUrl,
//                            height: 100,
//                            width: 100,
//                          ),
//                        ),
//                        Container(
//                          alignment: Alignment.centerLeft,
//                          width: MediaQuery
//                              .of(context)
//                              .size
//                              .width - 120,
//                          child: ListTile(
//                            title: Text(
//                                "${widget.info.storeName}",
//                                maxLines: 2,
//                                overflow: TextOverflow.ellipsis,
//                                style: TextStyle(fontSize: 16)
//                            ),
//                            subtitle: Column(
//                              mainAxisSize: MainAxisSize.min,
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: <Widget>[
//                                Text(
//                                    "${widget.info.location.toUpperCase()}",
//                                    maxLines: 2,
//                                    overflow: TextOverflow.ellipsis,
//                                    style: TextStyle(fontSize: 13)
//                                ),
//                                SizedBox(height: 17,),
//                              ],
//                            ),
////                                trailing: widget.info.rating == 0
////                                    ? SizedBox()
////                                    : Container(
////                                    alignment: Alignment.centerRight,
////                                    width: 50,
////                                    child: Row(
////                                      children: <Widget>[
////                                        Icon(
////                                          Icons.grade,
////                                          color: Colors.amberAccent,
////                                        ),
////                                        Text("${widget.info.rating}")
////                                      ],
////                                    )
////                                )
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
//                ),
//                Column(
//                  children: <Widget>[
//                    Container(
////                      padding: EdgeInsets.only(left: 4, right: 4, bottom: 4),
////                      alignment: Alignment.bottomRight,
//                      width: MediaQuery
//                          .of(context)
//                          .size
//                          .width,
//                      child: Padding(
//                        padding: const EdgeInsets.all(4.0),
//                        child: Row(
//                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                          mainAxisSize: MainAxisSize.max,
//                          children: <Widget>[
//                            Expanded(
//                              child: Row(
//                                mainAxisSize: MainAxisSize.min,
//                                children: <Widget>[
//                                  Text("Distance:", style: TextStyle(fontSize: 12),),
//                                  Text(" ${(widget.info.distance * 1.60934)
//                                      .toStringAsFixed(2)}Km",
//                                      style: TextStyle(color: mColor, fontSize: 12)),
//                                ],
//                              ),
//                            ),
////                            SizedBox(width: 20,),
//                            RatingBar(
//                              onRatingUpdate: null,
//                              itemSize: 12,
//                              initialRating: double.parse(widget.info.rating.toString()),
//                              minRating: 1,
//                              direction: Axis.horizontal,
//                              allowHalfRating: true,
//                              glow: false,
//                              itemCount: 5,
////                            unratedColor: Colors.grey,
//                              itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                              itemBuilder: (context, _) =>
//                                  Icon(
//                                    Icons.star,
//                                    color: mColor,
//                                  ),
//                            ),
////                            SizedBox(width: 20,),
//                            Expanded(
//                              child: Row(
//                                mainAxisAlignment: MainAxisAlignment.end,
//                                children: <Widget>[
//                                  Text("Min Order:", style: TextStyle(fontSize: 12),),
//                                  Text(
//                                    " ₹ ${widget.info.minOrder}",
//                                    style: TextStyle(color: mColor, fontSize: 12),
//                                  )
//                                ],
//                              ),
//                            )
//                          ],
//                        ),
//                      ),
//                    ),
//                  ],
//                ),
//              ],
//            ),
//          ),
//          Positioned(
//              top: 7,
//              right: 7,
//              child: InkWell(
//                onTap: () {
//                  setState(() {
//                    if (widget.info.favouriteShop == 0) {
//                      widget.info.favouriteShop = 1;
//                      addToFav();
//                    } else {
//                      widget.info.favouriteShop = 0;
//                      removeFav();
//                    }
//                  });
//                },
//                child: widget.info.favouriteShop == 1
//                    ? Icon(Icons.favorite, color: mColor)
//                    : Icon(
//                  Icons.favorite_border,
//                  color: mColor,
//                ),
//              )
//          ),
//          Positioned(
//            bottom: 20,
//            right: 65,
//            child: ButtonTheme(
//              height: 20,
//              minWidth: 20,
//              materialTapTargetSize: MaterialTapTargetSize.padded,
//              child: OutlineButton(
//                padding: EdgeInsets.all(4),
//                shape: CircleBorder(),
//                splashColor: Colors.redAccent,
//                onPressed: (){
//                  MapsLauncher.launchCoordinates(widget.info.lat, widget.info.lng);
////                  _launchURL(widget.info.lat, widget.info.lng);
//                  //url :  https://www.google.com/maps/search/?api=1&query=$lat,$lon
////                  UrlLauncher.launch("geo:${widget.info.lat},${widget.info.lng}");
//                },
//                textColor: Colors.redAccent,
//                color: Colors.redAccent,
//                borderSide: BorderSide(color: Colors.redAccent),
//                child: Icon(Icons.location_on, size: 20,),
//              ),
//            ),
//          ),
//          Positioned(
//            bottom: 20,
//            right: 30,
//            child: ButtonTheme(
//              height: 20,
//              minWidth: 20,
//              child: OutlineButton(
//                padding: EdgeInsets.all(4),
//                shape: CircleBorder(),
//                splashColor: Colors.blue,
//                onPressed: (){
//                  UrlLauncher.launch("sms:${widget.info.mobile}");
//                },
//                textColor: Colors.blue,
//                color: Colors.blue,
//                borderSide: BorderSide(color: Colors.blue),
//                child: Icon(Icons.message, size: 20,),
//              ),
//            ),
//          ),
//          Positioned(
//            bottom: 20,
//            right: -5,
//            child: ButtonTheme(
//              height: 20,
//              minWidth: 20,
//              materialTapTargetSize: MaterialTapTargetSize.padded,
//              child: OutlineButton(
//                padding: EdgeInsets.all(4),
//                shape: CircleBorder(),
//                splashColor: Colors.green,
//                onPressed: (){
//                  UrlLauncher.launch("tel:${widget.info.mobile}");
//                },
//                textColor: Colors.green,
//                color: Colors.green,
//                borderSide: BorderSide(color: Colors.green),
//                child: Icon(Icons.call, size: 20,),
//              ),
//            ),
//          )
//        ],
//      ),
//    );
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return InkWell(
//      onTap: () {
////                        print((widget.store));
////                        String storeId =
////                            widget.store['info'][index]['store_id'].toString();
////                        List categories =
////                            widget.store['info'][index]['categoryid_array'];
////                        double distance =
////                            widget.store['info'][index]['distance'];
//        Navigator.push(
//            context,
//            MaterialPageRoute(
//                builder: (context) =>
//                    StoreInventory(
//                      nearbyStores,
//                      widget.user,
//                      widget.info,
//                      widget.categoryidArray,
//                    )));
//      },
//      child: Card(
//          shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.circular(5),
//              side: BorderSide(
//                  color: bColor
//              )
//          ),
//          elevation: 4.0,
//          child: widget.info.order_enabled == 1
//          ? ClipRRect(
//            child: Banner(
//              color: mColor,
//              location: BannerLocation.topStart,
//              message: "Order Now",
//              child: storeCard()
//            ),
//          )
//          : storeCard()
//      ),
//    );
//  }
//}
