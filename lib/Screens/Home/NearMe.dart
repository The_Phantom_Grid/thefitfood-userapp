// import 'dart:async';
// import 'dart:convert';
//
// import 'package:chips_choice/chips_choice.dart';
// import 'package:data_connection_checker/data_connection_checker.dart';
// import 'package:flushbar/flushbar.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_rating_bar/flutter_rating_bar.dart';
// import 'package:flutter_share_me/flutter_share_me.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:maps_launcher/maps_launcher.dart';
// import 'package:url_launcher/url_launcher.dart';
// import 'package:userapp/Components/CommonUtility.dart';
// import 'package:userapp/Getters/GetNearbyStores.dart';
// import 'package:userapp/Getters/GetProfile.dart';
// import 'package:userapp/Getters/GetStoreType.dart';
// import 'package:userapp/Screens/MainApp.dart';
// import 'package:userapp/Screens/StoreInventory.dart';
// import 'package:userapp/Network/httpRequests.dart';
// import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
// import 'package:userapp/Components/rating_icon_icons.dart';
// import '../../Network/CheckInternetConnection.dart';
// import '../../Getters/GetUser.dart';
// import 'package:flutter/animation.dart';
//
// import '../../constants.dart';
//
// _NearMeState nearmeState;
//
// class NearMe extends StatefulWidget {
//   var token;
//
//   NearMe(this.token);
//
//   @override
//   _NearMeState createState() {
//     nearmeState = _NearMeState();
//     return nearmeState;
//   }
// }
//
// class _NearMeState extends State<NearMe> {
//   var auth = new HttpRequests();
//   var storeDetail;
//   var mColor = Color(0xFFe9991e);
//   ScrollController listViewController;
//   double scrollPosition;
//
// //  var mAccentColor = Color(0xFFf82d70);
//   bool noInternet = false,
//       loading = true,
//       loadMore = false,
//       storesNotAvailable = false;
//   List<String> selectedStoreTypes = [];
//
//   ///search variables//////
//   List<String> searchBy = [
//     "Search by Store Name",
//     "Search by Store Address",
//     "Search by Store Category"
//   ];
//   String searchByValue = "Search by Store Name", selectedStoreType, rating = "";
//   var searchController = TextEditingController();
//   var distanceController = TextEditingController();
//   bool showAppbar = true;
//   bool filter = false,
//       notFound = false,
//       enableApplyButton = true,
//       searching = false;
//   List<Info> temp = List<Info>();
//   List<Info> filterSearch = List<Info>();
//   int _selectedRadio;
//   double distance = 25.0;
//   int max = 10,
//       min = 0,
//       searchMoreMin = 0,
//       searchMoreMax = 35,
//       filterOption = 1,
//       selectedChoice = -1;
//   String storeName = "", storeType = "", locality = "", category = "";
//   double lat, lng;
//
// //  List<String> options = [
// //    'Grocery',
// //    'Pet Shop',
// //    'Footwear',
// //    'Electronics',
// //    'Beauty & Wellness',
// //    'Restaurants',
// //    'Stationery',
// //    'Kitchenware',
// //    'Dairy',
// //    'Bags & Luggages',
// //    'Beverages',
// //    'Bakery',
// //    'Electricals',
// //    'Pharmacy',
// //    'Gifts & Toys',
// //    'Clothing',
// //    'Dry Fruits',
// //    'Fruits & Vegetables',
// //    'Home Needs & Decor',
// //    'Jewellery',
// //    'Meat Shop',
// //    'Medical Services',
// //    'Namkeen Shop',
// //    'Organic Store',
// //    'Photography',
// //    'Real Estate',
// //    'Spa & Salon',
// //    'Hardware & Sanitary',
// //    'Industrial',
// //    'Aata Chakki',
// //    'Wine & Liquor',
// //    'Automobile',
// //    'Cigarette & Paan Shop',
// //    'Fitness',
// //    'Sports',
// //    'Chai & Cafe',
// //    'Flowers',
// //  ];
//
//   ///////////end search variables//////
//
// //  filterByDistance() {
// //    print("=====distance=====");
// //    double d;
// //    setState(() {
// //      for (int x = 0; x < temp.length; x++) {
// //        d = temp[x].distance;
// //        if (d > (distance / 1.60934)) {
// //          temp.removeAt(x);
// //          x--;
// //        }
// //      }
// //    });
// //    filterByRating();
// //  }
// //
// //  filterByRating() {
// //    print("rat${temp.length}");
// //    print("=====rating===== $_selectedRadio");
// //    switch (_selectedRadio) {
// //      case 1:
// //        setState(() {
// //          print("5 STAR");
// //          for (int x = 0; x < temp.length; x++) {
// //            if (temp[x].rating < 4 || temp[x].rating > 4) {
// //              print(temp[x].storeName);
// //              print(temp[x].rating);
// //              temp.removeAt(x);
// //              print(temp.length);
// //              x--;
// //            }
// //          }
// //        });
// //        break;
// //
// //      case 2:
// //        setState(() {
// //          print("4 STAR");
// //          for (int x = 0; x < temp.length; x++) {
// //            if (temp[x].rating < 3 || temp[x].rating > 3) {
// //              temp.removeAt(x);
// //              x--;
// //            } else
// //              print(temp.length);
// //          }
// //        });
// //        break;
// //
// //      case 3:
// //        setState(() {
// //          print("3 STAR");
// //          for (int x = 0; x < temp.length; x++) {
// //            if (temp[x].rating < 2 || temp[x].rating > 2) {
// //              temp.removeAt(x);
// //              x--;
// //            }
// //          }
// //        });
// //        break;
// //
// //      case 4:
// //        setState(() {
// //          print("2 STAR");
// //          for (int x = 0; x < temp.length; x++) {
// //            if (temp[x].rating < 1 || temp[x].rating > 1) {
// //              temp.removeAt(x);
// //              x--;
// //            }
// //          }
// //        });
// //        break;
// //    }
// //    filterSearch.addAll(temp);
// //    print("length ${temp.length}");
// //    if (temp.isEmpty)
// //      setState(() {
// //        notFound = true;
// //      });
// //  }
//
//   ///////end of search functions////
//
//   _scrollListener() {
//     if (listViewController.position.pixels ==
//         listViewController.position.maxScrollExtent) {
// //      setState(() {
// //        loadMore = true;
// //      });
//
//       print("FILTER:: $filter");
//       if (searching || filter) {
//         searchMore();
//       }
//     }
//   }
//
//   bool showMoreStore = false;
//
//   Widget showMore() {
//     return GestureDetector(
//       onTap: () {
//         setState(() {
//           showMoreStore = true;
//         });
//         if (searching || filter) {
//           searchMore();
//         } else
//           loadMoreStores();
//       },
//       child: Container(
//         decoration: BoxDecoration(
//             color: Colors.white, borderRadius: BorderRadius.circular(8)),
//         padding: EdgeInsets.all(16),
//         margin: EdgeInsets.all(16),
//         child: showMoreStore
//             ? Text(
//                 "Loading...",
//                 textAlign: TextAlign.center,
//               )
//             : Text(
//                 "Show more",
//                 textAlign: TextAlign.center,
//               ),
//       ),
//     );
//   }
//
// //  loadMoreStoresInBG() async {
// ////    min = min + 25;
// ////    nearbyStores = null;
// //    var r = await auth.getNearbyStores(
// //        25,
// //        2000,
// //        0,
// //        USER_DETAILES.data.loginDetails.userId,
// //        widget.token);
// //    var jData = json.decode(r.body);
// //    nearbyStores = NearbyStores.fromJson(jData);
// //    setState(() {
// //      temp.clear();
// //      temp.addAll(nearbyStores.info);
// ////        loading = false;
// //    });
// //    rmStore();
// //    print("complete:: ${temp.length}");
// //  }
//
//   loadMoreStores() async {
//     min += max;
//     var r = await auth.getNearbyStores(
//         25, max, min, USER_DETAILES.data.loginDetails.userId, widget.token);
//     var jData = json.decode(r.body);
//     nearbyStores = NearbyStores.fromJson(jData);
//     setState(() {
//       temp.addAll(nearbyStores.info);
//       showMoreStore = false;
//     });
// //    if (nearbyStores.info.length != 0 && temp.length != 0) {
// //      setState(() {
// ////      temp.clear();
// //
// //      });
// //    }
//   }
//
//   Future getData() async {
//     min = 0;
//     setState(() {
//       loading = true;
//       nearbyStores = null;
//     });
//     print("userID ${USER_DETAILES.data.loginDetails.userId}");
//     print("userID ${USER_DETAILES.data.loginDetails.authKey}");
//     var storeData = await auth.getNearbyStores(
//         25, 10, 0, USER_DETAILES.data.loginDetails.userId, widget.token);
//     setState(() {
//       var jData = json.decode(storeData.body);
//       nearbyStores = NearbyStores.fromJson(jData);
//       if (nearbyStores.info.isEmpty) {
//         loading = false;
//         storesNotAvailable = true;
//       } else {
//         rmStore();
//       }
//     });
//   }
//
//   rmStore() {
//     int x = 0;
//     while (nearbyStores.info[x].distance == null) {
//       setState(() {
//         nearbyStores.info.removeAt(x);
//       });
//     }
//     setState(() {
//       temp.clear();
//       temp.addAll(nearbyStores.info);
//       getStoreTypeList();
//     });
//     print("complete1:: ${temp.length}");
//   }
//
//   List<String> storeTypes = [];
//   List<String> storeTypeId = [];
//   StoreType storeTypeList;
//
//   getStoreTypeList() async {
//     storeTypes.clear();
//     storeTypeId.clear();
//     var r =
//         await auth.getStoreTypeList(USER_DETAILES.data.loginDetails.authKey);
//     storeTypeList = StoreType.fromJson(r);
//     if (storeTypeList.data.isNotEmpty) {
//       storeTypeList.data.forEach((element) {
//         storeTypes.add(element.catName);
//       });
//       storeTypeList.data.forEach((element) {
//         storeTypeId.add(element.id);
//       });
//       getLatLng();
//     }
//   }
//
//   /////filter//////
// //  Widget _bottomAppbar(BuildContext context, StateSetter setState) {
// //    return Card(
// //      elevation: 8.0,
// //      child: Container(
// //        padding: EdgeInsets.all(8.0),
// //        width: MediaQuery
// //            .of(context)
// //            .size
// //            .width,
// //        child: Column(
// //          children: <Widget>[
// //            Container(
// //              width: double.infinity,
// //              child: Text(
// //                "Store Type:",
// //                style: TextStyle(
// //                    fontFamily: 'MBK-medium'),
// //                textAlign: TextAlign.start,
// //              ),
// //            ),
// //            Divider(),
// //            ChipsChoice<String>.multiple(
// //              isWrapped: true,
// //              value: tag,
// //              options: ChipsChoiceOption.listFrom<String, String>(
// //                source: options,
// //                value: (i, v) => v,
// //                label: (i, v) => v,
// //              ),
// //              onChanged: (val) =>
// //                  setState(() {
// //                    tag = val;
// //                    print(tag);
// //                  }),
// //            )
// //          ],
// //        ),
// //      ),
// //    );
// //  }
//
//   List<String> selectedChoices = [];
//
//   selectedRadio(int val) {
//     setState(() {
//       _selectedRadio = val;
//       switch (_selectedRadio) {
//         case 1:
//           rating = "4";
//           break;
//         case 2:
//           rating = "3";
//           break;
//         case 3:
//           rating = "2";
//           break;
//         case 4:
//           rating = "1";
//           break;
//         case 5:
//           rating = "5";
//           break;
//         default:
//           rating = "";
//       }
//     });
//   }
//
//   resetFilter() {
//     setState(() {
//       print("RESET");
//       selectedChoices.clear();
//       filter = false;
//       selectedStoreType = null;
//       distance = 25;
//       distanceController.text = "";
//       _selectedRadio = null;
//       rating = "";
//       filterOption = 1;
//       print(filter);
//       setPageState();
//     });
//   }
//
//   showFilter() async {
//     showModalBottomSheet(
//         context: context,
//         isDismissible: true,
//         isScrollControlled: true,
//         builder: (context) {
//           return StatefulBuilder(
//             builder: (BuildContext context, StateSetter setState) {
//               return Container(
//                 padding: EdgeInsets.all(8),
//                 height: MediaQuery.of(context).size.height - 25,
//                 width: MediaQuery.of(context).size.width,
//                 child: Center(
//                   child: Column(
//                     children: <Widget>[
//                       Expanded(
//                         flex: 1,
//                         child: Container(
//                           child: ListTile(
//                             leading: IconButton(
//                               onPressed: () {
//                                 Navigator.pop(context);
//                                 setState(() {});
//                               },
//                               icon: Icon(
//                                 Icons.close,
//                                 color: Colors.grey,
//                               ),
//                             ),
//                             title: Text("Filter"),
//                             trailing: GestureDetector(
//                                 onTap: () {},
//                                 child: Row(
//                                   mainAxisSize: MainAxisSize.min,
//                                   children: <Widget>[
//                                     GestureDetector(
//                                       onTap: () {
// //                                              if (enableApplyButton) {
//                                         setState(() {
//                                           Navigator.pop(context);
//                                           print("APPLY");
//                                           filterOption = 1;
// //                                          filter = true;
//                                           searchMoreMin = 0;
//                                           searchStore();
//                                         });
//                                         if (selectedChoices.isEmpty &&
//                                             distance == 25.0 &&
//                                             _selectedRadio == null &&
//                                             rating == "" &&
//                                             selectedStoreType == null) {
//                                           filter = false;
//                                           setPageState();
//                                         } else {
//                                           filter = true;
//                                           setPageState();
//                                         }
// //                                              }
//                                       },
//                                       child: Text(
//                                         "APPLY",
//                                         style: TextStyle(color: Colors.blue),
//                                       ),
//                                     ),
//                                     SizedBox(
//                                       width: 5,
//                                     ),
//                                     Text("|"),
//                                     SizedBox(
//                                       width: 5,
//                                     ),
//                                     GestureDetector(
//                                       onTap: () {
//                                         resetFilter();
//                                       },
//                                       child: Text(
//                                         "RESET",
//                                         style: TextStyle(color: Colors.blue),
//                                       ),
//                                     ),
//                                   ],
//                                 )),
//                           ),
//                         ),
//                       ),
//                       Expanded(
//                         flex: 7,
//                         child: SingleChildScrollView(
//                           child: Column(
//                             children: <Widget>[
//                               Container(
// //                                height: 100,
//                                 child: Row(
//                                   mainAxisAlignment: MainAxisAlignment.center,
//                                   crossAxisAlignment: CrossAxisAlignment.start,
//                                   children: <Widget>[
//                                     Expanded(
//                                       flex: 1,
//                                       child: Container(
//                                         color: Colors.white,
//                                         child: Column(
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.center,
//                                           crossAxisAlignment:
//                                               CrossAxisAlignment.center,
//                                           children: <Widget>[
//                                             GestureDetector(
//                                               onTap: () {
//                                                 setState(() {
//                                                   filterOption = 1;
//                                                 });
//                                               },
//                                               child: Container(
//                                                 height: 50,
//                                                 color: filterOption == 1
//                                                     ? Colors.grey
//                                                         .withOpacity(.1)
//                                                     : Colors.white,
//                                                 child: Center(
//                                                     child: Text("Store Type")),
//                                               ),
//                                             ),
//                                             GestureDetector(
//                                               onTap: () {
//                                                 setState(() {
//                                                   filterOption = 2;
//                                                 });
//                                               },
//                                               child: Container(
//                                                 height: 50,
//                                                 color: filterOption == 2
//                                                     ? Colors.grey
//                                                         .withOpacity(.1)
//                                                     : Colors.white,
//                                                 child: Center(
//                                                     child: Text("Distance")),
//                                               ),
//                                             ),
//                                             GestureDetector(
//                                               onTap: () {
//                                                 setState(() {
//                                                   filterOption = 3;
//                                                 });
//                                               },
//                                               child: Container(
//                                                 height: 50,
//                                                 color: filterOption == 3
//                                                     ? Colors.grey
//                                                         .withOpacity(.1)
//                                                     : Colors.white,
//                                                 child: Center(
//                                                     child: Text("Rating")),
//                                               ),
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                     Expanded(
//                                       flex: 2,
//                                       child: filterOption == 1
//                                           ? Container(
//                                               padding: EdgeInsets.symmetric(
//                                                   horizontal: 8, vertical: 4),
//                                               color:
//                                                   Colors.grey.withOpacity(.05),
//                                               child: Wrap(
//                                                   spacing: 4,
//                                                   runSpacing: 6,
//                                                   children: storeTypeList.data
//                                                       .map((e) {
//                                                     return GestureDetector(
//                                                       onTap: () {
//                                                         setState(() {
//                                                           if (selectedChoices
//                                                               .contains(e.id))
//                                                             selectedChoices
//                                                                 .remove(e.id);
//                                                           else
//                                                             selectedChoices
//                                                                 .add(e.id);
//                                                         });
//                                                       },
//                                                       child: Container(
//                                                           padding: EdgeInsets
//                                                               .symmetric(
//                                                                   horizontal: 8,
//                                                                   vertical: 4),
//                                                           decoration: BoxDecoration(
//                                                               border: Border.all(
//                                                                   color: selectedChoices
//                                                                           .contains(e
//                                                                               .id)
//                                                                       ? mColor
//                                                                       : Colors
//                                                                           .grey),
//                                                               color: Colors
//                                                                   .white54,
//                                                               borderRadius:
//                                                                   BorderRadius
//                                                                       .circular(
//                                                                           15)),
//                                                           child: Text(
//                                                             e.catName,
//                                                             style: TextStyle(
//                                                                 color: selectedChoices
//                                                                         .contains(e
//                                                                             .id)
//                                                                     ? mColor
//                                                                     : Colors
//                                                                         .black),
//                                                           )),
//                                                     );
//                                                   }).toList())
// //                                        ChipsChoice<String>.multiple(
// //                                          isWrapped: true,
// ////                                          padding: EdgeInsets.all(4),
// //                                          itemConfig: ChipsChoiceItemConfig(
// //                                              selectedColor: mColor,
// //                                              unselectedColor: Colors.black,
// ////                                                  runSpacing: 4,
// ////                                                  spacing: 4,
// //                                              labelStyle: TextStyle(
// //                                                  fontSize: 12)
// //                                          ),
// //                                          value: tag,
// //                                          options: ChipsChoiceOption.listFrom<String, String>(
// //                                            source: storeTypes,
// //                                            value: (i, v) => v,
// //                                            label: (i, v) => v,
// //                                          ),
// //                                          onChanged: (val) =>
// //                                              setState(() {
// //                                                tag = val;
// //                                              }),
// //                                        ),
//                                               )
//                                           : filterOption == 2
//                                               ? Container(
//                                                   height: 150,
//                                                   color: Colors.grey
//                                                       .withOpacity(.05),
//                                                   child: Row(
//                                                     mainAxisAlignment:
//                                                         MainAxisAlignment
//                                                             .center,
//                                                     mainAxisSize:
//                                                         MainAxisSize.min,
//                                                     children: <Widget>[
// //                                            Text("Distance:"),
//                                                       SizedBox(
//                                                         width: 5,
//                                                       ),
//                                                       Expanded(
//                                                         child: TextFormField(
//                                                           controller:
//                                                               distanceController,
//                                                           keyboardType:
//                                                               TextInputType
//                                                                   .number,
//                                                           inputFormatters: [
//                                                             WhitelistingTextInputFormatter(
//                                                                 RegExp(
//                                                                     "[0-9, A-Z, a-z]"))
//                                                           ],
//                                                           maxLengthEnforced:
//                                                               true,
//                                                           maxLength: 3,
//                                                           decoration: InputDecoration(
//                                                               contentPadding:
//                                                                   EdgeInsets.symmetric(
//                                                                       vertical:
//                                                                           12,
//                                                                       horizontal:
//                                                                           8),
//                                                               counterText: "",
//                                                               hintText:
//                                                                   "E.g: 5",
//                                                               enabledBorder: OutlineInputBorder(
//                                                                   borderSide: BorderSide(
//                                                                       color: Colors
//                                                                           .black)),
//                                                               focusedBorder: OutlineInputBorder(
//                                                                   borderSide: BorderSide(
//                                                                       color: Colors
//                                                                           .black)),
//                                                               border: OutlineInputBorder(
//                                                                   borderSide: BorderSide(
//                                                                       color: Colors
//                                                                           .black)),
//                                                               hasFloatingPlaceholder:
//                                                                   true,
//                                                               labelStyle: TextStyle(
//                                                                   color: Colors
//                                                                       .grey)),
// //                                                controller: gstinController,
//                                                           onChanged: (text) {
//                                                             setState(() {
//                                                               distance = double
//                                                                       .tryParse(
//                                                                           text) /
//                                                                   1.60934;
//                                                             });
//                                                           },
//                                                         ),
//                                                       ),
//                                                       SizedBox(
//                                                         width: 5,
//                                                       ),
//                                                       Text("Km")
//                                                     ],
//                                                   ),
//                                                 )
//                                               : Container(
//                                                   color: Colors.grey
//                                                       .withOpacity(.05),
//                                                   child: Column(
//                                                     children: <Widget>[
//                                                       RadioListTile(
//                                                         activeColor: mColor,
//                                                         value: 5,
//                                                         groupValue:
//                                                             _selectedRadio,
//                                                         onChanged: (val) {
//                                                           setState(() {
//                                                             selectedRadio(val);
//                                                           });
//                                                         },
//                                                         title: RatingBar(
//                                                           onRatingUpdate: null,
//                                                           itemSize: 18,
//                                                           initialRating: 5,
//                                                           minRating: 1,
//                                                           direction:
//                                                               Axis.horizontal,
//                                                           allowHalfRating: true,
//                                                           glow: false,
//                                                           itemCount: 5,
//                                                           itemPadding: EdgeInsets
//                                                               .symmetric(
//                                                                   horizontal:
//                                                                       4.0),
//                                                           itemBuilder:
//                                                               (context, _) =>
//                                                                   Icon(
//                                                             Icons.star,
//                                                             color: mColor,
//                                                           ),
//                                                         ),
//                                                       ),
//                                                       RadioListTile(
//                                                         activeColor: mColor,
//                                                         value: 1,
//                                                         groupValue:
//                                                             _selectedRadio,
//                                                         onChanged: (val) {
//                                                           setState(() {
//                                                             selectedRadio(val);
//                                                           });
//                                                         },
//                                                         title: RatingBar(
//                                                           onRatingUpdate: null,
//                                                           itemSize: 18,
//                                                           initialRating: 4,
//                                                           minRating: 1,
//                                                           direction:
//                                                               Axis.horizontal,
//                                                           allowHalfRating: true,
//                                                           glow: false,
//                                                           itemCount: 5,
//                                                           itemPadding: EdgeInsets
//                                                               .symmetric(
//                                                                   horizontal:
//                                                                       4.0),
//                                                           itemBuilder:
//                                                               (context, _) =>
//                                                                   Icon(
//                                                             Icons.star,
//                                                             color: mColor,
//                                                           ),
//                                                         ),
//                                                       ),
//                                                       RadioListTile(
//                                                         activeColor: mColor,
//                                                         value: 2,
//                                                         groupValue:
//                                                             _selectedRadio,
//                                                         onChanged: (val) {
//                                                           setState(() {
//                                                             selectedRadio(val);
//                                                           });
//                                                         },
//                                                         title: RatingBar(
//                                                           onRatingUpdate: null,
//                                                           itemSize: 18,
//                                                           initialRating: 3,
//                                                           minRating: 1,
//                                                           direction:
//                                                               Axis.horizontal,
//                                                           allowHalfRating: true,
//                                                           glow: false,
//                                                           itemCount: 5,
//                                                           itemPadding: EdgeInsets
//                                                               .symmetric(
//                                                                   horizontal:
//                                                                       4.0),
//                                                           itemBuilder:
//                                                               (context, _) =>
//                                                                   Icon(
//                                                             Icons.star,
//                                                             color: mColor,
//                                                           ),
//                                                         ),
//                                                       ),
//                                                       RadioListTile(
//                                                         activeColor: mColor,
//                                                         value: 3,
//                                                         groupValue:
//                                                             _selectedRadio,
//                                                         onChanged: (val) {
//                                                           setState(() {
//                                                             selectedRadio(val);
//                                                           });
//                                                         },
//                                                         title: RatingBar(
//                                                           onRatingUpdate: null,
//                                                           itemSize: 18,
//                                                           initialRating: 2,
//                                                           minRating: 1,
//                                                           direction:
//                                                               Axis.horizontal,
//                                                           allowHalfRating: true,
//                                                           glow: false,
//                                                           itemCount: 5,
//                                                           itemPadding: EdgeInsets
//                                                               .symmetric(
//                                                                   horizontal:
//                                                                       4.0),
//                                                           itemBuilder:
//                                                               (context, _) =>
//                                                                   Icon(
//                                                             Icons.star,
//                                                             color: mColor,
//                                                           ),
//                                                         ),
//                                                       ),
//                                                       RadioListTile(
//                                                         activeColor: mColor,
//                                                         value: 4,
//                                                         groupValue:
//                                                             _selectedRadio,
//                                                         onChanged: (val) {
//                                                           setState(() {
//                                                             selectedRadio(val);
//                                                           });
//                                                         },
//                                                         title: RatingBar(
//                                                           onRatingUpdate: null,
//                                                           itemSize: 18,
//                                                           initialRating: 1,
//                                                           minRating: 1,
//                                                           direction:
//                                                               Axis.horizontal,
//                                                           allowHalfRating: true,
//                                                           glow: false,
//                                                           itemCount: 5,
//                                                           itemPadding: EdgeInsets
//                                                               .symmetric(
//                                                                   horizontal:
//                                                                       4.0),
//                                                           itemBuilder:
//                                                               (context, _) =>
//                                                                   Icon(
//                                                             Icons.star,
//                                                             color: mColor,
//                                                           ),
//                                                         ),
//                                                       )
//                                                     ],
//                                                   ),
//                                                 ),
//                                     ),
//                                   ],
//                                 ),
//                               ),
// //                              _searchBy(context, ModalsetState),
// //                              _bottomAppbar(context, setState),
// //                              _distance(context, setState),
// //                              _byRating(context, setState),
//                             ],
//                           ),
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               );
//             },
//           );
//         });
//   }
//
// //  filterStore() {
// //    print("=====filter=====");
// //      print(temp.length);
// //      if(tag.isNotEmpty) {
// //        print("tag not empty");
// //        for (int x = 0; x < nearbyStores.info.length; x++) {
// //          for (int i = 0; i < tag.length; i++) {
// //            if (nearbyStores.info[x].storeType.toLowerCase().contains(tag[i].toLowerCase())) {
// //            print(nearbyStores.info[x].storeType);
// //            setState(() {
// //              temp.add(nearbyStores.info[x]);
// //            });
// ////              if(temp.length != 0)
// ////                setState(() {
// ////                  temp.forEach((element) {
// ////                    if(element.storeName.toString().contains(nearbyStores.info[x].storeName.toString()))
// ////                      temp.remove(element);
// ////                  });
// ////            });
// //          } else{
// //              print("else  ${nearbyStores.info[x].storeType}");
// //            }
// //          }
// //          print(temp.length);
// //        }
// //      }
// //    if (temp.isEmpty)
// //      setState(() {
// //        loading = false;
// //        notFound = true;
// //      });
// //    else {
// //      setState(() {
// ////        temp.addAll(nearbyStores.info);
// //        loading = false;
// //        notFound = false;
// //      });
// //    }
// ////    filterByDistance();
// //  }
//
//   int Max = 200, Min = 0;
//
//   setSearchField() {
//     if (searchByValue.contains("Search by Store Name")) {
//       print("By Name");
//       storeName = searchController.text;
//       locality = "";
//       category = "";
//     } else if (searchByValue.contains("Search by Store Address")) {
//       print("By Area");
//       storeName = "";
//       category = "";
//       locality = searchController.text;
//     } else if (searchByValue.contains("Search by Store Category")) {
//       print("By Category");
//       storeName = "";
//       category = searchController.text;
//       locality = "";
//     }
//   }
//
//   searchMore() async {
//     setState(() {
//       loadMore = true;
//     });
//     searchMoreMin = searchMoreMin + 35;
//     setSearchField();
//     print("STORE NAME: $storeName");
//     print(
//         "STORE Cat: ${selectedChoices.toList().toString().replaceAll('[', "").replaceAll(']', "").replaceAll(' ', "")}");
//     var r = await auth.searchStore(
//         storeName,
//         category,
//         "",
//         distance.toStringAsFixed(2),
//         rating,
//         selectedChoices
//             .toList()
//             .toString()
//             .replaceAll('[', "")
//             .replaceAll(']', "")
//             .replaceAll(' ', ""),
//         searchMoreMax,
//         searchMoreMin,
//         locality,
//         widget.token);
//     nearbyStores = NearbyStores.fromJson(r);
//     setState(() {
//       loadMore = false;
//       showMoreStore = false;
//     });
//     if (nearbyStores.info.length == 0 && temp.length == 0) {
//       setState(() {
//         notFound = true;
//         loading = false;
//       });
//     } else {
//       print("LENGTH ::: ${nearbyStores.info.length}");
//       if (nearbyStores.info.length == 0) {
//         Fluttertoast.showToast(
//           msg: "No more stores found!",
//           timeInSecForIosWeb: 2,
//         );
//       }
//     }
//   }
//
//   searchStore() async {
//     setState(() {
//       temp.clear();
//       loading = true;
//     });
//
//     setSearchField();
//
//     print("STORE cat: $category");
//     print("STORE BY: $searchByValue");
//     print("STORE distance: ${distance.toStringAsFixed(2)}");
//     print(
//         "STORE Cat: ${selectedChoices.toList().toString().replaceAll('[', "").replaceAll(']', "").replaceAll(' ', "")}");
//     var r = await auth.searchStore(
//         storeName,
//         category,
//         "",
//         distance.toStringAsFixed(2),
//         rating,
//         selectedChoices
//             .toList()
//             .toString()
//             .replaceAll('[', "")
//             .replaceAll(']', "")
//             .replaceAll(' ', ""),
//         Max,
//         Min,
//         locality,
//         widget.token);
//     nearbyStores = NearbyStores.fromJson(r);
//     print("DATA LIST LENGTH: ${nearbyStores.info.length}");
//     if (nearbyStores.info.length == 0) {
//       setState(() {
//         notFound = true;
//         loading = false;
//       });
//     } else {
//       setState(() {
//         notFound = false;
//         loading = false;
//         temp.addAll(nearbyStores.info);
//       });
//     }
//   }
//
//   getLatLng() async {
//     var r = await auth.getProfile(USER_DETAILES.data.loginDetails.authKey);
// //    ProfileData profileData = ProfileData.fromJson(r);
//     lat = double.tryParse(r['prifiledata'][0]['lat'].toString());
//     lng = double.tryParse(r['prifiledata'][0]['lng'].toString());
//     print("LAT::: ${lat} || LONG::: ${lng}");
//     setState(() {
//       loading = false;
//       notFound = false;
//     });
//   }
//
//   setPageState() {
//     setState(() {});
//   }
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
// //    getData();
//     print("INITSTATE");
// //    print("NEARBY STORE: ${nearbyStores.info}");
// //    if(nearbyStores == null) {
// //      print("NEARBY STORE: ${nearbyStores}");
//     getData();
// //    }
// //    else {
// //      print("NEARBY STOR LENGTHE: ${nearbyStores.info.length}");
// //      rmStore();
// //      if(nearbyStores.info.length == 0)
// //        notFound = true;
// //    }
//     listViewController = ScrollController();
//     scrollPosition = 0.0;
//     listViewController = ScrollController();
//     listViewController.addListener(_scrollListener);
//   }
//
//   Widget storeNotAvailable() {
//     return Container(
//       width: double.infinity,
//       child: SingleChildScrollView(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.center,
//           mainAxisSize: MainAxisSize.max,
//           children: <Widget>[
//             SizedBox(
//               height: 50,
//             ),
//             Image.asset(
//               'drawables/sad_emoji.png',
//               height: 70,
//               width: 70,
//             ),
//             Text(
//               "Oops!!!",
//               style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//             ),
//             Text(
//               "No Stores available in your area.",
//               textAlign: TextAlign.center,
//               style: TextStyle(fontSize: 18, color: Colors.grey),
//             ),
//             SizedBox(
//               height: 50,
//             ),
//             Text(
//               "Share application to other merchants for registration.",
//               style: TextStyle(fontSize: 18),
//               textAlign: TextAlign.center,
//             ),
//             SizedBox(
//               height: 20,
//             ),
//             ButtonTheme(
//               splashColor: mColor,
//               height: 40,
//               minWidth: 150,
//               shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(30)),
//               child: RaisedButton(
//                 onPressed: () async {
//                   var response = await FlutterShareMe().shareToSystem(
//                     msg:
//                         "Click on the url and download the app to enjoy our services.\n"
//                         "https://play.google.com/store/apps/details?id=com.nukkadse.merchantapp",
//                   );
//                   if (response == 'success') {
//                     print('navigate success');
//                   }
//                 },
//                 textColor: Colors.white,
//                 child: Text("Share Now"),
//               ),
//             ),
//             SizedBox(
//               height: 50,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     Future<Null> _refresh() async {
//       Completer<Null> completer = Completer<Null>();
//       resetFilter();
//       getData();
//       // getMyLocation();
//       Future.delayed(Duration(seconds: 2)).then((_) {
//         completer.complete();
//       });
// //      setState(() {
// //        searchController.text = "";
// //      });
//       return completer.future;
//     }
//
//     return Scaffold(
//       extendBody: true,
//       body: loading
//           ? neoLoader()
//           : storesNotAvailable
//               ? storeNotAvailable()
//               : Stack(
//                   children: <Widget>[
//                     Center(
//                       child: Container(
//                         child: Column(
//                           mainAxisSize: MainAxisSize.max,
//                           crossAxisAlignment: CrossAxisAlignment.center,
//                           children: <Widget>[
//                             Container(
//                               padding: EdgeInsets.all(4),
//                               color: Colors.white,
//                               height: 40,
//                               child: Row(
//                                 children: <Widget>[
//                                   Expanded(
//                                     child: TextField(
//                                       controller: searchController,
//                                       inputFormatters: searchByValue
//                                               .contains("Pincode")
//                                           ? [
//                                               WhitelistingTextInputFormatter(
//                                                   RegExp("[0-9]"))
//                                             ]
//                                           : searchByValue.contains("Address")
//                                               ? [
//                                                   WhitelistingTextInputFormatter(
//                                                       RegExp(
//                                                           "[a-zA-Z 0-9 ,-/]"))
//                                                 ]
//                                               : [
//                                                   WhitelistingTextInputFormatter(
//                                                       RegExp(
//                                                           "[a-zA-Z 0-9 - ,]"))
//                                                 ],
//                                       autofocus: false,
//                                       onSubmitted: (val) {
//                                         print("FILTER:: $filter");
//                                         if (val.length == 0 &&
//                                             filter == false) {
//                                           searching = false;
//                                           getData();
//                                         } else {
//                                           print("SEARCHING>>>>");
//                                           searching = true;
//                                           searchMoreMin = 0;
//                                           searchStore();
//                                         }
//                                       },
//                                       decoration: InputDecoration(
//                                         suffixIcon: PopupMenuButton<String>(
//                                           itemBuilder: (context) {
//                                             return searchBy.map((str) {
//                                               return PopupMenuItem(
//                                                 value: str,
//                                                 child: Text(str),
//                                               );
//                                             }).toList();
//                                           },
//                                           child: Icon(Icons.arrow_drop_down,
//                                               color: Colors.black),
//                                           onSelected: (v) {
//                                             setState(() {
//                                               searchByValue = v;
//                                               searchController.text = "";
//                                             });
//                                           },
//                                         ),
//                                         disabledBorder: OutlineInputBorder(
//                                             borderSide: BorderSide(
//                                                 color: Colors.grey, width: 1),
//                                             borderRadius:
//                                                 BorderRadius.circular(12)),
//                                         enabledBorder: OutlineInputBorder(
//                                             borderSide: BorderSide(
//                                                 color: Colors.grey, width: 1),
//                                             borderRadius:
//                                                 BorderRadius.circular(12)),
//                                         focusedBorder: OutlineInputBorder(
//                                             borderSide: BorderSide(
//                                                 color: Colors.grey, width: 1),
//                                             borderRadius:
//                                                 BorderRadius.circular(12)),
//                                         contentPadding: EdgeInsets.all(4),
//                                         hintStyle: TextStyle(fontSize: 13),
//                                         labelStyle: TextStyle(fontSize: 13),
//                                         prefixIcon: Icon(
//                                           Icons.search,
//                                           color: Colors.black,
//                                         ),
//                                         enabled: true,
//                                         hintText: "$searchByValue...",
//                                       ),
//                                       cursorColor: Colors.black,
//                                       showCursor: true,
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     width: 10,
//                                   ),
//                                   GestureDetector(
//                                     onTap: () {
//                                       showFilter();
//                                     },
//                                     child: Row(
//                                       mainAxisSize: MainAxisSize.min,
//                                       children: <Widget>[
//                                         Text("Filter",
//                                             style: TextStyle(
//                                                 color: Colors.black,
//                                                 fontSize: 15)),
//                                         SizedBox(
//                                           width: 5,
//                                         ),
//                                         Stack(
//                                           overflow: Overflow.visible,
//                                           children: <Widget>[
//                                             Icon(
//                                               RatingIcon.filter,
//                                               color: Colors.black,
//                                               size: 17,
//                                             ),
//                                             filter
//                                                 ? Positioned(
//                                                     top: -5,
//                                                     right: -5,
//                                                     child: Icon(
//                                                       Icons.fiber_manual_record,
//                                                       size: 13,
//                                                       color: mColor,
//                                                     ),
//                                                   )
//                                                 : SizedBox()
//                                           ],
//                                         ),
//                                         SizedBox(
//                                           width: 5,
//                                         ),
//                                       ],
//                                     ),
//                                   )
//                                 ],
//                               ),
//                             ),
//                             notFound
//                                 ? Expanded(
//                                     child: Container(
//                                       child: SingleChildScrollView(
//                                         child: Column(
// //                  mainAxisAlignment: MainAxisAlignment.center,
//                                           crossAxisAlignment:
//                                               CrossAxisAlignment.center,
//                                           mainAxisSize: MainAxisSize.min,
//                                           children: <Widget>[
//                                             SizedBox(
//                                               height: 50,
//                                             ),
//                                             Image.asset(
//                                               'drawables/no_result_found.png',
//                                               height: 206,
//                                               width: 172,
//                                             ),
//                                             Text(
//                                               "Oops!!!",
//                                               style: TextStyle(fontSize: 20),
//                                             ),
//                                             Text(
//                                               "No result found.",
//                                               style: TextStyle(fontSize: 20),
//                                             ),
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                   )
//                                 : Expanded(
//                                     child: RefreshIndicator(
//                                       onRefresh: _refresh,
//                                       child: ListView.builder(
//                                           controller: listViewController,
//                                           itemCount: temp.length < min
//                                               ? temp.length
//                                               : temp.length + 1,
//                                           itemBuilder: (context, index) {
//                                             return index == temp.length
//                                                 ? showMore()
//                                                 : buildStoreList(
//                                                     USER_DETAILES,
//                                                     temp[index],
//                                                     temp[index].categoryidArray,
//                                                     lat,
//                                                     lng);
//                                           }),
//                                     ),
//                                   ),
//                           ],
//                         ),
//                       ),
//                     ),
//                     Visibility(
//                       visible: loadMore,
//                       child: neoLoader(),
//                     )
//                   ],
//                 ),
//     );
//   }
// }
//
// class buildStoreList extends StatefulWidget {
//   User user;
//
//   Info info;
//   List<CategoryidArray> categoryidArray;
//   double lat, lng;
//
//   buildStoreList(
//       this.user, this.info, this.categoryidArray, this.lat, this.lng);
//
//   @override
//   _buildStoreListState createState() => _buildStoreListState();
// }
//
// class _buildStoreListState extends State<buildStoreList>
//     with SingleTickerProviderStateMixin {
//   var auth = new HttpRequests();
//   var storeDetail;
//   static var mColor = Color(0xFFe9991e);
//
// //  static var mColor2 = Color(0xFF6D4C41);
//
// //  var mAccentColor = Color(0xFFf82d70);
//   NearbyStores nearbyStores;
//   var distance = 25;
//   bool f;
//   int clrIndex;
//   var bColor = Color(0xFFe9991e);
//
//   var storeTypeList = [
//     "Grocery",
//     "Pets",
//     "Toys, Baby Products",
//     "Car Accessories",
//     "Footwear",
//     "Electronics",
//     "Beauty & Grooming",
//     "Restaurants",
//     "Stationery",
//     "Kitchen Appliances",
//     "Kitchen Ware",
//     "Dairy Products",
//     "Bags & Luggage",
//     "Sports",
//     "Fitness & Outdoor",
//     "Beverages",
//     "Fashion Accessories",
//     "Health & Hygiene",
//     "Desserts",
//     "Bakery",
//     "Florist & Garden Tools",
//     "Electricals",
//     "Super Toys",
//     "Departmental Store",
//     "Pharmacy"
//   ];
//
//   var borderColor = [
//     Color(0xFF6D4C41),
//     Color(0xFF388E3C),
//     Color(0xFF1565C0),
//     Color(0xFFB3E5FC),
//     Color(0xFFA1887F),
//     Color(0xFFCDDC39),
//     Color(0xFFFF5252),
//     Color(0xFF880E4F),
//     Color(0xFFAA00FF),
//     Color(0xFF448AFF),
//     Color(0xFF4DB6AC),
//     Color(0xFFFFECB3),
//     Color(0xFFB0BEC5),
//     Color(0xFFF57C00),
//     Color(0xFF76FF03),
//     Color(0xFFCE93D8),
//     Color(0xFFe9991e),
//     Color(0xFFEEFF41),
//     Color(0xFFF48FB1),
//     Color(0xFFAD1457),
//     Color(0xFF43A047),
//     Color(0xFFA7FFEB),
//     Color(0xFF01579B),
//     Color(0xFFAFB42B),
//     Color(0xFFE65100),
//   ];
//
//   addToFav() async {
//     print(USER_DETAILES.data.loginDetails.userId);
//     print(widget.info.storeId);
//     print(USER_DETAILES.data.loginDetails.authKey);
//     var r = await auth.addToFavourite(USER_DETAILES.data.loginDetails.userId,
//         widget.info.storeId, distance, USER_DETAILES.data.loginDetails.authKey);
// //    setState(() {
// //      if (r == "success") {
// //        Flushbar(
// //          message: "Shop Added to your Favourite List",
// //          icon: Icon(
// //            Icons.favorite,
// //            color: mColor,
// //          ),
// //          duration: Duration(seconds: 2),
// //          flushbarPosition: FlushbarPosition.BOTTOM,
// //        )..show(context);
// //      } else
// //        Flushbar(
// //          message: "Something went wrong!",
// //          icon: Icon(
// //            Icons.info_outline,
// //            color: mColor,
// //          ),
// //          duration: Duration(seconds: 2),
// //          flushbarPosition: FlushbarPosition.BOTTOM,
// //        )..show(context);
// //    });
//   }
//
//   removeFav() async {
//     var r = await auth.removeFavStore(USER_DETAILES.data.loginDetails.userId,
//         widget.info.storeId, distance, USER_DETAILES.data.loginDetails.authKey);
//
//     setState(() {
//       if (r == "success") {
//         Flushbar(
//           message: "Shop Removed from Favourites",
//           duration: Duration(seconds: 2),
//           icon: Icon(
//             Icons.favorite_border,
//             color: mColor,
//           ),
//         )..show(context);
//       }
//     });
//   }
//
// //  setStoreImage() {
// //    setState(() {
// //      if (widget.info.logo.toString().isNotEmpty)
// //        imageUrl = widget.info.logo.toString();
// //      else if (widget.info.imageUrl.toString().isNotEmpty)
// //        imageUrl = widget.info.imageUrl.toString();
// //      else
// //        imageUrl = null;
// ////      if(widget.info.logo.toString().isEmpty)
// ////        imageUrl = widget.info.imageUrl.toString();
// ////      else imageUrl = null;
// //    });
// //
// //  }
//
//   setBorderColor() {
//     setState(() {
//       for (int x = 0; x < storeTypeList.length; x++) {
//         if (storeTypeList[x].contains(widget.info.storeType)) {
//           clrIndex = x;
//           bColor = borderColor[x];
//         }
//       }
//     });
//   }
//
//   /////////Order now Color Animation/////////
// //  Animation<Color> animation;
// //  AnimationController animationController;
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     print("NAME: ${widget.info.storeName} || IMAGE URL: ${widget.info.logo}");
//     super.initState();
// //    setStoreImage();
//     setBorderColor();
// //    animationController =    AnimationController(duration: const Duration(milliseconds: 1999), vsync: this);
// //    animation = ColorTween(begin: mColor, end: Colors.lime).animate(animationController);
// //    changeColors();
//   }
//
// //  Future changeColors() async {
// //    while (true) {
// //      await new Future.delayed(const Duration(milliseconds: 2000), () {
// //        if (animationController.status == AnimationStatus.completed) {
// //          animationController.reverse();
// //          setState(() {});
// //        } else {
// //          animationController.forward();
// //          setState(() {});
// //        }
// //      });
// //    }
// //  }
// //  startAnimation() {
// //    animationController.forward();
// //    startAnimation();
// //}
//
// //  @override
// //  void dispose() {
// //    // TODO: implement dispose
// //    super.dispose();
// //    animationController.dispose();
// //  }
// //  _launchURL(var lat, var lng) async {
// //    var url = 'geo:$lat,$lng';
// //    if (await canLaunch(url)) {
// //      await launch(url);
// //    } else {
// //      throw 'Could not launch $url';
// //    }
// //  }
//
//   Widget storeCard() {
//     return Container(
//       color: widget.info.order_enabled == 1 ? Colors.white : Colors.black12,
//       child: Stack(
//         children: <Widget>[
//           Container(
//             padding: EdgeInsets.all(2),
// //              height: 250,
//             margin: EdgeInsets.all(0),
//             child: Column(
//               mainAxisSize: MainAxisSize.max,
//               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//               children: <Widget>[
//                 InkWell(
//                   onTap: () async {
//                     print("STORE TYPE & CAT: ${widget.info.storeType} ||}");
//                     if (widget.info.order_enabled == 1) {
//                       await Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => StoreInventory(
//                                     nearbyStores,
//                                     widget.info,
//                                     widget.categoryidArray,
//                                   )));
//                       // mainAppState.getShoppingListCount();
//                     } else {
//                       Fluttertoast.showToast(
//                           textColor: Colors.white,
//                           toastLength: Toast.LENGTH_LONG,
//                           timeInSecForIosWeb: 4,
//                           msg: "Please contact merchant via call & message...",
//                           backgroundColor: Colors.blueAccent);
//                     }
//                   },
//                   child: Container(
//                     padding: EdgeInsets.all(4),
//                     child: Row(
//                       mainAxisSize: MainAxisSize.min,
// //                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: <Widget>[
//                         ClipRRect(
//                           borderRadius: BorderRadius.circular(12),
//                           child: Image.network(
//                             widget.info.logo,
//                             fit: BoxFit.cover,
//                             height: 100,
//                             width: 100,
//                           ),
//                         ),
//                         Container(
//                           alignment: Alignment.centerLeft,
//                           width: MediaQuery.of(context).size.width - 120,
//                           child: ListTile(
//                             title: Column(
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: <Widget>[
//                                 Text("${widget.info.storeType}",
//                                     maxLines: 1,
//                                     overflow: TextOverflow.ellipsis,
//                                     style: TextStyle(fontSize: 13)),
//                                 Text("${widget.info.storeName}",
//                                     maxLines: 2,
//                                     overflow: TextOverflow.ellipsis,
//                                     style: TextStyle(fontSize: 16)),
//                               ],
//                             ),
//                             subtitle: Column(
//                               mainAxisSize: MainAxisSize.min,
//                               crossAxisAlignment: CrossAxisAlignment.start,
//                               children: <Widget>[
//                                 Text(
//                                     "${widget.info.address}, ${widget.info.location}",
//                                     maxLines: 3,
//                                     overflow: TextOverflow.ellipsis,
//                                     style: TextStyle(fontSize: 13)),
//                                 SizedBox(
//                                   height: 17,
//                                 ),
//                               ],
//                             ),
// //                                trailing: widget.info.rating == 0
// //                                    ? SizedBox()
// //                                    : Container(
// //                                    alignment: Alignment.centerRight,
// //                                    width: 50,
// //                                    child: Row(
// //                                      children: <Widget>[
// //                                        Icon(
// //                                          Icons.grade,
// //                                          color: Colors.amberAccent,
// //                                        ),
// //                                        Text("${widget.info.rating}")
// //                                      ],
// //                                    )
// //                                )
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//                 Column(
//                   children: <Widget>[
//                     Container(
// //                      padding: EdgeInsets.only(left: 4, right: 4, bottom: 4),
// //                      alignment: Alignment.bottomRight,
//                       width: MediaQuery.of(context).size.width,
//                       child: Padding(
//                         padding: const EdgeInsets.all(4.0),
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           mainAxisSize: MainAxisSize.max,
//                           children: <Widget>[
//                             Expanded(
//                               child: Row(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 mainAxisSize: MainAxisSize.min,
//                                 children: <Widget>[
//                                   Text(
//                                     "Distance:",
//                                     style: TextStyle(fontSize: 11),
//                                   ),
//                                   widget.info.distance != null
//                                       ? Text(
//                                           " ${(widget.info.distance).toStringAsFixed(2)}Km",
//                                           style: TextStyle(
//                                               color: mColor, fontSize: 11))
//                                       : Text("--"),
//                                 ],
//                               ),
//                             ),
//                             RatingBar(
//                               onRatingUpdate: null,
//                               itemSize: 12,
//                               initialRating:
//                                   double.parse(widget.info.rating.toString()),
//                               minRating: 1,
//                               direction: Axis.horizontal,
//                               allowHalfRating: true,
//                               glow: false,
//                               itemCount: 5,
// //                            unratedColor: Colors.grey,
//                               itemPadding:
//                                   EdgeInsets.symmetric(horizontal: 4.0),
//                               itemBuilder: (context, _) => Icon(
//                                 Icons.star,
//                                 color: mColor,
//                               ),
//                             ),
//                             Expanded(
//                               child: Row(
//                                 mainAxisAlignment: MainAxisAlignment.end,
//                                 crossAxisAlignment: CrossAxisAlignment.end,
//                                 children: <Widget>[
//                                   Text(
//                                     "Min Order:",
//                                     style: TextStyle(fontSize: 11),
//                                   ),
//                                   Text(
//                                     " ₹ ${widget.info.minOrder}",
//                                     style:
//                                         TextStyle(color: mColor, fontSize: 11),
//                                   )
//                                 ],
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             ),
//           ),
//           Positioned(
//               top: 7,
//               right: 7,
//               child: InkWell(
//                 onTap: () {
//                   setState(() {
//                     if (widget.info.favouriteShop == 0) {
//                       widget.info.favouriteShop = 1;
//                       addToFav();
//                     } else {
//                       widget.info.favouriteShop = 0;
//                       removeFav();
//                     }
//                   });
//                 },
//                 child: widget.info.favouriteShop == 1
//                     ? Icon(Icons.favorite, color: mColor)
//                     : Icon(
//                         Icons.favorite_border,
//                         color: mColor,
//                       ),
//               )),
//           Positioned(
//             bottom: 15,
//             right: 65,
//             child: ButtonTheme(
//               height: 20,
//               minWidth: 20,
//               materialTapTargetSize: MaterialTapTargetSize.padded,
//               child: OutlineButton(
//                 padding: EdgeInsets.all(4),
//                 shape: CircleBorder(),
//                 splashColor: Colors.redAccent,
//                 onPressed: () {
// //                  MapsLauncher.launchCoordinates(
// //                      widget.info.lat, widget.info.lng);
//                   print(
//                       "http://maps.google.com/maps?daddr=(${widget.info.lat},${widget.info.lng})&saddr=(${widget.lat},${widget.lng})");
//                   UrlLauncher.launch(
//                       "http://maps.google.com/maps?daddr=(${widget.info.lat},${widget.info.lng})&saddr=(${widget.lat},${widget.lng})");
// //                  _launchURL(widget.info.lat, widget.info.lng);
//                   //url :  https://www.google.com/maps/search/?api=1&query=$lat,$lon
// //                  UrlLauncher.launch("https://maps.apple.com/?q=${widget.info.lat},${widget.info.lng}");
//                 },
//                 textColor: Colors.redAccent,
//                 color: Colors.redAccent,
//                 borderSide: BorderSide(color: Colors.redAccent),
//                 child: Icon(
//                   Icons.location_on,
//                   size: 20,
//                 ),
//               ),
//             ),
//           ),
//           Positioned(
//             bottom: 15,
//             right: 30,
//             child: ButtonTheme(
//               height: 20,
//               minWidth: 20,
//               child: OutlineButton(
//                 padding: EdgeInsets.all(4),
//                 shape: CircleBorder(),
//                 splashColor: Colors.blue,
//                 onPressed: () {
//                   UrlLauncher.launch("sms:${widget.info.mobile}");
//                 },
//                 textColor: Colors.blue,
//                 color: Colors.blue,
//                 borderSide: BorderSide(color: Colors.blue),
//                 child: Icon(
//                   Icons.message,
//                   size: 20,
//                 ),
//               ),
//             ),
//           ),
//           Positioned(
//             bottom: 15,
//             right: -5,
//             child: ButtonTheme(
//               height: 20,
//               minWidth: 20,
//               materialTapTargetSize: MaterialTapTargetSize.padded,
//               child: OutlineButton(
//                 padding: EdgeInsets.all(4),
//                 shape: CircleBorder(),
//                 splashColor: Colors.green,
//                 onPressed: () {
//                   print("phn num:  ${widget.info.mobile}");
//                   UrlLauncher.launch("tel:0${widget.info.mobile}");
//                 },
//                 textColor: Colors.green,
//                 color: Colors.green,
//                 borderSide: BorderSide(color: Colors.green),
//                 child: Icon(
//                   Icons.call,
//                   size: 20,
//                 ),
//               ),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Card(
//         shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(5),
//             side: BorderSide(color: bColor)),
//         elevation: 4.0,
//         child: widget.info.order_enabled == 1
//             ? ClipRRect(
//                 child: Banner(
//                     color: mColor,
//                     location: BannerLocation.topStart,
//                     message: "Order Now",
//                     child: storeCard()),
//               )
//             : storeCard());
//   }
// }
//
// //@override
// //Widget build(BuildContext context) {
// //  return Card(
// //      elevation: 0.0,
// //      margin: EdgeInsets.only(top: 8.0),
// //      child: Stack(
// //        children: <Widget>[
// //          Container(
// ////              height: 250,
// //            margin: EdgeInsets.all(0),
// //            child: Column(
// //              mainAxisSize: MainAxisSize.max,
// //              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
// //              children: <Widget>[
// //                InkWell(
// //                  onTap: () async {
// //                    await Navigator.push(
// //                        context,
// //                        MaterialPageRoute(
// //                            builder: (context) =>
// //                                StoreInventory(
// //                                  nearbyStores,
// //                                  USER_DETAILES,
// //                                  widget.info,
// //                                  widget.categoryidArray,
// //                                )));
// //                    mainAppState.getShoppingListCount();
// //                  },
// //                  child: Container(
// ////                      padding: const EdgeInsets.all(4.0),
// //                    child: Row(
// //                      mainAxisSize: MainAxisSize.min,
// ////                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
// //                      children: <Widget>[
// ////                          imageUrl == null ? Image(
// ////                                  image: AssetImage(
// ////                                      'drawables/logo_neo_mart_splash.png'),
// ////                                  height: 100,
// ////                                  width: 100,
// ////                                )
// ////                              :
// ////                          Image.network(
// ////                            imageUrl,
// ////                            height: 100,
// ////                            width: 100,
// ////                          ),
// //                        Container(
// //                          alignment: Alignment.centerLeft,
// //                          width: MediaQuery.of(context).size.width - 8,
// //                          child: ListTile(
// //                              title: Text(
// //                                  "${widget.info.storeName}",
// //                                  maxLines: 2,
// //                                  overflow: TextOverflow.ellipsis,
// //                                  style: TextStyle(fontSize: 18)
// //                              ),
// //                              subtitle: Text(
// //                                  "${widget.info.address}",
// //                                  maxLines: 3,
// //                                  overflow: TextOverflow.ellipsis,
// //                                  style: TextStyle(fontSize: 15)
// //                              ),
// //                              trailing: Container(
// //                                  alignment: Alignment.centerRight,
// //                                  width: 50,
// //                                  child: Row(
// //                                    children: <Widget>[
// //                                      Icon(
// //                                        Icons.grade,
// //                                        color: Colors.amberAccent,
// //                                      ),
// //                                      Text("${widget.info.rating}")
// //                                    ],
// //                                  ))),
// //                        ),
// //                      ],
// //                    ),
// //                  ),
// //                ),
// //                Column(
// //                  children: <Widget>[
// //                    Divider(),
// //                    Container(
// //                      padding: EdgeInsets.only(left: 4, right: 4),
// //                      alignment: Alignment.bottomRight,
// //                      width: MediaQuery
// //                          .of(context)
// //                          .size
// //                          .width,
// //                      height: 30,
// //                      child: Row(
// //                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //                        children: <Widget>[
// //                          Row(
// //                            children: <Widget>[
// //                              Icon(
// //                                Icons.local_offer,
// //                                color: mColor,
// //                              ),
// //                              Text("% Discount offer"),
// //                            ],
// //                          ),
// //                          Row(
// //                            children: <Widget>[
// //                              Text("Min Order:"),
// //                              Text(
// //                                " ₹ ${widget.info.minOrder}",
// //                                style: TextStyle(color: mColor),
// //                              )
// //                            ],
// //                          )
// //                        ],
// //                      ),
// //                    ),
// //                  ],
// //                ),
// //              ],
// //            ),
// //          ),
// //          Positioned(
// //              top: 7,
// //              right: 7,
// //              child: InkWell(
// //                onTap: () {
// //                  setState(() {
// //                    if (widget.info.favouriteShop == 0) {
// //                      widget.info.favouriteShop = 1;
// //                      addToFav();
// //                    } else {
// //                      widget.info.favouriteShop = 0;
// //                      removeFav();
// //                    }
// //                  });
// //                },
// //                child: widget.info.favouriteShop == 1
// //                    ? Icon(Icons.favorite, color: mColor)
// //                    : Icon(
// //                  Icons.favorite_border,
// //                  color: mColor,
// //                ),
// //              ))
// //        ],
// //      ));
// //}
// //}
