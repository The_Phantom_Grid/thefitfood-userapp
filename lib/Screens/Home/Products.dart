import 'package:auto_size_text/auto_size_text.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:userapp/Getters/GetSearchProduct.dart';
import 'package:userapp/Getters/GetTopSellingProducts.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Components/SearchProductDescription.dart';
import 'package:userapp/Components/TopSellingProductDescription.dart';
import 'package:userapp/Network/httpRequests.dart';

import '../../constants.dart';
import '../MainApp.dart';
import '../StoreInventory.dart';

_ProductsState productsState;

class Products extends StatefulWidget {
  User user;

  Products(this.user);

  @override
  _ProductsState createState() {
    productsState = _ProductsState();
    return productsState;
  }
}

class _ProductsState extends State<Products> {
  var mColor = Color(0xFFe9991e);
  var searchController = TextEditingController();
  bool loading = true, showAppbar = true, notFound = false, comingSoon = false;
  HttpRequests auth = HttpRequests();
  SearchProduct product = SearchProduct();
  var scrollController = ScrollController();
  double scrollPosition, lastScrollPosition;
  int sortValue = 1;
  TopSellingProduct topSellingProduct;
  bool showCloseIcon = false,
      showSearchContainer = false,
      showTopSelling = false;

  StreamVars streamVars = Get.put(StreamVars());

//  String resultCount = "";
  String searchByValue = "Search for Products";
  List<String> searchBy = [
    "Search for Products",
    "Search for Brands",
  ];

  searchProducts() async {
    product = null;
    loading = true;
    print("SEARCH BY VALUE: $searchByValue");

    if (searchBy[0].contains(searchByValue)) {
      var r = await auth.searchProduct("", searchController.text, "product");
      setState(() {
        product = SearchProduct.fromJson(r);
        if (product.data.length > 0) {
          loading = false;
          notFound = false;
          switch (sortValue) {
            case 1:
              high2low();
              break;
            case 2:
              low2high();
              break;
          }
        } else {
          notFound = true;
          loading = false;
        }
      });
    } else if (searchBy[1].contains(searchByValue)) {
      var r = await auth.searchProduct("", searchController.text, "");
      setState(() {
        product = SearchProduct.fromJson(r);
        product.data.forEach((element) {
          print("PRICE MRP: ${element.mrp}");
          if (element.mrp == null) product.data.remove(element);
        });
        if (product.data.length > 0) {
          loading = false;
          notFound = false;
          switch (sortValue) {
            case 1:
              high2low();
              break;
            case 2:
              low2high();
              break;
          }
        } else {
          notFound = true;
          loading = false;
        }
      });
    }
  }

  getTopSellingProduct() async {
    topSellingProduct = null;
    loading = true;
    var r = await auth.getTopSellingProducts(
        "", "", widget.user.data.loginDetails.authKey);
    setState(() {
      topSellingProduct = TopSellingProduct.fromJson(r);
      if (topSellingProduct.comingSoon == 1) {
        setState(() {
          comingSoon = true;
          loading = false;
        });
      } else {
        if (topSellingProduct.data.topSellingProducts.isNotEmpty) {
          loading = false;
          notFound = false;
          showTopSelling = true;
          if (!showSearchContainer)
            switch (sortValue) {
              case 1:
                high2low();
                break;
              case 2:
                low2high();
                break;
            }
        } else {
          loading = false;
          showTopSelling = false;
        }
      }
    });
  }

  refresh() {
    if (searchController.text.length < 1)
      getTopSellingProduct();
    else
      searchProducts();
  }

  _scrollListener() {
    setState(() {
//      scrollPosition = scrollController.position.pixels;
      if (scrollController.position.userScrollDirection
          .toString()
          .contains("ScrollDirection.forward")) {
        showAppbar = true;
      } else {
        showAppbar = false;
      }
    });
  }

  selectSortValue(int val) {
    setState(() {
      sortValue = val;
    });
    switch (sortValue) {
      case 1:
        high2low();
        break;
      case 2:
        low2high();
        break;
    }
  }

  showSortFilter() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
            return Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "SORT",
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                  Divider(
                    height: 0,
                  ),
                  RadioListTile(
                    onChanged: (val) {
                      selectSortValue(val);
                      Navigator.pop(context);
                    },
                    value: 1,
                    groupValue: sortValue,
                    title: Text("Price  -  High to Low"),
                  ),
                  RadioListTile(
                    onChanged: (val) {
                      selectSortValue(val);
                      Navigator.pop(context);
                    },
                    value: 2,
                    groupValue: sortValue,
                    title: Text("Price  -  Low to High"),
                  ),
                ],
              ),
            );
          });
        });
  }

  low2high() {
    if (!showSearchContainer) {
      Comparator<TopSellingProducts> priceComparator = (a, b) =>
          double.tryParse(a.sellingPrice.toString())
              .compareTo(double.tryParse(b.sellingPrice.toString()));
      setState(() {
        topSellingProduct.data.topSellingProducts.sort(priceComparator);
        topSellingProduct.data.topSellingProducts.forEach((element) {
          print("NAME: ${element.productName} :: ${element.sellingPrice}");
        });
      });
    } else {
      Comparator<SearchProductData> priceComparator = (a, b) =>
          double.tryParse(a.sellingPrice.toString())
              .compareTo(double.tryParse(b.sellingPrice.toString()));
      setState(() {
        product.data.sort(priceComparator);
      });
    }
  }

  high2low() {
    if (!showSearchContainer) {
      Comparator<TopSellingProducts> priceComparator = (b, a) {
        return double.tryParse(a.sellingPrice.toString())
            .compareTo(double.tryParse(b.sellingPrice.toString()));
      };
      setState(() {
        topSellingProduct.data.topSellingProducts.sort(priceComparator);
      });
    } else {
      Comparator<SearchProductData> priceComparator = (b, a) =>
          double.tryParse(a.sellingPrice.toString())
              .compareTo(double.tryParse(b.sellingPrice.toString()));
      setState(() {
        product.data.sort(priceComparator);
        product.data.forEach((element) {
          print(element.sellingPrice);
        });
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    scrollPosition = 0.0;
    lastScrollPosition = 0.0;
    scrollController = ScrollController();
    scrollController.addListener(_scrollListener);
    searchController.text = "";
//    searchProducts();
    getTopSellingProduct();
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2 - 20;
    final double itemWidth = size.width / 2;

    return IgnorePointer(
      ignoring: streamVars.addingItem.value,
      child: Scaffold(
        extendBody: true,
        body: comingSoon
            ? Center(
                child: Text(
                  "Coming Soon",
                  style: TextStyle(
                      fontSize: 16, fontWeight: FontWeight.bold, color: mColor),
                ),
              )
            : loading
                ? neoLoader()
                : Stack(
                    children: <Widget>[
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(4),
                            color: Colors.white,
                            height: 40,
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  child: TextField(
                                    controller: searchController,
                                    autofocus: false,
                                    onChanged: (text) {
                                      if (searchController.text.isNotEmpty) {
                                        setState(() {
                                          showCloseIcon = true;
                                        });
                                      }
                                    },
                                    onSubmitted: (text) {
                                      if (searchController.text.isNotEmpty) {
                                        searchController.text = text;
                                        setState(() {
                                          loading = true;
                                          showSearchContainer = true;
                                        });
                                        searchProducts();
                                      }
                                    },
                                    decoration: InputDecoration(
                                      suffixIcon: showCloseIcon
                                          ? GestureDetector(
                                              onTap: () {
                                                setState(() {
                                                  searchController.text = "";
                                                  showCloseIcon = false;
                                                  showSearchContainer = false;
                                                  notFound = false;
                                                });
                                              },
                                              child: Icon(
                                                Icons.close,
                                                color: Colors.black,
                                                size: 18,
                                              ),
                                            )
                                          : PopupMenuButton<String>(
                                              itemBuilder: (context) {
                                                return searchBy.map((str) {
                                                  return PopupMenuItem(
                                                    value: str,
                                                    child: Text(str),
                                                  );
                                                }).toList();
                                              },
                                              child: Icon(Icons.arrow_drop_down,
                                                  color: Colors.black),
                                              onSelected: (v) {
                                                setState(() {
                                                  searchByValue = v;
                                                  print(searchByValue);
                                                  searchController.text = "";
                                                });
                                              },
                                            ),
                                      filled: true,
                                      fillColor: Colors.black12,
                                      enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black12, width: 0),
                                          borderRadius:
                                              BorderRadius.circular(12)),
                                      focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.black12, width: 0),
                                          borderRadius:
                                              BorderRadius.circular(12)),
                                      contentPadding: EdgeInsets.all(4),
                                      hintStyle: TextStyle(fontSize: 13),
                                      labelStyle: TextStyle(fontSize: 13),
                                      prefixIcon: Icon(Icons.search,
                                          color: Colors.black),
//                    suffixIcon: Text(resultCount),
                                      enabled: true,
                                      hintText: "$searchByValue...",
                                    ),
                                    cursorColor: Colors.black,
                                    showCursor: true,
                                  ),
                                ),
                                SizedBox(
                                  width: 6,
                                ),
                                GestureDetector(
                                  onTap: () {
                                    showSortFilter();
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.all(4.0),
                                    child: Row(
                                      children: <Widget>[
                                        Text("Sort"),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Icon(
                                          Icons.sort,
                                          color: Colors.black,
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                notFound
                                    ? Expanded(
                                        child: Container(
                                          child: SingleChildScrollView(
                                            child: Column(
//                  mainAxisAlignment: MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisSize: MainAxisSize.min,
                                              children: <Widget>[
                                                SizedBox(
                                                  height: 50,
                                                ),
                                                Image.asset(
                                                  'drawables/no_result_found.png',
                                                  height: 206,
                                                  width: 172,
                                                ),
                                                Text(
                                                  "Oops!!!",
                                                  style:
                                                      TextStyle(fontSize: 20),
                                                ),
                                                Text(
                                                  "No result found.",
                                                  style:
                                                      TextStyle(fontSize: 20),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      )
                                    : showSearchContainer
                                        ? Expanded(
                                            child: ListView.builder(
                                            shrinkWrap: true,
                                            physics: ScrollPhysics(),
                                            itemCount: product.data.length,
                                            itemBuilder: (context, index) {
                                              return SingleCartItem(widget.user,
                                                  product.data[index]);
                                            },
                                          ))
                                        : showSearchContainer
                                            ? SizedBox()
                                            : Expanded(
                                                child: ListView(
                                                  shrinkWrap: true,
                                                  scrollDirection:
                                                      Axis.vertical,
                                                  physics: ScrollPhysics(),
                                                  children: <Widget>[
                                                    showTopSelling
                                                        ? Container(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: <
                                                                  Widget>[
                                                                SizedBox(
                                                                  width: 5,
                                                                ),
                                                                Flexible(
                                                                    child: Text(
                                                                  "Top Selling Products",
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .start,
                                                                )),
                                                                Icon(
                                                                    Icons
                                                                        .flash_on,
                                                                    color: Colors
                                                                        .amberAccent)
                                                              ],
                                                            ),
                                                          )
                                                        : SizedBox(),
                                                    showTopSelling
                                                        ? ListView.builder(
                                                            shrinkWrap: true,
                                                            physics:
                                                                NeverScrollableScrollPhysics(),
                                                            itemCount:
                                                                topSellingProduct
                                                                    .data
                                                                    .topSellingProducts
                                                                    .length,
                                                            itemBuilder:
                                                                (context,
                                                                    index) {
                                                              print(
                                                                  "${topSellingProduct.data.topSellingProducts[index].shoppingListQty}");
                                                              return topSellingProduct
                                                                          .data
                                                                          .topSellingProducts[
                                                                              index]
                                                                          .mrp ==
                                                                      null
                                                                  ? SizedBox()
                                                                  : TopSellingItem(
                                                                      widget
                                                                          .user,
                                                                      topSellingProduct
                                                                          .data
                                                                          .topSellingProducts[index]);
                                                            },
                                                          )
                                                        : SizedBox(),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    topSellingProduct
                                                                .data
                                                                .productHistory
                                                                .length ==
                                                            0
                                                        ? SizedBox()
                                                        : Container(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .start,
                                                              mainAxisSize:
                                                                  MainAxisSize
                                                                      .min,
                                                              children: <
                                                                  Widget>[
                                                                SizedBox(
                                                                  width: 5,
                                                                ),
                                                                Flexible(
                                                                    child: Text(
                                                                  "Previously bought items",
                                                                  style: TextStyle(
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold),
                                                                  textAlign:
                                                                      TextAlign
                                                                          .start,
                                                                )),
                                                                Icon(
                                                                    Icons
                                                                        .restore,
                                                                    color: Colors
                                                                        .blue)
                                                              ],
                                                            ),
                                                          ),
                                                    topSellingProduct
                                                                .data
                                                                .productHistory
                                                                .length ==
                                                            0
                                                        ? SizedBox()
                                                        : Container(
                                                            height: 130,
                                                            child: ListView
                                                                .builder(
                                                              shrinkWrap: true,
                                                              scrollDirection:
                                                                  Axis.horizontal,
                                                              physics:
                                                                  ScrollPhysics(),
                                                              itemCount:
                                                                  topSellingProduct
                                                                      .data
                                                                      .productHistory
                                                                      .length,
                                                              itemBuilder:
                                                                  (context,
                                                                      index) {
                                                                return topSellingProduct
                                                                            .data
                                                                            .productHistory[
                                                                                index]
                                                                            .mrp ==
                                                                        ""
                                                                    ? SizedBox()
                                                                    : BoughtItem(
                                                                        widget
                                                                            .user,
                                                                        topSellingProduct
                                                                            .data
                                                                            .productHistory[index]);
                                                              },
                                                            ),
                                                          ),
                                                  ],
                                                ),
                                              ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      neoLoaderStack()
                    ],
                  ),
      ),
    );
  }
}

class BoughtItem extends StatefulWidget {
  User user;
  ProductHistory productHistory;

  BoughtItem(this.user, this.productHistory);

  @override
  _BoughtItemState createState() => _BoughtItemState();
}

class _BoughtItemState extends State<BoughtItem> {
  var mColor = Color(0xFFe9991e);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width - 20,
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              InkWell(
                onTap: () {},
                child: Container(
                  padding: EdgeInsets.only(top: 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      widget.productHistory.productImage == null
                          ? Image(
                              image: AssetImage(
                                  'drawables/logo_neo_mart_splash.png'),
                              height: 100,
                              width: 100,
                            )
                          : Image.network(
                              widget.productHistory.productImage.toString(),
                              height: 100,
                              width: 100,
                            ),
                      Expanded(
                        child: ListTile(
                          title: Text(
                            "${widget.productHistory.productName}",
                            maxLines: 2,
                            style: TextStyle(fontSize: 13),
                            overflow: TextOverflow.ellipsis,
                          ),
                          subtitle:
                              Text("${widget.productHistory.productUnit}"),
                          trailing: Text(
                            "₹ ${widget.productHistory.mrp}",
                            style: TextStyle(color: mColor, fontSize: 18),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TopSellingItem extends StatefulWidget {
  User user;
  TopSellingProducts topSellingProduct;

  TopSellingItem(this.user, this.topSellingProduct);

  @override
  _TopSellingItemState createState() => _TopSellingItemState();
}

class _TopSellingItemState extends State<TopSellingItem> {
  var mColor = Color(0xFFe9991e);

//  var mAccentColor = Color(0xFFac1647);
//
//  var mAccentColor2 = Color(0xFFf82d70);
  var offer_id = "", offer_price = "";
  StreamVars streamVars = Get.put(StreamVars());

  int _itemCount;

  bool _addButton = true, _plusMinus = false;
  HttpRequests auth = HttpRequests();

  addToCart(itemCount) async {
    print(widget.topSellingProduct.masterproductid);
    print(widget.topSellingProduct.merchantProductListId);
    print(widget.topSellingProduct.merchantid);
    print(widget.topSellingProduct.storeId);
    print(widget.user.data.loginDetails.userId);
    print(widget.user.data.loginDetails.authKey);
    await auth.addItemToCart(
        widget.topSellingProduct.masterproductid,
        widget.topSellingProduct.merchantProductListId,
        offer_id,
        offer_price,
        itemCount,
        widget.topSellingProduct.merchantid,
        widget.topSellingProduct.storeId,
        widget.user.data.loginDetails.userId,
        widget.user.data.loginDetails.authKey);
    setState(() {
      streamVars.addingItem.value = false;
    });
  }

  addItem() {
    if (_itemCount == widget.topSellingProduct.itemLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color:  mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color:  mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
      addToCart(_itemCount);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _itemCount = widget.topSellingProduct.shoppingListQty;
    if (widget.topSellingProduct.shoppingListQty != 0) {
      _addButton = false;
      _plusMinus = true;
    }
  }

  Widget newProductCard() {
    return Card(
      elevation: 0.0,
      child: InkWell(
        onTap: () async {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => new TopSellingProductDescription(
                      widget.user,
                      widget.topSellingProduct,
                      _itemCount,
                      widget.topSellingProduct.storeId.toString(),
                      widget.topSellingProduct.merchantid.toString())));

          if (productsState.showCloseIcon)
            productsState.searchProducts();
          else
            productsState.getTopSellingProduct();
        },
        child: Container(
          padding: EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: 75,
                width: 75,
                child: Image.network(
                  widget.topSellingProduct.productImage,
                  height: 75,
                  width: 75,
                  fit: BoxFit.contain,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Text(
                              "${widget.topSellingProduct.storeName}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: mColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Expanded(
                            child: AutoSizeText(
                                "₹ ${double.tryParse(widget.topSellingProduct.sellingPrice.toString()).toStringAsFixed(2)}",
                                maxLines: 1,
                                minFontSize: 9,
                                maxFontSize: 13,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    color: mColor,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                      Text(
                          widget.topSellingProduct.distance == null
                              ? "--"
                              : "${(widget.topSellingProduct.distance).toStringAsFixed(2)} Km",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 10, color: mColor)),
                      Text(
                        "${widget.topSellingProduct.productName}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Container(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                          color: mColor,
                          child: Text("${widget.topSellingProduct.productUnit}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold))),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          widget.topSellingProduct.itemLeft <= 5
                              ? Text(
                                  "Hurry only ${widget.topSellingProduct.itemLeft} left",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic))
                              : SizedBox(),
                          Container(
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  width: 110,
                                  height: 35,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(25)),
                                      border: Border.all(
                                          color: widget.topSellingProduct
                                                      .itemLeft ==
                                                  0
                                              ? Colors.grey
                                              : mColor)),
                                  child: Visibility(
                                    visible: _addButton,
                                    child: OutlineButton(
                                      textColor: mColor,
                                      onPressed:
                                          widget.topSellingProduct.itemLeft == 0
                                              ? null
                                              : () {
                                                  setState(() {
                                                    addItem();
                                                    _addButton = false;
                                                    _plusMinus = true;
                                                  });
                                                },
                                      borderSide:
                                          BorderSide(color: Colors.transparent),
                                      child: Text("Add"),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: _plusMinus,
                                  child: Container(
                                    height: 35,
                                    width: 110,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            height: 40,
                                            width: 30,
                                            child: IconButton(
//                                            textColor: mColor,
                                              onPressed: widget
                                                          .topSellingProduct
                                                          .itemLeft ==
                                                      0
                                                  ? null
                                                  : () {
                                                      setState(() {
                                                        if (_itemCount <= 1) {
                                                          _plusMinus = false;
                                                          _addButton = true;
                                                          _itemCount--;
                                                          addToCart(_itemCount);
                                                          streamVars.addingItem
                                                              .value = true;
                                                        } else
                                                          _itemCount--;
                                                        addToCart(_itemCount);
                                                        streamVars.addingItem
                                                            .value = true;
                                                      });
                                                    },
//                                            borderSide:
//                                            BorderSide(color: Colors.transparent),
                                              icon: Icon(
                                                Icons.remove,
                                                size: 16,
                                                color: mColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                              width: 20,
                                              child: Center(
                                                  child: Text("$_itemCount"))),
                                        ),
                                        Expanded(
                                          child: Container(
                                            height: 40,
                                            width: 30,
                                            child: IconButton(
//                                            textColor: mColor,
                                              onPressed: widget
                                                          .topSellingProduct
                                                          .itemLeft ==
                                                      0
                                                  ? null
                                                  : () {
                                                      setState(() {
                                                        addItem();
                                                        streamVars.addingItem
                                                            .value = true;
                                                      });
                                                    },
//                                            borderSide:
//                                            BorderSide(color: Colors.transparent),
                                              icon: Icon(
                                                Icons.add,
                                                size: 16,
                                                color: mColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2 - 20;
    final double itemWidth = size.width / 2;

    return newProductCard();
  }
}

class SingleCartItem extends StatefulWidget {
  User user;
  SearchProductData productData;

  SingleCartItem(
    this.user,
    this.productData,
  );

  @override
  _SingleCartItemState createState() => _SingleCartItemState();
}

class _SingleCartItemState extends State<SingleCartItem> {
  var mColor = Color(0xFFe9991e);

//  var mAccentColor = Color(0xFFac1647);
//
//  var mAccentColor2 = Color(0xFFf82d70);
  var offer_id = "", offer_price = "";

  int _itemCount;

  bool _addButton = true, _plusMinus = false;
  StreamVars streamVars = Get.put(StreamVars());
  HttpRequests auth = HttpRequests();

  addToCart(itemCount) async {
    streamVars.addingItem.value = true;
    await auth.addItemToCart(
        widget.productData.masterProductId,
        widget.productData.merchantproductList,
        offer_id,
        offer_price,
        itemCount,
        widget.productData.merchantid,
        widget.productData.storeId,
        widget.user.data.loginDetails.userId,
        widget.user.data.loginDetails.authKey);
//

//    Fluttertoast.showToast(
//        msg: "Cart Updated",
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        timeInSecForIosWeb: 1,
//        backgroundColor: mColor,
//        textColor: Colors.white,
//        fontSize: 16.0
//    );
    streamVars.addingItem.value = false;
  }

  addItem() {
    if (_itemCount == widget.productData.totalLeft) {
      Flushbar(
        icon: Icon(
          Icons.cancel,
          color:  mColor,
        ),
        duration: Duration(seconds: 2),
        messageText: Text(
          "Out of stock",
          style: TextStyle(color:  mColor),
        ),
      )..show(context);
    } else {
      _itemCount++;
      addToCart(_itemCount);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _itemCount = widget.productData.shoppingListQty;
    if (widget.productData.shoppingListQty != 0) {
      _addButton = false;
      _plusMinus = true;
    }
  }

  Widget newProductCard() {
    return Card(
      elevation: 0.0,
      child: InkWell(
        onTap: () async {
          await Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => new SearchProductDescription(
                      widget.user,
                      widget.productData,
                      _itemCount,
                      widget.productData.storeId.toString(),
                      widget.productData.merchantid.toString())));

          if (productsState.showCloseIcon)
            productsState.searchProducts();
          else
            productsState.getTopSellingProduct();
        },
        child: Container(
          padding: EdgeInsets.all(4),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: 75,
                width: 75,
                child: Image.network(
                  widget.productData.productImage,
                  height: 75,
                  width: 75,
                  fit: BoxFit.contain,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(left: 8, right: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            flex: 2,
                            child: Text(
                              "${widget.productData.storename}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 14,
                                  color: mColor,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Expanded(
                            child: AutoSizeText(
                                "₹ ${double.tryParse(widget.productData.sellingPrice.toString()).toStringAsFixed(2)}",
                                maxLines: 1,
                                minFontSize: 9,
                                maxFontSize: 13,
                                overflow: TextOverflow.ellipsis,
                                textAlign: TextAlign.end,
                                style: TextStyle(
                                    color: mColor,
                                    fontWeight: FontWeight.bold)),
                          ),
                        ],
                      ),
                      Text(
                          "${(widget.productData.distance).toStringAsFixed(2)} Km",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(fontSize: 10, color: mColor)),
                      Text(
                        "${widget.productData.productName}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Container(
                          padding: EdgeInsets.fromLTRB(5, 0, 5, 0),
                          color: mColor,
                          child: Text("${widget.productData.productUnit}",
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold))),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          widget.productData.totalLeft <= 5
                              ? Text(
                                  "Hurry only ${widget.productData.totalLeft} left",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontStyle: FontStyle.italic))
                              : SizedBox(),
                          Container(
                            child: Stack(
                              children: <Widget>[
                                Container(
                                  width: 110,
                                  height: 35,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.all(Radius.circular(25)),
                                      border: Border.all(
                                          color:
                                              widget.productData.totalLeft == 0
                                                  ? Colors.grey
                                                  : mColor)),
                                  child: Visibility(
                                    visible: _addButton,
                                    child: OutlineButton(
                                      textColor: mColor,
                                      onPressed:
                                          widget.productData.totalLeft == 0
                                              ? null
                                              : () {
                                                  setState(() {
                                                    addItem();
                                                    _addButton = false;
                                                    _plusMinus = true;
                                                  });
                                                },
                                      borderSide:
                                          BorderSide(color: Colors.transparent),
                                      child: Text("Add"),
                                    ),
                                  ),
                                ),
                                Visibility(
                                  visible: _plusMinus,
                                  child: Container(
                                    height: 35,
                                    width: 110,
                                    child: Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            height: 40,
                                            width: 30,
                                            child: IconButton(
//                                            textColor: mColor,
                                              onPressed: widget.productData
                                                          .totalLeft ==
                                                      0
                                                  ? null
                                                  : () {
                                                      setState(() {
                                                        if (_itemCount <= 1) {
                                                          _plusMinus = false;
                                                          _addButton = true;
                                                          _itemCount--;
                                                          addToCart(_itemCount);
                                                        } else
                                                          _itemCount--;
                                                        addToCart(_itemCount);
                                                      });
                                                    },
//                                            borderSide:
//                                            BorderSide(color: Colors.transparent),
                                              icon: Icon(
                                                Icons.remove,
                                                size: 16,
                                                color: mColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Expanded(
                                          child: Container(
                                              width: 20,
                                              child: Center(
                                                  child: Text("$_itemCount"))),
                                        ),
                                        Expanded(
                                          child: Container(
                                            height: 40,
                                            width: 30,
                                            child: IconButton(
//                                            textColor: mColor,
                                              onPressed: widget.productData
                                                          .totalLeft ==
                                                      0
                                                  ? null
                                                  : () {
                                                      setState(() {
                                                        addItem();
                                                        streamVars.addingItem
                                                            .value = true;
                                                      });
                                                    },
//                                            borderSide:
//                                            BorderSide(color: Colors.transparent),
                                              icon: Icon(
                                                Icons.add,
                                                size: 16,
                                                color: mColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2 - 20;
    final double itemWidth = size.width / 2;

    return newProductCard();
  }
}
