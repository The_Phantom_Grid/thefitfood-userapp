import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';
import 'package:userapp/Components/CategoryCard.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Components/LoaderComponents.dart';
import 'package:userapp/Components/MealCard.dart';
import 'package:userapp/Getters/GetProfile.dart';
import 'package:userapp/Getters/GetStoreDetails.dart';
import 'package:userapp/Getters/GetStoreInfo.dart';
import 'package:userapp/Screens/AllProducts.dart';
import 'package:userapp/constants.dart';

import '../../Components/CommonUtility.dart';
import '../../Components/CommonUtility.dart';
import '../../Components/CommonUtility.dart';
import '../../Components/CommonUtility.dart';
import '../../Components/CommonUtility.dart';
import '../../Components/LoaderComponents.dart';
import '../../Components/LoaderComponents.dart';
import '../../Components/LoaderComponents.dart';
import '../../Components/ProductSearch.dart';
import '../../Getters/GetProducts.dart';
import '../../Getters/GetProducts.dart';
import '../../Network/httpRequests.dart';
import '../../Network/httpRequests.dart';

_DashboardState dashboardState;

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() {
    dashboardState = _DashboardState();
    return dashboardState;
  }
}

class _DashboardState extends State<Dashboard> {
  TextEditingController searchController = TextEditingController();
  ProfileData profileData;

  HttpRequests requests = HttpRequests();
  bool loadStoreInfo = true,
      loadProducts = true,
      noProducts = false,
      loadStore = true;
  StoreInfo storeInfo;
  Position currentPosition;
  ProductList productList;
  List pList = [];
  String storeOpenTime, storeCloseTime;
  String storeTime;
  Profile profile;
  var auth = HttpRequests();

  String full_address, username, email, packageVersion;

  getStoreInfo() async {
    print(
        "CURRETN POSITION: ${currentPosition.latitude.toString()} || ${currentPosition.longitude.toString()}");
    var response = await requests.getStoreInfo(
        USER_DETAILES.data.loginDetails.userId.toString(),
        currentPosition.latitude.toString(),
        currentPosition.longitude.toString());
    if (response != null &&
        response['status'].toString().toLowerCase() == "success") {
      print(response);
      setState(() {
        storeInfo = StoreInfo.fromJson(response);
        STORE_INFO = storeInfo;
        MERCHANT_ID = STORE_INFO.info[0].merchantId.toString();
        // MERCHANT_ID = "13123";
        STORE_ID = STORE_INFO.info[0].storeId.toString();
        // STORE_ID = "12751";
        loadStoreInfo = false;
      });
      print("product Categories${storeInfo.info[0].productCategories[0].catName}");
      getProducts();
      getStoreDetails();
    } else {
      setState(() {
        loadStoreInfo = false;
      });
    }
  }

  // LATITUDE: 28.649042 || 77.0221509

  getStoreDetails() async {
    var response = await requests.getStoreDetails();
    if (response != null) {
      print("GETTING STORE DETAILS");
      STORE_DETAILS = StoreDetails.fromJson(response);
      print("NAME: ${STORE_DETAILS.data.shopDetails.storeName}");
      print("LATITUDE: ${STORE_DETAILS.data.shopDetails.lat} || ${STORE_DETAILS
          .data.shopDetails.lng}");
      setState(() {
        try {
          storeOpenTime =
              STORE_DETAILS.data.shopDetails.storeOpenTime.toString();
          print("STOROPEN TIME>>> $storeOpenTime");
          storeCloseTime =
              STORE_DETAILS.data.shopDetails.storeCloseTime.toString();
          print("STORCLOSE TIME>>> $storeCloseTime");
          storeTime =
          "${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(DateTime.parse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} $storeOpenTime").millisecondsSinceEpoch))} to ${DateFormat('hh:mma').format(DateTime.fromMillisecondsSinceEpoch(DateTime.parse("${DateFormat('yyyy-MM-dd').format(DateTime.now())} $storeCloseTime").millisecondsSinceEpoch))}";
        } catch (e) {
          showToast(e.toString(), Colors.black);
          loadStore = false;
        }
      });
    } else {
      loadStore = false;
    }
  }

  getProducts() async {
    pList.clear();
    print("hitttt");
    print("${STORE_INFO.info[0].storeId}");
    var response = await requests.getProducts(
        "10", "0", STORE_INFO.info[0].storeId.toString());
    productList = ProductList.fromJson(response);
    pList = productList.data;
    if (response != null && response['data'] != null) {
      print("RESPONSE ->> $response");
      if (pList.isNotEmpty) {
        setState(() {
          print("PRODUCT LIST: ${productList}");
          loadProducts = false;
          noProducts = false;
        });
      } else {
        setState(() {
          print("PRODUCT LIST: ${productList}");
          loadProducts = false;
          noProducts = true;
        });
      }
    } else {
      setState(() {
        loadProducts = false;
      });
    }
  }

  getMyLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    try {
      geolocator
          .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
          .then((Position position) {
        currentPosition = position;
        setLocation(position.latitude, position.longitude);
      });
    } catch (e) {
      print("LOCATION EXCEPTION -> $e");
    }
  }

  setLocation(double lat, double lng) async {
    await requests.updateLocation(
        lat, lng, USER_DETAILES.data.loginDetails.authKey);
    preferences?.setDouble('lat', lat);
    preferences?.setDouble('lng', lng);
    getStoreInfo();
  }
  getProfileData() async {
    var r = await auth.getProfile(USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      profile = Profile.fromJson(r);
      profileData = profile.prifiledata[0];
      print(profile.prifiledata[0].id);
      print(profile.status);
      print(profileData.username);
      print(profileData.email);
      print(profileData.address);
      print(profileData.avatarPath);
      if (profileData.email != null &&
          profileData.address != "" &&
          profileData.avatarPath != null) {
        profileData = profile.prifiledata[0];
        username = profileData.firstname;
        email = profileData.email;
        if (profileData.avatarPath.contains('jpg') ||
            profileData.avatarPath.contains('jpeg') ||
            profileData.avatarPath.contains('png'))

        print("token ======== ${USER_DETAILES.data.loginDetails.authKey}");
      } else {

      }
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    getMyLocation();
    getProfileData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar: AppBar(
title: Text("Hi ${username}",style: TextStyle(fontSize: 18),),
      ), backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.only(bottom: 100),
        child: Stack(
          children: <Widget>[
            Padding(padding: EdgeInsets.fromLTRB(15,15,15,0),child:
            Container(
                height: 150 + MediaQuery.of(context).padding.top,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: mColor.withOpacity(.4),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                        bottomLeft: Radius.circular(30),
                        bottomRight: Radius.circular(30))),
                child: noProducts
                    ? SizedBox()
                    : ClipRRect(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(30),
                            topRight: Radius.circular(30),

                            bottomLeft: Radius.circular(30),
                            bottomRight: Radius.circular(30)),
                    child: loadStoreInfo
                        ? Image.asset('assets/default-image.jpg')
                        : CachedNetworkImage(
                      placeholder: (context, _) =>
                          Image.asset('assets/default-image.jpg'),
                      imageUrl: storeInfo.info[0].logo,
                      fit: BoxFit.cover,
                    )))),

      Padding(
          padding: EdgeInsets.only(
              top: 100 + MediaQuery.of(context).padding.top),
            child:   Column(
                children: <Widget>[
                  Stack(
                    overflow: Overflow.visible,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 22),
                        height: 100,
                        decoration: BoxDecoration(
                            border:
                            Border.all(color: Colors.grey.withOpacity(.4)),
                            color: Colors.white,
                            borderRadius:
                            BorderRadius.all(Radius.circular(25))),
                        child: loadStoreInfo ? listTileLoader() : Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 8.0, vertical: 8),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Expanded(
                                flex: 1,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Text(
                                      "${storeInfo.info[0].storeName}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontSize: 15, color: mColor),
                                          ),
                                    Text(
                                      "${storeInfo.info[0].storeDescription}",
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(fontSize: 13),
                                    ),

                                  ],
                                ),
                              ),

                            ],
                          ),
                        ),
                      ),
                      Positioned(
                        top: -30,
                        left: 40,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.circular(12.0),
                              child: Container(
                                height: 60,
                                width: 60,
                                decoration: BoxDecoration(
                                  // border: Border.all(color: mColor),
                                  color: Colors.white,),
                                child: loadStoreInfo
                                    ? containerLoader()
                                    : CachedNetworkImage(
                                  placeholder: (context, _) =>
                                      Image.asset('assets/default-image.jpg'),
                                  imageUrl: storeInfo.info[0].logo,
                                  fit: BoxFit.cover,),
                              ),
                            ),
                            storeTime == null ? SizedBox() : Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                storeTime, style: textStyleMediumColor,),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(4),
                          height: 40,
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                child: TextField(
                                  autofocus: false,
                                  onTap: () {
                                    Get.to(ProductSearch());
                                  },
                                  readOnly: true,
                                  cursorRadius: Radius.circular(0),
                                  cursorWidth: 0,
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.grey.withOpacity(.1),
                                    disabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black12, width: 0),
                                        borderRadius:
                                        BorderRadius.circular(12)),
                                    enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black12, width: 0),
                                        borderRadius:
                                        BorderRadius.circular(12)),
                                    focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: Colors.black12, width: 0),
                                        borderRadius:
                                        BorderRadius.circular(12)),
                                    contentPadding: EdgeInsets.all(4),
                                    hintStyle: TextStyle(fontSize: 13),
                                    labelStyle: TextStyle(fontSize: 13),
                                    prefixIcon:
                                    Icon(Icons.search, color: Colors.black),
                                    enabled: true,
                                    hintText: "Search here...",
                                  ),
                                  cursorColor: Colors.black,
                                  showCursor: true,
                                ),
                              ),
                              // SizedBox(
                              //   width: 6,
                              // ),
                              // GestureDetector(
                              //   onTap: () {
                              //     // showSortFilter();
                              //   },
                              //   child: Padding(
                              //     padding: const EdgeInsets.all(4.0),
                              //     child: Row(
                              //       children: <Widget>[
                              //         Text("Filter"),
                              //         SizedBox(
                              //           width: 5,
                              //         ),
                              //         Image.asset(
                              //           'assets/filter-icon.png',
                              //           height: 15,
                              //         )
                              //       ],
                              //     ),
                              //   ),
                              // )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        loadStoreInfo ? categoryLoader() : CategoryCard(
                            storeInfo.info[0].productCategories),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text("Today Fresh Arrival",
                                style: textStyleMediumHeadingColor),
                            GestureDetector(
                              onTap: () {
                                Get.to(AllProducts(productList));
                              },
                              child: Text("View all",
                                  style: textStyleMediumHeadingColor),
                            ),
                          ],
                        ),
                        Container(
                            child: loadProducts ? productLoader() :
                            noProducts ? Center(child: Column(
                              children: <Widget>[
                                SizedBox(height: 60,),
                                Text(
                                  "No Products", style: largeTextStyleBlack,),
                              ],
                            )) :
                            GridView.builder(
                                addAutomaticKeepAlives: true,
                                itemCount: pList.length,
                                shrinkWrap: true,
                                padding: EdgeInsets.only(top: 8),
                                physics: NeverScrollableScrollPhysics(),
                                gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    childAspectRatio: MediaQuery
                                        .of(context)
                                        .size
                                        .width < 375 ? 0.85 : 0.95,
                                    crossAxisSpacing: 8,
                                    mainAxisSpacing: 8),
                                itemBuilder: (context, index) {
                                  return MealCard(UniqueKey(), pList[index]);
                                })
                        )
                      ],
                    ),
                  )
                ],

            ))

      ]),
    ));
  }
}
