// import 'package:flutter/material.dart';
// import 'package:userapp/Components/CommonUtility.dart';
// import 'package:userapp/Getters/GetNearbyStores.dart';
// import 'package:userapp/Getters/GetUser.dart';
// import 'package:userapp/Screens/Home/NearMe.dart';
//
// import 'Products.dart';
//
// _HomeScreenState homeScreenState;
//
// class HomeScreen extends StatefulWidget {
//   double lat, lng;
//
//   HomeScreen(this.lat, this.lng);
//
//   @override
//   _HomeScreenState createState() {
//     homeScreenState = _HomeScreenState();
//     return homeScreenState;
//   }
// }
//
// class _HomeScreenState extends State<HomeScreen>
//     with SingleTickerProviderStateMixin {
// //  var mAccentColor = Color(0xFFf82d70);
//   TabController tabController;
//   var store;
//   var token;
//   var nearbyStores = NearbyStores();
//   List<CategoryidArray> categories = List<CategoryidArray>();
//
// //  getData() async {
// //    var storeData = await _getData.getNearbyStores(
// //        10, USER_DETAILES.data.loginDetails.userId, token);
// //    setState(() {
// //      var jData = json.decode(storeData.body);
// //      nearbyStores = NearbyStores.fromJson(jData);
// ////      categories = CategoryidArray.fromJson(jData);
// //    });
// //  }
//
//   static List<Tab> _tabs = <Tab>[
// //    Tab(
// //      child: Container(
// //        child: Center(
// //          child: Text(
// //            "Favourite",
// //          ),
// //        ),
// //      ),
// //    ),
//     Tab(
//       child: Container(
//         child: Center(
//           child: Text(
//             "Stores",
//           ),
//         ),
//       ),
//     ),
//     Tab(
//       child: Container(
//         child: Center(
//           child: Text(
//             "Products",
//           ),
//         ),
//       ),
//     ),
//   ];
//
//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     tabController = new TabController(vsync: this, length: 2, initialIndex: 0);
//     token = USER_DETAILES.data.loginDetails.authKey;
// //    getData();
//   }
//
//   @override
// //  void dispose() {
// //    // TODO: implement dispose
// //    super.dispose();
// //    tabController.dispose();
// //  }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           backgroundColor: Colors.white,
//           elevation: 0.0,
//           bottom: PreferredSize(
//             preferredSize: Size.fromHeight(-7.0),
//             child: TabBar(
//               indicatorWeight: 3,
//               tabs: _tabs,
//               unselectedLabelColor: Colors.black,
//               controller: tabController,
//               labelColor: mColor,
//               indicatorColor: mColor,
//             ),
//           ),
//         ),
//         body: TabBarView(
//           physics: ScrollPhysics(),
//           controller: tabController,
//           children: [
//             // NearMe(token),
//             Products(USER_DETAILES),
//           ],
//         ));
//   }
// }
