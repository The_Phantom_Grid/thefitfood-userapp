import 'dart:convert';
import 'dart:ui';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:userapp/Components/CommonUtility.dart';
import 'package:userapp/Getters/GetFavStores.dart';
import 'package:userapp/Getters/GetNearbyStores.dart';
import 'package:userapp/Getters/GetUser.dart';
import 'package:userapp/Screens/Home/HomeScreen.dart';
import 'package:userapp/Screens/Home/NearMe.dart';
import 'package:userapp/Screens/StoreInventoryFav.dart';
import 'package:userapp/Network/httpRequests.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

import '../../Components/CommonUtility.dart';
import '../../constants.dart';
import '../MainApp.dart';
import '../StoreInventory.dart';

class Favourites extends StatefulWidget {
  @override
  _FavouritesState createState() => _FavouritesState();
}

class _FavouritesState extends State<Favourites> {
  HttpRequests auth = HttpRequests();
  var distance = 10;
  FavStores favStores;

//  NearbyStores nearbyStores;
  bool loading = true, showDefault = false;
  List<FavInfo> favstoreList = [];
  int max = 25, min = 0;
  ScrollController listviewController = ScrollController();
  double lat, lng;

  getFavList() async {
    var r = await auth.getFavourites(USER_DETAILES.data.loginDetails.userId,
        max, min, distance, USER_DETAILES.data.loginDetails.authKey);
    setState(() {
      favStores = FavStores.fromJson(r);
      clearFavList();
    });
  }

  clearFavList() {
    setState(() {
      print(favStores.info.length);
//      print(nearbyStores.info.length);
      for (int x = 0; x < favStores.info.length; x++) {
        if (favStores.info[x].favouriteShop == 0) {
          favStores.info.removeAt(x);
//          nearbyStores.info.removeAt(x);
          x--;
        }
      }
      favstoreList.clear();
      favstoreList.addAll(favStores.info);
      getLatLng();
    });
  }

  getLatLng() async {
    var r = await auth.getProfile(USER_DETAILES.data.loginDetails.authKey);
//    ProfileData profileData = ProfileData.fromJson(r);
    lat = r['prifiledata'][0]['lat'];
    lng = r['prifiledata'][0]['lng'];
    print("LAT::: ${lat} || LONG::: ${lng}");
    setState(() {
      loading = false;
      if (favstoreList.isEmpty) showDefault = true;
    });
  }

  removeFav(int index) async {
    var r = await auth.removeFavStore(
        USER_DETAILES.data.loginDetails.userId,
        favstoreList[index].storeId,
        distance,
        USER_DETAILES.data.loginDetails.authKey);

    setState(() {
      if (r == "success") {
        Flushbar(
          message: "Store Removed from Favourites",
          duration: Duration(seconds: 2),
          icon: Icon(
            Icons.check_circle,
            color: mColor,
          ),
        )..show(context);
        favstoreList.removeAt(index);
        print("LIST LENGTH FAV:: ${favstoreList.length}");
        if (favstoreList.isEmpty)
          setState(() {
            showDefault = true;
          });
//        nearbyStores.info.removeAt(index);
      }
    });
  }

  _scrollListener() {
    print("load");
    if (listviewController.position.pixels >=
        listviewController.position.maxScrollExtent) {
      loadMoreStores();
    }
  }

  loadMoreStores() async {
    min = min + 25;
    var r = await auth.getFavourites(USER_DETAILES.data.loginDetails.userId,
        max, min, distance, USER_DETAILES.data.loginDetails.authKey);
    favStores = FavStores.fromJson(r);
    setState(() {
//      temp.clear();
      favstoreList.addAll(favStores.info);
      loading = false;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listviewController = ScrollController();
    listviewController = ScrollController();
    listviewController.addListener(_scrollListener);
    getFavList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: mColor,
        title: Text(
          "Favourites",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: loading
          ? neoLoader()
          : showDefault
              ? Container(
                  height: MediaQuery.of(context).size.height,
                  child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * .3,
              ),
              Icon(
                Icons.favorite_border,
                color: mColor,
                size: 50,
              ),
              Text("Your Favourite store list looks empty."),
              RichText(
                text: TextSpan(children: <TextSpan>[
                  TextSpan(
                    text: "Tap here",
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: mColor),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
//                            nearmeState
                        mainAppState.pageController.animateTo(0,
                                      duration: Duration(milliseconds: 600),
                                      curve: Curves.decelerate);
                                  // homeScreenState.tabController.animateTo(0,
                                  //     duration: Duration(milliseconds: 200),
                                  //     curve: Curves.decelerate);
                                  mainAppState.setIcon();
                                },
                  ),
                  TextSpan(
                      text: " to start adding.",
                      style: TextStyle(color: Colors.black))
                ]),
              )
            ],
          ),
        ),
      )
          : Center(
          child: Container(
            child: ListView.builder(
                controller: listviewController,
                itemCount: favstoreList.length,
                itemBuilder: (context, index) {
                  return Stack(
                    children: <Widget>[
                      buildFavList(
                        favstoreList[index].storeName,
                        favstoreList[index].location,
                        favstoreList[index].storeLogo,
                        favstoreList[index].rating,
                        favstoreList[index].favouriteShop,
                        USER_DETAILES,
                        favstoreList[index].categoryidArray,
                        favstoreList[index],
//                        nearbyStores,
//                        nearbyStores.info[index],
                        favstoreList[index].storeTypeIcon,
                        favstoreList[index].storeType,
                      ),
                      Positioned(
                        top: 5,
                        right: 0,
                        child: IconButton(
                          icon: Icon(
                            Icons.favorite,
                            color: mColor,
                          ),
                          onPressed: () {
                            print("hit remove");
                            removeFav(index);
                          },
                        ),
                      ),
                      Positioned(
                        bottom: 19,
                        right: 70,
                        child: ButtonTheme(
                          height: 20,
                          minWidth: 20,
                          materialTapTargetSize:
                          MaterialTapTargetSize.padded,
                          child: OutlineButton(
                            padding: EdgeInsets.all(4),
                            shape: CircleBorder(),
                            splashColor: Colors.redAccent,
                            onPressed: () {
//                              MapsLauncher.launchCoordinates(favstoreList[index].lat, favstoreList[index].lng);
                              UrlLauncher.launch(
                                  "http://maps.google.com/maps?daddr=(${favstoreList[index].lat},${favstoreList[index].lng})&saddr=($lat,$lng)");
//                  _launchURL(widget.info.lat, widget.info.lng);
                              //url :  https://www.google.com/maps/search/?api=1&query=$lat,$lon
//
//                             UrlLauncher.launch("geo:${favStores.info[index].lat},${favStores.info[index].lng}");
                            },
                            textColor: Colors.redAccent,
                            color: Colors.redAccent,
                            borderSide:
                            BorderSide(color: Colors.redAccent),
                            child: Icon(
                              Icons.location_on,
                              size: 20,
                            ),
                          ),
                        ),
                      ),

                      Positioned(
                        bottom: 19,
                        right: 35,
                        child: ButtonTheme(
                          height: 20,
                          minWidth: 20,
                          child: OutlineButton(
                            padding: EdgeInsets.all(4),
                            shape: CircleBorder(),
                            splashColor: Colors.blue,
                            onPressed: () {
                              UrlLauncher.launch(
                                  "sms:${favstoreList[index].mobile}");
                            },
                            textColor: Colors.blue,
                            color: Colors.blue,
                            borderSide: BorderSide(color: Colors.blue),
                            child: Icon(
                              Icons.message,
                              size: 20,
                            ),
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 19,
                        right: 0,
                        child: ButtonTheme(
                          height: 20,
                          minWidth: 20,
                          materialTapTargetSize:
                          MaterialTapTargetSize.padded,
                          child: OutlineButton(
                            padding: EdgeInsets.all(4),
                            shape: CircleBorder(),
                            splashColor: Colors.green,
                            onPressed: () {
                              UrlLauncher.launch(
                                  "tel:0${favstoreList[index].mobile}");
                            },
                            textColor: Colors.green,
                            color: Colors.green,
                            borderSide: BorderSide(color: Colors.green),
                            child: Icon(
                              Icons.call,
                              size: 20,
                            ),
                          ),
                        ),
                      ),
//                      Positioned(
//                        top: 5,
//                        left: 5,
//                        child: Container(
//                          padding: EdgeInsets.symmetric(vertical: 4, horizontal: 6),
//                          decoration: BoxDecoration(
//                              color: mColor,
//                              borderRadius: BorderRadius.only(
//                                topLeft: Radius.circular(4),
//                                bottomRight: Radius.circular(4),
//                              ) // green shaped
//                          ),
//                          child: Text("${favStores.info[index].storeType}",
//                            style: TextStyle(color: Colors.white, fontSize: 10),
//                          ),
//                        ),
//                      )
                    ],
                  );
                }),
          )),
    );
  }
}

class buildFavList extends StatefulWidget {
  final storeName;
  final location;
  final rating;
  int fav;
  final imageUrl;
  User user;
  List<CategoryidArrayFav> categoryidArrayfav;
  FavInfo Faveinfo;
  String storeTypeIcon;
  String storeType;

  buildFavList(
      this.storeName,
      this.location,
      this.imageUrl,
      this.rating,
      this.fav,
      this.user,
      this.categoryidArrayfav,
      this.Faveinfo,
      this.storeTypeIcon,
      this.storeType);

  @override
  _buildFavListState createState() => _buildFavListState();
}

class _buildFavListState extends State<buildFavList> {
  var mColor = Color(0xFFe9991e);

//  var mAccentColor = Color(0xFFf82d70);
  HttpRequests auth = HttpRequests();
  var distance = 10;
  var bColor = Color(0xFFe9991e);

  var storeTypeList = [
    "Grocery",
    "Pets",
    "Toys, Baby Products",
    "Car Accessories",
    "Footwear",
    "Electronics",
    "Beauty & Grooming",
    "Restaurants",
    "Stationery",
    "Kitchen Appliances",
    "Kitchen Ware",
    "Dairy Products",
    "Bags & Luggage",
    "Sports",
    "Fitness & Outdoor",
    "Beverages",
    "Fashion Accessories",
    "Health & Hygiene",
    "Desserts",
    "Bakery",
    "Florist & Garden Tools",
    "Electricals",
    "Super Toys",
    "Departmental Store",
    "Pharmacy"
  ];

  var borderColor = [
    Color(0xFF6D4C41),
    Color(0xFF388E3C),
    Color(0xFF1565C0),
    Color(0xFFB3E5FC),
    Color(0xFFA1887F),
    Color(0xFFCDDC39),
    Color(0xFFFF5252),
    Color(0xFF880E4F),
    Color(0xFFAA00FF),
    Color(0xFF448AFF),
    Color(0xFF4DB6AC),
    Color(0xFFFFECB3),
    Color(0xFFB0BEC5),
    Color(0xFFF57C00),
    Color(0xFF76FF03),
    Color(0xFFCE93D8),
    Color(0xFFe9991e),
    Color(0xFFEEFF41),
    Color(0xFFF48FB1),
    Color(0xFFAD1457),
    Color(0xFF43A047),
    Color(0xFFA7FFEB),
    Color(0xFF01579B),
    Color(0xFFAFB42B),
    Color(0xFFE65100),
  ];

  Widget storeCard() {
    return Container(
      color: widget.Faveinfo.order_enabled == 1 ? Colors.white : Colors.black12,
      padding: EdgeInsets.all(4.0),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
//              SizedBox(height: 20,),
          GestureDetector(
            onTap: () async {
              if (widget.Faveinfo.order_enabled == 1) {
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => StoreInventoryFav(
                              widget.Faveinfo,
                              widget.categoryidArrayfav,
                            )));
              } else {
                Fluttertoast.showToast(
                    textColor: Colors.white,
                    toastLength: Toast.LENGTH_LONG,
                    msg: "Please contact merchant via call & message...",
                    backgroundColor: Colors.blueAccent);
              }
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(12),
                  child: Image.network(
                    widget.Faveinfo.storeLogo,
                    height: 100,
                    width: 100,
                    fit: BoxFit.cover,
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  width: MediaQuery.of(context).size.width - 120,
                  child: ListTile(
                    title: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("${widget.storeType}",
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 13)),
                        Text(
                          "${widget.storeName}",
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                    subtitle: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                            "${widget.Faveinfo.address}, ${widget.Faveinfo.location}",
                            maxLines: 3,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 13)),
                        SizedBox(
                          height: 19,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Container(
//            padding: EdgeInsets.all(4),
            child: Padding(
              padding: const EdgeInsets.all(4.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Expanded(
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Distance:",
                          style: TextStyle(fontSize: 11),
                        ),
                        Text(
                            " ${(widget.Faveinfo.distance).toStringAsFixed(2)}Km",
                            style: TextStyle(color: mColor, fontSize: 11)),
                      ],
                    ),
                  ),
//                            SizedBox(width: 20,),
                  RatingBar(
                    onRatingUpdate: null,
                    itemSize: 12,
                    initialRating:
                        double.parse(widget.Faveinfo.rating.toString()),
                    minRating: 1,
                    direction: Axis.horizontal,
                    allowHalfRating: true,
                    glow: false,
                    itemCount: 5,
//                            unratedColor: Colors.grey,
                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                    itemBuilder: (context, _) => Icon(
                      Icons.star,
                      color: mColor,
                    ),
                  ),
//                            SizedBox(width: 20,),
                  Expanded(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Text(
                          "Min Order:",
                          style: TextStyle(fontSize: 11),
                        ),
                        Text(
                          " ₹ ${widget.Faveinfo.minOrder}",
                          style: TextStyle(color: mColor, fontSize: 11),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget myCard() {
    return Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5),
            side: BorderSide(color: bColor)),
        elevation: 4.0,
        child: widget.Faveinfo.order_enabled == 1
            ? ClipRRect(
                child: Banner(
                    color: mColor,
                    location: BannerLocation.topStart,
                    message: "Order Now",
                    child: storeCard()),
              )
            : storeCard());
  }

  Widget myCard2() {
    return Container(
      child: Stack(
        children: <Widget>[
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Card(
                elevation: 0.0,
                margin: EdgeInsets.only(top: 8.0),
                child: Container(
                  padding: EdgeInsets.only(left: 2.0, right: 2.0),
                  width: MediaQuery.of(context).size.width,
                  child: GestureDetector(
                    onTap: () async {
                      await Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => StoreInventoryFav(
                                    widget.Faveinfo,
                                    widget.categoryidArrayfav,
                                  )));
                    },
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: widget.imageUrl == null
                                        ? AssetImage(
                                            'drawables/logo_neo_mart_splash.png')
                                        : NetworkImage(widget.imageUrl))),
                            child: BackdropFilter(
                                filter: ImageFilter.blur(
                                  sigmaX: 1.0,
                                  sigmaY: 1.0,
                                ),
                                child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.black.withOpacity(0.4)),
                                )))),
                  ),
                )),
          ),
          Container(
            child: Positioned(
                bottom: 15,
                left: 15,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "${widget.storeName}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20),
                      ),
                      Text(
                        "${widget.location}",
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                    ],
                  ),
                )),
          ),
          Positioned(
            top: 40,
            right: -7,
            child: Container(
                alignment: Alignment.centerRight,
                width: 50,
                child: Row(
                  children: <Widget>[
                    Text("${widget.rating}",
                        style: TextStyle(
                          color: Colors.white,
                        )),
                    Icon(
                      Icons.star,
                      color: mColor,
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  setBorderColor() {
    print("${storeTypeList.length} ||||||||| ${borderColor.length}");
    setState(() {
      for (int x = 0; x < storeTypeList.length; x++) {
        if (storeTypeList[x].contains(widget.storeType)) {
          bColor = borderColor[x];
        }
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setBorderColor();
  }

  @override
  Widget build(BuildContext context) {
    return myCard();
  }
}
