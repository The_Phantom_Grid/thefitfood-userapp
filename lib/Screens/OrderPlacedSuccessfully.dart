import 'package:flushbar/flushbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';

import '../Components/CommonUtility.dart';
import '../Components/CommonUtility.dart';
import '../Network/httpRequests.dart';
import 'MainApp.dart';
import 'MyOrders.dart';

class OrderPlacedSuccessfully extends StatefulWidget {
  String orderId, activeAddress, tSlot;
  int deliveryTypeOption;

  OrderPlacedSuccessfully(
      this.orderId, this.activeAddress, this.deliveryTypeOption, this.tSlot);

  @override
  _OrderPlacedSuccessfullyState createState() =>
      _OrderPlacedSuccessfullyState();
}

class _OrderPlacedSuccessfullyState extends State<OrderPlacedSuccessfully> {
  double rating = 3;
  Widget rateIcon = Image.asset(
    'drawables/good.png',
    height: 80,
    width: 80,
  );
  String dimension;
  HttpRequests requests = HttpRequests();

  setRating(int val) {
    setState(() {
      if (val == 0) {
        rateIcon = Image.asset(
          'drawables/miserable.png',
          height: 80,
          width: 80,
        );
        dimension = "miserable";
      } else if (val < 2) {
        rateIcon = Image.asset(
          'drawables/sad.png',
          height: 80,
          width: 80,
        );
        dimension = "sad";
      } else if (val < 3) {
        rateIcon = Image.asset(
          'drawables/good.png',
          height: 80,
          width: 80,
        );
        dimension = "good";
      } else if (val < 4) {
        rateIcon = Image.asset(
          'drawables/awesome.png',
          height: 80,
          width: 80,
        );
        dimension = "awesome";
      } else if (val < 5) {
        rateIcon = Image.asset(
          'drawables/marvellous.png',
          height: 80,
          width: 80,
        );
        dimension = "marvellous";
      }
    });
  }

  showRateAlert() {
    showDialog(
        context: context,
        builder: (_) => StatefulBuilder(
              builder: (BuildContext context, StateSetter setState) {
                return AlertDialog(
                  title: Text(
                    "Rate Your Experience",
                    style: TextStyle(fontSize: 15),
                    textAlign: TextAlign.center,
                  ),
                  content: Container(
                      height: 150,
                      width: MediaQuery.of(context).size.width - 100,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          rateIcon,
                          Slider(
                            min: 0,
                            max: 4,
                            value: rating,
                            onChanged: (val) {
                              print(val.toInt());
                              setState(() {
                                rating = val;
                              });
                              setRating(val.toInt());
                            },
                          ),
                        ],
                      )),
                  actions: <Widget>[
                    ButtonTheme(
                      height: 40,
                      minWidth: MediaQuery.of(context).size.width,
                      child: OutlineButton(
                        onPressed: () {
                          Navigator.pop(context);
                          giveFeedback();
                        },
                        child: Text("Submit"),
                        borderSide: BorderSide(color: mColor),
                        textColor: mColor,
                      ),
                    )
                  ],
                );
              },
            ));
  }

  giveFeedback() async {
    var r = await requests.giveFeedback(
        "", dimension, widget.orderId, rating, TOKEN);

    if (r['message'].toString().contains("Thank you for your feedback.")) {
      Flushbar(
        icon: Icon(Icons.insert_emoticon, color: mColor),
        messageText: Text(
          "Thank you for your feedback.",
          style: TextStyle(color: Colors.white),
        ),
        duration: Duration(seconds: 2),
      )..show(context);
    }
  }

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

  @override
  void initState() {
    super.initState();
    print("PICKUP");
    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
    var android = AndroidInitializationSettings('@mipmap/ic_launcher');
    var iOS = IOSInitializationSettings(
        requestAlertPermission: false,
        requestBadgePermission: false,
        requestSoundPermission: true,
        onDidReceiveLocalNotification: onDidReceiveLocalNotification);
    var initSettings = InitializationSettings(android, iOS);
    flutterLocalNotificationsPlugin.initialize(initSettings,
        onSelectNotification: onSelectNotification);
//    if(Theme.of(context).platform == TargetPlatform.iOS)
//      isIOSDevice = true;

    print("kghjgkhgkhgkjhgkjhgkj");
//    showRateAlert();
    setRating(rating.toInt());
    Future.delayed(Duration(seconds: 2)).then((_) {
      showRateAlert();
      showNotification();
    });
  }

  Future onDidReceiveLocalNotification(
      int id, String title, String body, String payload) async {
    // display a dialog with the notification details, tap ok to go to another page
    showDialog(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(title),
        content: Text(body),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
              })
        ],
      ),
    );
  }

  Future onSelectNotification(String payload) async {
    print('Working on select');
    print('PAYLOAD: $payload');
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MyOrders()));
  }

  showNotification() async {
    var android = AndroidNotificationDetails(
        'channel id', 'channel NAME', 'CHANNEL DESCRIPTION');

    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(android, iOS);
    print("${platform.iOS.toString()} platform");

    await flutterLocalNotificationsPlugin.show(
        0,
        "Order Placed",
        "Thanks for shopping! You order has been placed.\n Your order id is ${widget.orderId}.",
        platform);
  }

  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => MainApp()),
            (Route<dynamic> route) => false);
      },
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              children: <Widget>[
                Stack(
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).padding.top),
                      color: Colors.green,
                      child: Column(
                        children: <Widget>[
                          Center(
                              child: Container(
                            height: 70,
                            width: 70,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border:
                                    Border.all(color: Colors.green, width: 2),
                                shape: BoxShape.circle),
                            child: Icon(
                              Icons.check,
                              color: Colors.green,
                              size: 50,
                            ),
                          )),
                          SizedBox(
                            height: 15,
                          ),
                          Text(
                            "Order Placed",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20,
                                color: Colors.white),
                            textAlign: TextAlign.left,
                          ),
                          Text(
                            "Order id: #${widget.orderId}",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14,
                                color: Colors.white),
                          ),
                          SizedBox(
                            height: 60,
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: 150 + MediaQuery.of(context).padding.top,
                          left: 16,
                          right: 16),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            child: Card(
                              color: Colors.white,
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Text("Order Detail"),
                                  Divider(
                                    indent: 30,
                                    endIndent: 30,
                                  ),
                                  SizedBox(
                                    height: 8,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 4, right: 4),
                                    child: Text(
                                      "${widget.activeAddress}",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                  ),
                                  Divider(),
                                  Text(widget.deliveryTypeOption == 0
                                      ? "Home Delivery, ${widget.tSlot}"
                                      : "Store Pickup, ${widget.tSlot}",
                                    style: mediumTextStyleBlack,),
                                  SizedBox(
                                    height: 20,
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
          ),
        ),
        extendBody: true,
        bottomNavigationBar: BottomAppBar(
          elevation: 0.0,
          color: Colors.white30,
          child: Container(
            height: 60,
            padding: EdgeInsets.all(8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: ButtonTheme(
                    height: 100,
                    child: RaisedButton(
                      child: Text("Continue Shopping"),
                      onPressed: () {
                        Get.offAll(MainApp());
                      },
                      color: Colors.green,
                      textColor: Colors.white,
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(27)),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Expanded(
                  flex: 1,
                  child: ButtonTheme(
                    height: 100,
                    child: RaisedButton(
                      child: Text("My Orders"),
                      onPressed: () {
                        Get.offAll(MyOrders());
                      },
                      color: Colors.white,
                      textColor: Colors.green,
                      elevation: 8.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(27)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
