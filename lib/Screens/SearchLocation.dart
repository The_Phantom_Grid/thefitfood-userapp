import 'dart:io';
import 'dart:ui';

import 'package:flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:search_map_place/search_map_place.dart';
import 'package:userapp/Getters/GetUser.dart';

import '../Components/CommonUtility.dart';
import '../Network/httpRequests.dart';
import '../constants.dart';

class SearchLocation extends StatefulWidget {
  String token;
  bool newAccount;

  SearchLocation(this.token, this.newAccount);

  @override
  _SearchLocationState createState() => _SearchLocationState();
}

class _SearchLocationState extends State<SearchLocation> {

  var searchController = TextEditingController();
  String cLocation;
  var marker = [].toSet();
  Position _currentPosition;
  List<Address> addresses;
  var first;
  PermissionStatus permissionStatus;
  bool _loading = true, locating = false;
  List<String> locationList = [];
  List coordinates = [];
  List postalCodes = [];
  double lat, lng;
  HttpRequests auth = HttpRequests();
  String androidApiKey = "AIzaSyAa2HHkeEv6nUPQ8REDxnbBtiY1FjrRorA",
      iosApiKey = "AIzaSyCflFUrYBUOu-X-Izx0voYqtdaXsuq4SVQ";

  LatLng cameraTarget, initialPosition;
  GoogleMapController gController;
  String placeInfo;

  onCameraMove(CameraPosition position) {
    cameraTarget = position.target;
  }

  getFullAddress() async {
    final coordinates =
        Coordinates(cameraTarget.latitude, cameraTarget.longitude);
    addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

    setState(() {
      first = addresses.first;
      placeInfo = first.featureName;
      cLocation = first.subLocality;
      marker = [
        Marker(
            onTap: () async {
              await getFullAddress();
            },
            markerId: MarkerId('cur_loc'),
            infoWindow: InfoWindow(title: placeInfo),
            position: cameraTarget)
      ].toSet();

      lat = first.coordinates.latitude;
      lng = first.coordinates.longitude;
      print("lat: $lat || lng: $lng");
      _loading = false;
      locating = false;
    });
    if (gController != null)
      gController.animateCamera(CameraUpdate.newLatLng(
          LatLng(cameraTarget.latitude, cameraTarget.longitude)));
    showUpdateLocation();
  }

  showUpdateLocation() {
    showModalBottomSheet(
        context: context,
        isDismissible: true,
        isScrollControlled: true,
        builder: (context) {
          return Container(
              padding: EdgeInsets.all(15),
//              height: MediaQuery
//                  .of(context)
//                  .size
//                  .height * .32,
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("Update Location",
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 15,
                  ),
                  Text("Do you wish change your location to $placeInfo."),
                  SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: OutlineButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text("Cancel"),
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        flex: 1,
                        child: FlatButton(
                          onPressed: () {
                            setState(() {
                              locating = true;
                            });
//                          getFullAddress();
                            Navigator.pop(context);
                            setUserLocation();
                          },
                          child: Text("Update"),
                          textColor: Colors.white,
                          color: mColor,
                        ),
                      )
                    ],
                  )
                ],
              ));
        });
  }

  setUserLocation() async {
    print("SETTING:: LAT $lat || LONG $lng || token:: ${widget.token}");
    var r = await auth.updateLocation(lat, lng, widget.token);
    if (r['message'].toString().contains("Location updated successfully")) {
      setState(() {
        locating = false;
      });
      Fluttertoast.showToast(
          msg: "Location updated successfully", toastLength: Toast.LENGTH_LONG);

      if (widget.newAccount)
        Navigator.pop(context,
            ["$cLocation, ${first.locality}", first.postalCode, lat, lng]);
      else
        Navigator.pop(context, [cLocation, first.postalCode, lat, lng]);
    } else {
      setState(() {
        locating = false;
      });
      Fluttertoast.showToast(
          msg: "${r['message'].toString()}", toastLength: Toast.LENGTH_LONG);
    }
  }

  getCurrentLocation() {
    final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      cameraTarget = LatLng(position.latitude, position.longitude);
      getFullAddress();
    }).catchError((e) {
      if (e.toString().contains("PERMISSION_DENIED")) {
//        showUpdateLocation();
      }
    });
  }

  getLatLng() async {
    var r = await auth.getProfile(widget.token);
//    ProfileData profileData = ProfileData.fromJson(r);
    print(r['prifiledata'][0]['lat'].toString());
    print(r['prifiledata'][0]['lng'].toString());
    lat = double.tryParse(r['prifiledata'][0]['lat'].toString());
    lng = double.tryParse(r['prifiledata'][0]['lng'].toString());
    setState(() {
      initialPosition = LatLng(lat, lng);
      cameraTarget = initialPosition;
    });
    getFullAddress();
    print(" profile data LAT::: ${lat} || LONG::: ${lng}");
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLatLng();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    gController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          centerTitle: true,
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios,
              color: Colors.white,
            ),
          ),
          title: Text("Search Location")),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: locating
            ? null
            : () {
                setState(() {
                  locating = true;
                });
                getCurrentLocation();
              },
        label: Text(
          "Get current location",
          style: TextStyle(color: Colors.blue),
        ),
        icon: Icon(Icons.my_location, color: Colors.blue),
        backgroundColor: Colors.white,
      ),
      body: _loading
          ?  neoLoader()
          : Stack(
              children: <Widget>[
                SizedBox.expand(
                  child: Stack(
                    children: <Widget>[
                      GoogleMap(
                        myLocationButtonEnabled: false,
                        rotateGesturesEnabled: true,
                        zoomGesturesEnabled: true,
                        scrollGesturesEnabled: true,
                        onTap: (LatLng value) {
//                    setState(() {
                          cameraTarget = value;
                          getFullAddress();
//                  gController.animateCamera(CameraUpdate.newLatLng(value));
//                    });
                        },
                        initialCameraPosition: CameraPosition(
                          target: initialPosition,
                          zoom: 14.4746,
                          bearing: 30,
                        ),
                        markers: marker,
                        onCameraMove: onCameraMove,
                        mapType: MapType.normal,
                        onMapCreated: (GoogleMapController controller) {
                          gController = controller;
                          gController.animateCamera(
                              CameraUpdate.newLatLng(initialPosition));
                        },
                      ),
                      Positioned(
                        top: 15,
                        right: 15,
                        left: 15,
                        child: SearchMapPlaceWidget(
//                  location: LatLng(cameraTarget.latitude, cameraTarget.longitude),
//                  radius: 30000,
                          placeType: PlaceType.address,
                          apiKey: Platform.isIOS ? iosApiKey : androidApiKey,
                          onSelected: (Place place) async {
                            final geolocation = await place.geolocation;
                            setState(() {
                              cameraTarget = geolocation.coordinates;
                              getFullAddress();
//                    print(geolocation.coordinates.toString() +
//                        "PLACE COORDINATEs" +
//                        cameraTarget.longitude.toString());
//                    print(cameraTarget.latitude.toString() + "||" +
//                        cameraTarget.longitude.toString());
//                    gController.animateCamera(
//                        CameraUpdate.newLatLng(geolocation.coordinates));
//                    gController.animateCamera(
//                        CameraUpdate.newLatLngBounds(geolocation.bounds, 0));
//                    placeInfo = place.description.toString();
////                    getFullAddress();
//                    print(place.description);
//                    print(place.placeId);
//                    print(place.types);
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Visibility(
                  visible: locating,
                  child: neoLoader(),
                )
              ],
            ),
    );
  }
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        leading: IconButton(
//          onPressed: () {
//            Navigator.pop(context);
//          },
//          icon: Icon(
//            Icons.arrow_back_ios,
//            color: Colors.white,
//          ),
//        ),
//        title: TextField(
//          controller: searchController,
//          autofocus: true,
//          style: TextStyle(color: Colors.white),
//          onChanged: (text) {
//            print(searchController.text);
//            searchAddress();
////            searchItem();
//          },
//          decoration: InputDecoration(
//            hintStyle: TextStyle(color: Colors.white),
//            enabled: true,
//            hintText: "Search...",
//          ),
//          cursorColor: Colors.white,
//          showCursor: true,
//        ),
//      ),
//      body: Stack(
//        children: <Widget>[
//          Column(
//            children: <Widget>[
//              InkWell(
//                onTap: loading ? null : () {
//                  setState(() {
//                    loading = true;
//                  });
//                  getLocation();
//                },
//                child: Card(
//                  elevation: 0.0,
//                  child: ListTile(
//                    leading: Icon(
//                      Icons.my_location,
//                      color: Colors.blue,
//                    ),
//                    title: Text("Use my current location"),
//                  ),
//                ),
//              ),
//              Expanded(
//                child: ListView.separated(
//                    separatorBuilder: (BuildContext context, int index) {
//                      return Divider(height: 0,);
//                    },
//                    itemBuilder: (BuildContext context, int index) {
//                      return InkWell(
//                        onTap: loading ? null : () {
//                          setState(() {
//                            loading = true;
//                          });
//                          setLocation(index);
//                        },
//                        child: Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: ListTile(
//                            leading: Icon(
//                              Icons.location_on,
//                              color: Colors.blue,
//                            ),
//                            title: Text("${locationList[index]}"),
//                            subtitle: Text("${locationList[index + 1]}"),
//                          ),
//                        ),
//                      );
//                    },
//                    itemCount: locationList.length ~/ 2
//                ),
//              )
//            ],
//          ),
//          Visibility(
//            visible: loading,
//            child: Container(
//              child: BackdropFilter(
//                filter: ImageFilter.blur(
//                  sigmaX: 5.0,
//                  sigmaY: 5.0,
//                ),
//                child: Center(child: Text(
//                    "Locating...", style: TextStyle(color: mColor))),
//              ),
//            ),
//          )
//        ],
//      ),
//    );
//  }
}
