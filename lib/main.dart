import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:userapp/Screens/LoginRegister/CreateAccount.dart';
import 'package:userapp/Screens/SearchLocationMap.dart';
import 'package:userapp/Screens/SplashScreen.dart';

final FirebaseOptions firebaseOptions = FirebaseOptions(
    googleAppID: '1:865895484133:android:c5c3371fd5072bd925620c',
    apiKey: 'AIzaSyAtwz9bZxJeDPAdAvEg3ggTAwh4wck3pT0',
    databaseURL: 'https://bhadanamart-287f8-default-rtdb.firebaseio.com');

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
//  final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarBrightness: Brightness.dark));
//    var mColor = Color(0xFF3dad00);
//    var mAccentColor = Color(0xFF3dad00);

    MaterialColor mColor = MaterialColor(0xFF3dad00, <int, Color>{
      50: Color(0xFF3dad00),
      100: Color(0xFF3dad00),
      200: Color(0xFF3dad00),
      300: Color(0xFF3dad00),
      400: Color(0xFF3dad00),
      500: Color(0xFF3dad00),
      600: Color(0xFF3dad00),
      700: Color(0xFF3dad00),
      800: Color(0xFF3dad00),
      900: Color(0xFF3dad00),
    });

    MaterialColor mAccentColor = MaterialColor(0xFF3dad00, <int, Color>{
      50: Color(0xFF3dad00),
      100: Color(0xFF3dad00),
      200: Color(0xFF3dad00),
      300: Color(0xFF3dad00),
      400: Color(0xFF3dad00),
      500: Color(0xFF3dad00),
      600: Color(0xFF3dad00),
      700: Color(0xFF3dad00),
      800: Color(0xFF3dad00),
      900: Color(0xFF3dad00),
    });

    return GetMaterialApp(
        title: 'Bhadana Mart',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: mColor,
          fontFamily: 'Poppins-Regular',
          bannerTheme: MaterialBannerThemeData(backgroundColor: mAccentColor),
          appBarTheme: AppBarTheme(
              textTheme: TextTheme(
                  title: TextStyle(fontFamily: 'Poppins-Regular', fontSize: 12),
                  subtitle:
                  TextStyle(fontFamily: 'Poppins-Regular', fontSize: 12))),
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: mColor,
          accentColor: mAccentColor,
        ),
//        navigatorKey: navigatorKey,
        home: SplashScreen()
//      routes: <String, WidgetBuilder>{'LoginRegisterPage': (context) => LoginRegisterPage()},
//      routes: <String, WidgetBuilder>{
//        '/LoginRegisterPage': (BuildContext context) => LoginRegisterPage()},
    );
  }
}
